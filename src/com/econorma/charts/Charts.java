package com.econorma.charts;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItemCollection;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickMarkPosition;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.labels.XYSeriesLabelGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.Application.TIPO_MISURE;
import com.econorma.data.Lettura;
import com.econorma.data.Lettura.TipoMisura;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.gui.EconormaDrawingSupplier;
import com.econorma.logic.LettureProva;
import com.econorma.logic.SensoreManager;
import com.econorma.ui.EconormaChartBase;
import com.econorma.ui.Events;
import com.econorma.util.Logger;
import com.econorma.util.StringUtils;
import com.econorma.util.Theme;


public class Charts {

	private static final String TAG = "EconormaChart";
	private static final Logger logger = Logger.getLogger(Charts.class);

	private JFreeChart chart;
	private XYPlot plot; 
 
	private Application.THEME theme;
	private Application app;
	private boolean seriesShape;
	private boolean autoRange1;
	private boolean autoRange2;
	private int lowerRange1;
	private int lowerRange2;
	private int upperRange1;
	private int upperRange2;
	private int tickUnit1;
	private int tickUnit2;

	private EconormaChartBase umiditaChar;
	private EconormaChartBase temperaturaChar;
	private EconormaChartBase voltChar;
	private EconormaChartBase milliampereChar;
	private EconormaChartBase ohmChar;
	private EconormaChartBase apertoChiusoChar;
	private List<EconormaChartBase> baseCharts;
	private TIPO_MISURE tipoMisure;

	private static final int DATASET_TEMP_IDX = 0;
	private static final int DATASET_UMID_IDX = 1;
	
	private static final int DATASET_VOLT_IDX = 2;
	private static final int DATASET_MILLIAMPERE_IDX = 3;
	private static final int DATASET_OHM_IDX = 4;
	private static final int DATASET_APERTO_CHIUSO_IDX = 5;
	

	{
		baseCharts = new ArrayList<EconormaChartBase>();

	}

	public Charts(TIPO_MISURE tipoMisure){
		this.tipoMisure=tipoMisure;
		init(null, null, null);
		EventBusService.subscribe(this);
	}


	//TODO this is broken! fix restore!
	public Charts(LettureProva lettureProva, List<Sensore> sensori, Prova prova){
		init(lettureProva, sensori, prova);
	}

	private void init(LettureProva lettureProva, List<Sensore> sensori, Prova prova){

		getAppDefault();

		EconormaChartBase ecb ;
		switch (tipoMisure) {
		case TEMPERATURA_UMIDITA:
			if(umiditaChar==null){
				ecb = new EconormaChartBase(Lettura.TipoMisura.UMIDITA, DATASET_UMID_IDX);
				baseCharts.add(ecb);
				umiditaChar = ecb;
			}
			break;
		case TEMPERATURA:
			if(temperaturaChar==null){
				ecb = new EconormaChartBase(Lettura.TipoMisura.TEMPERATURA, DATASET_TEMP_IDX);
				baseCharts.add(ecb);
				temperaturaChar = ecb;
			}
			break;
		case VOLT:
			if(voltChar==null){
				ecb = new EconormaChartBase(Lettura.TipoMisura.VOLT, DATASET_VOLT_IDX);
				baseCharts.add(ecb);
				voltChar = ecb;
			}
			break;
		case MILLIAMPERE:
			if(milliampereChar==null){
				ecb = new EconormaChartBase(Lettura.TipoMisura.MILLIAMPERE, DATASET_MILLIAMPERE_IDX);
				baseCharts.add(ecb);
				milliampereChar = ecb;
			}
			break;
		case OHM:
			if(ohmChar==null){
				ecb = new EconormaChartBase(Lettura.TipoMisura.OHM, DATASET_OHM_IDX);
				baseCharts.add(ecb);
				ohmChar = ecb;
			}
			break;
		case APERTO_CHIUSO:
			if(apertoChiusoChar==null){
				ecb = new EconormaChartBase(Lettura.TipoMisura.APERTO_CHIUSO, DATASET_APERTO_CHIUSO_IDX);
				baseCharts.add(ecb);
				apertoChiusoChar = ecb;
			}
			break;
		}


		if(chart==null){
			chart = createOverlaidChart();
		}

		customizeAppearance(chart);
		 
		for(EconormaChartBase i:baseCharts){
			i.setChart(chart);
			if(sensori!=null) {
				i.restore(lettureProva,sensori,prova);
			}
		}

	}


	public void restore(LettureProva lettureProva, List<Sensore> sensori){
		clear();
		init(lettureProva, sensori, null);
	}


	/**
	 * Creates an overlaid chart.
	 *
	 * @return The chart.
	 */
	@SuppressWarnings("deprecation")
	private JFreeChart createOverlaidChart() {

		final DateAxis domainAxis = new DateAxis(Application.getInstance().getBundle().getString("econormaChart.data"));
		domainAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
		
		ValueAxis rangeAxis1 = null;
		switch (tipoMisure) {
		case TEMPERATURA:
			rangeAxis1 = new NumberAxis(Application.getInstance().getBundle().getString("econormaChart.temperatura"));		
			break;
		case TEMPERATURA_UMIDITA:
			rangeAxis1 = new NumberAxis(Application.getInstance().getBundle().getString("econormaChart.umidita"));
			break;
		case VOLT:
			rangeAxis1 = new NumberAxis(Application.getInstance().getBundle().getString("econormaChart.volt"));
			break;
		case MILLIAMPERE:
			rangeAxis1 = new NumberAxis(Application.getInstance().getBundle().getString("econormaChart.milliampere"));
			break;
		case OHM:
			rangeAxis1 = new NumberAxis(Application.getInstance().getBundle().getString("econormaChart.ohm"));
			break;
		case APERTO_CHIUSO:
			rangeAxis1 = new NumberAxis(Application.getInstance().getBundle().getString("econormaChart.apertoChiuso"));
			break;
		default:
			break;
		}
	
		EconormaChartBase umiditaTmp = null;
		EconormaChartBase temperatura = null;
		EconormaChartBase voltTmp = null;
		EconormaChartBase milliTmp = null;
		EconormaChartBase ohmTmp = null;
		EconormaChartBase acTmp = null;
		for(EconormaChartBase ecb: baseCharts){
			if(ecb.getTipoLettura()==TipoMisura.TEMPERATURA)
				temperatura = ecb;
			if(ecb.getTipoLettura()==TipoMisura.UMIDITA)
				umiditaTmp = ecb;
			if(ecb.getTipoLettura()==TipoMisura.VOLT)
				voltTmp= ecb;
			if(ecb.getTipoLettura()==TipoMisura.MILLIAMPERE)
				milliTmp= ecb;
			if(ecb.getTipoLettura()==TipoMisura.OHM)
				ohmTmp= ecb;
			if(ecb.getTipoLettura()==TipoMisura.APERTO_CHIUSO)
				acTmp= ecb;

		}

		
		EconormaChartBase chartBase = null;
		
		switch (tipoMisure) {
		case TEMPERATURA:
			chartBase = temperatura;
			break;
		case TEMPERATURA_UMIDITA:
			chartBase = umiditaTmp;	
			break;
		case VOLT:
			chartBase = voltTmp;	
			break;
		case MILLIAMPERE:
			chartBase = milliTmp;	
			break;
		case OHM:
			chartBase = ohmTmp;	
			break;
		case APERTO_CHIUSO:
			chartBase = acTmp;	
			break;
		default:
			break;
		}
		
		


		// create plot...
		final IntervalXYDataset temperature = chartBase.getDataSet();
		//         final XYItemRenderer temperatureRenderer = new DefaultXYItemRenderer();
		final XYItemRenderer temperatureRenderer = new StandardXYItemRenderer();
		plot = new XYPlot(temperature, domainAxis, rangeAxis1, temperatureRenderer){

			@Override
			public LegendItemCollection getLegendItems() {
					return super.getLegendItems();
 			}
				
		};

		plot.setDrawingSupplier(new EconormaDrawingSupplier());
 
			switch(tipoMisure){
			case TEMPERATURA:
			case TEMPERATURA_UMIDITA:
			case VOLT:
			case MILLIAMPERE:
			case OHM:
			case APERTO_CHIUSO:
			{
				temperatureRenderer.setLegendItemLabelGenerator(new TemperaturaUmiditaLegentItemLabelGenerator());
				break;
			}
				 

			default:
				throw new RuntimeException("Tipo non gestito");
			}

		 

		return new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

	}


	public void addLettura(Lettura lettura) {
		for(EconormaChartBase ecb: baseCharts){
			if(lettura.isValoreMisuraValid(ecb.getTipoLettura()))
				ecb.addLettura(lettura);
		}
	}

	public void clear() {
		for(EconormaChartBase ecb:baseCharts){
			ecb.clear();
		}
	}

	public JFreeChart getJFreeChart(){
		return chart;
	}

	public File saveAsPNG(){
		File file = null;

		try {
			file = File.createTempFile("econorma_reports_", UUID.randomUUID().toString());	
//			ChartUtilities.saveChartAsPNG(file, chart, 780, 470);
			ChartUtilities.saveChartAsPNG(file, chart, 670, 470);
			logger.info(TAG, "Grafico salvato in "+file.toString());
		} catch (IOException e) {
			logger.error(TAG, "impossibile salvare il chart in "+file.toString(),e);
		}

		return file;
	}

	public void setManageSingole(boolean value){
		for(EconormaChartBase ecb:baseCharts)
			ecb.setManageSingole(value);
	}

	@EventHandler
	public void handleEvent(final Events.ChartFilterEvent filterChartEvent){

		switch(filterChartEvent.action){
		case ADD_FILTER:
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					Sensore sensore = filterChartEvent.sensore;
					for(EconormaChartBase ecb:baseCharts){
						ecb.applyFilter(sensore);
					}
				}
			});
			break;
		case REMOVE_FILTER:
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					for(EconormaChartBase ecb:baseCharts){
						ecb.removeFilters();
					}
				}
			});
			break;
		default:
			break;
		}	
	}

	private void customizeAppearance(JFreeChart chart) {
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setBackgroundPaint(Color.WHITE);
		chart.setBackgroundPaint(UIManager.getColor("Panel.background"));

		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		switch (theme) {
		case DARK:
			plot.getRangeAxis().setTickLabelPaint(Theme.WHITE);
			plot.getDomainAxis().setTickLabelPaint(Theme.WHITE);
			plot.getRangeAxis().setLabelPaint(Theme.WHITE);
			plot.getDomainAxis().setLabelPaint(Theme.WHITE);
			plot.setBackgroundPaint(Theme.DARK);
			break;
		}


		LegendTitle legend = chart.getLegend();
		Font labelFont = legend.getItemFont().deriveFont(11f);
		legend.setItemFont(labelFont);
		legend.setBorder(0, 0, 0, 0); 
		legend.setBackgroundPaint(UIManager.getColor("Panel.background"));

		switch (theme) {
		case DARK:
			legend.setItemPaint(Theme.WHITE);
			break;
		}
 

		NumberAxis na = (NumberAxis)chart.getXYPlot().getRangeAxis();
		na.setNumberFormatOverride(new DecimalFormat("#0.00")); 

			plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
			plot.setDomainGridlinePaint(Color.LIGHT_GRAY);

			plot.setRangeGridlineStroke(new BasicStroke(0.1f));
			plot.setDomainGridlineStroke(new BasicStroke(0.1f));

			if (!autoRange1){
				if (lowerRange1 !=0 && upperRange1 !=0) {
					na.setRange(lowerRange1, upperRange1);
				}
				if (tickUnit1 != 0){
					na.setTickUnit(new NumberTickUnit(tickUnit1));
				}
			}
 
			if (seriesShape){
				StandardXYItemRenderer std = new StandardXYItemRenderer();
				Shape shape = new Ellipse2D.Double(-3.0,-3.0, 4.0, 4.0);
				std.setShape(shape);
				std.setBaseShape(shape);
				std.setBaseShapesFilled(true);
				std.setBaseShapesVisible(true);
				plot.setRenderer(std);	
			}

			XYItemRenderer myRenderer = plot.getRenderer();

			DateAxis da = (DateAxis)chart.getXYPlot().getDomainAxis();  
			da.setDateFormatOverride(new SimpleDateFormat("dd-MM-yyyy HH:mm"));
			
			switch (tipoMisure) {
			case TEMPERATURA:
				if (autoRange1){
					na.setAutoRange(true);	
				}
				plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0, Color.red); 
				break;
			case VOLT:
				if (autoRange1){
					na.setAutoRange(true);	
				}
				plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0, Color.GREEN); 
				break;
			case MILLIAMPERE:
				if (autoRange1){
					na.setAutoRange(true);	
				}
				plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0, Color.PINK); 
				break;
			case OHM:
				if (autoRange1){
					na.setAutoRange(true);	
				}
				plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0, Color.MAGENTA); 
				break;
			case APERTO_CHIUSO:
//				if (autoRange1){
//					na.setAutoRange(true);	
//				}
			 	na.setAutoRangeStickyZero(false);
		     	na.setAutoRange(true);
		     	na.setAutoRangeMinimumSize(1);
		     	chart.getXYPlot().setRangeAxis(na);
//				plot.getRangeAxis().setAutoRange(true); 
//				plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0, Color.ORANGE); 
//				((NumberAxis)plot.getRangeAxis()).setAutoRangeIncludesZero(false);
				break;
			case TEMPERATURA_UMIDITA:
				na.setRange(10, 100.00);
				na.setTickUnit(new NumberTickUnit(5));
				plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0, Color.CYAN);
			default:
				break;
			}
 

		// DA QUI //
		XYItemRenderer r = plot.getRenderer();
		r.setBaseToolTipGenerator(new StandardXYToolTipGenerator(org.jfree.chart.labels.StandardXYToolTipGenerator.DEFAULT_TOOL_TIP_FORMAT,
				new SimpleDateFormat("dd/MM/yyyy - HH:mm"), new DecimalFormat("#0.00")));
		plot.getDomainAxis().setLabelFont(new Font("Tahoma", Font.PLAIN, 11));
		plot.getRangeAxis().setLabelFont(new Font("Tahoma", Font.PLAIN, 11));
		// A QUI //


		na.setTickLabelFont(new Font("Tahoma", Font.PLAIN, 11));

		ValueAxis domainAxis = plot.getDomainAxis();
		domainAxis.setTickLabelFont(new Font("Tahoma", Font.PLAIN, 11));

	}


	private class TemperaturaUmiditaLegentItemLabelGenerator implements XYSeriesLabelGenerator{

		@Override
		public String generateLabel(XYDataset d, int i) {
			int indexOf = plot.indexOf(d);
			String tipo = null;
			
			switch (tipoMisure) {
			case TEMPERATURA:
				tipo="(C�)";
				break;
			case TEMPERATURA_UMIDITA:
				tipo = "(UR%)";
				break;
			case VOLT:
				tipo = "(Volt)";
				break;
			case MILLIAMPERE:
				tipo = "(mA)";
				break;
			case OHM:
				tipo = "(Pt100)";
				break;
			case APERTO_CHIUSO:
				tipo = "(Aperto/Chiuso)";
				break;
			default:
				break;
			}
 			
			String id_sonda = (String)d.getSeriesKey(i);
			Sensore sensore = SensoreManager.getInstance().getSensoreById(id_sonda);
 
				return sensore+tipo+" - "+StringUtils.nullToEmpty(sensore.getDescrizione());
			 

		}
	}

	private class TemperaturaLegentItemLabelGenerator implements XYSeriesLabelGenerator{

		@Override
		public String generateLabel(XYDataset d, int i) {
			String id_sonda = (String)d.getSeriesKey(i);
			Sensore sensore = SensoreManager.getInstance().getSensoreById(id_sonda);

			if (sensore==null){
				return id_sonda;	
			} 

			return sensore+" - "+StringUtils.nullToEmpty(sensore.getDescrizione());
		}
	}

	public void getAppDefault(){
		app = Application.getInstance();
		theme = app.getTheme();
	}


}
