package com.econorma.charts;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.Application.TIPO_MISURE;
import com.econorma.data.Lettura;
import com.econorma.io.Receiver.ReadListener;
import com.econorma.logic.SensoreManager;
import com.econorma.ui.Events;
import com.econorma.ui.Events.ChartFilterEvent;
import com.econorma.util.Logger;

import net.miginfocom.swing.MigLayout;

public class ChartsPanel extends JPanel {

	private final ChartPanel chartPanel;

	private final Charts chart;

	public static final String DEFAULT_TOOL_TIP_FORMAT = "{0} | {1} |  {2}";

	private boolean canRender = false;

	private final boolean isMain;


	private static final Logger logger = Logger.getLogger(ChartsPanel.class);
	private static final String TAG = ChartsPanel.class.getCanonicalName();

	private final ReadListener myReadListener = new ReadListener() {

		@Override
		public void onRead(Lettura lettura) {
			addLettura(lettura);

		}
	};

	/**
	 * Create the chart and register for events from virtual sensors
	 * MEDIA_ESTERNA, MEDIA_INTERNA
	 * 
	 * @param sensoreManager
	 */
	public ChartsPanel(SensoreManager sensoreManager, boolean isMain, TIPO_MISURE tipoMisure) {
		this.isMain = isMain;
		//		setLayout(new MigLayout("insets 0", "[grow]"));
		setLayout(new MigLayout("insets 5 5 5 5", "[fill,100%]"));

		chart = new Charts(tipoMisure);

		boolean properties = true;
		boolean save = true;
		boolean print = true;
		boolean zoom = true;
		boolean tooltips = true;
		chartPanel = new ChartPanel(chart.getJFreeChart(), properties, save, print, zoom,
				tooltips);
		chartPanel.addMouseListener(new MouseAdapter() {


			@Override
			public void mouseClicked(MouseEvent e) {


				if(e.getClickCount()==2){
					ChartFilterEvent chartFilterEvent = new Events.ChartFilterEvent(null, Events.ChartFilterEvent.ACTION.REMOVE_FILTER);
					logger.debug(TAG, "double click  restore ");
					EventBusService.publish(chartFilterEvent);
				}

			}
		});

		chartPanel.setPreferredSize(new Dimension(2000, 2000));
		//		chartPanel.setMouseWheelEnabled(Application.getInstance().isEnableZoom());
		//		chartPanel.setDomainZoomable(Application.getInstance().isEnableDomainZoom());
		//		chartPanel.setRangeZoomable(Application.getInstance().isEnableRangeZoom());
		add(chartPanel);

		if(sensoreManager!=null){
			sensoreManager.addOnReadListener(myReadListener);
		}
	}

	public void addLettura(Lettura lettura) {

		if (canRender && lettura.getSensore() != null) {
			if(lettura.isDiscovery())
				return;
			else
				chart.addLettura(lettura);
		}
	}

	public void clear() {
		chart.clear();
	}


//	@EventHandler
//	public void handleEvent(final Events.RealTimeEvent event) {
//		SwingUtilities.invokeLater(new Runnable() {
//
//			@Override
//			public void run() {
//
//			}
//		});
//
//	}

	@EventHandler
	public void handleEvent(String event) {

		if(Events.SENSORI_RESTORE_COMPLETED.equals(event)){
			canRender = true;
		}


		if(Application.getInstance().isConfigMode()){
			canRender = false;
		} else {
			canRender = true;
		}


	}


	public Charts getEconormaChart(){
		return chart;
	}

}