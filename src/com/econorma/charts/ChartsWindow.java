package com.econorma.charts;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.border.EmptyBorder;

import com.adamtaft.eb.EventBusService;
import com.econorma.Application.TIPO_MISURE;
import com.econorma.logic.SensoreManager;
import com.econorma.ui.RowHooverAdapter;

public class ChartsWindow extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6746683396726822437L;
	private ChartsPanel tempChartPanel;
	private ChartsPanel urChartPanel;
	private Dimension minimumSize;
	private Dimension mapsSize;
	
	//events managers
	private RowHooverAdapter rowHooverAdapter;
	 
	
	public ChartsWindow() {
		initComponents();
	}

	/**
	 * Create the frame.
	 */
	private void initComponents() {
		
 		setFocusable(true);
 
 		setLayout(new BorderLayout());
 		setBorder(new EmptyBorder(15, 15, 15, 15));
 		
 		tempChartPanel = new ChartsPanel(SensoreManager.getInstance(), true, TIPO_MISURE.TEMPERATURA);
 		urChartPanel = new ChartsPanel(SensoreManager.getInstance(), true, TIPO_MISURE.TEMPERATURA_UMIDITA);
 		EventBusService.subscribe(tempChartPanel);
 		EventBusService.subscribe(urChartPanel);
 		
 		tempChartPanel.setMinimumSize(new Dimension(500, 50));
 		urChartPanel.setMinimumSize(new Dimension(500, 50));
 		 
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				tempChartPanel, urChartPanel);

 		splitPane.setOneTouchExpandable(false);
 		splitPane.setMaximumSize(new Dimension(Integer.MAX_VALUE,
 				Integer.MAX_VALUE));
 		splitPane.setResizeWeight(0.5);
 		
 		splitPane.setEnabled(false);
	 
		splitPane.addComponentListener(new ComponentListener() {

			@Override
			public void componentShown(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentResized(ComponentEvent event) {
				 

			}

			@Override
			public void componentMoved(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentHidden(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
 
		
//		selectMapsPanel.setMinimumSize(minimumSize);
//		selectMapsPanel.setMaximumSize(minimumSize);
//		mapsPanel.setMaximumSize(mapsSize);
		
 		add(splitPane);
		 
	 
	}
 
 

}
