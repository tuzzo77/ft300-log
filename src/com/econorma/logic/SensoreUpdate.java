package com.econorma.logic;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.econorma.Application;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.util.Logger;

public class SensoreUpdate {

	private SensoreManager sensoreManager;
	private static final Logger logger = Logger.getLogger(SensoreUpdate.class);
	private static final String TAG = SensoreUpdate.class.getSimpleName();

	public SensoreUpdate() {

	}

	public void SaveSensoriProva(Prova prova) {

		 
		List<Sensore> sensori_interni = sensoreManager.getInstance().retrieveSensors(Location.INTERNO);


		if (prova==null){


			try{
				prova = Application.getInstance().getDao().findUltimaProva();
			}catch (Exception e) {
				logger.info(TAG, "nessuna prova in corso... Non posso salvare nuovi sensori");
				return;
			}

		}
 
		Set<Sensore> merge = new HashSet<Sensore>();
		merge.addAll(sensori_interni);
		merge.add(Sensore.MEDIA_INTERNA);
		
		
		StringBuilder id_sensori = new StringBuilder();
		for(Sensore s: merge){
			id_sensori.append(s.getId_sonda()).append(",");
		}
		int lastComma = id_sensori.lastIndexOf(",");
		if(lastComma>=0)
			id_sensori.setLength(lastComma);
		
		
	 
		prova.setId_sensore(id_sensori.toString());

		Application.getInstance().getDao().insertOrUpdateProva(prova);

		logger.info(TAG, "Dati sensori Prova aggiornati");


	}
}
