package com.econorma.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.econorma.data.LetturaProva;

public class StandardDeviation {

	private List<LetturaProva> lettureProva = new ArrayList<LetturaProva>(); 
	private List<Double> valueTempArray =  new ArrayList<Double>();
	private List<Double> valueUrtArray =  new ArrayList<Double>();
	public double mediaTemp;
	public double mediaUrt;
	public double stdDeviationTemp;
	public double minValueTemp;
	public double maxValueTemp;
	public double stdDeviationUrt;
	public double minValueUrt;
	public double maxValueUrt;
	
	public Date minDate;
	public Date maxDate;
	
	public StandardDeviation(List<LetturaProva> lettureProva){
		this.lettureProva = lettureProva;
		init();
	}
	
	public void init( ){
	
		int size = lettureProva.size();

		for(int i=0; i<size ; i++){
			LetturaProva p = lettureProva.get(i);
			double temp = p.getValore();
			valueTempArray.add(temp);
			double urt = p.getUmidita();
			valueUrtArray.add(urt);
		}
		 
	}

	public void calculate( ) {
	 
		double sumTemp = 0.0, standardDeviationTemp = 0.0;
		double sumUrt = 0.0, standardDeviationUrt = 0.0;

		for(double num : valueTempArray) {
			sumTemp += num;
		}
		for(double num : valueUrtArray) {
			sumUrt += num;
		}

		mediaTemp = sumTemp/valueTempArray.size();
		mediaUrt = sumUrt/valueUrtArray.size();

		for(double num: valueTempArray) {
			standardDeviationTemp += Math.pow(num - mediaTemp, 2);
		}
		for(double num: valueUrtArray) {
			standardDeviationUrt += Math.pow(num - mediaUrt, 2);
		}
		
		if (valueTempArray.size()>0) {
			stdDeviationTemp = Math.sqrt(standardDeviationTemp/(valueTempArray.size()-1));
			minValueTemp = Collections.min(valueTempArray);
			maxValueTemp =  Collections.max(valueTempArray);
		} else  {
			stdDeviationTemp = 0.0;
			minValueTemp = 0.0;
			maxValueTemp = 0.0;
		}
		
		if (valueUrtArray.size()>0) {
			stdDeviationUrt = Math.sqrt(standardDeviationUrt/(valueUrtArray.size()-1));
			minValueUrt = Collections.min(valueUrtArray);
			maxValueUrt =  Collections.max(valueUrtArray);
		} else  {
			stdDeviationUrt = 0.0;
			minValueUrt = 0.0;
			maxValueUrt = 0.0;
		}
		
	}

	public double getMediaTemp() {
		return mediaTemp;
	}
 
	public double getStdDeviationTemp() {
		return stdDeviationTemp;
	}
 

	public double getMinValueTemp() {
		return minValueTemp;
	}

	public double getMaxValueTemp() {
		return maxValueTemp;
	}
	
	public double getMediaUrt() {
		return mediaUrt;
	}

	public double getMinValueUrt() {
		return minValueUrt;
	}

	public double getMaxValueUrt() {
		return maxValueUrt;
	}

	public Date getMinDate() {
		return minDate;
	}

	public Date getMaxDate() {
		return maxDate;
	}

	public double getStdDeviationUrt() {
		return stdDeviationUrt;
	}
 
}
