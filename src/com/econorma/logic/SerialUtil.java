package com.econorma.logic;

import jssc.SerialPortList;

 

public class SerialUtil {

	public static String getFirstPortAvailable() {
		String [] ports = getSerialPorts();
		
		for(String s : ports) {
			return s;
		}
		 
		return null;
	}
	
	public static String[] getSerialPorts() {
		
		String[] portNames = SerialPortList.getPortNames();
	
	return portNames;
	}
}
