package com.econorma.logic;

import com.econorma.data.Lettura;
import com.econorma.data.Sensore;

public class RangeCheck {
	
	private static Double min =Double.MIN_VALUE;
	private static Double max =Double.MAX_VALUE;
	private static Double val =Double.MIN_VALUE;
	private static Sensore sensore;
	
 
 
	private RangeCheck()
    {
       
    }

     

	public static boolean outOfLimit(Lettura lettura){

		sensore = lettura.getSensore();
		min = sensore.getRangeMin();
		max = sensore.getRangeMax();
		val = lettura.getValore();
	 
		if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0){
			
		 	if (lettura.getValore()<0 && sensore.getRangeMin()<0 && sensore.getRangeMax()<0) {
				min = min*-1;
				max = max*-1;
				val = val*-1;
			}
			
		 	if (val<min || val>max) 
		 	 return true;	
		 	 
 	}  
		
			return false;
		}

	}



 
