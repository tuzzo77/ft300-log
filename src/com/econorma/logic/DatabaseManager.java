package com.econorma.logic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.econorma.persistence.DAO;

public class DatabaseManager {

	private static DatabaseManager manager;

	public static synchronized DatabaseManager getInstance() {
		if (manager == null) {
			manager = new DatabaseManager();
		}
		return manager;
	}

	public boolean init() {

		try {
			if (!DAO.USE_SQLITE)
				Class.forName("org.firebirdsql.jdbc.FBDriver");
			else
				Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			// mLogger.e(TAG, "non stato trovato il driver SQLite");
			return false;
		}
		return true;
	}

	public Connection getConnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection("jdbc:sqlite:database.db");
		} catch (SQLException e) {
			// mLogger.e(TAG, "non � stato possibile accedere al db");
		}
		return conn;

	}

}
