package com.econorma.logic;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter; 
import java.io.IOException; 
import java.io.PrintStream;
import java.io.PrintWriter; 
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList; 
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;

import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.LetturaProva;
import com.econorma.data.Lettura.TipoMisura;
import com.econorma.data.Prova;
import com.econorma.resources.Testo;
import com.econorma.util.Logger;


public class FileLogger { 
	
	private static final Logger logger = Logger.getLogger(FileLogger.class);
	private static final String TAG = FileLogger.class.getSimpleName();


	private static final SimpleDateFormat FORMATTER_DATE = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
	public static final NumberFormat nf = new DecimalFormat("###.##");

	private static Double letturaT;
	private static int letturaA=0;
	private static Double letturaY;
	private static Double letturaX;
	private static Double letturaZ;
	private static Double letturaB;
	private static Double letturaC;
	private static String lettura=null;
	private static String lettura2=null;
	private static int MaxVal=4096;
	private static int MinRangeV=0;
	private static int MaxRangeV=10;
	private static int MinRangemA=0;
	private static int MaxRangemA=20;
	Lettura.TipoMisura tipoMisura; 
	double temperatura = Double.MIN_VALUE;
	double umidita = 0d;

 
	public static void main(String args[]) {

	
		
		try {


			File file_txt = new File("out.txt");

			String splitted[]=null; 
			boolean header = true;	
			boolean isCorrect=true;

			List<String> contents = FileUtils.readLines(file_txt);

			for (String line : contents) {

				String name = null;
				String tipo = null;

				StringTokenizer st1 = new StringTokenizer(line.trim(), "_");
				while (st1.hasMoreTokens()){
					
					isCorrect=true;

					String riga = st1.nextToken();

					if (riga.length()==0){
					continue;	
					}
				 	
					String check = riga.substring(0, 1);
					String check2 = riga.substring(0, 2);
					String check3 = riga.substring(3, 4);

					if (check.equals("�")) {
						continue;
					}
					
					if (check3.equals("�")) {
						continue;
					}

					if (check2.equals("-<")) {
						name = riga.substring(3, 9);
						tipo = riga.substring(10, 11);
						continue;
					}
					if (check.equals("-")) {
						continue;
					}


					if (riga.length()<14)
						continue;


					if (tipo.equals("A")) {
						if (riga.length()!=14)
							continue;
					}
					else {
			 			if (riga.length()!=20)
						 	riga = riga.substring(0, 20);
//							continue;
					}
				 

					int i = 0;
					Date data = null;


					StringTokenizer st2 = new StringTokenizer(riga, "/");
					while (st2.hasMoreTokens()){
					 
						i++;

						String riga2 = st2.nextToken();

						if (i==1) {
							
							String datastr = riga2.substring(0, 12);
							String day = datastr.substring(0, 2);
							String month = datastr.substring(2, 4);
							String year = datastr.substring(4, 6);
							String hour = datastr.substring(6, 8);
							String minute = datastr.substring(8, 10);
							String second = datastr.substring(10, 12);
							
							if (day.equals("00") && month.equals("00") && year.equals("00")){
								isCorrect=false;
								continue;
							}
							
							String datastr2=day+"/" + month + "/" + year + " - " + hour + ":" + minute + ":"+ second; 
							SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy - HH:mm:ss");
							try {
								data = (Date)format.parse(datastr2);
							}
							catch (ParseException e) {
								logger.error(TAG, "Data in formato non valido");
								
//								e.printStackTrace();
								try {
									data = (Date)format.parse("01/01/1900 - 00:00:00");
								} catch (ParseException e1) {
									logger.error(TAG, "Data in formato non valido");
//									e1.printStackTrace();
								}
								
						}
							 
						}
						if (i==2) {
							try {


								if (tipo.equals("T")) {

									letturaA = Integer.parseInt(riga2,16);

									if (letturaA > 2048) {
										letturaA = (-1 * (4096 - letturaA));
									}

									letturaT = letturaA / 10d;
								
									 
								}

								if (tipo.equals("E")) {

									letturaA = Integer.parseInt(riga2,16);
  
									letturaX = (double) letturaA;

									letturaY = (-39.63 + (letturaA * 0.04));
									letturaT = (double) letturaA;

									letturaY = Math.round(letturaY * 100.00)/100.0;
									
								
									

								}


								if (tipo.equals("C")) {
									letturaA = Integer.parseInt(riga2,16);
									letturaA = letturaA/2;
									
								}


								if (tipo.equals("A")) {
									lettura =riga2;
									if (lettura.equals("O")){
										lettura = "A";
									}
									lettura2="";
								}

								if (tipo.equals("V")) {
									letturaA = Integer.parseInt(riga2,16);
									letturaT = (double) letturaA;
									letturaB = Math.round(((letturaT*MaxRangeV)/MaxVal)*100.0)/100.0;
									letturaC = Math.round(((letturaT*MaxRangemA)/MaxVal)*100.0)/100.0;
									
								}



							}
							catch (Exception e)
							{ 
								e.printStackTrace();
							}

						}
						if (i==3) {
							if (tipo.equals("E")) {
 
							}
						}

					}

					if (!(name.trim().equals("")) && isCorrect) {
						 
						
						if("E".equals(tipo)){
					 		
						}else if("T".equals(tipo)){
							
						}
					 
						if (FORMATTER_DATE.format(data).equals("14/06/2013 - 13:51:40")){
							System.out.println("Ecco");
						}

						System.out.println(name + " " + FORMATTER_DATE.format(data) + " " + letturaT);
				
						
					}
				}
			}



		}	 
		catch (IOException e) 
		{
			e.printStackTrace();
		}   
		
			 
	}
	
	
	public FileLogger() {
		
	}
	
 


	public void FileBidirezionale(Prova prova) {

		try {


			File file_txt = new File("out.txt");

			String splitted[]=null; 
			boolean header = true;	
			boolean isCorrect=true;

			List<String> contents = FileUtils.readLines(file_txt);

			for (String line : contents) {

				String name = null;
				String tipo = null;

				StringTokenizer st1 = new StringTokenizer(line.trim(), "_");
				while (st1.hasMoreTokens()){
					
					isCorrect=true;

					String riga = st1.nextToken();

					if (riga.length()==0){
					continue;	
					}
				 	
					String check = riga.substring(0, 1);
					String check2 = riga.substring(0, 2);
					String check3 = riga.substring(3, 4);

					if (check.equals("�")) {
						continue;
					}
					
					if (check3.equals("�")) {
						continue;
					}

					if (check2.equals("-<")) {
						name = riga.substring(3, 9);
						tipo = riga.substring(10, 11);
						continue;
					}
					if (check.equals("-")) {
						continue;
					}


					if (riga.length()<14)
						continue;


					if (tipo.equals("A")) {
						if (riga.length()!=14)
							continue;
					}
					else {
			 			if (riga.length()!=20)
						 	riga = riga.substring(0, 20);
//							continue;
					}
				 

					int i = 0;
					Date data = null;


					StringTokenizer st2 = new StringTokenizer(riga, "/");
					while (st2.hasMoreTokens()){
					 
						i++;

						String riga2 = st2.nextToken();

						if (i==1) {
							
							String datastr = riga2.substring(0, 12);
							String day = datastr.substring(0, 2);
							String month = datastr.substring(2, 4);
							String year = datastr.substring(4, 6);
							String hour = datastr.substring(6, 8);
							String minute = datastr.substring(8, 10);
							String second = datastr.substring(10, 12);
							
							if (day.equals("00") && month.equals("00") && year.equals("00")){
								isCorrect=false;
								continue;
							}
							
							String datastr2=day+"/" + month + "/" + year + " - " + hour + ":" + minute + ":"+ second; 
							SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy - HH:mm:ss");
							try {
								data = (Date)format.parse(datastr2);
							}
							catch (ParseException e) {
								logger.error(TAG, "Data in formato non valido");
								
//								e.printStackTrace();
								try {
									data = (Date)format.parse("01/01/1900 - 00:00:00");
								} catch (ParseException e1) {
									logger.error(TAG, "Data in formato non valido");
//									e1.printStackTrace();
								}
								
						}
							 
						}
						if (i==2) {
							try {


								if (tipo.equals("T")) {

									letturaA = Integer.parseInt(riga2,16);

									if (letturaA > 2048) {
										letturaA = (-1 * (4096 - letturaA));
									}

									letturaT = letturaA / 10d;
									temperatura=letturaT;
									 
								}

								if (tipo.equals("E")) {

									letturaA = Integer.parseInt(riga2,16);
  
									letturaX = (double) letturaA;

									letturaY = (-39.63 + (letturaA * 0.04));
									letturaT = (double) letturaA;

									letturaY = Math.round(letturaY * 100.00)/100.0;
									
									temperatura=letturaY;
									

								}


								if (tipo.equals("C")) {
									letturaA = Integer.parseInt(riga2,16);
									letturaA = letturaA/2;
									temperatura=letturaA;
								}


								if (tipo.equals("A")) {
									lettura =riga2;
									if (lettura.equals("O")){
										lettura = "A";
									}
									lettura2="";
								}

								if (tipo.equals("V")) {
									letturaA = Integer.parseInt(riga2,16);
									letturaT = (double) letturaA;
									letturaB = Math.round(((letturaT*MaxRangeV)/MaxVal)*100.0)/100.0;
									letturaC = Math.round(((letturaT*MaxRangemA)/MaxVal)*100.0)/100.0;
									temperatura=letturaB;
								}



							}
							catch (Exception e)
							{ 
								e.printStackTrace();
							}

						}
						if (i==3) {
							if (tipo.equals("E")) {
 
							}
						}

					}

					if (!(name.trim().equals("")) && isCorrect) {
						 
						
						if("E".equals(tipo)){
					 		tipoMisura = TipoMisura.TEMPERATURA_UMIDITA;
						}else if("T".equals(tipo)){
							tipoMisura = TipoMisura.TEMPERATURA;
						}
						
					 	
						
						final Lettura lettura = new Lettura();
						lettura.setTipoMisura(tipoMisura);
						lettura.setStatoBatteria("");
						lettura.setTemperaturaGrezza(temperatura);
						lettura.setUmiditaGrezza(umidita);
						lettura.setIdSonda(name);
						lettura.setData(data);
						
						
						final LetturaProva letturaProva = new LetturaProva(prova, lettura);
						
						Application.getInstance().getDao().insertLettura(lettura);
						Application.getInstance().getDao().insertLetturaProva(letturaProva);
						
//						Runnable salvaLettura = new Runnable() {
//							
//							@Override
//							public void run() {
//								Application.getInstance().getDao().insertLettura(lettura);
//								Application.getInstance().getDao().insertLetturaProva(letturaProva);
//							}
//						};
						
					}
				}
			}



		}	 
		catch (IOException e) 
		{
			e.printStackTrace();
		}    

	}
}	

