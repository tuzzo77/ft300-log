package com.econorma.logic;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.log4j.MDC;

import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.io.Receiver.ReadListener;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;
import com.econorma.util.SwingUtils;

public class SensoreManager implements ReadListener {

	private static final String TAG = SensoreManager.class.getSimpleName();

	private static final Logger logger = Logger.getLogger(SensoreManager.class);

	private static SensoreManager manager;

	private HashMap<String, Sensore> sensori = new HashMap<String, Sensore>();
 
	final private List<ReadListener> listeners = new ArrayList<ReadListener>();
 
	private Future<?> last_user_edit_update;

	private HashMap<String,Lettura> ultimeLetture = new HashMap<String,Lettura>();
 
	private boolean initPerformed = false;

	private DebuggerDuplicati debuggerDuplicati;

	public static synchronized SensoreManager getInstance() {
		if (manager == null)
			manager = new SensoreManager();
		return manager;
	}

	public static synchronized void setInstance(SensoreManager sm) {
		if (manager == null)
			manager = sm;
		else{
			throw new  RuntimeException("sensore manager gia creato");
		}
	}

	public boolean init() {

		if(DebuggerDuplicati.logger.isDebugEnabled()){
			debuggerDuplicati = new DebuggerDuplicati();
		}


		initPerformed = true;
		
		return loadSensors();
	}

	private boolean loadSensors() {
		List<Sensore> loadSensori = Application.getInstance().getDao()
				.loadSensori();
		for (Sensore s : loadSensori) {
			addSensore(s);
		}
		return true;
	}
	
	private Sensore loadSensorById(String idSonda) {
		Sensore sensore = Application.getInstance().getDao()
				.loadSensorById(idSonda);
		 
		return sensore;
	}

	public void restoreLastMeasure() {
		for (Sensore s : sensori.values()) {
			Lettura ultimaLettura = Application.getInstance().getDao()
					.getUltimaLettura(s);
			if (ultimaLettura != null)
				onRead(ultimaLettura, false);
		}
	}

	private void addSensore(Sensore sensore) {
		Sensore s = sensori.get(sensore.getId_sonda());
		if (s != null)
			throw new RuntimeException("Un sensore con id "
					+ sensore.getId_sonda() + "� gi� presente");
		sensori.put(sensore.getId_sonda(), sensore);
	}

	@Override
	public void onRead(final Lettura lettura) {
		onRead(lettura, true);
	}

	protected void onRead(final Lettura lettura, boolean persist) {

		if (lettura.getSensore() == null || lettura.getSensore().getLocation() != Location.VIRTUAL) {
			// le letture virtuali arrivano con già tutto impostato			

			Sensore sensore = sensori.get(lettura.getIdSonda());
			if (sensore != null) {
				lettura.setSensore(sensore);
			} else {
				// lo aggiungiamo ai sensori sconosciuti al volo
				sensore = Sensore.newInstance(lettura.getIdSonda());
				addSensore(sensore);
				lettura.setSensore(sensore);

			}

				if (lettura.getSensore().getLocation() == Location.INTERNO) {
					ultimeLetture.put(sensore.getId_sonda(),lettura);
				}
			
			
			if (persist){
				if(debuggerDuplicati!=null){
					debuggerDuplicati.logDupplicati(lettura);
				}
				Application.getInstance().getExecutorservice().submit(new Runnable() {

					@Override
					public void run() {
						Application.getInstance().getDao().insertLettura(lettura);
					}
				});
			}
		}

 

		notifyListeners(lettura);

	 
	}

	public synchronized void addOnReadListener(ReadListener listener) {
		listeners.add(listener);

	}

	public synchronized void removeOnReadListener(ReadListener listener) {
		listeners.remove(listener);
	}

	private void notifyListeners(Lettura lettura) {
		List<ReadListener> tmpListeners = new ArrayList<ReadListener>(listeners);
		for (ReadListener l : tmpListeners) {
			l.onRead(lettura);
		}
	}

	public List<Sensore> retrieveSensors(Location location) {
		if (!initPerformed)
			throw new RuntimeException(
					"You must first restore persistent sensors or this list will be empty for sure!");
		List<Sensore> result = new ArrayList<Sensore>();
		for (Sensore s : sensori.values()) {
			if (s.getLocation() != null && s.getLocation() == location) {
				result.add(s);
			}
		}
		return result;
	}

	/**
	 * salva lo stato in maniera asincrona
	 */
	public void saveState() {
		if (last_user_edit_update != null) {
			if (!last_user_edit_update.isDone())
				last_user_edit_update.cancel(false);
			last_user_edit_update = null;
		}

		Future<?> submit = Application.getInstance().getExecutorservice().submit(new Runnable() {

			@Override
			public void run() {
				Application.getInstance().getDao().deleteSensori();

				Application.getInstance().getDao().insertSensori(sensori.values());

				MDC.put("event_name", "Sensori");

				logger.info(TAG, "Stato Sensori salvato " + sensori);
				logger.log(LoggerCustomLevel.AUDIT, "Stato Sensori salvato " + sensori);

				
				SensoreUpdate sensoreUpdate = new SensoreUpdate();
				sensoreUpdate.SaveSensoriProva(null);
				

			}
		});

		last_user_edit_update = submit;
	}
	
	public void saveSensore(final Sensore sensor) {
		if (last_user_edit_update != null) {
			if (!last_user_edit_update.isDone())
				last_user_edit_update.cancel(false);
			last_user_edit_update = null;
		}
		
		Future<?> submit = Application.getInstance().getExecutorservice().submit(new Runnable() {

			@Override
			public void run() {
				Application.getInstance().getDao().insertSensore(sensor);
				
				changeSensori(sensor);
				
				MDC.put("event_name", "Sensore");
				
				logger.info(TAG, "Stato Sensore salvato " + sensor);
				logger.log(LoggerCustomLevel.AUDIT, "Stato Sensore salvato " + sensor);
			}
		});
		
		last_user_edit_update = submit;
	}


	public void saveNote(final Prova prova, final boolean detail) {
		if (last_user_edit_update != null) {
			if (!last_user_edit_update.isDone())
				last_user_edit_update.cancel(false);
			last_user_edit_update = null;
		}

		Future<?> submit = Application.getInstance().getExecutorservice().submit(new Runnable() {

			@Override
			public void run() {
				Application.getInstance().getDao().insertOrUpdateProva(prova);
				
				if (detail) {
					List<LetturaProva> lettureProva = Application.getInstance().getDao().findLettureProvaByProva(prova, false);
					Application.getInstance().getDao().deleteLettureProvaByProva(prova);
					
					for (LetturaProva lp: lettureProva){
						Date data_inizio = prova.getData_inizio();
						
						Calendar fromDate = Calendar.getInstance();
						fromDate.setTime(data_inizio);
						
						Calendar toDate = Calendar.getInstance();
						toDate.setTime(lp.getData());
						toDate.set(Calendar.YEAR, fromDate.get(Calendar.YEAR));
						toDate.set(Calendar.MONTH, fromDate.get(Calendar.MONTH));
						toDate.set(Calendar.DAY_OF_MONTH, fromDate.get(Calendar.DAY_OF_MONTH));
			 
						lp.setData(toDate.getTime());
						Application.getInstance().getDao().insertLetturaProva(lp);	
					}
					
//					Application.getInstance().getDao().insertOrUpdateLetturaProva(prova);
				}
				
				logger.info(TAG, "Aggiornamento prova completato");
			}
		});

		last_user_edit_update = submit;
	}

	public void updateState(final Sensore sensore) {


		Future<?> submit = Application.getInstance().getExecutorservice().submit(new Runnable() {

			@Override
			public void run() {


				Application.getInstance().getDao().updateSensore(sensore);


				logger.info(TAG, "Stato Sensore aggiornato con indirizzo Logger");


			}
		});

	}


	public Sensore getSensoreById(String id_sonda){
		//TODO, check db if not found
//		return sensori.get(id_sonda);
		
		return loadSensorById(id_sonda);
		
	}


	//TODO improve me, manage location and restored letture
	public List<Lettura> getUltimeLetture(){
		return new ArrayList<Lettura>(ultimeLetture.values());
	}
	
	public Lettura getUltimeLettureByIdentity(String identity){
		for(Lettura l: ultimeLetture.values()){
			if (l.getIdSonda().equals(identity)){
				return l;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param s
	 * @return true se aggiunto
	 */
	public boolean addUnknownIfNew(Sensore s){
		Sensore exists = getSensoreById(s.getId_sonda());
		if(exists==null){
			s.setLocation(Location.UNKNOWN);
			addSensore(s);
			return true;
		}else
			return false;

	}

	public void aggiornaIndirizzo(String idSonda, int indirizzo){
		// imposto l'indirizzo nella sonda 
		// rimuv
		Sensore s = SensoreManager.getInstance().getSensoreById(idSonda);
		

		
	}

	public Sensore getByIndirizzo(int indirizzo){
		return null;
	}


	public Collection<Sensore> getAllSensori(){
		return sensori.values();
	}
	
	/**
	 * restituisce una lista nuova filtrando per location
	 * @param location
	 * @return
	 */
	public Collection<Sensore> getAllSensori(Location location){
		SwingUtils.checkEDT();
		Collection<Sensore> values = sensori.values();
		ArrayList<Sensore> arrayList = new ArrayList<Sensore>();
		for(Sensore s : values){
			if(location.equals(s.getLocation())){
				arrayList.add(s);
			}
		}
		return arrayList;
	}

	private static class DebuggerDuplicati {

		private static final Logger logger = Logger.getLogger(DebuggerDuplicati.class);
		private static final String TAG = DebuggerDuplicati.class.getName();

		private HashMap<String, Lettura> lasts = new HashMap<String, Lettura>();

		public DebuggerDuplicati(){
			logger.debug(TAG, "Debugger Duplicati creato");
		}


		public void logDupplicati(Lettura lettura) {
			String idSonda = lettura.getIdSonda();
			Lettura prev = lasts.get(idSonda);
			if(prev!=null && (prev.getData().getTime() - lettura.getData().getTime())==0){
				logger.debug(TAG, "ricevuto duplicato : "+idSonda+" time : "+lettura.getData().getTime(), new Throwable("insert duplicato!"));
			}
			lasts.put(idSonda, lettura);

		}

	}
	
	public void changeSensori(Sensore sensore) {
		sensori.remove(sensore);
		sensori.put(sensore.getId_sonda(), sensore);
	}
	
	
	public String provaDuration(Date startDate, Date endDate){

	    long different = endDate.getTime() - startDate.getTime();

	    long secondsInMilli = 1000;
	    long minutesInMilli = secondsInMilli * 60;
	    long hoursInMilli = minutesInMilli * 60;

	    long elapsedHours = different / hoursInMilli;
	    different = different % hoursInMilli;

	    long elapsedMinutes = different / minutesInMilli;
	    different = different % minutesInMilli;

	    long elapsedSeconds = different / secondsInMilli;
	    
	    return  String.format ("%d:%d:%d", elapsedHours, elapsedMinutes, elapsedSeconds);

	}
 
}
