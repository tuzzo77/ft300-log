package com.econorma.logic;

import java.util.Comparator;
import java.util.Date;

public interface Measurable {

	public double getValore();
	
	public Date getData();
	
	public static final Comparator<Measurable> COMPARATOR = new MeasurableComparator();
	
	public class MeasurableComparator implements Comparator<Measurable>{

		@Override
		public int compare(Measurable o1, Measurable o2) {
			return o1.getData().compareTo(o2.getData());
		}
		
	}
}
