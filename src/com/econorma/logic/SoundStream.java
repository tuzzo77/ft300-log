package com.econorma.logic;

 

import java.awt.EventQueue;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

 
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class SoundStream implements Runnable {
	
	private Timer elapsedTimer;

	public SoundStream() {
		
	}
	
	
	
	public boolean initialize() {
		
	 
		InputStream soundFile = Thread.currentThread().getContextClassLoader().getResourceAsStream("com/econorma/ui/resources/smokealarm.wav");
	 	 
		AudioFileFormat aff;
		AudioInputStream ais;


		try {
			aff=AudioSystem.getAudioFileFormat(soundFile);

			ais=AudioSystem.getAudioInputStream(soundFile);


			AudioFormat af=aff.getFormat();


			DataLine.Info info = new DataLine.Info(
					Clip.class,
					ais.getFormat(),
					((int) ais.getFrameLength() *
							af.getFrameSize()));

			Clip ol = (Clip) AudioSystem.getLine(info);

			ol.open(ais);

			ol.loop(0);
		} catch(UnsupportedAudioFileException ee){} catch(IOException ea){} catch(LineUnavailableException LUE){};

		
		
		 return true;
	}

	 
	
	public void start(){
        
		 
		
		elapsedTimer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				EventQueue.invokeLater(new Runnable() {

					@Override
					public void run() {
						initialize(); 
					}
				});

			}
		}, 500 , 500 );
		
		
		 
		
    }
	
	public void play(){
		
		 try{
            
//            InputStream soundFile = Thread.currentThread().getContextClassLoader().getResourceAsStream("com/econorma/ui/resources/smokealarm.wav");
//			  InputStream soundFile = Thread.currentThread().getContextClassLoader().getResourceAsStream("smokealarm.wav");
			  
			  URL soundFile = Thread.currentThread().getContextClassLoader().getResource("smokealarm.wav");
		       AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundFile);             
 

                Clip clip = AudioSystem.getClip();
                clip.open(audioInputStream);
                clip.start();
            }catch(Exception ex){
               ex.printStackTrace();
            }
	        
			 
			
	} 
		



	@Override
	public void run() {
	 
		
	}

}

 

