package com.econorma.logic.logger;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.econorma.Application;
import com.econorma.data.Sensore;
import com.econorma.util.Logger;
import com.econorma.util.OS;

import jssc.SerialPort;
import jssc.SerialPortException;

public class LoggerChannel {

	private static class DeepLog{}
	private static final Logger DEEP_LOGGER = Logger.getLogger(DeepLog.class);
	private static final Logger LOGGER = Logger.getLogger(LoggerChannel.class);
	private static final String TAG = "LoggerChannel";

	private String portName;
	private int baudRate;
	private SerialPort serialPort;
	private final int MAX_LENGHT_V1=35850;
	private final int MAX_LENGHT_V2=35857;
	private int MAX_LENGHT = 0;
	private String template;
	
	public LoggerChannel(Sensore sensore){
		switch (sensore.getVersione()) {
		case V1:
			MAX_LENGHT=MAX_LENGHT_V1;
			try {
				template = FileUtils.readFileToString(new File("template_V1.txt"), "UTF-8");
			} catch (Exception e) {
				 LOGGER.error(TAG, "Errore in lettura template");
			}
			 	
			break;
		case V2:
			MAX_LENGHT=MAX_LENGHT_V2;
			try {
				template = FileUtils.readFileToString(new File("template_V2.txt"), "UTF-8");
			} catch (Exception e) {
				 LOGGER.error(TAG, "Errore in lettura template");
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * 
	 * @return ritorna sempre true/false se si connette
	 * @throws non lancia mai eccezioni
	 */
	public boolean connect() {
		portName = Application.getInstance().getPreferences().getPort();
		baudRate = Application.getInstance().getPreferences().getBaudRate();
		try {

			SerialPort serialPort = new SerialPort(portName);
			boolean opened = false;
			try{
				opened = serialPort.openPort();
				if(opened){
					this.serialPort = serialPort;
					serialPort.setParams(baudRate,
							SerialPort.DATABITS_8,
							SerialPort.STOPBITS_1,
							SerialPort.PARITY_NONE);
				}
			}catch(SerialPortException spe){
				LOGGER.error(TAG, "Error while opening port",spe);
			}
			return opened;
		} catch (Exception e) {
			LOGGER.fatal(TAG, "Receiver su " + portName
					+ ": Generic error while using port", e);
			return false;
		}
	}


	public void disconnect(){
		try { 
			if(serialPort!=null) { 
				LOGGER.debug(TAG, "closing "+serialPort.getPortName());
				try {
					serialPort.closePort();
				} catch (SerialPortException e) {
					/* ignore */
					LOGGER.error(TAG,"Error While Closing Port", e);
				}
			}
		} catch (Exception e) {
			LOGGER.fatal(TAG,"Errore imprevisto", e);
		}
	}

	/**
	 * concatena count volte il carattere fillChar
	 * 
	 * @param fillChar
	 * @param count
	 * @return
	 */
	public static String fillString(char fillChar, int count){
		char[] chars = new char[count];
		while (count>0) chars[--count] = fillChar;
		return new String(chars);
	} 


	/**
	 * cancellazione ottimistica.
	 * @param indirizzo
	 * @throws InterruptedException
	 * @throws SerialPortException
	 */
	public void cancellaDati(Sensore sensore) throws InterruptedException, SerialPortException{
		final int MAX_RETRY = 2;
		int retry_count = 0;
		while(retry_count<MAX_RETRY){
			Thread.sleep(1000);
			String cmd = ">" + "dD??!\r\n";
			serialPort.writeString(cmd.trim());
			Thread.sleep(2000);
			int available = serialPort.getInputBufferBytesCount();
			String lines = null;
			if(available>0){
				lines = serialPort.readString(available).trim();
			}
			LOGGER.debug(TAG, "clear response : "+lines);
			if (lines!=null) {
				break;
			}
			//			LoggerResponse response = ParsersLogger.StatusResponse.buildStatusResponse(lines, sensore);
			//			if(response.isValid()){
			//				LOGGER.debug(TAG, "risponsta valida");
			//				break;
			//			}else{
			//				LOGGER.debug(TAG, "risponsta errata");
			//			}
			retry_count++;
		}
	}

	/**
	 * ritorna una riga letta dallo strumento.
	 * La riga letta ha superato il checksum
	 * 
	 * @param indirizzo
	 * @param sleepMS
	 * @param start
	 * @return una o più righe lette
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws SerialPortException
	 */
	public String writeAndRead(int sleepMS, int start) throws InterruptedException, IOException, SerialPortException {
		String lines = null;
		Boolean checkSumOK = null;
		Exception ee = null;
		try {
			LOGGER.debug(TAG, "writeAndRead: " + sleepMS + " " + start);
			char campioni = (char) start;
			String hexStart = Integer.toHexString(campioni).toUpperCase();
			hexStart = fillString('0',  (6-(hexStart.length()))) + hexStart;
			String cmd = ">" +  "dLR_" + hexStart + "_??!\r\n"; 

			serialPort.writeString(cmd.trim());
			//			Thread.sleep(sleepMS);

			lines = readAllBytes();

			if(lines.trim().length()>0){

				LOGGER.debug(TAG, "\"["+lines+"]\" size: \""+ ((lines==null) ? "null ": lines.length())+"\"");
				checkSumOK = checkCheckSum(lines);
				LOGGER.debug(TAG, "CheckSum result: " + checkSumOK);
				if (!checkSumOK)
					return null;
			}

			//			if(LOGGER.isDebugEnabled()){
			//				LOGGER.debug(TAG, "\"["+lines+"]\" size: \""+ ((lines==null) ? "null ": lines.length())+"\"");
			//			}
			if (lines != null && !lines.trim().isEmpty()) {
				return lines;
			} else {
				return null;
			}
		}catch (Exception e){
			ee = e;
			return null;
		}finally{
			if(DEEP_LOGGER.isDebugEnabled()){
				DEEP_LOGGER.debug(TAG, "\t riga="+lines);
				DEEP_LOGGER.debug(TAG, "\t checkSumOK="+checkSumOK);
				if(ee!=null){
					DEEP_LOGGER.debug(TAG, "\t exception="+ee.getMessage(),ee);
				}
			}
		} 
	}

	private static boolean checkCheckSum(String s) {
		if (s == null)
			return false; 

		int FirstGreater = s.indexOf("<");
 
		if (FirstGreater >= 0 && s.length() >= 0) {
			
			String check_sum = calculateCheckSum(s);
			 
			String stringa_check_sum = s.substring(s.trim().length()-2,
					s.trim().length());

			LOGGER.debug(TAG, "Cheksum calculated: " + stringa_check_sum.toUpperCase());
			LOGGER.debug(TAG, "Cheksum device: " + check_sum);

			boolean result = stringa_check_sum.toUpperCase().equals(
					check_sum.toUpperCase());
			return result;
		} else
			return false;
	}
	
	public static String calculateCheckSum(String s) {
	  	 
			String Stringa = s.substring(0, s.trim().length()-2); 

			int sum = 0;
			int size = Stringa.length();
			for (int i = 0; i < size; i++) {
				sum = sum + (int) Stringa.charAt(i);
			}

			String check_sum1 = Integer.toHexString(sum);
			String check_sum = check_sum1.substring(check_sum1.length()-2, check_sum1.length());
			
			return check_sum;
		 
	}

	public String readAllBytes() {
		
		String result = null;
		
		StringBuilder sb = new StringBuilder();
		int repetition = 0;
		if (OS.isWindows()) {
			repetition = 6;
		} else {
			repetition = 35;
		}
			
		try {

			for(int i=0; i<=repetition; i++){
				Thread.sleep(1000);
				int available = serialPort.getInputBufferBytesCount();
				String line = serialPort.readString(available);
				
				if (line.contains("---------------------")) {
					sb.append(line);
					break;
				}
				
				LOGGER.debug(TAG, line);
				sb.append(line);
			}

		} catch (Exception e) {
			return null;	 
		}
		
//		try {
//			FileUtils.writeStringToFile(new File("template.txt"), sb.toString().length()>MAX_LENGHT ? sb.toString().substring(0, MAX_LENGHT) : sb.toString(), "UTF-8");
//		} catch (Exception e) {
//			LOGGER.error(TAG, "Errore in creazione file");
//		}
		
		if (sb.toString().length()>=MAX_LENGHT) {
			result = sb.toString().substring(0, MAX_LENGHT);
		} else {
			 
//			String template = FileUtils.readFileToString(new File("template.txt"), "UTF-8");
			result = sb.toString() + template.substring(sb.toString().length(), template.length());
			
//			try {
//				FileUtils.writeStringToFile(new File("scarico_part1.txt"), sb.toString(), "UTF-8");
//				FileUtils.writeStringToFile(new File("scarico_part2.txt"), template.substring(sb.toString().length(), template.length()), "UTF-8");
//				FileUtils.writeStringToFile(new File("scarico_all.txt"), result, "UTF-8");
//			} catch (Exception e) {
//				LOGGER.error(TAG, "Errore in creazione file");
//			}
			
			String checkSum = calculateCheckSum(result);
//			FileUtils.writeStringToFile(new File("scarico_before.txt"), result, "UTF-8");
			result = result.substring(0, result.length() - 3) + checkSum; 
//			FileUtils.writeStringToFile(new File("scarico_after.txt"), result, "UTF-8");
		 
		}
	
		LOGGER.info(TAG, "Bytes read:" + result + "|Length:" + result.length());
		return result;

	}

}
