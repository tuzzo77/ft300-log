package com.econorma.logic.logger;

public class LoggerException extends Exception{

	public LoggerException() {
		super();
	}

	public LoggerException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public LoggerException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public LoggerException(String arg0) {
		super(arg0);
	}

	public LoggerException(Throwable arg0) {
		super(arg0);
	}

}
