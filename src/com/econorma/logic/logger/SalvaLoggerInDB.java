package com.econorma.logic.logger;

import java.util.Date;
import java.util.List;

import org.apache.log4j.MDC;

import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.logic.SensoreManager;
import com.econorma.logic.logger.ScaricoSensoreLoggerLogic.Result;
import com.econorma.resources.Testo;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

public class SalvaLoggerInDB extends ScaricoSensoreLoggerLogic.EmptyListener{
	
	private static final Logger LOGGER = Logger.getLogger(SalvaLoggerInDB.class);
	private static final String TAG = SalvaLoggerInDB.class.getSimpleName();

	@Override
	public void onSuccess(Result result) {
		creaProva(result);
	}
	
	private void creaProva(Result result){
		if(result.isFound()){
			
			List<Lettura> letture = result.getLetture();
			if(letture.isEmpty()){
				return; 
			}
			
			LOGGER.info(TAG, "INIZIO SCRITTURA CAMPIONI: "+ result.getSensore().getId_sonda());
			MDC.put("event_name", "Inizio scrittura campioni: " + letture.size() + " Logger " + result.getSensore().getId_sonda());
			LOGGER.log(LoggerCustomLevel.AUDIT, "INIZIO SCRITTURA CAMPIONI: " + letture.size() + "  LOGGER: " + result.getSensore().getId_sonda());
			
			Sensore sensore = result.getSensore();
			
			String startEventType = result.getStartEventType();
			
			
			//TODO mettere in transazione
			
			//BEGIN SALVATAGGIO
	
			Prova prova = new Prova();
			prova.setResponsabile(Testo.SCARICO_LOGGER + startEventType + ": " + sensore.getId_sonda());
			prova.setData_inizio(letture.get(0).getData());
			prova.setData_fine(letture.get(letture.size()-1).getData());
			prova.setOra_inizio(letture.get(0).getData());
			prova.setOra_fine(letture.get(letture.size()-1).getData());
			prova.setData_scarico(new Date());
	
//			StringBuilder id_sensori = new StringBuilder();
//			List<Sensore> sensori = new ArrayList<Sensore>();
//			{
//				sensori.add(sensore);
//	
//				for(Sensore s: sensori){
//					id_sensori.append(s.getId_sonda()).append(",");
//				}
//				int lastComma = id_sensori.lastIndexOf(",");
//				if(lastComma>=0)
//					id_sensori.setLength(lastComma);
//	
//	
//			}
	
			prova.setId_sensore(letture.get(0).getIdSonda());
			Application.getInstance().getDao().insertOrUpdateProva(prova);
			 
			LetturaProva letturaProva = new LetturaProva(prova, letture);
			List<LetturaProva> lettureProva = letturaProva.getLettureProva();
	 		
			Application.getInstance().getDao().insertLetture(letture);
			Application.getInstance().getDao().insertLettureProva(lettureProva);
			
			SensoreManager.getInstance().onRead(letture.get(letture.size()-1));
			
			LOGGER.info(TAG, "FINE SCRITTURA CAMPIONI: "+ result.getSensore().getId_sonda());
			MDC.put("event_name", "Fine scrittura campioni: " + letture.size() + " Logger " + result.getSensore().getId_sonda());
			LOGGER.log(LoggerCustomLevel.AUDIT, "FINE INIZIO SCRITTURA CAMPIONI: " + letture.size() + "  LOGGER: " + result.getSensore().getId_sonda());
		}
	}
	
	

}
