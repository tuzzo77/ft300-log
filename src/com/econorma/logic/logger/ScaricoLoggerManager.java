package com.econorma.logic.logger;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.SwingUtilities;

import org.apache.log4j.MDC;

import com.adamtaft.eb.EventBusService;
import com.econorma.Application;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.logic.SensoreManager;
import com.econorma.logic.logger.ScaricoSensoreLoggerLogic.Listener;
import com.econorma.logic.logger.ScaricoSensoreLoggerLogic.Result;
import com.econorma.resources.Testo;
import com.econorma.ui.Events;
import com.econorma.ui.Events.DischargeEvent;
import com.econorma.ui.Events.MainWindowInfoEvent;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

public class ScaricoLoggerManager {
	
	private static final String TAG = "ScaricoLoggerManager";
	private static final Logger LOGGER = Logger.getLogger(ScaricoLoggerManager.class);
	
	private AtomicBoolean _scaricoInCorso = new AtomicBoolean(false);
	private boolean manaulQueued;
	
	private final Object LOCK = new Object();
	
	private boolean initilized;
	
	// gestite in background 
	private boolean AUTO_DISCHARGE;
	private int DOWNLOAD_RETRY_MS;
	private int MINIMUN_SUCCESS_DELAY_MS;
	
	//gestite in background
	private ScheduledFuture<?> downloadTask;
	private HashMap<String, Long> lastDownloadSuccess = new HashMap<String, Long>();
	
	private final ScheduledExecutorService SCHEDULED_EX_SE = Executors.newScheduledThreadPool(1,  new ThreadFactory() {

		@Override
		public Thread newThread(Runnable runnable) {
			Thread thread = new Thread(runnable,"ScaricoLoggerManagerThread");
			thread.setDaemon(true);
			return thread;
		}
	});
	
	public ScaricoLoggerManager(){
		
	}
	
	public void init(){
		if(!initilized){
			initilized = true;
			SCHEDULED_EX_SE.scheduleWithFixedDelay(new Runnable() {
				
				@Override
				public void run() {
					
					int oldDelay = DOWNLOAD_RETRY_MS;
					
					//task per aggiornare ogni tanto la configurazione
					AUTO_DISCHARGE = false;
					DOWNLOAD_RETRY_MS = 10*1000*60;	
					MINIMUN_SUCCESS_DELAY_MS = 10*1000*60*60;
					
					if(!AUTO_DISCHARGE){
						if(downloadTask!=null){
							if(LOGGER.isDebugEnabled()){
								LOGGER.debug(TAG, "scarico automatico disabilitato (prima era abilitato)");
							}
							downloadTask.cancel(false);
						}
						downloadTask = null;
					}else{
						//scarico abilitato
						
						if(downloadTask==null || oldDelay != DOWNLOAD_RETRY_MS){
							if(downloadTask!=null){
								if(LOGGER.isDebugEnabled()){
									LOGGER.debug(TAG, "cancello la schedulazione precedente perchè il delay tra schedulazioni e' cambiato");
								}
								downloadTask.cancel(false);
							}
							
							downloadTask = SCHEDULED_EX_SE.scheduleWithFixedDelay(
									new ScaricoRunnable(new ScaricoAutomatico(ScaricoLoggerManager.this, 
											new ScaricoAutomaticoInfoListener())), 0, DOWNLOAD_RETRY_MS, TimeUnit.MILLISECONDS);
						}
						
					}
				}
			}, 1, 1, TimeUnit.MINUTES);
		}
	}
	
	
	private static class ScaricoAutomaticoInfoListener implements ScaricoAutomatico.Listener{

		@Override
		public void onBegin(int toProcess) {
			EventBusService.publish(new Events.MainWindowInfoEvent(MainWindowInfoEvent.Type.START, System.currentTimeMillis(), System.currentTimeMillis(), "Scarico automatico avviato"));
		}

		@Override
		public void onProcessing(Sensore s, int step, int totalSteps) {
			EventBusService.publish(new Events.MainWindowInfoEvent(MainWindowInfoEvent.Type.RUNNING, System.currentTimeMillis(), System.currentTimeMillis(), "Scarico automatico in corso ("+step+"/"+totalSteps+") - Sensore "+s.getId_sonda() ));
			
		}

		@Override
		public void onComplete() {
			EventBusService.publish(new Events.MainWindowInfoEvent(MainWindowInfoEvent.Type.END, System.currentTimeMillis(), System.currentTimeMillis(),"Scarico automatico completato"));
		}
		
	}

	/**
	 * 
	 * @param sensore
	 * @return true se l'operazioen viene presa in carico
	 */
	public boolean scaricoSingolo(final Sensore sensore, final Listener l){
		synchronized (LOCK) {
			if(_scaricoInCorso.get()){
				LOGGER.info(TAG, "altro scarico in corso...");
				return false;
			}
			
			if(manaulQueued){
				LOGGER.info(TAG, "C'e' gi� uno scarico manuale in coda...");
				return false;
			}
			
			manaulQueued = true;
			
			Runnable runnable = new Runnable() {
				
				@Override
				public void run() {
					try {
						DischargeEvent dischargeEvent = new Events.DischargeEvent(sensore, Events.DischargeEvent.ACTION.RUNNING, Testo.LOGGER_MANUALE);
						LOGGER.info(TAG, "BEGIN SCARICO MANUALE: "+ sensore.getId_sonda());
						MDC.put("event_name", "Scarico Logger");
						LOGGER.log(LoggerCustomLevel.AUDIT, "BEGIN SCARICO MANUALE: "+ sensore.getId_sonda());
						Application.getInstance().handleEvent(dischargeEvent); 
						EventBusService.publish(dischargeEvent); 
						
						String startEventType = Testo.LOGGER_MANUALE;
						final ScaricoSensoreLoggerLogic logic = 
								new ScaricoSensoreLoggerLogic(sensore, startEventType, Arrays.asList(new SalvaLoggerInDB(),l));
						Result scarica = logic.scarica();
						//TODO
					} catch (LoggerException e) {
						LOGGER.error(TAG, e);
					}finally{
						DischargeEvent dischargeEvent = new Events.DischargeEvent(sensore, Events.DischargeEvent.ACTION.COMPLETE, Testo.LOGGER_MANUALE);
						LOGGER.info(TAG, "END SCARICO MANUALE: "+ sensore.getId_sonda());
						LOGGER.log(LoggerCustomLevel.AUDIT, "END SCARICO MANUALE:  "+ sensore.getId_sonda());
						Application.getInstance().handleEvent(dischargeEvent);
						EventBusService.publish(dischargeEvent); 
						synchronized (LOCK) {
							manaulQueued = false;
						}
					}
					
				}
			};
			
			SCHEDULED_EX_SE.execute(new ScaricoRunnable(runnable));
			return true;
		}
		
		
	}
	
	
	
	private static class ScaricoAutomatico implements Runnable{

		public interface Listener {
			
			public void onBegin(int toProcess);
			
			public void onProcessing(Sensore s, int step, int totalSteps);
			
			public void onComplete();
			
		}
		
		private Listener l;
		
		Collection<Sensore> sensoriInterni;
		private boolean isConfigMode;
		ScaricoLoggerManager m ;
		
		public ScaricoAutomatico(ScaricoLoggerManager m, Listener l ){
			this.m = m;
			this.l = l;
		}
		
		@Override
		public void run() {
			synchronized (m.LOCK) {
				if(m.manaulQueued){
					LOGGER.info(TAG, "Uno scarico manuale e' in coda... salto il giro");
					return;
				}
			}
			
			try {
				SwingUtilities.invokeAndWait(new Runnable() {
					
					@Override
					public void run() {
						sensoriInterni = SensoreManager.getInstance().getAllSensori(Location.INTERNO);
						isConfigMode = Application.getInstance().isConfigMode();
					}
				});
			} catch (InvocationTargetException e) {
				/* ignore */
				LOGGER.error(TAG, e);
			} catch (InterruptedException e) {
				/* ignore */
				return;
			}
			
			
			if(sensoriInterni.isEmpty()){
				return;
			}
			
			if(isConfigMode){
				LOGGER.info(TAG, "Scarico automatico annullato - isConfigMode="+isConfigMode);
				MDC.put("event_name", "Scarico Logger");
				LOGGER.log(LoggerCustomLevel.AUDIT, "Scarico automatico annullato - isConfigMode="+isConfigMode);
				return;
			}
			
			boolean oneSuccess = false;
			
			try{
				DischargeEvent dischargeEvent = new Events.DischargeEvent(null, Events.DischargeEvent.ACTION.RUNNING, Testo.LOGGER_AUTOMATICO);
				LOGGER.info(TAG, "BEGIN SCARICO AUTOMATICO " );
			 	
				Application.getInstance().handleEvent(dischargeEvent);
				EventBusService.publish(dischargeEvent);
				
			
				int totalSteps = sensoriInterni.size();
				l.onBegin(totalSteps);
				
				int i = 0;
				for(Sensore sensore : sensoriInterni){
					i++;
					String id_sonda = sensore.getId_sonda();
					Long last = m.lastDownloadSuccess.get(id_sonda);
					if(last==null) last = 0L;
					
					long currentTimeMillis = System.currentTimeMillis();
					
					long delta = currentTimeMillis - (last+m.MINIMUN_SUCCESS_DELAY_MS);
					if(delta>0){
						try{
							l.onProcessing(sensore, i, totalSteps);
							String startEventType = Testo.LOGGER_AUTOMATICO;
							final ScaricoSensoreLoggerLogic logic = 
									new ScaricoSensoreLoggerLogic(sensore, startEventType, Arrays.asList(new SalvaLoggerInDB()));
						
							Result result = logic.scarica();
							if(result.isFound()){
								m.lastDownloadSuccess.put(id_sonda, System.currentTimeMillis());
								oneSuccess = true;
								MDC.put("event_name", "Scarico Logger");
								LOGGER.log(LoggerCustomLevel.AUDIT, "END SCARICO AUTOMATICO SENSORE: " + sensore.getId_sonda());
							}
						}catch(LoggerException bEx){
							//TODO
							// questo tipo di ecce
						}
					}else{
						LOGGER.debug(TAG, "ignoro il sensore  "+id_sonda+". GIA SCARICATO "+((currentTimeMillis-last)/1000/60)+" minuti fa");
					}
				}
			}finally{
				l.onComplete();
				DischargeEvent dischargeEvent = new Events.DischargeEvent(null, Events.DischargeEvent.ACTION.COMPLETE, Testo.LOGGER_AUTOMATICO);
				LOGGER.info(TAG, "END SCARICO AUTOMATICO " );
				Application.getInstance().handleEvent(dischargeEvent);
				EventBusService.publish(dischargeEvent);
//				if(oneSuccess){
//					LoggerUIHelper.refreshGrafico();
//				}
			}
		}
		
	}
	
	
	public class ScaricoRunnable implements Runnable {
		
		private Runnable delegate;
		
		public ScaricoRunnable(Runnable delegate){
			this.delegate = delegate;
		}
		

		@Override
		public void run() {
			try{
				 boolean compareAndSet = _scaricoInCorso.compareAndSet(false, true);
				 if(!compareAndSet){
					 throw new RuntimeException("ERRORE IN SVILUPPO, più operazioni di scarico parallele...");
				 }
				 try{
					 this.delegate.run();
				 }catch(Exception e){
					 LOGGER.fatal(TAG, "Errore non gestito in scarico autoamtico", e);
				 }
			}finally{
				boolean compareAndSet = _scaricoInCorso.compareAndSet(true, false);
				if(!compareAndSet){
					throw new RuntimeException("ERRORE IN SVILUPPO, più operazioni di scarico parallele...");
				}
				
			}
		}
		
	}
	
	public boolean isScaricoRunning(){
		return _scaricoInCorso.get();
	}
	
}
