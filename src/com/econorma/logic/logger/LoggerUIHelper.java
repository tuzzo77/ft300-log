package com.econorma.logic.logger;

import java.util.List;

import javax.swing.SwingUtilities;

import com.econorma.Application;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.logic.LettureProva;
import com.econorma.ui.EconormaChart;

public class LoggerUIHelper {

	private LoggerUIHelper(){ /**/ }
	
	public static void refreshGrafico(){
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				List<Sensore> sensori = Application.getInstance().getDao().loadSensori();
				Prova ultimaProva = Application.getInstance().getDao().findUltimaProva();
				
				LettureProva lettureProva = new LettureProva.LettureProvaCreator().loadAll(ultimaProva);
				EconormaChart econormaChart = Application.getInstance().getGui().getEconormaChartProvider().getValue();

				econormaChart.clear();
				econormaChart.restore(lettureProva, sensori );
			}
		});
		
	}
}
