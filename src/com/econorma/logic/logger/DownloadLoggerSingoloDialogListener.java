package com.econorma.logic.logger;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import com.econorma.Application;
import com.econorma.data.Sensore;
import com.econorma.logic.logger.ScaricoSensoreLoggerLogic.Result;
import com.econorma.resources.Testo;
import com.econorma.ui.ProgressBar;

public class DownloadLoggerSingoloDialogListener implements ScaricoSensoreLoggerLogic.Listener{
	
	private static Application app = Application.getInstance();
	private static final long DURATA = 60000;
	private static final int TIME_VISIBLE = 5000;
	private JDialog dialog;
	private JProgressBar progressBar;
	private long startTime;
	
	@Override
	public void onBegin(Sensore sensore) {
	
		startTime = System.currentTimeMillis();
		
		progressBar = new JProgressBar(0,100);  
//		progressBar.setPreferredSize(new Dimension(266,11));
		progressBar.setPreferredSize(new Dimension(300,20));
		progressBar.setIndeterminate(true);
		
		switch (app.getTheme()) {
		case DARK:
			progressBar.setBackground(Color.DARK_GRAY);
			break;
		case LIGHT:
			progressBar.setBackground(Color.WHITE);
		default:
			break;
		}
		
		progressBar.setUI(new ProgressBar());
//		progressBar.setString("In esecuzione");
//		progressBar.setStringPainted(true);
		
		dialog = new JDialog(Application.getInstance().getMainFrame(), sensore.getId_sonda() + " - " + "Importazione in corso....", true);
		dialog.setContentPane(progressBar);
		dialog.setResizable(false);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				dialog.setVisible(true);
			}
		});
	}

	@Override
	public void onSuccess(Result result) {

	}

	@Override
	public void onComplete(final Result result, final LoggerException ex) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				
				dialog.dispose();
				dialog = null;
				
				JOptionPane pane; 
				if(ex != null){
					pane = new JOptionPane("Errore scarico", JOptionPane.ERROR_MESSAGE);
				}else{
					if (result==null)
						return;
					if(!result.isFound()){
						pane =  new JOptionPane("Il sensore non � stato trovato", JOptionPane.WARNING_MESSAGE);
					}else{
						pane =  new JOptionPane(Testo.DISCHARGE_COMPLETATO, JOptionPane.INFORMATION_MESSAGE);
//						LoggerUIHelper.refreshGrafico();
					}
				}
				
				final JDialog dialog = pane.createDialog(null, "Econorma");
				dialog.setModal(false);
				dialog.setVisible(true); 
				
				Timer timer = new Timer();


				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						dialog.setVisible(false);
					}
				}, TIME_VISIBLE);
			}
		});
		
		
		
	}

	@Override
	public void onProgress(Sensore sensore, final int letture) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				long current = System.currentTimeMillis();
				long elapsed = (current - startTime);
//				progressBar.setValue(Math.min((int)((elapsed)/((double)DURATA)*100),100));
//				progressBar.setString("Letture ricevute : "+letture);
			}
		});
		
		
	} 

}
