package com.econorma.logic.logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.MDC;

import com.adamtaft.eb.EventBusService;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.LoggerResponse;
import com.econorma.data.Sensore;
import com.econorma.io.ParsersLogger;
import com.econorma.io.VerificaLogger;
import com.econorma.ui.Events;
import com.econorma.ui.Events.MainWindowInfoEvent;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

import jssc.SerialPortException;

public class ScaricoSensoreLoggerLogic {

	private static final Logger LOGGER = Logger.getLogger(ScaricoSensoreLoggerLogic.class);
	private static final String TAG = ScaricoSensoreLoggerLogic.class.getSimpleName();
	
	private Sensore sensore;
	private String startEventType;
	
	private boolean fileDebug = true;
	
	private ListenerList listenerDelegates = new ListenerList();

	public ScaricoSensoreLoggerLogic(Sensore sensore, String startEventType, List<? extends Listener> listeners) {
		this.sensore = sensore;
		this.startEventType = startEventType;
		listenerDelegates.addAll(listeners);
	}

	/**
	 * 
	 * @return le letture oppure found=false se non ha trovato il logger
	 * @throws LoggerException in caso di errore non previsto
	 */
	public Result scarica() throws LoggerException {
		listenerDelegates.onBegin(sensore);
		
		LoggerException ex = null;
		Result successResult = null;
		try{
			 
				VerificaLogger verificaLogger = new VerificaLogger(sensore);
				try {
					 LoggerResponse response = verificaLogger.verifica();
					 
					 LOGGER.debug(TAG, "Risposta Logger " + response.isValid() + "|" +  response.getIdSonda());
					 
					if(response !=null && response.isValid()){
						MDC.put("event_name", "Scarico Logger " + sensore.getId_sonda());
						LOGGER.log(LoggerCustomLevel.AUDIT, "BEGIN SCARICO MANUALE SENSORE: " + sensore.getId_sonda());
						LoggerChannel loggerChannel = new LoggerChannel(sensore);
						boolean connected = loggerChannel.connect();
						
						if(connected){
							try{
								List<Lettura> lettureValide = _scarica2(loggerChannel);
								LOGGER.debug(TAG, "Letture valide: " + lettureValide);
								if(lettureValide!=null){
									successResult = new Result(this, true, lettureValide);
									listenerDelegates.onSuccess(successResult);
//							DA CONTROLLARE		
								if (Application.getInstance().isDeleteAfterDownload()) {
									loggerChannel.cancellaDati(sensore);
								}
								}else{
									throw new LoggerException("Errore Scarico "+sensore.getId_sonda());
								}
							}finally{
								loggerChannel.disconnect();
							}
						}else{
							throw new LoggerException("Impossibile aprire la porta seriale");
						}
					}else{
						successResult = new Result(this, false, null);
						listenerDelegates.onSuccess(successResult);
					}
				} catch (InterruptedException e) {
					LOGGER.error(TAG, e);
				} catch (IOException e) {
					throw new LoggerException(e);
				} catch (SerialPortException e) {
					throw new LoggerException(e);
				} catch (Exception e) {
					LOGGER.error(TAG, e);
				}
			 
			return successResult;
		}catch(LoggerException e){
			ex = e;
			throw e;
		}finally{
			listenerDelegates.onComplete(successResult, ex);
		}
	}
	
	private List<Lettura> _scarica2(LoggerChannel loggerChannel) throws InterruptedException, IOException, SerialPortException{
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			/*ignore*/
		}
		
//		final int MAX_STREAM_END=2;
		final int MAX_STREAM_END=1;
		final int MAX_ERROR_SEQUENCE=50;
		int error_seq_counter = 0;
		int end_seq_counter = 0;
		int start=0;
	
		String sensorName = sensore.getId_sonda();
		
		List<Lettura> tmpValido = new ArrayList<Lettura>();
		
		HashSet<Date> dateLetture = new HashSet<Date>(); 
		
		while(true){
			LOGGER.debug(TAG, "inizio loop");
			String raw = loggerChannel.writeAndRead(2000, start); //riga valida o null
			if(raw!=null){
				LOGGER.debug(TAG, "Raw:" + raw);
				if(ParsersLogger.isSensorReadMessage(sensorName, raw)){
					
					if(ParsersLogger.isReadEndToken(sensorName, raw)){
						start = start + 1024;
						error_seq_counter = 0;
						end_seq_counter++;
					}else{
						List<Lettura> letture = ParsersLogger.createLettura(raw);
						if(letture==null){
							throw new RuntimeException("errore in sviluppo \n"+raw+"\n");
						}else{
							boolean isrepetition = false;
//							AL MOMENTO SCRIVO SEMPRE UNA PROVA NUOVA ANCHE SE SCARICO DUE VOLTE
//							List<Date> date = new ArrayList<Date>(letture.size());
//							for(Lettura l : letture){
//								if(dateLetture.contains(l.getData())){
//									isrepetition = true;
//									LOGGER.error(TAG, "informazione gia' ricevuta per l'istante "+l.getData());
//								}else{
//									date.add(l.getData());
//								}
//							}
							
							if(!isrepetition){
//								for (Lettura l : letture) {
//									if (l.getTemperaturaGrezza()==0){
//										continue;
//									}
//									tmpValido.add(l);	
//								}
								tmpValido.addAll(letture);
								listenerDelegates.onProgress(sensore, tmpValido.size());
//								dateLetture.addAll(date);
								start = start + 1024;
								error_seq_counter = 0;
								end_seq_counter = 0;
								
//								 ESCO DAL CICLO
								if (raw.contains("---------------------")) {
									end_seq_counter=1;	
								}
								
								
							}else{
								error_seq_counter++;
							}
						}
					}
					
				}else{
					LOGGER.debug(TAG, "Parser not correct");
					error_seq_counter++;
				}
			}else{
				error_seq_counter++;
			}
			
			if(error_seq_counter >= MAX_ERROR_SEQUENCE){
				throw new IOException("Limite errori consecutivi raggiunto : "+error_seq_counter);
			}
			
			if(end_seq_counter>=MAX_STREAM_END){
				return tmpValido;
			}	
		}
	}
 
	
	

	/**
	 * Casi ammissibili
	 * 
	 * found = false ->  il sensore e' raggiungibile
	 * found = true ->
	 * found = true && letture.size>0 -> ho scaricato qualcosa
	 * @author alex
	 *
	 */
	public static class Result{

		public boolean found;
		private List<Lettura> letture;
		private Sensore sensore;
		private String startEventType;

		private Result(ScaricoSensoreLoggerLogic logic, boolean found, List<Lettura> letture){
			this.sensore = logic.sensore;
			this.startEventType = logic.startEventType;
			this.found = found;
			this.letture = letture;
		}

		public boolean isFound() {
			return found;
		}

		public List<Lettura> getLetture() {
			return letture;
		}

		public Sensore getSensore() {
			return sensore;
		}

		public String getStartEventType() {
			return startEventType;
		}
		
		

	}
	
	private static class ListenerList implements Listener {
		
		private final List<Listener> listeners;
		private long startDate;
		private long endDate;
		
		public ListenerList(){
			listeners = new ArrayList<ScaricoSensoreLoggerLogic.Listener>();
			startDate = System.currentTimeMillis();
			endDate = System.currentTimeMillis();
		}
		
		public void addAll(List<? extends Listener> listeners){
			if(listeners!=null) {
				this.listeners.addAll(listeners);
			}
		}

		public void onSuccess(Result result){
			for(Listener l : listeners){
				try{
					l.onSuccess(result);
				}catch(Exception e){
					LOGGER.error(TAG, "errore listener", e);
				}
			}
		}
		
		@Override
		public void onBegin(Sensore sensore) {
			EventBusService.publish(new Events.MainWindowInfoEvent(MainWindowInfoEvent.Type.START, startDate, endDate, "Scarico Logger " + sensore.getId_sonda() + " avviato"));
			for(Listener l : listeners){
				try{
					l.onBegin(sensore);
				}catch(Exception e){
					LOGGER.error(TAG, "errore listener", e);
				}
			}
		}

		@Override
		public void onComplete(Result result, LoggerException ex) {
			endDate = System.currentTimeMillis();
			EventBusService.publish(new Events.MainWindowInfoEvent(MainWindowInfoEvent.Type.END, startDate, endDate, "Scarico Logger " + result.getSensore().getId_sonda() + " completato"));
			for(Listener l : listeners){
				try{
					l.onComplete(result, ex);
				}catch(Exception e){
					LOGGER.error(TAG, "errore listener", e);
				}
			}
		}

		@Override
		public void onProgress(Sensore sensore, int letture) {
			EventBusService.publish(new Events.MainWindowInfoEvent(MainWindowInfoEvent.Type.RUNNING, startDate, endDate,"Scarico campioni " + String.valueOf(letture) + " del Logger " + sensore.getId_sonda() + " in corso"));
			for(Listener l : listeners){
				try{
					l.onProgress(sensore, letture);
				}catch(Exception e){
					LOGGER.error(TAG, "errore listener", e);
				}
			}
		}
	}


	/**
	 * Tutti i metodi sono invocati dal thread di scarico
	 * @author alex
	 *
	 */
	public static interface Listener {
		
		public void onBegin(Sensore sensore);
		
		public void onSuccess(Result result);
		
		public void onComplete(Result result, LoggerException ex);
		
		public void onProgress(Sensore sensore, int letture);
	}
	
	public static class EmptyListener implements Listener{

		@Override
		public void onBegin(Sensore sensore) {
		}

		@Override
		public void onSuccess(Result result) {
		}

		@Override
		public void onComplete(Result result, LoggerException ex) {
		}

		@Override
		public void onProgress(Sensore sensore, int letture) {
		}
		
	}
	
	
}
