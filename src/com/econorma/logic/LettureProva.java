package com.econorma.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.econorma.Application;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;

public class LettureProva {

	private Prova prova;
	private HashMap<String,List<LetturaProva>> letturePerSensore = new HashMap<String, List<LetturaProva>>();
	
	public LettureProva(Prova prova, List<LetturaProva> letture){
		this.prova = prova;
		
		for(LetturaProva lettura : letture){
			String id_sonda = lettura.getId_sonda();
			List<LetturaProva> list = letturePerSensore.get(id_sonda);
			if(list==null){
				list = new ArrayList<LetturaProva>();
				letturePerSensore.put(id_sonda, list);
			}
			list.add(lettura);
		}
		
		for(Entry<String, List<LetturaProva>> e : letturePerSensore.entrySet()){
			List<LetturaProva> value = e.getValue();
			Collections.sort(value,Measurable.COMPARATOR);
			
		}
	}
	
	public Prova getProva() {
		return prova;
	}
	
	public List<String> getIdSensoriSorted(){
		ArrayList<String> arrayList = new ArrayList<String>(letturePerSensore.keySet());
		Collections.sort(arrayList);
		return arrayList;
	}
	
	public List<LetturaProva> lettureDelSensore(String idSonda){
		return letturePerSensore.get(idSonda);
	}
	
	public List<LetturaProva> lettureDelSensore(Sensore sensore){
		return lettureDelSensore(sensore.getId_sonda());
	}
	
	public static class LettureProvaCreator{
		
		public LettureProva loadOnlyMedie(Prova prova){
			
			String variant = null;
			
			 
			 
			List<LetturaProva> letture = 
					Application.getInstance().getDao().
					findLettureProvaByProvaAndSondaPrefix(prova, variant);
			
			return new LettureProva(prova, letture);
		}
		
		public LettureProva loadAll(Prova prova){
			
			List<LetturaProva> letture = 
					Application.getInstance().getDao().findLettureProvaByProva(prova, false);
			
			return new LettureProva(prova, letture);
		}
		
		public LettureProva loadProvaIdSonda(Prova prova,String idSonda){
			
			List<LetturaProva> letture = Application.getInstance().getDao().findLettureProvaByProvaAndSonda(prova, idSonda);
			return new LettureProva(prova, letture);
		}
	}
}
