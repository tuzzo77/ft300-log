package com.econorma.sinottico;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.econorma.data.Lettura;
import com.econorma.data.Sensore;
import com.econorma.logic.SensoreManager;
import com.econorma.resources.Testo;

/**
 * Delegato responsabile del trasinamento di un sensore nel plan
 * 
 * @author marcobettiol
 *
 */
public class PlanSensorMoveDelegate {

	private PlanPresenter planPresenter;
	private MouseDrag mouseDrag;

	public PlanSensorMoveDelegate(PlanPresenter planPresenter) {
		mouseDrag = new MouseDrag();
		setPlanPresenter(planPresenter);

	}


	private class MouseDrag extends MouseAdapter {

		PlanSensorPresenter selectedSensorPresenter;

		@Override
		public void mousePressed(MouseEvent m) {
			Point point = m.getPoint();
			selectedSensorPresenter  = planPresenter.getFirstPresentersOnScreen(point.x, point.y);
		}

		@Override
		public void mouseReleased(MouseEvent m) {
			selectedSensorPresenter = null;
		}

		@Override
		public void mouseDragged(MouseEvent m) {
			if(selectedSensorPresenter!=null){
				Point point = m.getPoint();
				PlanProjector planProjector = planPresenter.getPlanProjector();
				selectedSensorPresenter.moveSensor(planProjector, point.x, point.y);
			}

		}

		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2 && !e.isConsumed()) {
				Point point = e.getPoint();
				selectedSensorPresenter  = planPresenter.getFirstPresentersOnScreen(point.x, point.y);
				if(selectedSensorPresenter!=null){
				    PlanSensorModel model = selectedSensorPresenter.getModel();
					displayInfo(model.getIdentity(), model.getDescription());
				}
			}
		}
	}


	public void setPlanPresenter(PlanPresenter planPresenter){
		if(this.planPresenter!=null){
			planPresenter.getDisplay().removeMouseListener(mouseDrag);
			planPresenter.getDisplay().removeMouseMotionListener(mouseDrag);
		}
		this.planPresenter = planPresenter;
		if(this.planPresenter!=null){
			planPresenter.getDisplay().addMouseListener(mouseDrag);
			planPresenter.getDisplay().addMouseMotionListener(mouseDrag);
		}
	}
	
	public void displayInfo(String identity, String description) {
		
		Lettura lettura = SensoreManager.getInstance().getUltimeLettureByIdentity(identity);
		Sensore sensore = lettura.getSensore();	 
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
	    NumberFormat nf = new DecimalFormat("#####.##");
		String batteria=null;
		
		String message = "<html> Sonda: " + lettura.getIdSonda() + " ";
		message = message + (description != null ? " - " + description: " ") + "<br>";
		message = message + "Ultima data : " + "<b>" + sdf.format(lettura.getData()) + "</b>" + "&nbsp" + "<br>";
		message = message + "Ultima temp : " + "<b>" + nf.format(lettura.getValore()) + "�C"+ "</b>" + "&nbsp" + "<br>";
		message = message + "Ultima ur% : " + "<b>" + nf.format(lettura.getUmidita()) + "%" + "</b>" + "&nbsp" + "<br>";
		 
		String batteriaLog = lettura.getStatoBatteria();
		
		switch (batteriaLog) {
		case "e":
			batteria = Testo.BATTERIA_CARICA;
			break;
		case "d":
			batteria = Testo.BATTERIA_CARICA;
			break;
		case "c":
			batteria = Testo.BATTERIA_CARICA;
			break;
		case "b":
			batteria = Testo.BATTERIA_SCARICA;
			break;
		case "a":
			batteria = Testo.BATTERIA_SCARICA;
			break;
		default:
			break;
		}
		 
		message = message + "Stato Batteria : " + "<b>" + batteria + "</b>" + "&nbsp" + "<br>";
		 
		message = message + "</html>";

		JLabel label = new JLabel();
		label.setText(message);
	
		JOptionPane.showMessageDialog(null, label ,"Informazione Logger", JOptionPane.INFORMATION_MESSAGE);
	}


}
