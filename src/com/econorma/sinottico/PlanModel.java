package com.econorma.sinottico;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.event.SwingPropertyChangeSupport;

/**
 * Classe responsabile di rappresentare un rettangolo su cui sono 
 * posizionati i sensori.
 * 
 * All'interno del rettangolo è posizionata un'immagine
 * 
 * Le posizioni logiche degli elementi possono variare tra 0 e 1
 * 
 * @author marcobettiol
 *
 */
public class PlanModel {
	
	public static String SENSOR_ADDED = "SENSOR_ADDED";
	public static String SENSOR_REMOVED = "SENSOR_REMOVED";
	public static String IMAGE = "image";
	
	public static final double X_LEFT   = 0;
	public static final double X_RIGHT  = +1;
	public static final double Y_TOP    = 0;
	public static final double Y_BOTTOM = +1;
	
	transient private final SwingPropertyChangeSupport pcs = new SwingPropertyChangeSupport(this, true) ;
	
	private List<PlanSensorModel> sensors = new ArrayList<PlanSensorModel>();
	
	private String imagePath = "";

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		String oldValue = this.imagePath;
		this.imagePath = imagePath;
		pcs.firePropertyChange(IMAGE, oldValue, imagePath);
	}
	
	
	public void addSensor(PlanSensorModel sensor){
		sensors.add(sensor);
		pcs.firePropertyChange(SENSOR_ADDED, null, sensor);
	}
	
 	
//	public PlanSensorModel removeSensor(PlanSensorModel sensor){
// 		int index = sensors.indexOf(sensor);
//		PlanSensorModel model =  index>0 ? sensors.remove(index) : null;
//		pcs.firePropertyChange(SENSOR_REMOVED, sensor, null);
//		return model;
//	}
	
	public void removeSensor(PlanSensorModel sensor){
		sensors.remove(sensor);
		pcs.firePropertyChange(SENSOR_REMOVED, sensor, null);
	}
	
	public Iterator<PlanSensorModel> sensorIterator(){
		return sensors.iterator();
	}
	
	
	public void addPropertyChangeListener(PropertyChangeListener l) {
		pcs.addPropertyChangeListener(l);
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		pcs.removePropertyChangeListener(l);
	}

}
