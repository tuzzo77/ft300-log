package com.econorma.sinottico;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventObject;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jdesktop.application.Application.ExitListener;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.io.Receiver.ReadListener;
import com.econorma.logic.SensoreManager;
import com.econorma.resources.Testo;
import com.econorma.ui.ConfigWindow;
import com.econorma.ui.Events;
import com.econorma.ui.Events.ChangeSinotticoEvent;
import com.econorma.util.ImageUtils;
import com.econorma.util.Logger;
import com.econorma.util.OS;
import com.econorma.util.SwingUtils;

/**
 * Si occupa di gestire un plan ed gli eventi a lui collegati
 * 
 * Delega ai figli la responsabilità di presentarsi
 * 
 * @author marcobettiol
 *
 */
public class PlanPresenter implements PropertyChangeListener, ExitListener  {
	
	private static final Logger logger = Logger.getLogger(PlanPresenter.class);
	private static final String TAG = "PlanPresenter";
	
	private PlanModel planModel;
	
	private final List<PlanSensorPresenter> planSensorPresenters = new ArrayList<PlanSensorPresenter>();
	private PlanSensorMoveDelegate moveDelegate;
	private PlanProjector planProjector;
	private PlanManagedSensorDelegate managedSensorDelegate;
	private PlanPersistenceDelegate persistenceDelegate;
	private SensorInfoDelegate sensorInfoDelegate;
	private String sinottico;
	private PlanPanel panel;
	
	private final ReadListener myReadListener = new ReadListener() {

		@Override
		public void onRead(Lettura lettura) {
	 		panel.repaint();
		}
	};
	
	
	
	public PlanPresenter(){
		panel = new PlanPanel();
		this.moveDelegate = new PlanSensorMoveDelegate(this);
		this.sensorInfoDelegate = new SensorInfoDelegate(this);
		this.managedSensorDelegate = new PlanManagedSensorDelegate(this); // keep the reference
		if (Application.getInstance().getPreferences().getFileSinottico()==null) {
			String currentDir =  System.getProperty("user.dir");
			String imagePath = currentDir+"\\home.jpg";
			onBackgroungImageChanged(imagePath);
		}
	 	EventBusService.subscribe(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if(PlanModel.IMAGE.equals(evt.getPropertyName())){
			String imagePath = (String)evt.getNewValue();
			onBackgroungImageChanged(imagePath);
		}else if(PlanModel.SENSOR_ADDED.equals(evt.getPropertyName())){
			planSensorPresenters.add(new PlanSensorPresenter((PlanSensorModel)evt.getNewValue(), this));
			onSensorNumberChanged();
		}else if(PlanModel.SENSOR_REMOVED.equals(evt.getPropertyName())){
			planSensorPresenters.remove(new PlanSensorPresenter((PlanSensorModel)evt.getOldValue(), this));
			onSensorNumberChanged();
		}
		
	}
	
	public JComponent getDisplay(){
		return panel;
	}
	
	private class PlanPanel extends JPanel {
		
		private static final long serialVersionUID = 9177879298794417524L;
		private Image backgroungImage;
		private Image scaledBackgroung;
		
		int previousWidth = -1; //invalid position
		int previuosHeight = -1; //invalid position
		
		public PlanPanel() {
			this.setLayout(new BorderLayout());
			JPanel buttonPanel = new JPanel();
			buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
			 JButton configura = new JButton();
			if (OS.isWindows()) {
				configura.setText("Impostazioni");
			} else {
				configura.setText("Impostazioni");
			}
			 
			configura.addActionListener(new ActionListener() { 
				  public void actionPerformed(ActionEvent e) { 
						ConfigWindow trasmissione = new ConfigWindow
								(Application
										.getInstance().getMainFrame(), false);
						SwingUtils.showCenteredDialog(trasmissione);
				  } 
				} );
			configura.setPreferredSize(new Dimension(135, 35));
			buttonPanel.add(configura);
			this.add(buttonPanel,BorderLayout.SOUTH);
		}

		@Override
		protected void paintComponent(Graphics g) {
			setOpaque(false);
			
			Dimension size = Application.getInstance().getMainFrame().getSize();
			
//			Dimension size = getSize();
			int height = size.height;
			int width = size.width;
			
			if(scaledBackgroung==null || previousWidth!=width || previuosHeight!=height){
				if(backgroungImage!=null){
					//scale the image
					scaledBackgroung = ImageUtils.scale(backgroungImage, width, height);
 					//logger.info(TAG, "pannello ridimensionato: height:"+height+" width:"+width);
					//g.drawImage(backgroungImage, 0, 0, width, height, null);
				}
			}
			
			if(scaledBackgroung!=null){
				
				int height2 = ((BufferedImage)scaledBackgroung).getHeight();
				int width2 = ((BufferedImage)scaledBackgroung).getWidth();
				int x_offset = Math.max((size.width- width2)>>1,0);
				int y_offset = Math.max((size.height- height2)>>1,0);
				g.drawImage(scaledBackgroung, x_offset, y_offset, null);
				planProjector = new PlanProjector(y_offset, x_offset, height2, width2);
			}else{
				planProjector = null;
			}
			super.paintComponent(g);
			
			if(scaledBackgroung!=null && !planSensorPresenters.isEmpty()){
				for(PlanSensorPresenter sensorPresenter :  planSensorPresenters){
					sensorPresenter.draw(planProjector, g);
				}
			}
		}
		
		
		
		public void setBackgroungImage(Image image){
			scaledBackgroung = null;
			this.backgroungImage = image;
			Dimension size = getSize();
			if(size.width>0 && size.height>0 && backgroungImage!=null){
				this.scaledBackgroung = ImageUtils.scale(image, size.width, size.height);
				repaint();
			}
		}
	}


	public PlanModel getPlanModel() {
		return planModel;
	}



	public void setPlanModel(PlanModel planModel) {
		if(this.planModel!=null){
			planModel.removePropertyChangeListener(this);
		}
		this.planModel = planModel;
		this.planSensorPresenters.clear();
		if(this.planModel!=null){
			planModel.addPropertyChangeListener(this);
			try {
 			onBackgroungImageChanged(planModel.getImagePath());
			}catch (Exception e) {
			logger.error(TAG, "caricamento immagine fallito");
			}
			Iterator<PlanSensorModel> sensorIterator = planModel.sensorIterator();
			while(sensorIterator.hasNext()){
				planSensorPresenters.add(new PlanSensorPresenter(sensorIterator.next(),this));
			}
			onSensorNumberChanged();
		}
	}

	
	private void onBackgroungImageChanged(String imagePath){
		Image backgroungImage = null;
		try{
			backgroungImage =  ImageIO.read(new File(imagePath));
			panel.setBackgroungImage(backgroungImage);
		}catch (Exception e) {
			logger.error(TAG, "caricamento immagine fallito");
		}
	
	}
	
	private void onSensorNumberChanged(){
		invalidate();
	}
	
	
	public PlanSensorMoveDelegate getPlanSensorMoveDelegate(){
		return moveDelegate;
	}
	
	public SensorInfoDelegate getSensorInfoDelegate(){
		return sensorInfoDelegate;
	}
	
	public Iterator<PlanSensorPresenter> sensorPresenters(){
		return planSensorPresenters.iterator();
	}
	
	public PlanProjector getPlanProjector(){
		return planProjector;
	}
	
	public SensorInfoDelegate getInfoDelegate(){
		return sensorInfoDelegate;
	}
	
	public void invalidate(){
		panel.repaint();
	}
	
	public void setPersistenceDelegate(PlanPersistenceDelegate persistenceDelegate){
		this.persistenceDelegate = persistenceDelegate;
	}
	
	@Override
	public boolean canExit(EventObject arg0) {
		return true;
	}

	@Override
	public void willExit(EventObject arg0) {
		persistenceDelegate.save(getPlanModel());
		
	}
	
	@EventHandler
	public void handleEvent(final ChangeSinotticoEvent changeSinotticoEvent){

		 
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				sinottico = changeSinotticoEvent.sinottico;
				onBackgroungImageChanged(sinottico);
 
			}
		});


	}
	
	@EventHandler
	public void handleEvent(final Events.VerificaLoggerEvent verificaEvent){
		 
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					panel.repaint();
	 
				}
			});
	}
	
	@EventHandler
	public void handleEvent(final Events.DischargeEvent dischargeEvent){
		switch(dischargeEvent.action){
		case RUNNING:
			break;
		case COMPLETE:
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					if(Testo.LOGGER_AUTOMATICO.equals(dischargeEvent.type)){
						panel.repaint();
					}else if (Testo.LOGGER_MANUALE.equals(dischargeEvent.type)){
						panel.repaint(); 
					}		 			
				}
			});
		}
	}

	
	public void bind(SensoreManager sensoreManager) {

		sensoreManager.addOnReadListener(myReadListener);
			
	}
	
	@SuppressWarnings("unchecked")
	public List<PlanSensorPresenter> getPresentersOnScreen(int x, int y){
		final PlanPresenter planPresenter = this;
		Iterator<PlanSensorPresenter> sensorPresenters = planPresenter.sensorPresenters();
        PlanProjector planProjector = planPresenter.getPlanProjector();
        
        List<PlanSensorPresenter> emptyList = Collections.emptyList();
        List<PlanSensorPresenter> result = emptyList;
        
        if(planProjector!=null){
        	 while(sensorPresenters.hasNext()){
             	PlanSensorPresenter sensorPresenter = sensorPresenters.next();
             	if(sensorPresenter.isClickInside(planProjector, x, y)){
             		if(result==emptyList)
             			result = new ArrayList<PlanSensorPresenter>();
             		result.add(sensorPresenter);
             	}
             }
        }
        
		return result;
	}
	
	
	public PlanSensorPresenter getFirstPresentersOnScreen(int x, int y){
		List<PlanSensorPresenter> presentersOnScreen = getPresentersOnScreen(x, y);
		if(!presentersOnScreen.isEmpty())
			return presentersOnScreen.get(0);
		else
			return null;
	}
}
