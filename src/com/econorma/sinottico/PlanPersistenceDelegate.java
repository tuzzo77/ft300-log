package com.econorma.sinottico;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.logic.SensoreManager;
import com.econorma.persistence.Preferences;
import com.econorma.util.Logger;
import com.econorma.util.StringUtils;
import com.google.gson.Gson;

/**
 * Delegato responsabile della gestione del salvataggio/caricamento
 * del plan e dei sensori contenuti
 * 
 * @author marcobettiol
 *
 */
public class PlanPersistenceDelegate{
	
	private static final Logger logger = Logger.getLogger(PlanPersistenceDelegate.class);
	private static final String TAG = "PlanPersistenceDelegate";
	
	private Preferences preferences;
	
	public PlanPersistenceDelegate(Preferences preferences){
		this.preferences =  preferences;
	}
	
	public void save(PlanModel plan){
		Gson gson = new Gson();
		
		PersistablePlan persistablePlan = new PersistablePlan();
		Iterator<PlanSensorModel> sensorIterator = plan.sensorIterator();
		while(sensorIterator.hasNext()){
			PlanSensorModel next = sensorIterator.next();
			PersistablePlanSensor persistablePlanSensor = new PersistablePlanSensor();
			persistablePlanSensor.identity = next.getIdentity();
			persistablePlanSensor.description = next.getDescription();
			persistablePlanSensor.x = next.getPosition()[0];
			persistablePlanSensor.y = next.getPosition()[1];
			persistablePlan.sensors.add(persistablePlanSensor);
			
		}
		String json = gson.toJson(persistablePlan);
		
		preferences.setSensoriSinottico(json);
	}
	
	public PlanModel load(){
		try{
			
			PlanModel planModel = new PlanModel();
			
			String fileSinottico = preferences.getFileSinottico();
			if (fileSinottico!=null) {
				planModel.setImagePath(preferences.getFileSinottico());
			} else {
				String dir = System.getProperty("user.dir");
				planModel.setImagePath(dir + "\\"+ "home.jpg");	
			}
			
			String json = preferences.getSensoriSinottico();
			if(StringUtils.nullOrEmpty(json))
				return null;
			Gson gson = new Gson();
			PersistablePlan persistablePlan = gson.fromJson(json, PersistablePlan.class);
			
			boolean clear = false;
			
			if(persistablePlan.sensors!=null){
				for(PersistablePlanSensor s: persistablePlan.sensors){
					Sensore sensoreById = SensoreManager.getInstance().getSensoreById(s.identity);
					if(sensoreById!=null && !Location.UNKNOWN.equals(sensoreById.getLocation())){
						planModel.addSensor(new PlanSensorModel(s.identity, s.description, s.x, s.y));
					}else{
						clear = true;
					}
				}
			}
			if(clear){
				save(planModel);
			}
			
			return planModel;
		}catch (Exception e) {
			logger.error(TAG, "errore caricamento sinottico", e);
			return null;
		}
		
	}
	
	private static class PersistablePlan{
		List<PersistablePlanSensor> sensors = new ArrayList<PersistablePlanSensor>();
	}
	
	private static class PersistablePlanSensor{
		double x;
		double y;
		String identity;
		String description;
	}

}
