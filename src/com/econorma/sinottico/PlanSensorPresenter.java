package com.econorma.sinottico;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.TextAttribute;
import java.awt.geom.Ellipse2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.Sensore;
import com.econorma.logic.SensoreManager;

/**
 * Si occupa di disegnare un sensore e gestire le sue interazioni
 * @author marcobettiol
 *
 */
public class PlanSensorPresenter implements PropertyChangeListener {

	private PlanSensorModel model;
	private PlanPresenter planPresenter;
	Ellipse2D.Double circle;
	private Rectangle rectangle;
	private static Application app = Application.getInstance();
	private static long DEAD_TIME_CHECK = 1000 * 3600;
	private Map<TextAttribute, Object> attributes = new HashMap<>();

	public PlanSensorPresenter(PlanSensorModel model, PlanPresenter planPresenter){
		if(model==null){
			throw new IllegalArgumentException("null model!!");
		}
		this.model = model;
		this.planPresenter = planPresenter;
		model.addPropertyChangeListener(this);
		
		attributes.put(TextAttribute.FAMILY, Font.DIALOG);
		attributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_SEMIBOLD);
		attributes.put(TextAttribute.SIZE, 14);

	}

	public void draw(PlanProjector projector, Graphics g){
		
		double[] position = model.getPosition();
		int[] xy = projector.projectPointToPlan(position[0], position[1]);
		Graphics2D g2d = (Graphics2D)g;
		circle = new Ellipse2D.Double(xy[0]-8, xy[1]-8, 16, 16);
		rectangle = new Rectangle(xy[0]-8, xy[1]-8, 40, 40);
		
		
		int timeMancataTrasmissione = 0;
		if (timeMancataTrasmissione!=0){
			DEAD_TIME_CHECK= timeMancataTrasmissione *60 * 1000;
		}
 
		List<Lettura> lettura = SensoreManager.getInstance().getUltimeLetture();
		for(Lettura l: lettura){
			if (model.getIdentity().equals(l.getIdSonda())){
				
				Double valore = Double.MIN_VALUE;
				Double umidita = Double.MIN_VALUE;
				boolean error = false;

				Sensore sensore = l.getSensore();
				if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0 || sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0) {

					if (l != null) {

				 		valore = l.getValore();
				 		
						if (valore != null && outOfLimit(l))
						{
							error=true;
					 	} else {
							error=false;
					 	}
						
						
						if (!error){
					 	switch (app.getTipoMisure()) {
						case TEMPERATURA_UMIDITA:

							valore = l.getValore();
							umidita = l.getUmidita();
							
							if (umidita != null && outOfLimitURT(l))
							{
								error=true;
						 	} else {
								error=false;
						 	}

						}
						}
						
						
						if (error)
						{
							g2d.setColor(Color.RED);
						} else {
							g2d.setColor(Color.BLACK);
						}
						
						
							Date data = l.getData();
							if (data != null
									&& (System.currentTimeMillis() - DEAD_TIME_CHECK) > data
									.getTime()) {
								g2d.setColor(Color.BLUE);
							} else {
								 
							}
						 
						
						g2d.fill(rectangle);
//						g2d.setFont(new Font(null, Font.PLAIN, 12));
						g2d.setFont(Font.getFont(attributes));
						g2d.draw(rectangle);
						
						g2d.setColor(Color.WHITE);
						g.drawString(Double.toString(l.getValore()),xy[0]-4, xy[1]+4);

 
						switch (app.getTipoMisure()) {
						case TEMPERATURA_UMIDITA:

							valore = l.getValore();
							umidita = l.getUmidita();

							g2d.setColor(Color.WHITE);
						
							if (l.getUmidita()!=0){
							g.drawString(Double.toString(l.getUmidita()),xy[0]-4, xy[1]+16);
							}
						}
						 
					}

 
					 
					break;
				} else {
					
					g2d.setColor(Color.BLACK);
					
					Date data = l.getData();
					if (data != null
							&& (System.currentTimeMillis() - DEAD_TIME_CHECK) > data
							.getTime()) {
						g2d.setColor(Color.BLUE);
					} else {
						 
					}
					
					g2d.fill(rectangle);
			
//					g2d.setFont(new Font(null, Font.PLAIN, 12));
					g2d.setFont(Font.getFont(attributes));
					g2d.draw(rectangle);
					
					g2d.setColor(Color.WHITE);
					g.drawString(Double.toString(l.getValore()),xy[0]-4, xy[1]+4);
					
					switch (app.getTipoMisure()) {
					case TEMPERATURA_UMIDITA:

						valore = l.getValore();
						umidita = l.getUmidita();

						g2d.setColor(Color.WHITE);
						 
						if (l.getUmidita()!=0){
						g.drawString(Double.toString(l.getUmidita()),xy[0]-4, xy[1]+16);
						}
					}
					 
					
				}
			}
			
		}

		
	
		
			g2d.setColor(Color.BLACK);
//			g2d.setFont(new Font(null, Font.PLAIN, 14));
			g2d.setFont(Font.getFont(attributes));
			
			if (model.getDescription()!=null){
			g.drawString(model.getIdentity() ,xy[0]-12, xy[1]-35);
			g.drawString(model.getDescription(),xy[0]-50, xy[1]-18);
			} else {
			g.drawString(model.getIdentity(),xy[0]-18, xy[1]-18);	
			}
		}


		@Override
		public int hashCode() {
			return model.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if(obj instanceof PlanSensorPresenter){
				return model.equals(((PlanSensorPresenter) obj).model);
			}
			return false;
		}


		public PlanSensorMoveDelegate getPlanSensorMoveDelegate(){
			return planPresenter.getPlanSensorMoveDelegate();
		}

		public SensorInfoDelegate getSensorInfoDelegate(){
			return planPresenter.getSensorInfoDelegate();
		}

		public boolean isClickInside(PlanProjector projector, int x, int y){
			if(rectangle!=null){
				return rectangle.contains(x, y);
			}
			return false;
		}

		public void moveSensor(PlanProjector projector, int x, int y){
			double[] new_position = projector.projectPlanToPoint(x, y);
			if(new_position!=null){
				model.setPosition(new_position);
			}

		}



		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if(PlanSensorModel.POSITION.equals(evt.getPropertyName())){
				planPresenter.invalidate();
			}

		}

		public PlanSensorModel getModel(){
			return model;
		}

		private static boolean outOfLimit(Lettura lettura){
			Sensore sensore = lettura.getSensore();
			if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0){
				return lettura.getValore() < sensore.getRangeMin() || lettura.getValore() > sensore.getRangeMax();
			}
			return false;

		}

		private static boolean outOfLimitURT(Lettura lettura){
			Sensore sensore = lettura.getSensore();
			if (sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0){
				return lettura.getUmidita() < sensore.getRangeMinURT() || lettura.getUmidita() > sensore.getRangeMaxURT();
			}
			return false;

		}

	}
