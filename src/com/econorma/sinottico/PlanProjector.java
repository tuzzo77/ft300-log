package com.econorma.sinottico;

public class PlanProjector {

	public final int top;
	public final int left;
	public final int height;
	public final int width;
	
	public PlanProjector(int top, int left, int height , int width){
		this.top = top;
		this.left = left;
		this.height = height;
		this.width = width;
	}
	
	public int[] projectPointToPlan(double x, double y){
		int to_X = (int)((width)*x+left);
		int to_Y = (int)((height)*y+top);
		return new int[]{to_X, to_Y};
	}
	
	public double[] projectPlanToPoint(int x, int y){
		double to_x = (x - left)/(double)width;
		double to_y = (y- top) / (double) height;
		if(to_x>=0 && to_x<=1 && to_y>=0 && to_y<=1){
			return new double[]{to_x, to_y};
		}else{
			return null;
		}
	}
	
}
