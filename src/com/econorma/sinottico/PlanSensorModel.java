package com.econorma.sinottico;

import java.beans.PropertyChangeListener;

import javax.swing.event.SwingPropertyChangeSupport;

/**
 * Posizione di un sensore all'interno di un rettangolo
 * 
 * @author marcobettiol
 *
 */
public class PlanSensorModel {
	
	public static final String POSITION = "positon";
	public static final String IDENTITY = "identity";
	public static final String DESCRIPTION = "description";
	public static final String INFO = "info";
	
	transient private final SwingPropertyChangeSupport pcs = new SwingPropertyChangeSupport(this, true) ;
	
	private double position[];
	private double info;
	private String identity;
	private String description;
	
	public PlanSensorModel(String identity, String description){
		this(identity, description, 0.5,0.5); // centro
	}
	
	public PlanSensorModel(String identity, String description, double x, double y){
		this.identity = identity;
		this.description = description;
		this.position = new double[]{x,y};
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((identity == null) ? 0 : identity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanSensorModel other = (PlanSensorModel) obj;
		if (identity == null) {
			if (other.identity != null)
				return false;
		} else if (!identity.equals(other.identity))
			return false;
		return true;
	}

	public double[] getPosition() {
		return position;
	}

	public void setPosition(double[] position ) {
		
		double[] oldValue = this.position;
		this.position = position;
		pcs.firePropertyChange(POSITION, oldValue, position);
	}

	

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		String oldValue = this.identity;
		this.identity = identity;
		pcs.firePropertyChange(IDENTITY, oldValue, identity);
	}
	
	public void setPosition(double x, double y){
		setPosition(new double[]{x,y});
	}
	
	
	public void addPropertyChangeListener(PropertyChangeListener l) {
		pcs.addPropertyChangeListener(l);
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		pcs.removePropertyChangeListener(l);
	}
	
	public String getDescription() {
		return description;
	}
 
	public void setDescription(String description) {
		String oldValue = this.description;
		this.description = description;
		pcs.firePropertyChange(DESCRIPTION, oldValue, description);
	}
	
	
	public void setInfo(double info ) {
		
		double oldValue = this.info;
		this.info = info;
		pcs.firePropertyChange(INFO, oldValue, info);
	}
	
	
}
