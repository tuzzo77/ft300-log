package com.econorma.sinottico;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import com.econorma.Application;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.logic.SensoreManager;
import com.econorma.logic.logger.DownloadLoggerSingoloDialogListener;
import com.econorma.ui.ConfigWindow;
import com.econorma.util.SwingUtils;

/**
 * Delegato responsabile di inserire un nuovo sensore nel planmodel
 * a partire da una lista di sensori esistenti.
 * 
 * @author marcobettiol
 *
 */
public class PlanManagedSensorDelegate implements PopupMenuListener {

	private PlanPresenter planPresenter;
	private SondeMenu sondeMenu;

	public PlanManagedSensorDelegate(PlanPresenter planPresenter) {
		this.planPresenter = planPresenter;
		sondeMenu = new SondeMenu();
		setPlanPresenter(planPresenter);
		sondeMenu.addPopupMenuListener(this);
	 }
	
	

	private class SondeMenu extends JPopupMenu {
	 	
		private static final long serialVersionUID = -5020601846222133525L;
		JMenu addItem;
		JMenu sensorItem;
		
	    public SondeMenu(){
	    	addItem = new JMenu(Application.getInstance().getBundle().getString("planManagedSensorDelegate.aggiungi"));
	    	add(addItem);
	    	sensorItem = new JMenu(Application.getInstance().getBundle().getString("planManagedSensorDelegate.sensore"));
	        add(sensorItem);
	    }
	  
	}
	
	public void setPlanPresenter(PlanPresenter planPresenter){
		this.planPresenter = planPresenter;
		if(this.planPresenter!=null){
		 	planPresenter.getDisplay().setComponentPopupMenu(sondeMenu);
		}
	}

	@Override
	public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
		
		Point mousePosition = planPresenter.getDisplay().getMousePosition();
		if(mousePosition==null)
			return;
		int x = mousePosition.x;
		int y = mousePosition.y;
		
		PlanSensorPresenter selectedSensorPresenter = planPresenter.getFirstPresentersOnScreen(x, y);
		
		if(selectedSensorPresenter!=null){
		
			//remove
			sondeMenu.sensorItem.removeAll();
			
			sondeMenu.addItem.setVisible(false);
			sondeMenu.sensorItem.setVisible(true);
			
			
			PlanSensorModel model = selectedSensorPresenter.getModel();
			String identity = model.getIdentity();
			Sensore s = SensoreManager.getInstance().getSensoreById(identity);
			String msg ;
			if(s!=null){
				msg = getName(s.getId_sonda(), s.getDescrizione());
			}else{
				msg = "sensore sconosciuto";
				
			}
			sondeMenu.sensorItem.setText(msg);
			
			JCheckBoxMenuItem removeSensor = new JCheckBoxMenuItem();
			removeSensor.setAction(new RemoveSensorAction("Rimuovi",model));
			sondeMenu.sensorItem.add(removeSensor);
			
			JCheckBoxMenuItem downloadSensor = new JCheckBoxMenuItem();
			downloadSensor.setAction(new DownloadSensorAction("Scarica",model));
			sondeMenu.sensorItem.add(downloadSensor);
			
			JCheckBoxMenuItem configuraSensor = new JCheckBoxMenuItem();
			configuraSensor.setAction(new ConfigSensorAction("Impostazioni",model));
			sondeMenu.sensorItem.add(configuraSensor);
			
			
		}else{
			
			//add
			sondeMenu.addItem.removeAll();
			
			sondeMenu.addItem.setVisible(true);
			sondeMenu.sensorItem.setVisible(false);
        
        
			
			List<Sensore> interni = SensoreManager.getInstance().retrieveSensors(Location.INTERNO);
			List<Sensore> esterni = SensoreManager.getInstance().retrieveSensors(Location.ESTERNO);
//			List<Sensore> non_gestiti = SensoreManager.getInstance().retrieveSensors(Location.UNKNOWN);
			 
			for(Sensore s: interni){
				
				String name = getName(s.getId_sonda(), s.getDescrizione());
				JCheckBoxMenuItem sensorItem = new JCheckBoxMenuItem(name);
				sensorItem.setAction(new AddSensorAction(name, s.getId_sonda(), s.getDescrizione(), x,y));
				
				sensorItem.setEnabled(true);
				sensorItem.setSelected(checkSensor(s.getId_sonda()));
				if (sensorItem.isSelected()){
					sensorItem.setEnabled(false);	
				}
			 
				sondeMenu.addItem.add(sensorItem);
					
			}
			
			if(!esterni.isEmpty())
				sondeMenu.addSeparator();
			for(Sensore s: esterni){
				
				String name = getName(s.getId_sonda(), s.getDescrizione());
				JCheckBoxMenuItem sensorItem = new JCheckBoxMenuItem(name);
				sensorItem.setAction(new AddSensorAction(name, s.getId_sonda(), s.getDescrizione(), x,y));
				
				sensorItem.setEnabled(true);
				sensorItem.setSelected(checkSensor(s.getId_sonda()));
				if (sensorItem.isSelected()){
					sensorItem.setEnabled(false);	
				}
			 	
				sondeMenu.addItem.add(sensorItem);
			}
 
		}
	}

	@Override
	public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
	}

	@Override
	public void popupMenuCanceled(PopupMenuEvent e) {
	}

	
	
	private class AddSensorAction extends AbstractAction{

		private static final long serialVersionUID = 6849598228527794202L;
		private String id_sonda;
		private String descrizione;
		private int x;
		private int y;
		
		public AddSensorAction(String name, String id_sonda, String descrizione, int x, int y) {
 			super(name);
			this.id_sonda = id_sonda;
			this.descrizione = descrizione;
			this.x = x;
			this.y = y;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
		 
			double[] position = planPresenter.getPlanProjector().projectPlanToPoint(x, y);
			planPresenter.getPlanModel().addSensor(new PlanSensorModel(id_sonda, descrizione, position[0],position[1]));
			
		}
		
	}
	
	private class RemoveSensorAction extends AbstractAction{

		private static final long serialVersionUID = 6849598228527794202L;
		private PlanSensorModel sensor;
		
		public RemoveSensorAction(String actionDisplay, PlanSensorModel sensor) {
			super(actionDisplay);
			this.sensor = sensor;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			planPresenter.getPlanModel().removeSensor(sensor);
		}
		
	}
	
	private class ConfigSensorAction extends AbstractAction{

		private static final long serialVersionUID = 6849598228527794202L;
		private PlanSensorModel sensor;
		
		public ConfigSensorAction(String actionDisplay, PlanSensorModel sensor) {
			super(actionDisplay);
			this.sensor = sensor;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			ConfigWindow trasmissione = new ConfigWindow
					(Application
							.getInstance().getMainFrame(), false);
			SwingUtils.showCenteredDialog(trasmissione);
		}
		
	}
	
	private class DownloadSensorAction extends AbstractAction{

		private static final long serialVersionUID = 6849598228527794202L;
		private PlanSensorModel sensor;
		
		public DownloadSensorAction(String actionDisplay, PlanSensorModel sensor) {
			super(actionDisplay);
			this.sensor = sensor;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String identity = sensor.getIdentity();
			Sensore sensore = SensoreManager.getInstance().getSensoreById(identity);
			Application.getInstance().getScaricoLoggerManager().scaricoSingolo(sensore, new DownloadLoggerSingoloDialogListener());
		}
		
	}
	
	private String getName(String id, String descrizione){
		
		String name="";
		
		if (descrizione!=null) {
			name=id + " - " + descrizione;
			
		}else{
			name=id;
		}
		
		return name;
	}
	
	
	private boolean checkSensor(String id){
		
		boolean check=false;
		String identity = null;
				
		Iterator<PlanSensorModel> sensorIterator = planPresenter.getPlanModel().sensorIterator();
		
		while(sensorIterator.hasNext()){
			PlanSensorModel next = sensorIterator.next();
			identity = next.getIdentity();
			
			if (identity.equals(id)){
				check=true;
				break;
			}
		}
		
		return check;
		
	}
 
	

}
