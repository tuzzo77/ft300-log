package com.econorma.export;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.econorma.Application;
import com.econorma.data.LastSensor;
import com.econorma.data.LetturaProva;
import com.econorma.data.Sensore;
import com.econorma.gui.GUI;
import com.econorma.logic.SensoreManager;
import com.econorma.util.Logger;

public class ExporterLoggerAllPivot {

	private static final String TAG = ExporterLoggerAllPivot.class.getCanonicalName();

	private static final Logger logger = Logger.getLogger(ExporterLoggerAllPivot.class);

	private static final int COLONNA_ORA = 0;
	private static final int COLONNA_ID = 1;

	private static final int COLONNA_VALORE = 2;
	private static final int COLONNA_OFFSET = 3;
	private static final int COLONNA_VALORE_URT = 4;
	private static final int COLONNA_OFFSET_URT = 5;

	private static final int COLONNA_EFFETTO_LETALE= 4;
	private static final int COLONNA_F0 = 5;

	private static final int OFFSET_LETTURE = 5;

	private static Application app = Application.getInstance();

	private static GUI gui = Application.getInstance().getGui();
	private static final String TUTTI = "TUTTI";

	private String id_sonda;
	private String des_sonda;
	private Date dataDa;
	private Date dataA;
	private boolean onlyInAlarm;

	private CellStyle cellStyle;
	private CellStyle cellStyle2;
	private CellStyle cellStyle3;
	private CellStyle cellStyle4;
	private CellStyle cellStyle5;
	private CellStyle cellStyle6;
	private CellStyle cellStyle7;
	private SimpleDateFormat sdf = new SimpleDateFormat("HH"); 

	public ExporterLoggerAllPivot(Date dataDa, Date dataA){
		this.dataDa=dataDa;
		this.dataA=dataA;
	}


	/*
	 * Time consuming Export
	 * 
	 * @return the file exported or null if error occurreds
	 */
	public File export(File dir, String name){

		List<Sensore> allSensor = new ArrayList<Sensore>();

		Workbook wb = new HSSFWorkbook(); // keep 100 rows in memory, exceeding rows will be flushed to disk
		CreationHelper createHelper = wb.getCreationHelper();

		List<LetturaProva> lastByData = Application.getInstance().getDao().findLastByData(dataDa, dataA);
		LinkedHashMap<LastSensor, Double> sensori = new LinkedHashMap<LastSensor, Double>();

		for (LetturaProva lp: lastByData){
			Sensore sensore = SensoreManager.getInstance().getSensoreById(lp.getId_sonda());
			if (!allSensor.contains(sensore)){
				allSensor.add(sensore);
			}
			String time = sdf.format(lp.getData());
			LastSensor last = new LastSensor(time, lp.getId_sonda());
			sensori.put(last, lp.getValore());	
		}
  
		Sheet sh = null;
		Row row =null;
		Cell cell0=null;
		Cell cell1=null;
		Cell cell2=null;
		Cell cell3=null;
		Cell cell4=null;
		Cell cell5=null;
		Cell cell6=null;


		sh = wb.createSheet();

		cellStyle2 = wb.createCellStyle();
		HSSFPalette palette = ((HSSFWorkbook) wb).getCustomPalette();
		cellStyle2.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		cellStyle2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle2.setAlignment(HorizontalAlignment.CENTER);
		Font font = wb.createFont();
		font.setBold(true);
		cellStyle2.setFont(font);

		Row header = sh.createRow(0);


		Cell header_durata = header.createCell(0);
		header_durata.setCellValue("DATA DAL");
		header_durata.setCellStyle(cellStyle2);

		Cell header_datapro = header.createCell(1);
		header_datapro.setCellValue("AL");
		header_datapro.setCellStyle(cellStyle2);


		switch (app.getTipoMisure()) {
		case TEMPERATURA_UMIDITA:
		case MISTA:
			Cell header_filler = header.createCell(2);
			header_filler.setCellStyle(cellStyle2);
		}



		row = sh.createRow(1);
		cellStyle = wb.createCellStyle();
		cellStyle.setDataFormat(
				createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm:ss"));
		cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		font = wb.createFont();
		font.setBold(true);
		cellStyle.setFont(font);


		cell3 = row.createCell(0);
		cell3.setCellValue(dataDa);
		cell3.setCellStyle(cellStyle);
		cell4 = row.createCell(1);
		cell4.setCellValue(dataA);
		cell4.setCellStyle(cellStyle);
		switch (app.getTipoMisure()) {
		case TEMPERATURA_UMIDITA:
		case MISTA:
			Cell header_filler = row.createCell(2);
			header_filler.setCellStyle(cellStyle);
		}

		sh.autoSizeColumn(0);
		sh.autoSizeColumn(1);
		sh.autoSizeColumn(2);
		sh.autoSizeColumn(3);
		sh.autoSizeColumn(4);
		sh.autoSizeColumn(5);


		header = sh.createRow(3);


		cellStyle3 = wb.createCellStyle();
		cellStyle3.setAlignment(HorizontalAlignment.CENTER);
		font = wb.createFont();
		font.setBold(true);
		cellStyle3.setFont(font);


		cellStyle4 = wb.createCellStyle();
		cellStyle4.setAlignment(HorizontalAlignment.CENTER);

		cellStyle5 = wb.createCellStyle();
		cellStyle5.setAlignment(HorizontalAlignment.CENTER);
		cellStyle5.setDataFormat(
				createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm:ss"));


		header = sh.createRow(5);

		Cell header_data = header.createCell(COLONNA_ORA);
		header_data.setCellValue("ORA");
		header_data.setCellStyle(cellStyle3);
		
		if (allSensor.size() > 0) {
			  Collections.sort(allSensor, new Comparator<Sensore>() {
			      @Override
			      public int compare(final Sensore object1, final Sensore object2) {
			    	  if (object1.getDescrizione()==null){
			    		  return (object2.getDescrizione() == null) ? 0 : -1;
			    	  }
			    	  if (object2.getDescrizione()==null){
			    		  return 1;
			    	  }
			          return object1.getDescrizione().compareTo(object2.getDescrizione());
			      }
			  });
			}

		for(int i=0; i<allSensor.size() ; i++){
			Cell header_id = header.createCell(COLONNA_ID+i);
			Sensore s = allSensor.get(i);
			String sonda = getName(s.getId_sonda(), s.getDescrizione());
			header_id.setCellValue(sonda);
			header_id.setCellStyle(cellStyle3);
		}

		sh=wb.getSheetAt(0);
		wb.setSheetName(0, "ALL");


		if(lastByData==null || lastByData.isEmpty()){
			row = sh.createRow(OFFSET_LETTURE);
			cell1 = row.createCell(1);
			cell1.setCellValue("Nessuna Lettura Ricevuta");
		}else{

			int x = 0;
			for (int h = 1; h <= 23; h++) {
				String interval = String.format("%02d", h);
				x++;

				int lastRowNum = sh.getLastRowNum();
				row = sh.createRow(x+OFFSET_LETTURE);
				cell0 = row.createCell(COLONNA_ORA);
				cell0.setCellStyle(cellStyle4);
				cell0.setCellValue(interval+":00");


				for(int i=0; i<allSensor.size() ; i++){
					Sensore s = allSensor.get(i);
					LastSensor lastSensor = new LastSensor(interval, s.getId_sonda());
					if (sensori.get(lastSensor) != null){
						Double temp = sensori.get(lastSensor).doubleValue()*100.0/100.0;
						cell0 = row.createCell(COLONNA_ID+i);
						cell0.setCellStyle(cellStyle4);
						cell0.setCellValue(temp);
						cell0.setCellStyle(cellStyle4);
					}
				}
				
			}
		}



		File outFile = new File(dir.getAbsolutePath()+"/"+name);

		FileOutputStream out;
		try {
			try{
				outFile.delete();
			}catch(Exception e){}
			outFile.createNewFile();
			out = new FileOutputStream(outFile);
			wb.write(out);
			out.close();
			return outFile;
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		}

	}

	private static void styleHeader(Workbook workbook, Cell cell){
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(true);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(font);
		style.setFillBackgroundColor(HSSFColor.AQUA.index);
		cell.setCellStyle(style);
	}


	private static boolean outOfLimit(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0){
			return letturaProva.getValore() < sensore.getRangeMin() || letturaProva.getValore() > sensore.getRangeMax();
		}
		return false;

	}

	private static boolean outOfLimitURT(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0){
			return letturaProva.getUmidita() < sensore.getRangeMinURT() || letturaProva.getUmidita() > sensore.getRangeMaxURT();
		}
		return false;

	}

	private String getName(String id, String descrizione){

		String name="";

		if (descrizione!=null && descrizione.trim().length()>0) {
			name=descrizione;

		}else{
			name=id;
		}

		return name;
	}
 
}
