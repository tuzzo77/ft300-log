package com.econorma.export;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.econorma.Application;
import com.econorma.data.LetturaProva;
import com.econorma.data.Sensore;
import com.econorma.logic.SensoreManager;
import com.econorma.util.Logger;

public class ExporterStorico {

	private static final String TAG = ExporterStorico.class.getCanonicalName();

	private static final Logger logger = Logger.getLogger(ExporterStorico.class);

	private static final String TUTTI = "TUTTI";

	private String id_sonda;
	private String des_sonda;
	private Date dataDa;
	private Date dataA;
	private boolean onlyInAlarm;

	private static final DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
	private static final DateFormat timeFormat=new SimpleDateFormat("HH:mm");

	private static final DateFormat filedateformat=new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
	public static final NumberFormat nf = new DecimalFormat("####0.00");
	

	public ExporterStorico(String id_sonda, String des_sonda, Date dataDa, Date dataA){
		this.id_sonda=id_sonda;
		this.dataDa=dataDa;
		this.dataA=dataA;
		this.des_sonda=des_sonda;
	}

	public File export(File dir){
		return export(dir,"Logger__"+id_sonda+"__"+filedateformat.format(dataDa));

	}

	/*
	 * Time consuming Export
	 * 
	 * @return the file exported or null if error occurreds
	 */
	public File export(File dir, String name){

		if (!Application.getInstance().isTemperature() && !Application.getInstance().isTemperatureHumidity()) {
			return null;
		}
		
		List<Sensore> sensori = new ArrayList<Sensore>();
		Sensore sensore=null;
		List<Sensore> loadSensori = Application.getInstance().getDao().loadSensori();
		StringBuilder sb = new StringBuilder();


		if (id_sonda.equals(TUTTI)){

			for (Sensore se : loadSensori) {
				switch (se.getLocation()) {
				case ESTERNO:
					sensori.add(se);
					break;
				case INTERNO:
					sensori.add(se);
					break;
				default:
				}	 
			}

		}
		else {
			sensore = SensoreManager.getInstance().getSensoreById(id_sonda);
			sensori.add(sensore);
		}

 
		for(int ii=0; ii<sensori.size();ii++){

			Sensore s = sensori.get(ii);
			String id_sonda = s.getId_sonda();

			String descrizione = (!des_sonda.isEmpty()) ? des_sonda : s.getDescrizione();

			List<LetturaProva> findLettureProvaBySonda	=  Application.getInstance().getDao().findLettureProvaBySonda(id_sonda, dataDa, dataA, onlyInAlarm);

			if(findLettureProvaBySonda==null || findLettureProvaBySonda.isEmpty()){
				return null;
			}else{

				int size = findLettureProvaBySonda.size();

				for(int i=0; i<size ; i++){
					LetturaProva p = findLettureProvaBySonda.get(i);

					String idSonda = p.getId_sonda();
					String data = dateFormat.format(p.getData());
					String ora = timeFormat.format(p.getData());
					String temp = nf.format(p.getValore());

					sb.append(idSonda);
					sb.append(";");
					sb.append(data);
					sb.append(";");
					sb.append(ora);
					sb.append(";");
					sb.append(temp.replace(",", "."));
					sb.append(";");
					if (Application.getInstance().isTemperatureHumidity()) {
						String urt = nf.format(p.getUmidita());
						sb.append(urt.replace(",", "."));
					}
					sb.append("\r\n");

				}

			}
 
		}


		try {

			File outFile = new File(dir.getAbsolutePath()+"/"+name);
			PrintWriter writer = new PrintWriter(outFile);
			writer.write(sb.toString());

			try {
				writer.close();
			}
			catch (Exception e) {

			}
			return outFile;
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		}

	}
 


}
