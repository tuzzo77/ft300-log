package com.econorma.export;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.econorma.Application;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.gui.GUI;
import com.econorma.logic.SensoreManager;
import com.econorma.util.Logger;

public class ExporterProvaAllOne {

	private static final String TAG = ExporterProvaAllOne.class.getCanonicalName();

	private static final Logger logger = Logger.getLogger(ExporterProvaAllOne.class);

	private final Prova prova;

	private static final int COLONNA_ID = 0;
	private static final int COLONNA_DATA = 1;
	private static final int COLONNA_VALORE = 2;
	private static final int COLONNA_OFFSET = 3;
	private static final int COLONNA_INCERTEZZA = 4;
	private static final int COLONNA_VALORE_URT = 4;
	private static final int COLONNA_OFFSET_URT = 5;

	private static final int COLONNA_INCERTEZZA_TEMP = 6;
	private static final int COLONNA_INCERTEZZA_URT = 7;

	private static final int COLONNA_EFFETTO_LETALE = 4;
	private static final int COLONNA_F0 = 5;


	private static final int OFFSET_LETTURE = 7;

	private static Application app = Application.getInstance();

	private static GUI gui = Application.getInstance().getGui();

	private static final DateFormat filedateformat=new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
	public static final NumberFormat nf = new DecimalFormat("#####.##");
	public static final NumberFormat nfATP = new DecimalFormat("####0.00");

	private CellStyle cellStyle;
	private CellStyle cellStyle2;
	private CellStyle cellStyle3;
	private CellStyle cellStyle4;
	private CellStyle cellStyle5;
	private CellStyle cellStyle6;
	private CellStyle cellStyle7;

	public ExporterProvaAllOne(Prova prova){
		this.prova = prova;
	}



	public File export(File dir){

		Date dataProva=null;
		File exporter=null;

		dataProva=new Date();	
		exporter= exporter(dir,"Registrazione__"+prova.getId()+"__"+filedateformat.format(dataProva));

		return exporter;

	}

	/*
	 * Time consuming Export
	 * 
	 * @return the file exported or null if error occurreds
	 */
	public File exporter(File dir, String name){

		List<Sensore> sensori = new ArrayList<Sensore>();
		Sensore mySensor = Sensore.newInstance(prova.getId_sensore(), 0, null, null, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null, 0, 0, 0, 0, 0, null, 0, 0, 0,  0, 0, 0, null);
		sensori.add(mySensor);
		

		Workbook wb = new HSSFWorkbook(); // keep 100 rows in memory, exceeding rows will be flushed to disk
		CreationHelper createHelper = wb.getCreationHelper();

		
		String responsabile = prova.getResponsabile();
		
		Date dataProva = prova.getData_inizio();


		int x = 0;
		for(int ii=0; ii<sensori.size();ii++){


			Sensore s = sensori.get(ii);
			String id_sonda = s.getId_sonda();
			List<LetturaProva> findByProvaAndSonda = Application.getInstance().getDao().findLettureProvaByProvaAndSonda(prova, id_sonda);

			if (id_sonda.equals(Application.getInstance().getBundle().getString("sensore.mediaInterna"))){
				continue;
			}


			Sheet sh = null;
			Row row =null;
			Cell cell0=null;
			Cell cell1=null;
			Cell cell2=null;
			Cell cell3=null;
			Cell cell4=null;
			Cell cell5=null;
			Cell cell6=null;


			if (ii==0) {

				sh = wb.createSheet();
				wb.setSheetName(x, id_sonda);


				CellStyle cellStyle2 = wb.createCellStyle();
				HSSFPalette palette = ((HSSFWorkbook) wb).getCustomPalette();
				cellStyle2.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
				cellStyle2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				Font font = wb.createFont();
				font.setBold(true);
				cellStyle2.setFont(font);

				Row header = sh.createRow(0);


				Cell header_durata_real = header.createCell(0);
				header_durata_real.setCellValue("DURATA REGISTRAZIONE");
				header_durata_real.setCellStyle(cellStyle2);
				Cell header_datapro_real = header.createCell(1);
				header_datapro_real.setCellValue("DATA INIZIO REGISTRAZIONE");
				header_datapro_real.setCellStyle(cellStyle2);

				switch (app.getTipoMisure()) {
				case TEMPERATURA_UMIDITA:
				case MISTA:
					Cell header_filler = header.createCell(2);
					header_filler.setCellStyle(cellStyle2);
				}



				row = sh.createRow(1);
				cellStyle = wb.createCellStyle();
				cellStyle.setDataFormat(
						createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm:ss"));
				cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
				cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				font = wb.createFont();
				font.setBold(true);
				cellStyle.setFont(font);



				cell3 = row.createCell(0);
				
				cell3.setCellStyle(cellStyle2);
				cell4 = row.createCell(1);
				cell4.setCellValue(dataProva);
				cell4.setCellStyle(cellStyle);
				switch (app.getTipoMisure()) {
				case TEMPERATURA_UMIDITA:
				case MISTA:
					Cell header_filler = row.createCell(2);
					header_filler.setCellStyle(cellStyle);
				}



				sh.autoSizeColumn(1);
				sh.autoSizeColumn(2);
				sh.autoSizeColumn(3);
				sh.autoSizeColumn(4);
				sh.autoSizeColumn(5);



				header = sh.createRow(3);


				cellStyle3 = wb.createCellStyle();
				cellStyle3.setAlignment(HorizontalAlignment.CENTER);
				font = wb.createFont();
				font.setBold(true);
				cellStyle3.setFont(font);

				cellStyle4 = wb.createCellStyle();
				cellStyle4.setAlignment(HorizontalAlignment.CENTER);

				header = sh.createRow(5);

				Cell header_id = header.createCell(COLONNA_ID);
				header_id.setCellValue("ID SONDA");
				header_id.setCellStyle(cellStyle3);

				Cell header_data = header.createCell(COLONNA_DATA);
				header_data.setCellValue("DATA");
				header_data.setCellStyle(cellStyle3);

				Cell header_valore = header.createCell(COLONNA_VALORE);
				header_valore.setCellValue("TEMPERATURA °C");

				Cell header_offset = header.createCell(COLONNA_OFFSET);
				header_offset.setCellValue("OFFSET");
				header_offset.setCellStyle(cellStyle3);

				 
					Cell header_incertezza = header.createCell(COLONNA_INCERTEZZA_TEMP);
					header_incertezza.setCellValue("INCERTEZZA");
					header_incertezza.setCellStyle(cellStyle3);
				 

			 

				switch (app.getTipoMisure()) {
				case TEMPERATURA:
					 
						header_incertezza = header.createCell(COLONNA_INCERTEZZA);
						header_incertezza.setCellValue("INCERTEZZA");
						header_incertezza.setCellStyle(cellStyle3);
					 
					break;
				case TEMPERATURA_UMIDITA:
				case MISTA:
					Cell header_valore_urt = header.createCell(COLONNA_VALORE_URT);
					header_valore_urt.setCellValue("UMIDITA RELATIVA %");
					header_valore_urt.setCellStyle(cellStyle3);
					Cell header_offset_urt = header.createCell(COLONNA_OFFSET_URT);
					header_offset_urt.setCellValue("OFFSET UR%");
					header_offset_urt.setCellStyle(cellStyle3);
				 
						header_incertezza = header.createCell(COLONNA_INCERTEZZA_TEMP);
						header_incertezza.setCellValue("INCERTEZZA");
						header_incertezza.setCellStyle(cellStyle3);
						Cell header_incertezzaUrt = header.createCell(COLONNA_INCERTEZZA_URT);
						header_incertezzaUrt.setCellValue("INCERTEZZA UR%");
						header_incertezzaUrt.setCellStyle(cellStyle3);
					 
					break;
				}

				 

			} else {
				sh=wb.getSheetAt(0);
				wb.setSheetName(0, "ALL");
			}



			if(findByProvaAndSonda==null || findByProvaAndSonda.isEmpty()){
				row = sh.createRow(OFFSET_LETTURE);
				cell1 = row.createCell(1);
				cell1.setCellValue("Nessuna Lettura Ricevuta");
			}else{

				int size = findByProvaAndSonda.size();

				cellStyle = wb.createCellStyle();
				cellStyle.setDataFormat(
						createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm:ss"));
				cellStyle.setAlignment(HorizontalAlignment.CENTER);


				Font font1 = wb.createFont();
				font1.setColor(HSSFColor.RED.index);
				cellStyle6 = wb.createCellStyle();
				cellStyle6.setAlignment(HorizontalAlignment.CENTER);
				cellStyle6.setFont(font1);

				cellStyle7 = wb.createCellStyle();
				cellStyle7.setAlignment(HorizontalAlignment.CENTER);
				cellStyle7.setDataFormat(
						createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm:ss"));
				cellStyle7.setFont(font1);

				int lastRowNum = sh.getLastRowNum();

				for(int i=0; i<size ; i++){

					LetturaProva p = findByProvaAndSonda.get(i);


					if (ii==0) {
						row = sh.createRow(i+OFFSET_LETTURE);
					} else {
						row = sh.createRow(i+lastRowNum);
					}

					cell0 = row.createCell(COLONNA_ID);
					cell0.setCellStyle(cellStyle4);
					cell0.setCellValue(p.getId_sonda());
					sh.autoSizeColumn(COLONNA_ID);

					cell1 = row.createCell(COLONNA_DATA);
					cell1.setCellStyle(cellStyle);
					cell1.setCellValue(p.getData());


					//					String	offset =  nf.format(p.getValore()-p.getValore_grezzo());

					String	temp =  nf.format(p.getValore());

					cell2 = row.createCell(COLONNA_VALORE);
					cell2.setCellValue(temp);
					cell2.setCellStyle(cellStyle4);

					double offset;
					if (p.getOffset() != null){
						offset = p.getOffset();
					} else {
						offset =  Math.round((p.getValore()-p.getValore_grezzo())*10.0)/10.0;
					}
					cell4 = row.createCell(COLONNA_OFFSET);
					if (offset !=0.0) 
						cell4.setCellValue(offset);
					cell4.setCellStyle(cellStyle4);

					switch (app.getTipoMisure()) {
					case TEMPERATURA:
						 
							cell4 = row.createCell(COLONNA_INCERTEZZA);
							double incertezza = Math.round(p.getIncertezza()*10.0/10.0);
							if (incertezza!=0.0) 
								cell4.setCellValue(incertezza);
							cell4.setCellStyle(cellStyle4);	
						 
						break;
					case TEMPERATURA_UMIDITA:
					case MISTA:
						double umid = Math.round(p.getUmidita()*100.0)/100.0;
						cell3 = row.createCell(COLONNA_VALORE_URT);
						cell3.setCellValue(umid);
						cell3.setCellStyle(cellStyle4);

						double offsetURT;
						if (p.getOffsetUrt() != null){
							offsetURT = p.getOffsetUrt();
						} else {
							offsetURT = Math.round((p.getUmidita()-p.getUmidita_grezzo())*10.0)/10.0;
						}
						//						String	offsetURT =  nf.format(p.getUmidita()-p.getUmidita_grezzo());
						cell3 = row.createCell(COLONNA_OFFSET_URT);
						if (offsetURT !=0.0) 
							cell3.setCellValue(offsetURT);
						cell3.setCellStyle(cellStyle4);

						 
							cell4 = row.createCell(COLONNA_INCERTEZZA_TEMP);
							incertezza = p.getIncertezza();
							if (incertezza!=0.0) 
								cell4.setCellValue(incertezza);
							cell4.setCellStyle(cellStyle4);	
							cell3 = row.createCell(COLONNA_INCERTEZZA_URT);
							double incertezzaUrt = p.getIncertezzaUrt();
							if (incertezzaUrt!=0.0) 
								cell3.setCellValue(incertezzaUrt);
							cell3.setCellStyle(cellStyle4);	
						 
						break;
					}

 

					 
						if (s!=null){
							if (s.getRangeMin()!=0 || s.getRangeMax()!= 0 || s.getRangeMinURT()!=0 || s.getRangeMaxURT()!= 0) {

								if (p.getValore() != 0 && outOfLimit(p))
								{
									cell1.setCellStyle(cellStyle7);
									cell2.setCellStyle(cellStyle6);
									cell4.setCellStyle(cellStyle6); 
								}

								switch (app.getTipoMisure()) {
								case TEMPERATURA_UMIDITA:
								case MISTA:
									if (p.getUmidita() != 0 && outOfLimitURT(p))
									{
										cell1.setCellStyle(cellStyle7);
										cell2.setCellStyle(cellStyle6);
										cell3.setCellStyle(cellStyle6); 
										cell4.setCellStyle(cellStyle6); 

									}
								}
							}
						 
					}


				}

				sh.autoSizeColumn(COLONNA_DATA);
				sh.autoSizeColumn(COLONNA_VALORE);
				sh.autoSizeColumn(COLONNA_OFFSET);
				sh.autoSizeColumn(COLONNA_INCERTEZZA_TEMP);
				switch (app.getTipoMisure()) {
				case TEMPERATURA_UMIDITA:
				case MISTA:
					sh.autoSizeColumn(COLONNA_VALORE_URT);
					sh.autoSizeColumn(COLONNA_OFFSET_URT);
					sh.autoSizeColumn(COLONNA_INCERTEZZA_URT);
				}
			}


			x++; 

		}


		File outFile = new File(dir.getAbsolutePath()+"/"+name + ".xls");

		FileOutputStream out;
		try {
			try{
				outFile.delete();
			}catch(Exception e){}
			outFile.createNewFile();
			out = new FileOutputStream(outFile);
			wb.write(out);
			out.close();
			return outFile;
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		}

	}

	private static void styleHeader(Workbook workbook, Cell cell){
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(true);
		style.setFont(font);
		style.setFillBackgroundColor(HSSFColor.AQUA.index);
		cell.setCellStyle(style);
	}

	private static boolean outOfLimit(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore != null) {
			if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0){
				return letturaProva.getValore() < sensore.getRangeMin() || letturaProva.getValore() > sensore.getRangeMax();
			}	
		}
		return false;

	}

	private static boolean outOfLimitURT(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore != null) {
			if (sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0){
				return letturaProva.getUmidita() < sensore.getRangeMinURT() || letturaProva.getUmidita() > sensore.getRangeMaxURT();
			}	
		}
		return false;

	}



}
