package com.econorma.export;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.ClientAnchor.AnchorType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;

import com.econorma.Application;
import com.econorma.Application.TIPO_MISURE;
import com.econorma.charts.Charts;
import com.econorma.data.LetturaProva;
import com.econorma.data.Sensore;
import com.econorma.gui.GUI;
import com.econorma.logic.LettureProva;
import com.econorma.logic.SensoreManager;
import com.econorma.logic.StandardDeviation;
import com.econorma.util.Logger;

public class ExporterLogger {

	private static final String TAG = ExporterLogger.class.getCanonicalName();

	private static final Logger logger = Logger.getLogger(ExporterLogger.class);

	private static final String COL_DATA = " Data ";
	public static final String COL_TEMP = " C°   ";
	public static final String COL_UMI = " UR%   ";
	public static final String COL_OFFSET = " Offset ";
	public static final String COL_OFFSET_URT = " Offset UR% ";
	public static final String COL_OFFSET_VOLT = " Offset Volt ";
	public static final String COL_OFFSET_MILLIAMPERE = " Offset mA ";
	public static final String COL_OFFSET_OHM = " Offset Pt100 ";
	public static final String COL_VOLT = " Volt ";
	public static final String COL_MILLIAMPERE = " mA ";
	public static final String COL_OHM = " Pt100 ";
	public static final String COL_APERTO_CHIUSO = " Aperto/Chiuso ";


	public static final int POS_DESCRIZIONE = 0;
	public static final int POS_VALORE = 1;

	public static final int POS_DATA = 0;
	public static int POS_TEMPERATURA;
	public static int POS_OFFSET;
	public static int POS_UMIDITA;
	public static int POS_OFFSET_URT;
	public static int POS_VOLT;
	public static int POS_OFFSET_VOLT;
	public static int POS_MILLIAMPERE;
	public static int POS_OFFSET_MILLIAMPERE;
	public static int POS_OHM;
	public static int POS_OFFSET_OHM;
	public static int POS_APERTO_CHIUSO;

	private static final List<String> COLUMN_NAMES = new ArrayList<String>();

	private static final int OFFSET_HEADER = 9;
	private static final int OFFSET_HEADER_URT = 13;
	private static final int OFFSET_CHART = 5;

	private static final int OFFSET_LETTURE_TEMP = 10;
	private static final int OFFSET_LETTURE_URT = 14;


	private static int OFFSET_LETTURE;

	private Charts temperatureChart;
	private Charts humidityChart;
	private Charts voltChart;
	private Charts milliampereChart;
	private Charts ohmChart;
	private Charts apertoChiusoChart;
	private File temperatureChartFile;
	private File humidityChartFile;
	private File voltChartFile;
	private File milliampereChartFile;
	private File ohmChartFile;
	private File apertoChiusoChartFile;

	private static Application app = Application.getInstance();

	private static GUI gui = Application.getInstance().getGui();
	private static final String TUTTI = "TUTTI";

	private String id_sonda;
	private String des_sonda;
	private Date dataDa;
	private Date dataA;
	private boolean onlyInAlarm;
	private boolean onlyMedie;
	private int minutes;

	private static final DateFormat filedateformat=new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
	private static final NumberFormat nfOffset = new DecimalFormat("#####.##");
	public static final NumberFormat nf = new DecimalFormat("####0.00");

	public ExporterLogger(String id_sonda, String des_sonda, Date dataDa, Date dataA, boolean onlyInAlarm, boolean onlyMedie, int hours){
		this.id_sonda=id_sonda;
		this.dataDa=dataDa;
		this.dataA=dataA;
		this.des_sonda=des_sonda;
		this.onlyInAlarm = onlyInAlarm;
		this.onlyMedie = onlyMedie;
		this.minutes = hours;
		createColumns();
	}

	public void createColumns() {
		boolean isTemperature=false;
		COLUMN_NAMES.clear();
		COLUMN_NAMES.add(COL_DATA);

		if (Application.getInstance().isTemperature()) {
			COLUMN_NAMES.add(COL_TEMP);
			POS_TEMPERATURA = COLUMN_NAMES.indexOf(COL_TEMP);
			COLUMN_NAMES.add(COL_OFFSET);
			POS_OFFSET = COLUMN_NAMES.indexOf(COL_OFFSET);
			isTemperature = true;
		}
		if (Application.getInstance().isTemperatureHumidity()) {
			if (!isTemperature) {
				COLUMN_NAMES.add(COL_TEMP);
				POS_TEMPERATURA = COLUMN_NAMES.indexOf(COL_TEMP);
				COLUMN_NAMES.add(COL_OFFSET);
				POS_OFFSET = COLUMN_NAMES.indexOf(COL_OFFSET);
			}
			COLUMN_NAMES.add(COL_UMI);
			POS_UMIDITA = COLUMN_NAMES.indexOf(COL_UMI);
			COLUMN_NAMES.add(COL_OFFSET_URT);
			POS_OFFSET_URT = COLUMN_NAMES.indexOf(COL_OFFSET_URT);
		}
		if (Application.getInstance().isVolt()) {
			COLUMN_NAMES.add(COL_VOLT);
			POS_VOLT = COLUMN_NAMES.indexOf(COL_VOLT);
			COLUMN_NAMES.add(COL_OFFSET_VOLT);
			POS_OFFSET_VOLT = COLUMN_NAMES.indexOf(COL_OFFSET_VOLT);
		}
		if (Application.getInstance().isMilliampere()) {
			COLUMN_NAMES.add(COL_MILLIAMPERE);
			POS_MILLIAMPERE = COLUMN_NAMES.indexOf(COL_MILLIAMPERE);
			COLUMN_NAMES.add(COL_OFFSET_MILLIAMPERE);
			POS_OFFSET_MILLIAMPERE = COLUMN_NAMES.indexOf(COL_OFFSET_MILLIAMPERE);
		}
		if (Application.getInstance().isOhm()) {
			COLUMN_NAMES.add(COL_OHM);
			POS_OHM = COLUMN_NAMES.indexOf(COL_OHM);
			COLUMN_NAMES.add(COL_OFFSET_OHM);
			POS_OFFSET_OHM = COLUMN_NAMES.indexOf(COL_OFFSET_OHM);
		}
		if (Application.getInstance().isApertoChiuso()) {
			COLUMN_NAMES.add(COL_APERTO_CHIUSO);
			POS_APERTO_CHIUSO = COLUMN_NAMES.indexOf(COL_APERTO_CHIUSO);
		}

	}

	public File export(File dir){
		return export(dir,"Logger__"+id_sonda+"__"+filedateformat.format(dataDa));

	}

	/*
	 * Time consuming Export
	 * 
	 * @return the file exported or null if error occurreds
	 */
	public File export(File dir, String name){

		String id_sensore = id_sonda;
		List<Sensore> sensori = new ArrayList<Sensore>();
		Sensore sensore=null;
		List<Sensore> loadSensori = Application.getInstance().getDao().loadSensori();
		List<Sensore> singleSensor =  new ArrayList<Sensore>();

		Workbook wb = new HSSFWorkbook(); // keep 100 rows in memory, exceeding rows will be flushed to disk
		CreationHelper createHelper = wb.getCreationHelper();


		if (id_sonda.equals(TUTTI)){

			for (Sensore se : loadSensori) {
				switch (se.getLocation()) {
				case ESTERNO:
					sensori.add(se);
					break;
				case INTERNO:
					sensori.add(se);
					break;
				default:
				}	 
			}

		}
		else {
			sensore = SensoreManager.getInstance().getSensoreById(id_sonda);
			sensori.add(sensore);
		}


		int x = 0;
		for(int ii=0; ii<sensori.size();ii++){

			Sensore s = sensori.get(ii);
			String id_sonda = s.getId_sonda();

			String descrizione = (!des_sonda.isEmpty()) ? des_sonda : s.getDescrizione();

			List<LetturaProva> findLettureProvaBySonda	=  Application.getInstance().getDao().findLettureProvaBySonda(id_sonda, dataDa, dataA, onlyInAlarm);
			StandardDeviation stdDeviation = new StandardDeviation(findLettureProvaBySonda);
			stdDeviation.calculate();	

			if (onlyMedie){
				findLettureProvaBySonda	= Application.getInstance().getDao().findMedieProvaBySonda(id_sonda, dataDa, dataA, onlyInAlarm, minutes);
			}

			LettureProva lettureProva = new LettureProva(null, findLettureProvaBySonda);

			try {
				singleSensor.clear();
				boolean isTemperature = false;
				Sensore mySensor = Sensore.newInstance(id_sonda, 0, null, null, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null, 0, 0, 0, 0, 0, null, 0, 0, 0, 0, 0, 0, null);

				if (Application.getInstance().isTemperature()) {
					temperatureChart = new Charts(TIPO_MISURE.TEMPERATURA);
					singleSensor.add(mySensor);
					temperatureChart.restore(lettureProva, singleSensor);
					temperatureChartFile = temperatureChart.saveAsPNG();
					isTemperature=true;
				}
				if (Application.getInstance().isTemperatureHumidity()) {
					if(!isTemperature) {
						temperatureChart = new Charts(TIPO_MISURE.TEMPERATURA);
						singleSensor.add(mySensor);
						temperatureChart.restore(lettureProva, singleSensor);
						temperatureChartFile = temperatureChart.saveAsPNG();
					}
					humidityChart = new Charts(TIPO_MISURE.TEMPERATURA_UMIDITA);
					singleSensor.add(mySensor);
					humidityChart.restore(lettureProva, singleSensor);
					humidityChartFile = humidityChart.saveAsPNG();
				}
				if (Application.getInstance().isVolt()) {
					voltChart = new Charts(TIPO_MISURE.VOLT);
					singleSensor.add(mySensor);
					voltChart.restore(lettureProva, singleSensor);
					voltChartFile = voltChart.saveAsPNG();
				}
				if (Application.getInstance().isMilliampere()) {
					milliampereChart = new Charts(TIPO_MISURE.MILLIAMPERE);
					singleSensor.add(mySensor);
					milliampereChart.restore(lettureProva, singleSensor);
					milliampereChartFile = milliampereChart.saveAsPNG();
				}
				if (Application.getInstance().isOhm()) {
					ohmChart = new Charts(TIPO_MISURE.OHM);
					singleSensor.add(mySensor);
					ohmChart.restore(lettureProva, singleSensor);
					ohmChartFile = ohmChart.saveAsPNG();
				}
				if (Application.getInstance().isApertoChiuso()) {
					apertoChiusoChart = new Charts(TIPO_MISURE.APERTO_CHIUSO);
					singleSensor.add(mySensor);
					apertoChiusoChart.restore(lettureProva, singleSensor);
					apertoChiusoChartFile = apertoChiusoChart.saveAsPNG();
				}


			} catch (Exception e) {

			}

			Sheet sh = wb.createSheet();
			wb.setSheetName(x, id_sonda);


			CellStyle cellStyle2 = wb.createCellStyle();
			HSSFPalette palette = ((HSSFWorkbook) wb).getCustomPalette();
			cellStyle2.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
			cellStyle2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			cellStyle2.setAlignment(HorizontalAlignment.CENTER);
			Font font = wb.createFont();
			font.setBold(true);
			cellStyle2.setFont(font);

			Row header = sh.createRow(0);


			Cell header_durata = header.createCell(0);
			header_durata.setCellValue("DATA DAL");
			header_durata.setCellStyle(cellStyle2);

			Cell header_datapro = header.createCell(1);
			header_datapro.setCellValue("AL");
			header_datapro.setCellStyle(cellStyle2);


			switch (app.getTipoMisure()) {
			case TEMPERATURA_UMIDITA:
			case MISTA:
				Cell header_filler = header.createCell(2);
				header_filler.setCellStyle(cellStyle2);
			}



			Row row = sh.createRow(1);
			CellStyle cellStyle = wb.createCellStyle();
			cellStyle.setDataFormat(
					createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm:ss"));
			cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
			cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			cellStyle.setAlignment(HorizontalAlignment.CENTER);
			font = wb.createFont();
			font.setBold(true);
			cellStyle.setFont(font);


			Cell cell1=null;
			Cell cell2=null;
			Cell cell3=null;
			Cell cell4=null;
			Cell cell5=null;
			Cell cell6=null;
			Cell cell7=null;

			cell3 = row.createCell(0);
			cell3.setCellValue(dataDa);
			cell3.setCellStyle(cellStyle);
			cell4 = row.createCell(1);
			cell4.setCellValue(dataA);
			cell4.setCellStyle(cellStyle);
			switch (app.getTipoMisure()) {
			case TEMPERATURA_UMIDITA:
			case MISTA:
				Cell header_filler = row.createCell(2);
				header_filler.setCellStyle(cellStyle);
			}

			sh.autoSizeColumn(1);
			sh.autoSizeColumn(2);
			sh.autoSizeColumn(3);
			sh.autoSizeColumn(4);
			sh.autoSizeColumn(5);

			CellStyle cellStyle4 = wb.createCellStyle();
			cellStyle4.setAlignment(HorizontalAlignment.CENTER);


			header = sh.createRow(3);
			Cell header_sonda = header.createCell(POS_DESCRIZIONE);
			header_sonda.setCellValue("SONDA:");
			styleHeader(wb, header_sonda);
			Cell header_nome = header.createCell(POS_VALORE);

			if(descrizione != null) {
				header_nome.setCellValue(id_sonda+ "-" + descrizione);
			}
			else {
				header_nome.setCellValue(id_sonda);
			}
			header_nome.setCellStyle(cellStyle4);


			header = sh.createRow(4);
			Cell header_media = header.createCell(POS_DESCRIZIONE);
			header_media.setCellValue("MEDIA:");
			styleHeader(wb, header_media);
			Cell header_media_value = header.createCell(POS_VALORE);
			header_media_value.setCellValue(nf.format(stdDeviation.getMediaTemp()));
			header_media_value.setCellStyle(cellStyle4);

			header = sh.createRow(5);
			Cell header_min_temp = header.createCell(POS_DESCRIZIONE);
			header_min_temp.setCellValue("MIN TEMP:");
			styleHeader(wb, header_min_temp);
			Cell header_temp_min_value = header.createCell(POS_VALORE);
			double minValue = stdDeviation.getMinValueTemp();
			header_temp_min_value.setCellValue(nf.format(minValue));
			header_temp_min_value.setCellStyle(cellStyle4);

			header = sh.createRow(6);
			Cell header_max_temp = header.createCell(POS_DESCRIZIONE);
			header_max_temp.setCellValue("MAX TEMP:");
			styleHeader(wb, header_max_temp);
			Cell header_temp_max_value = header.createCell(POS_VALORE);
			double maxValue = stdDeviation.getMaxValueTemp();
			header_temp_max_value.setCellValue(nf.format(maxValue));
			header_temp_max_value.setCellStyle(cellStyle4);

			header = sh.createRow(7);
			Cell header_std_deviation = header.createCell(POS_DESCRIZIONE);
			header_std_deviation.setCellValue("STD DEVIAZIONE:");
			styleHeader(wb, header_std_deviation);
			Cell header_std_deviation_value = header.createCell(POS_VALORE);
			double valueStdDeviation = stdDeviation.getStdDeviationTemp();
			header_std_deviation_value.setCellValue(nf.format(valueStdDeviation));
			header_std_deviation_value.setCellStyle(cellStyle4);

			switch (app.getTipoMisure()) {
			case TEMPERATURA_UMIDITA:
				header = sh.createRow(8);
				Cell header_media_urt = header.createCell(POS_DESCRIZIONE);
				header_media_urt.setCellValue("MEDIA URT:");
				styleHeader(wb, header_media_urt);
				Cell header_media_urt_value = header.createCell(POS_VALORE);
				header_media_urt_value.setCellValue(nf.format(stdDeviation.getMediaUrt()));
				header_media_urt_value.setCellStyle(cellStyle4);

				header = sh.createRow(9);
				Cell header_min_urt = header.createCell(POS_DESCRIZIONE);
				header_min_urt.setCellValue("MIN URT:");
				styleHeader(wb, header_min_urt);
				Cell header_temp_urt_value = header.createCell(POS_VALORE);
				double minUrtValue = stdDeviation.getMinValueUrt();
				header_temp_urt_value.setCellValue(nf.format(minUrtValue));
				header_temp_urt_value.setCellStyle(cellStyle4);

				header = sh.createRow(10);
				Cell header_max_urt = header.createCell(POS_DESCRIZIONE);
				header_max_urt.setCellValue("MAX URT:");
				styleHeader(wb, header_max_urt);
				Cell header_urt_max_value = header.createCell(POS_VALORE);
				double maxUrtValue = stdDeviation.getMaxValueUrt();
				header_urt_max_value.setCellValue(nf.format(maxUrtValue));
				header_urt_max_value.setCellStyle(cellStyle4);

				header = sh.createRow(11);
				Cell header_std_deviation_urt = header.createCell(POS_DESCRIZIONE);
				header_std_deviation_urt.setCellValue("STD DEVIAZIONE:");
				styleHeader(wb, header_std_deviation_urt);
				Cell header_std_deviation_urt_value = header.createCell(POS_VALORE);
				double valueStdDeviationUrt = stdDeviation.getStdDeviationUrt();
				header_std_deviation_urt_value.setCellValue(nf.format(valueStdDeviationUrt));
				header_std_deviation_urt_value.setCellStyle(cellStyle4);
			}


			CellStyle cellStyle3 = wb.createCellStyle();
			cellStyle3.setAlignment(HorizontalAlignment.CENTER);
			font = wb.createFont();
			font.setBold(true);
			cellStyle3.setFont(font);

			CellStyle cellStyle5 = wb.createCellStyle();
			cellStyle5.setAlignment(HorizontalAlignment.CENTER);
			cellStyle5.setDataFormat(
					createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm:ss"));

			switch (app.getTipoMisure()) {
			case TEMPERATURA:
				header = sh.createRow(OFFSET_HEADER);	
				OFFSET_LETTURE = OFFSET_LETTURE_TEMP;
				break;
			case TEMPERATURA_UMIDITA:
				header = sh.createRow(OFFSET_HEADER_URT);
				OFFSET_LETTURE = OFFSET_LETTURE_URT;
				break;
			}


			Cell header_data = header.createCell(POS_DATA);
			header_data.setCellValue("DATA");
			header_data.setCellStyle(cellStyle3);
			
			boolean isTemperatureHeader=false;
			
			if (Application.getInstance().isTemperature()) {
				Cell header_valore = header.createCell(POS_TEMPERATURA);
				header_valore.setCellValue("TEMPERATURA °C");
				header_valore.setCellStyle(cellStyle3);

				Cell header_offset = header.createCell(POS_OFFSET);
				header_offset.setCellValue("OFFSET");
				header_offset.setCellStyle(cellStyle3);
				isTemperatureHeader = true;
			}


			if (Application.getInstance().isTemperatureHumidity()) {
				if (!isTemperatureHeader) {
					Cell header_valore = header.createCell(POS_TEMPERATURA);
					header_valore.setCellValue("TEMPERATURA �C");
					header_valore.setCellStyle(cellStyle3);

					Cell header_offset = header.createCell(POS_OFFSET);
					header_offset.setCellValue("OFFSET");
					header_offset.setCellStyle(cellStyle3);
				}
				Cell header_valore_urt = header.createCell(POS_UMIDITA);
				header_valore_urt.setCellValue("UMIDITA RELATIVA %");
				header_valore_urt.setCellStyle(cellStyle3);

				Cell header_offset_urt = header.createCell(POS_OFFSET_URT);
				header_offset_urt.setCellValue("OFFSET UR%");
				header_offset_urt.setCellStyle(cellStyle3);
			}

			if (Application.getInstance().isVolt()) {
				Cell header_valore_urt = header.createCell(POS_VOLT);
				header_valore_urt.setCellValue("VOLT");
				header_valore_urt.setCellStyle(cellStyle3);

				Cell header_offset_urt = header.createCell(POS_OFFSET_VOLT);
				header_offset_urt.setCellValue("OFFSET VOLT");
				header_offset_urt.setCellStyle(cellStyle3);
			}

			if (Application.getInstance().isMilliampere()) {
				Cell header_valore_urt = header.createCell(POS_MILLIAMPERE);
				header_valore_urt.setCellValue("mA");
				header_valore_urt.setCellStyle(cellStyle3);

				Cell header_offset_urt = header.createCell(POS_OFFSET_MILLIAMPERE);
				header_offset_urt.setCellValue("OFFSET mA");
				header_offset_urt.setCellStyle(cellStyle3);
			}

			if (Application.getInstance().isOhm()) {
				Cell header_valore_urt = header.createCell(POS_OHM);
				header_valore_urt.setCellValue("Pt100");
				header_valore_urt.setCellStyle(cellStyle3);

				Cell header_offset_urt = header.createCell(POS_OFFSET_OHM);
				header_offset_urt.setCellValue("OFFSET Pt100");
				header_offset_urt.setCellStyle(cellStyle3);
			}

			if (Application.getInstance().isApertoChiuso()) {
				Cell header_valore_urt = header.createCell(POS_APERTO_CHIUSO);
				header_valore_urt.setCellValue("APERTO/CHIUSO");
				header_valore_urt.setCellStyle(cellStyle3);
			}


			Font font1 = wb.createFont();
			font1.setColor(HSSFColor.RED.index);
			CellStyle cellStyle6 = wb.createCellStyle();
			cellStyle6.setAlignment(HorizontalAlignment.CENTER);
			cellStyle6.setFont(font1);

			CellStyle cellStyle7 = wb.createCellStyle();
			cellStyle7.setAlignment(HorizontalAlignment.CENTER);
			cellStyle7.setDataFormat(
					createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm:ss"));
			cellStyle7.setFont(font1);

			if(findLettureProvaBySonda==null || findLettureProvaBySonda.isEmpty()){
				row = sh.createRow(OFFSET_LETTURE);
				cell1 = row.createCell(1);
				cell1.setCellValue("Nessuna Lettura Ricevuta");
			}else{

				int size = findLettureProvaBySonda.size();

				for(int i=0; i<size ; i++){
					LetturaProva p = findLettureProvaBySonda.get(i);
					row = sh.createRow(i+OFFSET_LETTURE);
					
					boolean isTemperature = false;
					
					cell1 = row.createCell(POS_DATA);
					cell1.setCellStyle(cellStyle5);
					cell1.setCellValue(p.getData());

					if (Application.getInstance().isTemperature()) {
						cell2 = row.createCell(POS_TEMPERATURA);
						cell2.setCellValue(nf.format(p.getValore()));
						cell2.setCellStyle(cellStyle4);

						double offset;
						if (p.getOffset() != null){
							offset = p.getOffset();
						} else {
							offset = p.getValore()-p.getValore_grezzo();
						}
						
						cell4 = row.createCell(POS_OFFSET);
						if (offset!=0.0) 
							cell4.setCellValue(nfOffset.format(offset));
						cell4.setCellStyle(cellStyle4);
						isTemperature=true;
					}

					if (Application.getInstance().isTemperatureHumidity()) {
						if (!isTemperature) {
							cell2 = row.createCell(POS_TEMPERATURA);
							cell2.setCellValue(nf.format(p.getValore()));
							cell2.setCellStyle(cellStyle4);

							double offset;
							if (p.getOffset() != null){
								offset = p.getOffset();
							} else {
								offset = p.getValore()-p.getValore_grezzo();
							}
							
							cell4 = row.createCell(POS_OFFSET);
							if (offset!=0.0) 
								cell4.setCellValue(nfOffset.format(offset));
							cell4.setCellStyle(cellStyle4);
						}
						double umid = p.getUmidita();
						cell3 = row.createCell(POS_UMIDITA);
						cell3.setCellValue(nf.format(umid));
						cell3.setCellStyle(cellStyle4);

						double offsetURT;
						if (p.getOffsetUrt() != null){
							offsetURT = p.getOffsetUrt();
						} else {
							offsetURT = p.getUmidita()-p.getUmidita_grezzo();
						}
						cell3 = row.createCell(POS_OFFSET_URT);
						if (offsetURT!=0.0) 
							cell3.setCellValue(nfOffset.format(offsetURT));
						cell3.setCellStyle(cellStyle4);
					}

					if (Application.getInstance().isVolt()) {
						double volt = p.getVolt();
						cell4 = row.createCell(POS_VOLT);
						cell4.setCellValue(nf.format(volt));
						cell4.setCellStyle(cellStyle4);

						double offsetVolt;
						if (p.getOffsetVolt() != null){
							offsetVolt = p.getOffsetVolt();
						} else {
							offsetVolt = p.getVolt()-p.getVolt_grezzo();
						}
						cell4 = row.createCell(POS_OFFSET_VOLT);
						if (offsetVolt!=0.0) 
							cell4.setCellValue(nfOffset.format(offsetVolt));
						cell4.setCellStyle(cellStyle4);
					}

					if (Application.getInstance().isMilliampere()) {
						double milliampere = p.getMilliampere();
						cell5 = row.createCell(POS_MILLIAMPERE);
						cell5.setCellValue(milliampere);
						cell5.setCellStyle(cellStyle4);

						double offsetMilliampere;
						if (p.getOffsetMilliampere() != null){
							offsetMilliampere = p.getOffsetMilliampere();
						} else {
							offsetMilliampere = (p.getMilliampere()-p.getMilliampere_grezzo());
						}
						cell5 = row.createCell(POS_OFFSET_MILLIAMPERE);
						if (offsetMilliampere!=0.0) 
							cell5.setCellValue(nfOffset.format(offsetMilliampere));
						cell5.setCellStyle(cellStyle4);

					}

					if (Application.getInstance().isOhm()) {
						double ohm = p.getOhm();
						cell6 = row.createCell(POS_OHM);
						cell6.setCellValue(nf.format(ohm));
						cell6.setCellStyle(cellStyle4);

						double offsetOhm;
						if (p.getOffsetOhm() != null){
							offsetOhm = p.getOffsetVolt();
						} else {
							offsetOhm = p.getOhm()-p.getOhm_grezzo();
						}
						cell6 = row.createCell(POS_OFFSET_OHM);
						if (offsetOhm!=0.0) 
							cell6.setCellValue(nfOffset.format(offsetOhm));
						cell6.setCellStyle(cellStyle4);

					}

					if (Application.getInstance().isApertoChiuso()) {
						String apertoChiuso = p.getApertoChiuso();
						cell7 = row.createCell(POS_APERTO_CHIUSO);
						cell7.setCellValue(apertoChiuso);
						cell7.setCellStyle(cellStyle4);
					}


					if (s.getRangeMin()!=0 || s.getRangeMax()!= 0 || s.getRangeMinURT()!=0 || s.getRangeMaxURT()!= 0) {

						if (Application.getInstance().isTemperature()) {
							if (p.getValore() != 0 && outOfLimit(p)) {
								cell1.setCellStyle(cellStyle7);
								cell2.setCellStyle(cellStyle6);
								cell3.setCellStyle(cellStyle6); 
								cell4.setCellStyle(cellStyle6);
							}
						}


						switch (app.getTipoMisure()) {
						case TEMPERATURA_UMIDITA:
							if (Application.getInstance().isTemperature()) {
								if (p.getUmidita() != 0 && outOfLimitURT(p)) {
									cell1.setCellStyle(cellStyle7);
									cell2.setCellStyle(cellStyle6);
									cell3.setCellStyle(cellStyle6); 
									cell4.setCellStyle(cellStyle6);

								}
							}

						}
					}

				}


			}

			int rows = sh.getPhysicalNumberOfRows()+OFFSET_CHART;

			if (temperatureChart!=null) {
				addCharts(wb, sh, temperatureChartFile, rows);
				rows = rows+30;
			}
			if (humidityChart!=null) {
				addCharts(wb, sh, humidityChartFile, rows);
				rows = rows+30;
			}
			if (voltChart!=null) {
				addCharts(wb, sh, voltChartFile, rows);
				rows = rows+30;
			}
			if (milliampereChart!=null) {
				addCharts(wb, sh, milliampereChartFile, rows);
				rows = rows+30;
			}
			if (ohmChart!=null) {
				addCharts(wb, sh, ohmChartFile, rows);
				rows = rows+30;
			}
			if (apertoChiusoChart!=null) {
				addCharts(wb, sh, apertoChiusoChartFile, rows);
				rows = rows+30;
			}


			x++; 

		}



		for(int sheetNum=0; sheetNum < wb.getNumberOfSheets(); sheetNum++) {
			Sheet sheet = wb.getSheetAt(sheetNum);
			for(short col=0; col<10; col++) sheet.autoSizeColumn(col); 
		}


		File outFile = new File(dir.getAbsolutePath()+"/"+name);

		FileOutputStream out;
		try {
			try{
				outFile.delete();
			}catch(Exception e){}
			outFile.createNewFile();
			out = new FileOutputStream(outFile);
			wb.write(out);
			out.close();
			return outFile;
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		}

	}



	private static void styleHeader(Workbook workbook, Cell cell){
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(true);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(font);
		style.setFillBackgroundColor(HSSFColor.AQUA.index);
		cell.setCellStyle(style);
	}


	private static boolean outOfLimit(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0){
			return letturaProva.getValore() < sensore.getRangeMin() || letturaProva.getValore() > sensore.getRangeMax();
		}
		return false;

	}

	private static boolean outOfLimitURT(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0){
			return letturaProva.getUmidita() < sensore.getRangeMinURT() || letturaProva.getUmidita() > sensore.getRangeMaxURT();
		}
		return false;

	}

	private void addCharts(Workbook wb, Sheet sh, File chartFile, int row) {
		
		try {
			final FileInputStream stream = new FileInputStream(chartFile);
			final CreationHelper helper = wb.getCreationHelper();
			final Drawing drawing = sh.createDrawingPatriarch();

			final ClientAnchor anchor = helper.createClientAnchor();
			anchor.setAnchorType( AnchorType.MOVE_AND_RESIZE );

			final int pictureIndex = wb.addPicture(IOUtils.toByteArray(stream), Workbook.PICTURE_TYPE_PNG);

			anchor.setCol1(1);
			anchor.setRow1(row);
			final Picture pict = drawing.createPicture(anchor, pictureIndex);
			pict.resize();
		} catch (Exception e) {
		}

	}


}
