package com.econorma.export;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import com.econorma.resources.Testo;
import com.econorma.util.Logger;

public class AutoExporterManager {
 
	private static final String TAG = "AutoExporterManager";
	private static final Logger LOGGER = Logger.getLogger(AutoExporterManager.class);
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private static final DateFormat filedateformat=new SimpleDateFormat("yyyy_MM_dd");
	private static final DateFormat filedateformatDetail=new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
	private Date dateFrom;
	private Date dateTo;
	private Date fixedTime;
	private File dir;
	private final String type = Testo.AUTO_DAILY_FILE_SEND;
	private final boolean sendTest= false;
	private boolean mailingAutoFileDaily; 
	private boolean autoFileDaily;
	private boolean autoFileDetailDaily;
	private boolean isDaylightSaving;

	public AutoExporterManager(boolean autoFileDaily, boolean autoFileDetailDaily){
		this.autoFileDaily=autoFileDaily;
		this.autoFileDetailDaily=autoFileDetailDaily;
		isDaylightSaving = TimeZone.getDefault().inDaylightTime( new Date() );
		initialize();
	}


	public void init(){

		

		try {

			Timer timer = new Timer();

			timer.schedule(new TimerTask() {

				@Override
				public void run() {
					calculate();

					if (autoFileDaily){ 
						ExporterLoggerAllPivot exporterLogger = new ExporterLoggerAllPivot(dateFrom, dateTo);
						String fileName = "Registrazioni_" + filedateformat.format(new Date()) + ".xls";
						exporterLogger.export(dir, fileName);
						LOGGER.info(TAG, "File automatico giornaliero generato: " + sdf.format(new Date()));
					 
					}

					if (autoFileDetailDaily){
						File parent = new File("").getAbsoluteFile();
						String fileName = "Logger__"+"TUTTI"+"__"+filedateformatDetail.format(dateFrom)+ ".xls";
						ExporterLogger exporterLogger = new ExporterLogger("TUTTI", "TUTTI", dateFrom, dateTo, false, false, 0);
						exporterLogger.export(parent, fileName);
						LOGGER.info(TAG, "File automatico giornaliero dettagliato generato: " + sdf.format(new Date()));
					 


					}
					
				}

				}, fixedTime, TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)); 


			} catch (Exception e) {

			}

		}

		public void initialize(){
			String path = System.getProperty("user.dir");
			dir = null;
			if (path!=null && path.trim().length() > 0 ){
				dir = new File(path);
			} else {
				dir = new File(".");
			}
			if (isDaylightSaving) {
				fixedTime = getDateFromHour(22, 59);	
			} else {
				fixedTime = getDateFromHour(23, 59);
			}
			
		}

		public void calculate(){
			dateFrom = getDateFromHour(01, 00);
			if (isDaylightSaving) {
				dateTo = getDateFromHour(22, 59);	
			} else {
				dateTo = getDateFromHour(23, 59);
			}
		}

		public Date getDateFromHour(int hours, int minutes){
			Calendar today = Calendar.getInstance();
			today.set(Calendar.HOUR_OF_DAY, hours);
			today.set(Calendar.MINUTE, minutes);
			today.set(Calendar.SECOND, 0);
			return today.getTime();
		}

	 


	}
