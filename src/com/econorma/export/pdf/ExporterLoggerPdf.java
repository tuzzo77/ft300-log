package com.econorma.export.pdf;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;

import com.econorma.Application;
import com.econorma.data.LetturaProva;
import com.econorma.data.Sensore;
import com.econorma.logic.LettureProva;
import com.econorma.logic.SensoreManager;
import com.econorma.logic.StandardDeviation;
import com.econorma.ui.EconormaChart;
import com.econorma.util.Logger;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

public class ExporterLoggerPdf extends PdfPageEventHelper {

	private static final String TAG = ExporterLoggerPdf.class.getCanonicalName();

	private static final Logger logger = Logger.getLogger(ExporterLoggerPdf.class);

	private static Application app = Application.getInstance();

	private static final String TUTTI = "TUTTI";
	
	private static final String COL_ID_SONDA = " Id Sonda ";
	private static final String COL_DESCRIZIONE = " Descrizione ";
	private static final String COL_DATA = " Data ";
	public static final String COL_TEMP = " C°   ";
	public static final String COL_UMI = " UR%   ";
	public static final String COL_OFFSET = " Offset ";
	public static final String COL_OFFSET_URT = " Offset UR% ";
	public static final String COL_OFFSET_VOLT = " Offset Volt ";
	public static final String COL_OFFSET_MILLIAMPERE = " Offset mA ";
	public static final String COL_OFFSET_OHM = " Offset Pt100 ";
	public static final String COL_VOLT = " Volt ";
	public static final String COL_MILLIAMPERE = " mA ";
	public static final String COL_OHM = " Pt100 ";
	public static final String COL_APERTO_CHIUSO = " Aperto/Chiuso ";
	
	
	public static final int POS_ID_SONDA = 0;
	public static final int POS_DESCRIZIONE = 1;
	public static final int POS_DATA = 2;
	public static int POS_TEMPERATURA;
	public static int POS_OFFSET;
	public static int POS_UMIDITA;
	public static int POS_OFFSET_URT;
	public static int POS_VOLT;
	public static int POS_OFFSET_VOLT;
	public static int POS_MILLIAMPERE;
	public static int POS_OFFSET_MILLIAMPERE;
	public static int POS_OHM;
	public static int POS_OFFSET_OHM;
	public static int POS_APERTO_CHIUSO;

	private static final List<String> COLUMN_NAMES = new ArrayList<String>();

	private String id_sonda;
	private String des_sonda;
	private Date dataDa;
	private Date dataA;
	private boolean onlyInAlarm;
	private boolean onlyMedie;
	private int minutes;
	private EconormaChart chart;
	private File chartFile;

	private PdfTemplate t;
	private Image total;

	private static final DateFormat filedateformat=new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
//	private static NumberFormat nf = new DecimalFormat("#####.##");
	public static NumberFormat nf = new DecimalFormat("####0.00");
	private static NumberFormat nfStd = new DecimalFormat("#####.##########");
	public static DateFormat df = new java.text.SimpleDateFormat("dd/MM/yy HH:mm:ss");


	public ExporterLoggerPdf(String id_sonda, String des_sonda, Date dataDa, Date dataA, boolean onlyInAlarm, boolean onlyMedie, int hours){
		this.id_sonda=id_sonda;
		this.dataDa=dataDa;
		this.dataA=dataA;
		this.des_sonda=des_sonda;
		this.onlyInAlarm = onlyInAlarm;
		this.onlyMedie = onlyMedie;
		this.minutes = hours;
		createColumns();
	}
	
	public void createColumns() {
		boolean isTemperature=false;
		COLUMN_NAMES.clear();
		COLUMN_NAMES.add(COL_ID_SONDA);
		COLUMN_NAMES.add(COL_DESCRIZIONE);
		COLUMN_NAMES.add(COL_DATA);

		if (Application.getInstance().isTemperature()) {
			COLUMN_NAMES.add(COL_TEMP);
			POS_TEMPERATURA = COLUMN_NAMES.indexOf(COL_TEMP);
//			COLUMN_NAMES.add(COL_OFFSET);
//			POS_OFFSET = COLUMN_NAMES.indexOf(COL_OFFSET);
			isTemperature = true;
		}
		if (Application.getInstance().isTemperatureHumidity()) {
			if (!isTemperature) {
				COLUMN_NAMES.add(COL_TEMP);
				POS_TEMPERATURA = COLUMN_NAMES.indexOf(COL_TEMP);
//				COLUMN_NAMES.add(COL_OFFSET);
//				POS_OFFSET = COLUMN_NAMES.indexOf(COL_OFFSET);
			}
			COLUMN_NAMES.add(COL_UMI);
			POS_UMIDITA = COLUMN_NAMES.indexOf(COL_UMI);
//			COLUMN_NAMES.add(COL_OFFSET_URT);
//			POS_OFFSET_URT = COLUMN_NAMES.indexOf(COL_OFFSET_URT);
		}
		if (Application.getInstance().isVolt()) {
			COLUMN_NAMES.add(COL_VOLT);
			POS_VOLT = COLUMN_NAMES.indexOf(COL_VOLT);
//			COLUMN_NAMES.add(COL_OFFSET_VOLT);
//			POS_OFFSET_VOLT = COLUMN_NAMES.indexOf(COL_OFFSET_VOLT);
		}
		if (Application.getInstance().isMilliampere()) {
			COLUMN_NAMES.add(COL_MILLIAMPERE);
			POS_MILLIAMPERE = COLUMN_NAMES.indexOf(COL_MILLIAMPERE);
//			COLUMN_NAMES.add(COL_OFFSET_MILLIAMPERE);
//			POS_OFFSET_MILLIAMPERE = COLUMN_NAMES.indexOf(COL_OFFSET_MILLIAMPERE);
		}
		if (Application.getInstance().isOhm()) {
			COLUMN_NAMES.add(COL_OHM);
			POS_OHM = COLUMN_NAMES.indexOf(COL_OHM);
//			COLUMN_NAMES.add(COL_OFFSET_OHM);
//			POS_OFFSET_OHM = COLUMN_NAMES.indexOf(COL_OFFSET_OHM);
		}
		if (Application.getInstance().isApertoChiuso()) {
			COLUMN_NAMES.add(COL_APERTO_CHIUSO);
			POS_APERTO_CHIUSO = COLUMN_NAMES.indexOf(COL_APERTO_CHIUSO);
		}

	}


	public void export(File dir){
		export(dir,"Logger__"+id_sonda+"__"+filedateformat.format(dataDa));
	}


	/*
	 * Time consuming Export
	 * 
	 * @return the file exported or null if error occurreds
	 */
	public void export(File dir, String name){

		String id_sensore = id_sonda;
		List<Sensore> sensori = new ArrayList<Sensore>();
		Sensore sensore=null;
		List<Sensore> loadSensori = Application.getInstance().getDao().loadSensori();
		List<Sensore> singleSensor =  new ArrayList<Sensore>();

		if (id_sonda.equals(TUTTI)){

			for (Sensore se : loadSensori) {
				switch (se.getLocation()) {
				case ESTERNO:
					sensori.add(se);
					break;
				case INTERNO:
					sensori.add(se);
					break;
				default:
				}	 
			}

		}
		else {
			sensore = SensoreManager.getInstance().getSensoreById(id_sonda);
			sensori.add(sensore);
		}


		int x = 0;
		for(int ii=0; ii<sensori.size();ii++){

			Sensore s = sensori.get(ii);
			String id_sonda = s.getId_sonda();

			String fileName = "Logger__"+id_sonda+"__"+filedateformat.format(dataDa) + ".pdf";
			Document document = null;
			PdfWriter pdfWriter = null;

			try {
				document = new Document();
				document.setPageSize(PageSize.A4.rotate());
				pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(dir.getAbsolutePath()+"/"+fileName));
				HeaderFooterPageEvent event = new HeaderFooterPageEvent();
				pdfWriter.setPageEvent(event);
				document.open();
			} catch (Exception e) {
			}


			String descrizione = (!des_sonda.isEmpty()) ? des_sonda : s.getDescrizione();

			try {
				document.add(new Paragraph("Data Dal: " +  df.format(dataDa)));
				document.add(new Paragraph("Data Al: " + df.format(dataA)));
				document.add(new Paragraph("Sonda: " + id_sonda + " " + descrizione));
			} catch (Exception e) {
			}

			List<LetturaProva> findLettureProvaBySonda	=  Application.getInstance().getDao().findLettureProvaBySonda(id_sonda, dataDa, dataA, onlyInAlarm);
			StandardDeviation stdDeviation = new StandardDeviation(findLettureProvaBySonda);
			stdDeviation.calculate();	

			double media =  Math.round(stdDeviation.getMediaTemp()*100.0)/100.0;
			double minTemp =  Math.round(stdDeviation.getMinValueTemp()*100.0)/100.0;
			double maxTemp =  Math.round(stdDeviation.getMaxValueTemp()*100.0)/100.0;
			double mediaUrt =  Math.round(stdDeviation.getMediaUrt()*100.0)/100.0;
			double minUrt =  Math.round(stdDeviation.getMinValueUrt()*100.0)/100.0;
			double maxUrt =  Math.round(stdDeviation.getMaxValueUrt()*100.0)/100.0;


			if (onlyMedie){
				findLettureProvaBySonda	= Application.getInstance().getDao().findMedieProvaBySonda(id_sonda, dataDa, dataA, onlyInAlarm, minutes);
			}

			LettureProva lettureProva = new LettureProva(null, findLettureProvaBySonda);

			try {
				singleSensor.clear();
				chart = new EconormaChart();
				Sensore mySensor = Sensore.newInstance(id_sonda, 0, null, null, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null, 0, 0, 0, 0, 0, null, 0, 0, 0, 0, 0, 0, null);
				singleSensor.add(mySensor);
				chart.restore(lettureProva, singleSensor);
				chartFile = File.createTempFile("econorma_reports_", UUID.randomUUID().toString());
				chartFile.deleteOnExit();  
				saveAsPNG(chart.getJFreeChart(), chartFile);
			} catch (Exception e) {

			}


			try {
				document.add(new Paragraph("Media: " + nf.format(media)));
				document.add(new Paragraph("Min Temp: " +  nf.format(minTemp)));
				document.add(new Paragraph("Max Temp: " + nf.format(maxTemp)));
				document.add(new Paragraph("Std Deviazione: " + nf.format(stdDeviation.getStdDeviationTemp())));
				document.add(new Paragraph("Media Urt: " + nf.format(mediaUrt)));
				document.add(new Paragraph("Min Urt: " + nf.format(minUrt)));
				document.add(new Paragraph("Max Urt: " + nf.format(maxUrt)));
				document.add(new Paragraph("Std Deviazione: " + nf.format(stdDeviation.getStdDeviationUrt())));
				document.add(new Paragraph("\n\n"));
			} catch (Exception e) {
			}

			PdfPTable table=null;
 			table = new PdfPTable(COLUMN_NAMES.size());


			Font fontRed = new Font();
			fontRed.setColor(Color.RED);

			PdfPCell c1 = new PdfPCell(new Phrase(COL_ID_SONDA));
			c1.setFixedHeight(30f);
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(COL_DESCRIZIONE));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(COL_DATA));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);
			
			boolean isTemperature = false;
			if (Application.getInstance().isTemperature()) {
				c1 = new PdfPCell(new Phrase(COL_TEMP));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c1);
				isTemperature = true;
			}
			
			if (Application.getInstance().isTemperatureHumidity()) {
				if (!isTemperature) {
					c1 = new PdfPCell(new Phrase(COL_TEMP));
					c1.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(c1); 
				}
				c1 = new PdfPCell(new Phrase(COL_UMI));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c1);
			}
			
			if (Application.getInstance().isVolt()) {
				c1 = new PdfPCell(new Phrase(COL_VOLT));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c1); 
			}
			
			if (Application.getInstance().isMilliampere()) {
				c1 = new PdfPCell(new Phrase(COL_MILLIAMPERE));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c1); 
			}
			
			if (Application.getInstance().isOhm()) {
				c1 = new PdfPCell(new Phrase(COL_OHM));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c1); 
			}
			
			if (Application.getInstance().isApertoChiuso()) {
				c1 = new PdfPCell(new Phrase(COL_APERTO_CHIUSO));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c1);  
			}


			table.setHeaderRows(1);

			if(findLettureProvaBySonda==null || findLettureProvaBySonda.isEmpty()){
				PdfPCell c2 = new PdfPCell(new Phrase("Nessuna Lettura Ricevuta"));
				table.addCell(c2);
			}else{

				int size = findLettureProvaBySonda.size();

				for(int i=0; i<size ; i++){
					LetturaProva p = findLettureProvaBySonda.get(i);

					PdfPCell c2 = new PdfPCell(new Phrase(id_sonda));
					if (outOfLimit(p) || outOfLimitURT(p) || outOfLimitOhm(p)){
						c2 = new PdfPCell(new Phrase(id_sonda, fontRed));
					} 
					table.addCell(c2);

					PdfPCell c3 = new PdfPCell(new Phrase(descrizione));
					if (outOfLimit(p) || outOfLimitURT(p) || outOfLimitOhm(p)){
						c3 = new PdfPCell(new Phrase(descrizione, fontRed));
					}
					table.addCell(c3);

					PdfPCell c4 = new PdfPCell(new Phrase(df.format(p.getData())));
					if (outOfLimit(p) || outOfLimitURT(p) || outOfLimitOhm(p)){
						c4 = new PdfPCell(new Phrase(df.format(p.getData()), fontRed));
					}
					table.addCell(c4);
					
					
					isTemperature = false;
					if (Application.getInstance().isTemperature()) {
						PdfPCell c5 = new PdfPCell(new Phrase(nf.format(p.getValore())));
						if (outOfLimit(p) || outOfLimitURT(p) || outOfLimitOhm(p)){
							c5 = new PdfPCell(new Phrase(nf.format(p.getValore()), fontRed));
						} 
						c5.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(c5);
						isTemperature = true;
					}
					
					if (Application.getInstance().isTemperatureHumidity()) {
						if (!isTemperature) {
							PdfPCell c5 = new PdfPCell(new Phrase(nf.format(p.getValore())));
							if (outOfLimit(p) || outOfLimitURT(p) || outOfLimitOhm(p)){
								c5 = new PdfPCell(new Phrase(nf.format(p.getValore()), fontRed));
							} 
							c5.setHorizontalAlignment(Element.ALIGN_CENTER);
							table.addCell(c5);
						}
						PdfPCell c6 = new PdfPCell(new Phrase(nf.format(p.getUmidita())));
						if (outOfLimit(p) || outOfLimitURT(p) || outOfLimitOhm(p)){
							c6 = new PdfPCell(new Phrase(nf.format(p.getUmidita()), fontRed));
						} 
						c6.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(c6);
						 
					}
					
					if (Application.getInstance().isVolt()) {
						PdfPCell c7 = new PdfPCell(new Phrase(nf.format(p.getVolt())));
						c7.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(c7);
					}
					
					if (Application.getInstance().isMilliampere()) {
						PdfPCell c8 = new PdfPCell(new Phrase(nf.format(p.getMilliampere())));
						c8.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(c8); 
					}
					
					if (Application.getInstance().isOhm()) {
						PdfPCell c9 = new PdfPCell(new Phrase(nf.format(p.getOhm())));
						if (outOfLimit(p) || outOfLimitURT(p) || outOfLimitOhm(p)){
							c9 = new PdfPCell(new Phrase(nf.format(p.getOhm()), fontRed));
						} 
						c9.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(c9);
					}
					
					if (Application.getInstance().isApertoChiuso()) {
						PdfPCell cA = new PdfPCell(new Phrase(nf.format(p.getApertoChiuso())));
						cA.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(cA);  
					}
 
				}

			}


			List<Float> widths = new ArrayList<Float>();
			widths.add(15f);
			widths.add(30f);
			widths.add(20f);
			
			isTemperature = false;
			if (Application.getInstance().isTemperature()) {
				widths.add(18f);
				isTemperature = true;
			}
			
			if (Application.getInstance().isTemperatureHumidity()) {
				if (!isTemperature) {
					widths.add(18f);
				}
				widths.add(15f);
			}
			
			if (Application.getInstance().isVolt()) {
				widths.add(15f);
			}
			
			if (Application.getInstance().isMilliampere()) {
				widths.add(15f);
			}
			
			if (Application.getInstance().isOhm()) {
				widths.add(18f);
			}
			
			if (Application.getInstance().isApertoChiuso()) {
				widths.add(15f);
			}	

			try {
				float[] columnWidths = new float[widths.size()];
				for(int i = 0; i < widths.size(); i++) {
					columnWidths[i] = widths.get(i);
				}
				table.setWidths(columnWidths);
				document.add(table);
				document.newPage();
				Image img = Image.getInstance(chartFile.getAbsolutePath());
				document.add(img);

			} catch (Exception e) {
				logger.error(TAG, e);
			}



			try {
				document.close();
				pdfWriter.close();
			} catch (Exception e) {
				logger.error(TAG, e);
			}

			x++; 

		}


	}

	private static boolean outOfLimit(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0){
			return letturaProva.getValore() < sensore.getRangeMin() || letturaProva.getValore() > sensore.getRangeMax();
		}
		return false;

	}

	private static boolean outOfLimitURT(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0){
			return letturaProva.getUmidita() < sensore.getRangeMinURT() || letturaProva.getUmidita() > sensore.getRangeMaxURT();
		}
		return false;

	}
	
	private static boolean outOfLimitOhm(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMinOhm()!=0 || sensore.getRangeMaxOhm()!= 0){
			return letturaProva.getOhm() < sensore.getRangeMinOhm() || letturaProva.getOhm() > sensore.getRangeMaxOhm();
		}
		return false;

	}

	private void saveAsPNG(JFreeChart chart, File file){
		try {
			ChartUtilities.saveChartAsPNG(file, chart, 780, 470);
			logger.info(TAG, "Grafico salvato in "+file.toString());
		} catch (IOException e) {
			logger.error(TAG, "impossibile salvare il chart in "+file.toString(),e);
		}
	}


}
