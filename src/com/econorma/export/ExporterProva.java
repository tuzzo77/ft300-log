package com.econorma.export;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.ClientAnchor.AnchorType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;

import com.econorma.Application;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.gui.GUI;
import com.econorma.logic.LettureProva;
import com.econorma.logic.SensoreManager;
import com.econorma.ui.EconormaChart;
import com.econorma.util.Logger;

public class ExporterProva {

	private static final String TAG = ExporterProva.class.getCanonicalName();

	private static final Logger logger = Logger.getLogger(ExporterProva.class);

	private final Prova prova;

	private static final String COL_DATA = " Data ";
	public static final String COL_TEMP = " C�   ";
	public static final String COL_UMI = " UR%   ";
	public static final String COL_OFFSET = " Offset ";
	public static final String COL_OFFSET_URT = " Offset UR% ";
	public static final String COL_OFFSET_VOLT = " Offset Volt ";
	public static final String COL_OFFSET_MILLIAMPERE = " Offset mA ";
	public static final String COL_OFFSET_OHM = " Offset Ohm ";
	public static final String COL_VOLT = " Volt ";
	public static final String COL_MILLIAMPERE = " mA ";
	public static final String COL_OHM = " Ohm ";
	public static final String COL_APERTO_CHIUSO = " Aperto/Chiuso ";


	public static final int POS_DESCRIZIONE = 0;
	public static final int POS_VALORE = 1;

	public static final int POS_DATA = 0;
	public static int POS_TEMPERATURA;
	public static int POS_OFFSET;
	public static int POS_UMIDITA;
	public static int POS_OFFSET_URT;
	public static int POS_VOLT;
	public static int POS_OFFSET_VOLT;
	public static int POS_MILLIAMPERE;
	public static int POS_OFFSET_MILLIAMPERE;
	public static int POS_OHM;
	public static int POS_OFFSET_OHM;
	public static int POS_APERTO_CHIUSO;

	private static final List<String> COLUMN_NAMES = new ArrayList<String>();

	private static final int OFFSET_LETTURE = 6;
	private File chartFile;

	private static Application app = Application.getInstance();

	private static GUI gui = Application.getInstance().getGui();

	private static final DateFormat filedateformat=new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
	public static final NumberFormat nf = new DecimalFormat("#####.##");
	public static final NumberFormat nfATP = new DecimalFormat("####0.00");

	public ExporterProva(Prova prova){
		this.prova = prova;
		createColumns();
	}



	public File export(File dir){

		Date dataProva=null;
		File exporter=null;

		dataProva = prova.getData_scarico();

		exporter = exporter(dir,"Prova__"+prova.getId()+"__"+filedateformat.format(dataProva));

		return exporter;

	}

	public void createColumns() {
		boolean isTemperature=false;
		COLUMN_NAMES.clear();
		COLUMN_NAMES.add(COL_DATA);

		if (Application.getInstance().isTemperature()) {
			COLUMN_NAMES.add(COL_TEMP);
			POS_TEMPERATURA = COLUMN_NAMES.indexOf(COL_TEMP);
			COLUMN_NAMES.add(COL_OFFSET);
			POS_OFFSET = COLUMN_NAMES.indexOf(COL_OFFSET);
			isTemperature = true;
		}
		if (Application.getInstance().isTemperatureHumidity()) {
			if (!isTemperature) {
				COLUMN_NAMES.add(COL_TEMP);
				POS_TEMPERATURA = COLUMN_NAMES.indexOf(COL_TEMP);
				COLUMN_NAMES.add(COL_OFFSET);
				POS_OFFSET = COLUMN_NAMES.indexOf(COL_OFFSET);
			}
			COLUMN_NAMES.add(COL_UMI);
			POS_UMIDITA = COLUMN_NAMES.indexOf(COL_UMI);
			COLUMN_NAMES.add(COL_OFFSET_URT);
			POS_OFFSET_URT = COLUMN_NAMES.indexOf(COL_OFFSET_URT);
		}
		if (Application.getInstance().isVolt()) {
			COLUMN_NAMES.add(COL_VOLT);
			POS_VOLT = COLUMN_NAMES.indexOf(COL_VOLT);
			COLUMN_NAMES.add(COL_OFFSET_VOLT);
			POS_OFFSET_VOLT = COLUMN_NAMES.indexOf(COL_OFFSET_VOLT);
		}
		if (Application.getInstance().isMilliampere()) {
			COLUMN_NAMES.add(COL_MILLIAMPERE);
			POS_MILLIAMPERE = COLUMN_NAMES.indexOf(COL_MILLIAMPERE);
			COLUMN_NAMES.add(COL_OFFSET_MILLIAMPERE);
			POS_OFFSET_MILLIAMPERE = COLUMN_NAMES.indexOf(COL_OFFSET_MILLIAMPERE);
		}
		if (Application.getInstance().isOhm()) {
			COLUMN_NAMES.add(COL_OHM);
			POS_OHM = COLUMN_NAMES.indexOf(COL_OHM);
			COLUMN_NAMES.add(COL_OFFSET_OHM);
			POS_OFFSET_OHM = COLUMN_NAMES.indexOf(COL_OFFSET_OHM);
		}
		if (Application.getInstance().isApertoChiuso()) {
			COLUMN_NAMES.add(COL_APERTO_CHIUSO);
			POS_APERTO_CHIUSO = COLUMN_NAMES.indexOf(COL_APERTO_CHIUSO);
		}

	}

	/*
	 * Time consuming Export
	 * 
	 * @return the file exported or null if error occurreds
	 */
	public File exporter(File dir, String name){

		List<Sensore> sensori = new ArrayList<Sensore>();
		Sensore mySensor = Sensore.newInstance(prova.getId_sensore(), 0, null, null, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null, 0, 0, 0, 0, 0, null, 0, 0, 0, 0, 0, 0, null);
		sensori.add(mySensor);

		Workbook wb = new HSSFWorkbook(); 
		CreationHelper createHelper = wb.getCreationHelper();

		String responsabile = prova.getResponsabile();
		Date dataProva = prova.getData_scarico();
		Date dataInizioProva = prova.getData_inizio();
		Date dataFineProva = prova.getData_fine();
		String durataProva = SensoreManager.getInstance().provaDuration(prova.getData_inizio(), prova.getData_fine());

		LettureProva lettureProva = new LettureProva.LettureProvaCreator().loadOnlyMedie(prova);

		EconormaChart econormaChart = new EconormaChart(lettureProva, sensori, prova);
		chartFile = econormaChart.saveAsPNG();


		int x = 0;
		for(int ii=0; ii<sensori.size();ii++){


			Sensore s = sensori.get(ii);
			String id_sonda = s.getId_sonda();
			List<LetturaProva> findByProvaAndSonda = Application.getInstance().getDao().findLettureProvaByProvaAndSonda(prova, id_sonda);

			Sheet sh = wb.createSheet();
			//		    wb.setSheetName(ii, id_sonda);
			wb.setSheetName(x, id_sonda);


			CellStyle cellStyle2 = wb.createCellStyle();
			HSSFPalette palette = ((HSSFWorkbook) wb).getCustomPalette();
			cellStyle2.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
			cellStyle2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			Font font = wb.createFont();
			font.setBold(true);
			cellStyle2.setFont(font);

			Row header = sh.createRow(0);

			Cell header_resp = header.createCell(0);
			header_resp.setCellValue(Application.getInstance().getBundle().getString("exporterProva.responsabile"));
			header_resp.setCellStyle(cellStyle2);

			Cell header_dataInizioPro = header.createCell(1);
			header_dataInizioPro.setCellValue(Application.getInstance().getBundle().getString("exporterProva.dataInizioProva"));
			header_dataInizioPro.setCellStyle(cellStyle2);


			Cell header_dataFinePro = header.createCell(2);
			header_dataFinePro.setCellValue(Application.getInstance().getBundle().getString("exporterProva.dataFineProva"));
			header_dataFinePro.setCellStyle(cellStyle2);


			Cell header_durata = header.createCell(3);
			header_durata.setCellValue(Application.getInstance().getBundle().getString("exporterProva.durataProva"));
			header_durata.setCellStyle(cellStyle2);


			Row row = sh.createRow(1);
			CellStyle cellStyle = wb.createCellStyle();
			cellStyle.setDataFormat(
					createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm:ss"));
			cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
			cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			font = wb.createFont();
			font.setBold(true);
			cellStyle.setFont(font);


			Cell cell1=null;
			Cell cell2=null;
			Cell cell3=null;
			Cell cell4=null;
			Cell cell5=null;
			Cell cell6=null;

			cell1 = row.createCell(0);
			cell1.setCellValue(responsabile);
			cell1.setCellStyle(cellStyle2);

			cell2 = row.createCell(1);
			cell2.setCellValue(dataInizioProva);
			cell2.setCellStyle(cellStyle);

			cell3 = row.createCell(2);
			cell3.setCellValue(dataFineProva);
			cell3.setCellStyle(cellStyle);

			cell4 = row.createCell(3);
			cell4.setCellValue(durataProva);
			cell4.setCellStyle(cellStyle2);

			sh.autoSizeColumn(1);
			sh.autoSizeColumn(2);
			sh.autoSizeColumn(3);
			sh.autoSizeColumn(4);
			sh.autoSizeColumn(5);



			header = sh.createRow(3);
			Cell header_sonda = header.createCell(POS_DESCRIZIONE);
			header_sonda.setCellValue(Application.getInstance().getBundle().getString("exporterProva.sonda"));
			styleHeader(wb, header_sonda);
			Cell header_nome = header.createCell(POS_VALORE);

			if(s.getDescrizione() != null) {
				header_nome.setCellValue(id_sonda+ "-" + s.getDescrizione());
			}
			else {
				header_nome.setCellValue(id_sonda);
			}


			CellStyle cellStyle3 = wb.createCellStyle();
			cellStyle3.setAlignment(HorizontalAlignment.CENTER);
			font = wb.createFont();
			font.setBold(true);
			cellStyle3.setFont(font);

			CellStyle cellStyle4 = wb.createCellStyle();
			cellStyle4.setAlignment(HorizontalAlignment.CENTER);

			header = sh.createRow(5);
			
			boolean isTemperatureHeader=false;
			
			if (Application.getInstance().isTemperature()) {
				Cell header_data = header.createCell(POS_DATA);
				header_data.setCellValue(Application.getInstance().getBundle().getString("exporterProva.data"));
				header_data.setCellStyle(cellStyle3);
				Cell header_valore = header.createCell(POS_TEMPERATURA);
				header_valore.setCellValue(Application.getInstance().getBundle().getString("exporterProva.temperatura"));
				Cell header_offset = header.createCell(POS_OFFSET);
				header_offset.setCellValue(Application.getInstance().getBundle().getString("exporterProva.offset"));
				header_offset.setCellStyle(cellStyle3);
				header_valore.setCellStyle(cellStyle3);
				isTemperatureHeader=true;
			}

			if (Application.getInstance().isTemperatureHumidity()) {
				if (!isTemperatureHeader) {
					Cell header_data = header.createCell(POS_DATA);
					header_data.setCellValue(Application.getInstance().getBundle().getString("exporterProva.data"));
					header_data.setCellStyle(cellStyle3);
					Cell header_valore = header.createCell(POS_TEMPERATURA);
					header_valore.setCellValue(Application.getInstance().getBundle().getString("exporterProva.temperatura"));
					Cell header_offset = header.createCell(POS_OFFSET);
					header_offset.setCellValue(Application.getInstance().getBundle().getString("exporterProva.offset"));
					header_offset.setCellStyle(cellStyle3);
					header_valore.setCellStyle(cellStyle3);
				}
				Cell header_valore_urt = header.createCell(POS_UMIDITA);
				header_valore_urt.setCellValue(Application.getInstance().getBundle().getString("exporterProva.umidita"));
				header_valore_urt.setCellStyle(cellStyle3);
				Cell header_offset_urt = header.createCell(POS_OFFSET_URT);
				header_offset_urt.setCellValue(Application.getInstance().getBundle().getString("exporterProva.offsetUr"));
				header_offset_urt.setCellStyle(cellStyle3);
			}

			if (Application.getInstance().isVolt()) {
				Cell header_valore_volt = header.createCell(POS_VOLT);
				header_valore_volt.setCellValue(Application.getInstance().getBundle().getString("exporterProva.volt"));
				header_valore_volt.setCellStyle(cellStyle3);
				Cell header_offset_volt = header.createCell(POS_OFFSET_VOLT);
				header_offset_volt.setCellValue(Application.getInstance().getBundle().getString("exporterProva.offsetVolt"));
				header_offset_volt.setCellStyle(cellStyle3);
			}


			if (Application.getInstance().isMilliampere()) {
				Cell header_valore_milliampere = header.createCell(POS_MILLIAMPERE);
				header_valore_milliampere.setCellValue(Application.getInstance().getBundle().getString("exporterProva.milliampere"));
				header_valore_milliampere.setCellStyle(cellStyle3);
				Cell header_offset_milliampere = header.createCell(POS_OFFSET_MILLIAMPERE);
				header_offset_milliampere.setCellValue(Application.getInstance().getBundle().getString("exporterProva.offsetMilliampere"));
				header_offset_milliampere.setCellStyle(cellStyle3);
			}


			if (Application.getInstance().isOhm()) {
				Cell header_valore_ohm = header.createCell(POS_OHM);
				header_valore_ohm.setCellValue(Application.getInstance().getBundle().getString("exporterProva.ohm"));
				header_valore_ohm.setCellStyle(cellStyle3);
				Cell header_offset_ohm = header.createCell(POS_OFFSET_OHM);
				header_offset_ohm.setCellValue(Application.getInstance().getBundle().getString("exporterProva.offsetOhm"));
				header_offset_ohm.setCellStyle(cellStyle3);
			}


			if (Application.getInstance().isApertoChiuso()) {
				Cell header_valore_apertoChiuso = header.createCell(POS_APERTO_CHIUSO);
				header_valore_apertoChiuso.setCellValue(Application.getInstance().getBundle().getString("exporterProva.apertoChiuso"));
				header_valore_apertoChiuso.setCellStyle(cellStyle3);
			}


			if(findByProvaAndSonda==null || findByProvaAndSonda.isEmpty()){
				row = sh.createRow(OFFSET_LETTURE);
				cell1 = row.createCell(1);
				cell1.setCellValue(Application.getInstance().getBundle().getString("exporterProva.nessunaLettura"));
			}else{

				int size = findByProvaAndSonda.size();

				cellStyle = wb.createCellStyle();
				cellStyle.setDataFormat(
						createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm:ss"));
				cellStyle.setAlignment(HorizontalAlignment.CENTER);


				Font font1 = wb.createFont();
				font1.setColor(HSSFColor.RED.index);
				CellStyle cellStyle6 = wb.createCellStyle();
				cellStyle6.setAlignment(HorizontalAlignment.CENTER);
				cellStyle6.setFont(font1);

				CellStyle cellStyle7 = wb.createCellStyle();
				cellStyle7.setAlignment(HorizontalAlignment.CENTER);
				cellStyle7.setDataFormat(
						createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm:ss"));
				cellStyle7.setFont(font1);


				for(int i=0; i<size ; i++){
					LetturaProva p = findByProvaAndSonda.get(i);
					row = sh.createRow(i+OFFSET_LETTURE);
					
					boolean isTemperature=false;

					cell1 = row.createCell(POS_DATA);
					cell1.setCellStyle(cellStyle);
					cell1.setCellValue(p.getData());

					if (Application.getInstance().isTemperature()) {
						cell2 = row.createCell(POS_TEMPERATURA);
						cell2.setCellValue(nf.format(p.getValore()));
						cellStyle4.setDataFormat(wb.getCreationHelper().createDataFormat().getFormat("####0.00"));
						cell2.setCellStyle(cellStyle4);

						double offsetTemp;
						if (p.getOffset() != null){
							offsetTemp = p.getOffset();
						} else {
							offsetTemp =  p.getValore()-p.getValore_grezzo();
						}

						cell4 = row.createCell(POS_OFFSET);
						if (offsetTemp!=0.0) 
							cell4.setCellValue(nf.format(offsetTemp));
						cell4.setCellStyle(cellStyle4);
						isTemperature=true;
					}


					if (Application.getInstance().isTemperatureHumidity()) {
						
						if (!isTemperature) {
							cell2 = row.createCell(POS_TEMPERATURA);
							cell2.setCellValue(nf.format(p.getValore()));
							cellStyle4.setDataFormat(wb.getCreationHelper().createDataFormat().getFormat("####0.00"));
							cell2.setCellStyle(cellStyle4);

							double offsetTemp;
							if (p.getOffset() != null){
								offsetTemp = p.getOffset();
							} else {
								offsetTemp =  p.getValore()-p.getValore_grezzo();
							}

							cell4 = row.createCell(POS_OFFSET);
							if (offsetTemp!=0.0) 
								cell4.setCellValue(nf.format(offsetTemp));
							cell4.setCellStyle(cellStyle4);
						}
						cell3 = row.createCell(POS_UMIDITA);
						cell3.setCellValue(nf.format(p.getUmidita()));
						cell3.setCellStyle(cellStyle4);

						double offsetURT;
						if (p.getOffsetUrt() != null){
							offsetURT = p.getOffsetUrt();
						} else {
							offsetURT = (p.getUmidita()-p.getUmidita_grezzo());
						}

						cell3 = row.createCell(POS_OFFSET_URT);
						if (offsetURT!=0.0) 
							cell3.setCellValue(nf.format(offsetURT));
						cell3.setCellStyle(cellStyle4);
					}
					
					if (Application.getInstance().isVolt()) {
						cell3 = row.createCell(POS_VOLT);
						cell3.setCellValue(nf.format(p.getVolt()));
						cell3.setCellStyle(cellStyle4);

						double offsetVolt;
						if (p.getOffsetVolt() != null){
							offsetVolt = p.getOffsetVolt();
						} else {
							offsetVolt = (p.getVolt()-p.getValore_grezzo());
						}

						cell3 = row.createCell(POS_OFFSET_VOLT);
						if (offsetVolt!=0.0) 
							cell3.setCellValue(nf.format(offsetVolt));
						cell3.setCellStyle(cellStyle4);
					}
					
					if (Application.getInstance().isMilliampere()) {
						cell3 = row.createCell(POS_MILLIAMPERE);
						cell3.setCellValue(nf.format(p.getMilliampere()));
						cell3.setCellStyle(cellStyle4);

						double offsetMilliampere;
						if (p.getOffsetMilliampere() != null){
							offsetMilliampere = p.getOffsetMilliampere();
						} else {
							offsetMilliampere = (p.getMilliampere()-p.getMilliampere_grezzo());
						}

						cell3 = row.createCell(POS_OFFSET_MILLIAMPERE);
						if (offsetMilliampere!=0.0) 
							cell3.setCellValue(nf.format(offsetMilliampere));
						cell3.setCellStyle(cellStyle4);
					}
					
					if (Application.getInstance().isOhm()) {
						cell3 = row.createCell(POS_OHM);
						cell3.setCellValue(nf.format(p.getOhm()));
						cell3.setCellStyle(cellStyle4);

						double offsetOhm;
						if (p.getOffsetOhm() != null){
							offsetOhm = p.getOffsetOhm();
						} else {
							offsetOhm = (p.getOhm()-p.getOhm_grezzo());
						}

						cell3 = row.createCell(POS_OFFSET_OHM);
						if (offsetOhm!=0.0) 
							cell3.setCellValue(nf.format(offsetOhm));
						cell3.setCellStyle(cellStyle4);
					}
					
					if (Application.getInstance().isApertoChiuso()) {
						cell3 = row.createCell(POS_APERTO_CHIUSO);
						cell3.setCellValue(p.getApertoChiuso());
						cell3.setCellStyle(cellStyle4);
 					}
					
					

					if (s.getRangeMin()!=0 || s.getRangeMax()!= 0 || s.getRangeMinURT()!=0 || s.getRangeMaxURT()!= 0) {

						if (Application.getInstance().isTemperature()) {
							if (p.getValore() != 0 && outOfLimit(p)) {
								cell1.setCellStyle(cellStyle7);
								cell2.setCellStyle(cellStyle6);
								cell3.setCellStyle(cellStyle6);
								cell4.setCellStyle(cellStyle6); 
							}
						}
						

						switch (app.getTipoMisure()) {
						case TEMPERATURA_UMIDITA:
							if (Application.getInstance().isTemperatureHumidity()) {
								if (p.getUmidita() != 0 && outOfLimitURT(p)) {
									cell1.setCellStyle(cellStyle7);
									cell2.setCellStyle(cellStyle6);
									cell3.setCellStyle(cellStyle6); 
									cell4.setCellStyle(cellStyle6); 
								}
							}
							
						}
					}



				}

				//				sh.autoSizeColumn(COLONNA_DATA);
				//				sh.autoSizeColumn(COLONNA_VALORE);
				//				sh.autoSizeColumn(COLONNA_OFFSET);
				switch (app.getTipoMisure()) {
				case TEMPERATURA_UMIDITA:
				case MISTA:
					//					sh.autoSizeColumn(COLONNA_VALORE_URT);
					//					sh.autoSizeColumn(COLONNA_OFFSET_URT);
				}
			}


			x++; 

		}

		// Pagina con grafico

		try {
			Sheet sh = wb.createSheet();
			wb.setSheetName(x++, "CHART");

			// read the image to the stream
			final FileInputStream stream = new FileInputStream(chartFile);
			final CreationHelper helper = wb.getCreationHelper();
			final Drawing drawing = sh.createDrawingPatriarch();

			final ClientAnchor anchor = helper.createClientAnchor();
			anchor.setAnchorType( AnchorType.MOVE_AND_RESIZE );

			final int pictureIndex = wb.addPicture(IOUtils.toByteArray(stream), Workbook.PICTURE_TYPE_PNG);

			anchor.setCol1(1);
			anchor.setRow1(3);
			final Picture pict = drawing.createPicture(anchor, pictureIndex);
			pict.resize();
		} catch (Exception e) {
			// TODO: handle exception
		}


		// Pagina con grafico


		for(int sheetNum=0; sheetNum < wb.getNumberOfSheets(); sheetNum++) {
			Sheet sheet = wb.getSheetAt(sheetNum);
			for(short col=0; col<10; col++) sheet.autoSizeColumn(col); 
		}


		String pathFile = System.getProperty("user.dir");
		File outFile =null;

		if (dir.toString().length()>0){
			outFile = new File(dir.getAbsolutePath()+"/"+name + ".xls");	
		} else {

			if (pathFile.length()>0){
				outFile = new File(pathFile+"/"+name + ".xls");	
			} else {
				outFile = new File(dir.getAbsolutePath()+"/"+name + ".xls");
			}

		}



		FileOutputStream out;
		try {
			try{
				outFile.delete();
			}catch(Exception e){}
			outFile.createNewFile();
			out = new FileOutputStream(outFile);
			wb.write(out);
			out.close();
			return outFile;
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		}

	}

	private static void styleHeader(Workbook workbook, Cell cell){
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(true);
		style.setFont(font);
		style.setFillBackgroundColor(HSSFColor.AQUA.index);
		cell.setCellStyle(style);
	}

	private static boolean outOfLimit(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0){
			return letturaProva.getValore() < sensore.getRangeMin() || letturaProva.getValore() > sensore.getRangeMax();
		}
		return false;

	}

	private static boolean outOfLimitURT(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0){
			return letturaProva.getUmidita() < sensore.getRangeMinURT() || letturaProva.getUmidita() > sensore.getRangeMaxURT();
		}
		return false;

	}

}
