package com.econorma.export;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.econorma.Application;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.gui.GUI;
import com.econorma.util.Logger;

public class ExporterDat {

	private static final String TAG = ExporterDat.class.getCanonicalName();

	private static final Logger logger = Logger.getLogger(ExporterDat.class);

	private final Prova prova;


	private static Application app = Application.getInstance();

	private static GUI gui = Application.getInstance().getGui();

	private static final DateFormat filedateformat=new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
	private static final NumberFormat nf = new DecimalFormat("#####.0");
//	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy  HH:mm:ss");
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private static final SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private PrintWriter writer;
	private List<LetturaProva> lettureProva;
	
	
	public ExporterDat(Prova prova){
		this.prova = prova;
	}


	public File export(File dir){
		Date dataProva=null;
		File exporter=null;
		dataProva = prova.getData_scarico();
		exporter = exporter(dir,"Export_"+prova.getId()+"__"+filedateformat.format(dataProva));
		return exporter;

	}

	public File exporter(File dir, String name){
	
		
		try {
			String pathFile = System.getProperty("user.dir");
			File outFile =null;
			
			if (dir.toString().length()>0){
				outFile = new File(dir.getAbsolutePath()+"/"+name + ".dat");	
			} else {

				if (pathFile.length()>0){
					outFile = new File(pathFile+"/"+name + ".dat");	
				} else {
					outFile = new File(dir.getAbsolutePath()+"/"+name + ".dat");
				}

			}
			
			writer = new PrintWriter(outFile, "UTF-8");
			
			List<Sensore> sensori = new ArrayList<Sensore>();
			Sensore mySensor = Sensore.newInstance(prova.getId_sensore(), 0, null, null, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null, 0, 0, 0, 0, 0, null, 0, 0, 0, 0, 0, 0, null);
			sensori.add(mySensor);
		
			
			int x = 0;
			for(int ii=0; ii<sensori.size();ii++){

				Sensore s = sensori.get(ii);
				String id_sonda = s.getId_sonda();
				lettureProva = Application.getInstance().getDao().findLettureProvaByProvaAndSonda(prova, id_sonda);
				
				writeMission();
				writeNote();
				writeAllarmi();
				writeHistogramma();


				if(lettureProva==null || lettureProva.isEmpty()){

				}else{

					int size = lettureProva.size();

					for(int i=0; i<size ; i++){
						LetturaProva p = lettureProva.get(i);
						writeMisure(i, size-1, p);
	 					x++; 

					}
				}

			}
			
			return outFile;
			
		} catch (Exception e) {
			logger.error(TAG, "Errore in export file Dat");
	    	 return null;
		} finally {
		    try {
		    	writer.close();
		    } catch (Exception e) {
		    	 logger.error(TAG, "Errore in export file Dat");
		    	 return null;
		    }
		} 
	

	}
	
	public void writeMission() {
		writer.println("[MISSIONE]");
		writer.println("01"+ "Matricola="+prova.getId_sensore());
		writer.println("02"+ "Status="+"NO");
		writer.println("03"+ "Rate[min]=1");
		writer.println("04"+ "Roll-Over=NO");
		writer.println("05"+ "Roll-Occured=NO");
		writer.println("06"+ "Start-Date=" + sdf.format(prova.getData_inizio()));
		writer.println("07"+ "Start-delay[min]=0");
		writer.println("08"+ "Samples="+lettureProva.size());
		writer.println("09"+ "TotalSamples="+lettureProva.size());
		writer.println("10"+ "High_threshold=  85.0");
		writer.println("11"+ "Low_threshold=   0.0");
	}
	
	public void writeNote() {
		writer.println("[NOTE]");
		writer.println("R00 =" + prova.getResponsabile());
		writer.println("R01 =");
		writer.println("R03 =");
		writer.println("R04 =");
		writer.println("R05 =");
		writer.println("R06 =");
		writer.println("R07 =");
		writer.println("R08 =");
		writer.println("R09 =");
		writer.println("R10 =");
		writer.println("R11 =");
		writer.println("R20 =JNS01815");
	}
	
	public void writeAllarmi() {
		writer.println("[ALLARMI]");
	}
	
	public void writeHistogramma() {
		writer.println("[HISTOGRAMMA]");
	}
	
	public void writeMisure(int start, int end, LetturaProva letturaProva) {
		if (start==0) {
			writer.println("[MISURE]");	
		} 
			writer.println("M "+ sdfDate.format(letturaProva.getData()) + "    " + nf.format(letturaProva.getValore()));	
			
		if (start==end) {
			writer.println("[FINE]");
		}
	}


}
