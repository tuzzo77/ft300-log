package com.econorma.data;

import java.util.Date;

public class Log {

	private Long id;
	
	private Date data;
	
	private String host;
	
	private String type;
	
	private String messagge;
	
	private String user;
	
	private String javaClass;
	
	private String dataString;
	
	private String oraString;
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessagge() {
		return messagge;
	}

	public void setMessagge(String messagge) {
		this.messagge = messagge;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getJavaClass() {
		return javaClass;
	}

	public void setJavaClass(String javaClass) {
		this.javaClass = javaClass;
	}

	public String getDataString() {
		return dataString;
	}

	public void setDataString(String dataString) {
		this.dataString = dataString;
	}

	public String getOraString() {
		return oraString;
	}

	public void setOraString(String oraString) {
		this.oraString = oraString;
	}
	
	
	
	
}
