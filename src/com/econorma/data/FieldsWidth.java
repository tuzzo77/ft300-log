package com.econorma.data;

import java.awt.Dimension;
import java.awt.Toolkit;

public class FieldsWidth {

	public int targaWidth;
	public int responsabileWidth;
	public int laboratorioWidth;
	public int numeroProvaWidth;
	public int dataProvaWidth;
	
	private int height;
	private int width;
	
	
	public FieldsWidth() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		width = (int) screenSize.getWidth();
		height = (int) screenSize.getHeight();
		calculate();
	}
	
	public void calculate(){
		switch (width) {
		case 1920:
		case 2560:
		case 3840:
			targaWidth = 100;
			responsabileWidth = 250;
			laboratorioWidth = 250;
			numeroProvaWidth = 150;
			dataProvaWidth = 150;
			break;
		case 1366:
		case 1280:
		case 1152:
		case 1024:
			targaWidth = 100;
			responsabileWidth = 100;
			laboratorioWidth = 100;
			numeroProvaWidth = 100;
			dataProvaWidth = 110;
			break;
		case 1680:
			targaWidth = 100;
			responsabileWidth = 200;
			laboratorioWidth = 200;
			numeroProvaWidth = 150;
			dataProvaWidth = 110;
			break;
		case 1600:
			targaWidth = 100;
			responsabileWidth = 200;
			laboratorioWidth = 200;
			numeroProvaWidth = 100;
			dataProvaWidth = 100;
			break;
		default:
			targaWidth = 100;
			responsabileWidth = 100;
			laboratorioWidth = 100;
			numeroProvaWidth = 100;
			dataProvaWidth = 100;
			break;
		}
	}

	public int getTargaWidth() {
		return targaWidth;
	}
 

	public int getResponsabileWidth() {
		return responsabileWidth;
	}
 

	public int getLaboratorioWidth() {
		return laboratorioWidth;
	}
 

	public int getNumeroProvaWidth() {
		return numeroProvaWidth;
	}
 

	public int getDataProvaWidth() {
		return dataProvaWidth;
	}
 
	
}
