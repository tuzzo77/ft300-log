package com.econorma.data;

import java.util.Date;

public class Prova {

	private Long id;
	
	private String responsabile;
	
	private Date data_inizio;
	
	private Date ora_inizio;

	private Date data_fine;
	
	private Date ora_fine;
	
	private String id_sensore;
	
	private Date data_scarico;
	
	
	public Prova() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
  
	public String getResponsabile() {
		return responsabile;
	}

	public void setResponsabile(String responsabile) {
		this.responsabile = responsabile;
	}

 	public Date getData_inizio() {
		return data_inizio;
	}

	public void setData_inizio(Date data_inizio) {
		this.data_inizio = data_inizio;
	}

	public Date getData_fine() {
		return data_fine;
	}

	public void setData_fine(Date data_fine) {
		this.data_fine = data_fine;
	}

	public Date getOra_inizio() {
		return ora_inizio;
	}

	public void setOra_inizio(Date ora_inizio) {
		this.ora_inizio = ora_inizio;
	}

	public Date getOra_fine() {
		return ora_fine;
	}

	public void setOra_fine(Date ora_fine) {
		this.ora_fine = ora_fine;
	}


	public String getId_sensore() {
		return id_sensore.toUpperCase();
	}

	public void setId_sensore(String id_sensore) {
		this.id_sensore = id_sensore;
	}

	public Date getData_scarico() {
		return data_scarico;
	}

	public void setData_scarico(Date data_scarico) {
		this.data_scarico = data_scarico;
	}
 	
	
}
