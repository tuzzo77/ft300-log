package com.econorma.data;


public class User {

	public String user;
	
	public String user_complete;
	
	public String password;
	
	public String type;
	
	public String language;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUser_complete() {
		return user_complete;
	}

	public void setUser_complete(String user_complete) {
		this.user_complete = user_complete;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
}
