package com.econorma.data;

import java.util.Date;

public class SampleResponse {
	
	 private String type;
	 private Date data;
	 private Double temperature;
	 private Double humidity;
	 private Double volt;
	 private Double milliampere;
	 private Double ohm;
	 private String apertoChiuso;
	 
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	public Double getHumidity() {
		return humidity;
	}
	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}
	public Double getVolt() {
		return volt;
	}
	public void setVolt(Double volt) {
		this.volt = volt;
	}
	public Double getMilliampere() {
		return milliampere;
	}
	public void setMilliampere(Double milliampere) {
		this.milliampere = milliampere;
	}
	public Double getOhm() {
		return ohm;
	}
	public void setOhm(Double ohm) {
		this.ohm = ohm;
	}
	public String getApertoChiuso() {
		return apertoChiuso;
	}
	public void setApertoChiuso(String apertoChiuso) {
		this.apertoChiuso = apertoChiuso;
	}
	 

}
