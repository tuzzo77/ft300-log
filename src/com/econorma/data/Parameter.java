package com.econorma.data;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Parameter {

	public static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	public static final NumberFormat nf = new DecimalFormat("#####.##");
	
	String id_logger;
	String descrizione_logger;
	String tipo_logger;
	String stato_batteria;
	Date data_logger;
	int trasmission_logger;
	double range_min;
	double range_max;
	double range_min_urt;
	double range_max_urt;
	double offset;
	double offset_urt;
	double offset_volt;
	double offset_milliampere;
	double offset_ohm;
	double last_temperature;
	double last_humidity;
	double last_milliampere;
	double last_ohm;
	double last_volt;
	String last_apertoChiuso;
	int campioni;
	
	public String getId_logger() {
		return id_logger.toUpperCase();
	}
	public void setId_logger(String id_logger) {
		this.id_logger = id_logger;
	}
	public String getDescrizione_logger() {
		return descrizione_logger;
	}
	public void setDescrizione_logger(String descrizione_logger) {
		this.descrizione_logger = descrizione_logger;
	}
	public String getTipo_logger() {
		return tipo_logger;
	}
	public void setTipo_logger(String tipo_logger) {
		this.tipo_logger = tipo_logger;
	}
	public Date getData_logger() {
		return data_logger;
	}
	public void setData_logger(Date data_logger) {
		this.data_logger = data_logger;
	}
	public int getTrasmission_logger() {
		return trasmission_logger;
	}
	public void setTrasmission_logger(int trasmission_logger) {
		this.trasmission_logger = trasmission_logger;
	}
	public double getRange_min() {
		return range_min;
	}
	public void setRange_min(double range_min) {
		this.range_min = range_min;
	}
	public double getRange_max() {
		return range_max;
	}
	public void setRange_max(double range_max) {
		this.range_max = range_max;
	}
	public double getRange_min_urt() {
		return range_min_urt;
	}
	public void setRange_min_urt(double range_min_urt) {
		this.range_min_urt = range_min_urt;
	}
	public double getRange_max_urt() {
		return range_max_urt;
	}
	public void setRange_max_urt(double range_max_urt) {
		this.range_max_urt = range_max_urt;
	}
	public double getOffset() {
		return offset;
	}
	public void setOffset(double offset) {
		this.offset = offset;
	}
	public double getOffset_urt() {
		return offset_urt;
	}
	public void setOffset_urt(double offset_urt) {
		this.offset_urt = offset_urt;
	}
	
	public String getStato_batteria() {
		return stato_batteria;
	}
	public void setStato_batteria(String stato_batteria) {
		this.stato_batteria = stato_batteria;
	}
	
	public double getLast_temperature() {
		return last_temperature;
	}
	public void setLast_temperature(double last_temperature) {
		this.last_temperature = last_temperature;
	}
	public double getLast_humidity() {
		return last_humidity;
	}
	public void setLast_humidity(double last_humidity) {
		this.last_humidity = last_humidity;
	}
	public int getCampioni() {
		return campioni;
	}
	public void setCampioni(int campioni) {
		this.campioni = campioni;
	}
	public double getLast_milliampere() {
		return last_milliampere;
	}
	public void setLast_milliampere(double last_milliampere) {
		this.last_milliampere = last_milliampere;
	}
	public double getLast_ohm() {
		return last_ohm;
	}
	public void setLast_ohm(double last_ohm) {
		this.last_ohm = last_ohm;
	}
	public double getLast_volt() {
		return last_volt;
	}
	public void setLast_volt(double last_volt) {
		this.last_volt = last_volt;
	}
	public String getLast_apertoChiuso() {
		return last_apertoChiuso;
	}
	public void setLast_apertoChiuso(String last_apertoChiuso) {
		this.last_apertoChiuso = last_apertoChiuso;
	}
	public double getOffset_volt() {
		return offset_volt;
	}
	public void setOffset_volt(double offset_volt) {
		this.offset_volt = offset_volt;
	}
	public double getOffset_milliampere() {
		return offset_milliampere;
	}
	public void setOffset_milliampere(double offset_milliampere) {
		this.offset_milliampere = offset_milliampere;
	}
	public double getOffset_ohm() {
		return offset_ohm;
	}
	public void setOffset_ohm(double offset_ohm) {
		this.offset_ohm = offset_ohm;
	}
	public ArrayList<String> toList() {
		ArrayList<String> parameters = new ArrayList<String>();
		parameters.add(id_logger);
		parameters.add(descrizione_logger);
		parameters.add(sdf.format(data_logger));
		parameters.add(String.valueOf(trasmission_logger));
		parameters.add(tipo_logger);
		parameters.add(stato_batteria);
	
		parameters.add(nf.format(last_temperature));
		parameters.add(nf.format(range_min));
		parameters.add(nf.format(range_max));
		parameters.add(nf.format(offset));
		
		parameters.add(nf.format(last_humidity));
		parameters.add(nf.format(range_min_urt));
		parameters.add(nf.format(range_max_urt));
		parameters.add(nf.format(offset_urt));

		parameters.add(nf.format(last_ohm));
		parameters.add(nf.format(offset_ohm));
		
		parameters.add(nf.format(last_volt));
		parameters.add(nf.format(offset_volt));
		
		parameters.add(nf.format(last_milliampere));
		parameters.add(nf.format(offset_milliampere));
		
		parameters.add(last_apertoChiuso);
		
		parameters.add(String.valueOf(campioni));
		return parameters;
	}
	
}
