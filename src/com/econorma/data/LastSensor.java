package com.econorma.data;

public class LastSensor  {
	
	private String time;
	private String idSonda;

	
	public LastSensor(String time, String idSonda){
		this.idSonda = idSonda;
		this.time = time;
	}
	
	public String getIdSonda() {
		return idSonda;
	}
	public void setIdSonda(String idSonda) {
		this.idSonda = idSonda;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSonda == null) ? 0 : idSonda.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LastSensor other = (LastSensor) obj;
		if (idSonda == null) {
			if (other.idSonda != null)
				return false;
		} else if (!idSonda.equals(other.idSonda))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}
 
}
