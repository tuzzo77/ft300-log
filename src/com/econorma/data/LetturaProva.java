package com.econorma.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.econorma.data.Lettura.TipoMisura;
import com.econorma.logic.Measurable;

public class LetturaProva implements Measurable{

	private Long id;
	
	private Long prova_id;
	
	private double valore;
	
	private double valore_grezzo;
	
	private String id_sonda;
	
	private Date data;
	
	private TipoMisura tipo_misura;
	
	private double umidita;
	
	private double umidita_grezzo;
	
	private String stato_batteria;

	private Double offset;
	private Double offset_urt;
	private Double offset_volt;
	private Double offset_milliampere;
	private Double offset_ohm;
	
	private Double incertezza;
	private Double incertezza_urt;
	
	private Double volt;
	private Double volt_grezzo;
	private Double milliampere;
	private Double milliampere_grezzo;
	private Double ohm;
	private Double ohm_grezzo;
	private String apertoChiuso;
	
	private List<LetturaProva> lettureProva = new ArrayList<LetturaProva>();
	
	public LetturaProva(){}
	
	public LetturaProva(Prova prova, Lettura lettura){
		prova_id = prova.getId();
		valore = lettura.getValore();
		valore_grezzo = lettura.getTemperaturaGrezza();
		id_sonda = lettura.getIdSonda();
		data = lettura.getData();
		tipo_misura = lettura.getTipoMisura();
		umidita = lettura.getUmidita();
		stato_batteria = lettura.getStatoBatteria();
		umidita_grezzo = lettura.getUmiditaGrezza();
		offset = lettura.getOffset();
		offset_urt = lettura.getOffsetUrt();
		incertezza = lettura.getIncertezza();
		incertezza_urt = lettura.getIncertezzaUrt();
		volt = lettura.getVolt();
		volt_grezzo = lettura.getVoltGrezzo();
		milliampere=lettura.getMilliampere();
		milliampere_grezzo=lettura.getMilliampereGrezzo();
		ohm = lettura.getOhm();
		ohm_grezzo=lettura.getOhmGrezzo();
		apertoChiuso=lettura.getApertoChiuso();
		offset_volt = lettura.getOffsetVolt();
		offset_milliampere=lettura.getOffsetMilliampere();
		offset_ohm=lettura.getOffsetOhm();
	}
	
	public LetturaProva (Prova prova, List<Lettura> letture){
		lettureProva.clear();
		for (Lettura l : letture) {
			LetturaProva letturaProva = new LetturaProva(prova, l);
			lettureProva.add(letturaProva);
		}
	}


	public Long getId() {
		return id;
	}


	public Long getProva_id() {
		return prova_id;
	}


	public double getValore() {
		return valore;
	}


	public double getValore_grezzo() {
		return valore_grezzo;
	}
	
	public double getUmidita_grezzo() {
		return umidita_grezzo;
	}


	public String getId_sonda() {
		return id_sonda.toUpperCase();
	}


	public Date getData() {
		return data;
	}


	public TipoMisura getTipo_misura() {
		return tipo_misura;
	}
	
	public String getStatoBatteria(){
		return stato_batteria;
	}
	
	public void setId(Long id){
		this.id = id;
	}


	public void setProva_id(Long prova_id) {
		this.prova_id = prova_id;
	}


	public void setValore(double valore) {
		this.valore = valore;
	}


	public void setValore_grezzo(double valore_grezzo) {
		this.valore_grezzo = valore_grezzo;
	}
	
	public void setUmidita_grezzo(double umidita_grezzo) {
		this.umidita_grezzo = umidita_grezzo;
	}


	public void setId_sonda(String id_sonda) {
		this.id_sonda = id_sonda;
	}


	public void setData(Date data) {
		this.data = data;
	}


	public void setTipo_misura(TipoMisura tipo_misura) {
		this.tipo_misura = tipo_misura;
	}
	
	public void setStatoBatteria (String stato_batteria){
		this.stato_batteria=stato_batteria;
	}
	
	public double getValoreMisura(TipoMisura tipo){
		switch (tipo) {
		case TEMPERATURA:
			return getValore();
		case UMIDITA:
			return getUmidita();
		case VOLT:
			return getVolt();
		case MILLIAMPERE:
			return getMilliampere();
		case OHM:
			return getOhm();
		case APERTO_CHIUSO:
			return Double.parseDouble(getApertoChiuso());
		case TENSIONE_CORRENTE:
			return getValore();
		default:
			throw new RuntimeException("Richiesta non supportata");
		}
	}
	
	public boolean isValoreMisuraValid(TipoMisura tipo){
		switch (tipo) {
		case TEMPERATURA:
			if(TipoMisura.TEMPERATURA.equals(getTipo_misura())|| 
			   TipoMisura.TEMPERATURA_UMIDITA.equals(getTipo_misura()))  {
				return Lettura.INVALID_MEASURE != getValoreMisura(tipo);
			}else{
				return false;
			}
		case UMIDITA:
			return getValoreMisura(tipo)>0;
		case VOLT:
			return getValoreMisura(tipo)>0;
		case MILLIAMPERE:
			return getValoreMisura(tipo)>0;
		case OHM:
			return getValoreMisura(tipo)>0;
		case APERTO_CHIUSO:
//			return getValoreMisura(tipo)>0;
			return true;
		case TENSIONE_CORRENTE: 
			return TipoMisura.TENSIONE_CORRENTE.equals(getTipo_misura());
		default:
			throw new RuntimeException("Richiesta non supportata");
		}
	}

	public double getUmidita() {
		return umidita;
	}

	public void setUmidita(double umidita) {
		this.umidita = umidita;
	}
	
	 
	public Double getOffset() {
		return offset;
	}

	public void setOffset(Double offset) {
		this.offset = offset;
	}

	public Double getOffsetUrt() {
		return offset_urt;
	}

	public void setOffsetUrt(Double offset_urt) {
		this.offset_urt = offset_urt;
	}

	public Double getIncertezza() {
		return incertezza;
	}

	public void setIncertezza(Double incertezza) {
		this.incertezza = incertezza;
	}

	public Double getIncertezzaUrt() {
		return incertezza_urt;
	}

	public void setIncertezzaUrt(Double incertezza_urt) {
		this.incertezza_urt = incertezza_urt;
	}

	public List<LetturaProva> getLettureProva() {
		return lettureProva;
	}

	public String getStato_batteria() {
		return stato_batteria;
	}

	public void setStato_batteria(String stato_batteria) {
		this.stato_batteria = stato_batteria;
	}

	public Double getVolt() {
		return volt;
	}

	public void setVolt(Double volt) {
		this.volt = volt;
	}

	public Double getMilliampere() {
		return milliampere;
	}

	public void setMilliampere(Double milliampere) {
		this.milliampere = milliampere;
	}

	public Double getOhm() {
		return ohm;
	}

	public void setOhm(Double ohm) {
		this.ohm = ohm;
	}

	public String getApertoChiuso() {
		return apertoChiuso;
	}

	public void setApertoChiuso(String apertoChiuso) {
		this.apertoChiuso = apertoChiuso;
	}

 	public Double getOffsetVolt() {
		return offset_volt;
	}

	public void setOffsetVolt(Double offset_volt) {
		this.offset_volt = offset_volt;
	}

	public Double getOffsetMilliampere() {
		return offset_milliampere;
	}

	public void setOffsetMilliampere(Double offset_milliampere) {
		this.offset_milliampere = offset_milliampere;
	}

	public Double getOffsetOhm() {
		return offset_ohm;
	}

	public void setOffsetOhm(Double offset_ohm) {
		this.offset_ohm = offset_ohm;
	}

	public Double getVolt_grezzo() {
		return volt_grezzo;
	}

	public void setVolt_grezzo(Double volt_grezzo) {
		this.volt_grezzo = volt_grezzo;
	}

	public Double getMilliampere_grezzo() {
		return milliampere_grezzo;
	}

	public void setMilliampere_grezzo(Double milliampere_grezzo) {
		this.milliampere_grezzo = milliampere_grezzo;
	}

	public Double getOhm_grezzo() {
		return ohm_grezzo;
	}

	public void setOhm_grezzo(Double ohm_grezzo) {
		this.ohm_grezzo = ohm_grezzo;
	}
	
}
