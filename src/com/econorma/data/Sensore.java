package com.econorma.data;

 

import java.util.Date;

import com.econorma.Application;
import com.econorma.data.Lettura.TipoMisura;
import com.econorma.io.ParsersLogger.StatusResponse.VERSION;
import com.econorma.resources.Testo;
import com.econorma.util.StringUtils;

public class Sensore implements Comparable<Sensore>{

	private String id_sonda;

	private double offset;
	
	private int trasmission;
	
	private Date data;
	
	private double incertezza;
	
	private double incertezzaURT;
	
	private double offsetURT;
	
	private double offsetVolt;
	
	private double offsetMilliampere;
	
	private double offsetOhm;

	private Location location = Location.UNKNOWN;

	private String descrizione;
	
	private double rangeMin = Double.MIN_VALUE;
	
	private double rangeMax = Double.MIN_VALUE;
	
	private double rangeMinURT = Double.MIN_VALUE;
	
	private double rangeMaxURT = Double.MIN_VALUE;
	
	private double rangeMinOhm = Double.MIN_VALUE;
	
	private double rangeMaxOhm = Double.MIN_VALUE;
	
	private TipoMisura tipoMisura;
	
	private String statoBatteria;
	
	private double temperature = Double.MIN_VALUE;
	
	private double humidity = Double.MIN_VALUE;
	
	private double milliampere = Double.MIN_VALUE;
	
	private double ohm = Double.MIN_VALUE;
	
	private double volt = Double.MIN_VALUE;
	
	private String apertoChiuso;
	
	private int campioni;
	
	private VERSION versione;
	
	private Parameter parameter;
	
	public Sensore(String id_sonda, Location location) {
		this(id_sonda, 0, null, location, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null, 0, 0, 0, 0, 0, 0, null, 0, 0, 0 , 0, 0, null, null);
	}

	private Sensore(String idsonda, double offset, String descrizione,
			Location location, double rangeMin, double rangeMax, double rangeMinURT, double rangeMaxURT,  
			double offsetURT,double incertezza, double incertezzaURT, int trasmission, Date data, TipoMisura tipoMisura, 
			String statoBatteria, double temperature, double humidity, int campioni, double volt, double milliampere, double ohm,
			String apertoChiuso, double offsetVolt, double offsetMilliampere, double offsetOhm, double rangeMinOhm, double rangeMaxOhm, VERSION versione, Parameter parameter) {
		this.id_sonda = idsonda;
		this.offset = offset;
		this.descrizione = descrizione;
		this.location = location;
		this.rangeMin = rangeMin;
		this.rangeMax = rangeMax;
		this.rangeMinURT = rangeMinURT;
		this.rangeMaxURT = rangeMaxURT;
		this.offsetURT=offsetURT;
		this.incertezza=incertezza;
		this.incertezzaURT=incertezzaURT;
		this.trasmission=trasmission;
		this.data=data;
		this.tipoMisura=tipoMisura;
		this.statoBatteria=statoBatteria;
		this.temperature=temperature;
		this.humidity=humidity;
		this.volt=volt;
		this.milliampere=milliampere;
		this.ohm=ohm;
		this.apertoChiuso=apertoChiuso;
		this.offsetVolt=offsetVolt;
		this.offsetMilliampere=milliampere;
		this.offsetOhm=offsetOhm;
		this.parameter=parameter;
		this.campioni=campioni;
		this.rangeMinOhm=rangeMinOhm;
		this.rangeMaxOhm=rangeMaxOhm;
		this.versione=versione;
	}

	public static Sensore newInstance(String idsonda, double offset,
			String descrizione, Location location, double rangeMin, double rangeMax, double rangeMinURT, double rangeMaxURT, 
			double offsetURT, double incertezza, double incertezzaURT, int trasmission, Date data, TipoMisura tipoMisura, String statoBatteria, 
			double temperature, double humidity, double volt, double milliampere, double ohm, String apertoChiuso, 
			double offsetVolt, double offsetMilliampere, double offsetOhm, double rangeMinOhm, double rangeMaxOhm,int campioni, VERSION versione) {
		
			Parameter parameter = new Parameter();
			parameter.setId_logger(idsonda);
			parameter.setDescrizione_logger(descrizione);
			parameter.setData_logger(data);
			parameter.setTrasmission_logger(trasmission);
			parameter.setTipo_logger(tipoMisura == null ? Testo.TEMPERATURA_UMIDITA : tipoMisura.name());
			parameter.setRange_min(rangeMin);
			parameter.setRange_max(rangeMax);
			parameter.setRange_min_urt(rangeMinURT);
			parameter.setRange_max_urt(rangeMaxURT);
			parameter.setStato_batteria(statoBatteria);
			parameter.setLast_temperature(temperature);
			parameter.setLast_humidity(humidity);
			parameter.setCampioni(campioni);
			parameter.setLast_volt(volt);
			parameter.setLast_milliampere(milliampere);
			parameter.setLast_ohm(ohm);
			parameter.setLast_apertoChiuso(apertoChiuso);
			parameter.setOffset_volt(offsetVolt);
			parameter.setOffset_milliampere(offsetMilliampere);
			parameter.setOffset_ohm(offsetOhm);
			
		return new Sensore(idsonda, offset, descrizione, location, rangeMin, rangeMax, rangeMinURT, rangeMaxURT, 
				offsetURT, incertezza, incertezzaURT, trasmission, data, tipoMisura, statoBatteria, temperature, 
				humidity, campioni, volt, milliampere, ohm, apertoChiuso, 
				offsetVolt, offsetMilliampere, offsetOhm, rangeMinOhm, rangeMaxOhm, versione, parameter);
	}

	public static Sensore newInstance(String id_sonda) {
		return new Sensore(id_sonda, Location.UNKNOWN);
	}
	
	public String getId_sonda() {
		
		if (!id_sonda.contains(LOGGER)) {
			return (LOGGER+id_sonda).toUpperCase();	
		}
		return id_sonda.toUpperCase();
	}

	public void setId_sonda(String id_sonda) {
		this.parameter.id_logger=id_sonda;
		this.id_sonda = id_sonda;
	}

	public double getOffset() {
		return offset;
	}

	public void setOffset(double offset) {
		this.parameter.offset=offset;
		this.offset = offset;
	}
	
	public double getOffsetURT() {
		return offsetURT;
	}
	
	public void setOffsetURT(double offsetURT) {
		this.parameter.offset_urt=offsetURT;
		this.offsetURT = offsetURT;
	}
	
	public double getOffsetVolt() {
		return offsetVolt;
	}

	public void setOffsetVolt(double offsetVolt) {
		this.parameter.offset_volt=offsetVolt;
		this.offsetVolt = offsetVolt;
	}
	
	public double getOffsetMilliampere() {
		return offsetMilliampere;
	}

	public void setOffsetMilliampere(double offsetMilliampere) {
		this.parameter.offset_milliampere=offsetMilliampere;
		this.offsetMilliampere = offsetMilliampere;
	}
	
	public double getOffsetOhm() {
		return offsetOhm;
	}

	public void setOffsetOhm(double offsetOhm) {
		this.parameter.offset_ohm=offsetOhm;
		this.offsetOhm = offsetOhm;
	}
	
	public double getRangeMin() {
		return rangeMin;
	}

	public void setRangeMin(double rangeMin) {
		this.parameter.range_min=rangeMin;
		this.rangeMin = rangeMin;
	}
	
	public double getRangeMax() {
		return rangeMax;
	}

	public void setRangeMax(double rangeMax) {
		this.parameter.range_max=rangeMax;
		this.rangeMax = rangeMax;
	}
	
	public double getRangeMinURT() {
		return rangeMinURT;
	}

	public void setRangeMinURT(double rangeMinURT) {
		this.parameter.range_min_urt=rangeMinURT;
		this.rangeMinURT = rangeMinURT;
	}
	
	public double getRangeMaxURT() {
		return rangeMaxURT;
	}

	public void setRangeMaxURT(double rangeMaxURT) {
		this.parameter.range_max_urt=rangeMaxURT;
		this.rangeMaxURT = rangeMaxURT;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.parameter.descrizione_logger=descrizione;
		this.descrizione = descrizione;
	}
	
	public int getTrasmission() {
		return trasmission;
	}

	public void setTrasmission(int trasmission) {
		this.parameter.trasmission_logger=trasmission;
		this.trasmission = trasmission;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	

	public TipoMisura getTipoMisura() {
		return tipoMisura;
	}

	public void setTipoMisura(TipoMisura tipoMisura) {
		this.tipoMisura = tipoMisura;
	}

	public double getMilliampere() {
		return milliampere;
	}

	public void setMilliampere(double milliampere) {
		this.milliampere = milliampere;
	}

	public double getOhm() {
		return ohm;
	}

	public void setOhm(double ohm) {
		this.ohm = ohm;
	}

	public double getVolt() {
		return volt;
	}

	public void setVolt(double volt) {
		this.volt = volt;
	}

	public String getApertoChiuso() {
		return apertoChiuso;
	}

	public void setApertoChiuso(String apertoChiuso) {
		this.apertoChiuso = apertoChiuso;
	}

	public double getRangeMinOhm() {
		return rangeMinOhm;
	}

	public void setRangeMinOhm(double rangeMinOhm) {
		this.rangeMinOhm = rangeMinOhm;
	}

	public double getRangeMaxOhm() {
		return rangeMaxOhm;
	}

	public void setRangeMaxOhm(double rangeMaxOhm) {
		this.rangeMaxOhm = rangeMaxOhm;
	}


	public static enum Location {
		INTERNO, ESTERNO, UNKNOWN, VIRTUAL, EUTETTICHE;

		public static Location fromString(String s) {
			try {
				String tmp = null;
				if (s != null)
					tmp = s.toUpperCase();
				if (StringUtils.equals(INTERNO.name(), tmp)) {
					return INTERNO;
				} else if (StringUtils.equals(ESTERNO.name(), tmp)) {
					return ESTERNO;
				} else if (StringUtils.equals(EUTETTICHE.name(), tmp)) {
					return EUTETTICHE;
				} else if (StringUtils.equals(VIRTUAL.name(), tmp)) {
					return VIRTUAL;
				} else
					return UNKNOWN;
			} catch (Exception e) {
				return UNKNOWN;
			}
		}
	}
 

	@Override
	public String toString() {
		return getId_sonda();
	}

	public static final Sensore MEDIA_ESTERNA;
	public static final Sensore MEDIA_INTERNA;
	public static final Sensore EUTETTICHE;
	public static final String LOGGER;

	static {

		MEDIA_ESTERNA = new Sensore(Application.getInstance().getBundle().getString("sensore.mediaEsterna"), Location.VIRTUAL);
		MEDIA_INTERNA = new Sensore(Application.getInstance().getBundle().getString("sensore.mediaInterna"), Location.VIRTUAL);
		EUTETTICHE = new Sensore(Application.getInstance().getBundle().getString("sensore.eutettiche"), Location.VIRTUAL);
		LOGGER = Testo.LOGGER;
	
	}

	@Override
	public int hashCode() {
		return getId_sonda().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Sensore){
			Sensore other = (Sensore) obj; 
			return StringUtils.equals(getId_sonda(), other.getId_sonda());
		}
		return false;
	}

	@Override
	public int compareTo(Sensore o) {
		return getId_sonda().compareTo(o.getId_sonda());
	}

	public static boolean isMedia(String idSensore){
		String variant = null;
		
		return idSensore!=null && idSensore.startsWith(variant);
	}

	
	public boolean isMedia(){
		return isMedia(getId_sonda());
	}

	public double getIncertezza() {
		return incertezza;
	}

	public void setIncertezza(double incertezza) {
		this.incertezza = incertezza;
	}

	public double getIncertezzaURT() {
		return incertezzaURT;
	}

	public void setIncertezzaURT(double incertezzaURT) {
		this.incertezzaURT = incertezzaURT;
	}

	public Parameter getParameter() {
		return parameter;
	}

	public void setParameter(Parameter parameter) {
		this.parameter = parameter;
	}

	public String getStatoBatteria() {
		return statoBatteria;
	}

	public void setStatoBatteria(String statoBatteria) {
		this.statoBatteria = statoBatteria;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public double getHumidity() {
		return humidity;
	}

	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}

	public int getCampioni() {
		return campioni;
	}

	public void setCampioni(int campioni) {
		this.campioni = campioni;
	}

	public VERSION getVersione() {
		return versione;
	}

	public void setVersione(VERSION versione) {
		this.versione = versione;
	}
	 
}
