package com.econorma.data;

import java.util.BitSet;
import java.util.Date;

import com.econorma.io.ParsersLogger.StatusResponse.VERSION;
import com.econorma.resources.Testo;

public class LoggerResponse {
	
	private boolean isValid;
	private String idSonda;
	private String full;
	private String statoBatteria;
	private int trasmission;
	private Date data;
	private Double temperature;
	private Double humidity;
	private Double milliampere;
	private Double ohm;
	private Double volt;
	private String apertoChiuso;
	private int samples;
	private String logger=Testo.LOGGER;
	private BitSet sensorsArray;
	private VERSION versione;
	
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	public String getIdSonda() {
		return idSonda;
	}
	public void setIdSonda(String idSonda) {
		this.idSonda = idSonda;
	}
	public String getFull() {
		return full;
	}
	public void setFull(String full) {
		this.full = full;
	}
	public String getStatoBatteria() {
		return statoBatteria;
	}
	public void setStatoBatteria(String statoBatteria) {
		this.statoBatteria = statoBatteria;
	}
	public int getTrasmission() {
		return trasmission;
	}
	public void setTrasmission(int trasmission) {
		this.trasmission = trasmission;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	public Double getHumidity() {
		return humidity;
	}
	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}
	public int getSamples() {
		return samples;
	}
	public void setSamples(int samples) {
		this.samples = samples;
	}
	
	public String getIdSondaComplete(){
		return (logger+getIdSonda()).toUpperCase();
	}
	public Double getMilliampere() {
		return milliampere;
	}
	public void setMilliampere(Double milliampere) {
		this.milliampere = milliampere;
	}
	public Double getOhm() {
		return ohm;
	}
	public void setOhm(Double ohm) {
		this.ohm = ohm;
	}
	public Double getVolt() {
		return volt;
	}
	public void setVolt(Double volt) {
		this.volt = volt;
	}
	public String getApertoChiuso() {
		return apertoChiuso;
	}
	public void setApertoChiuso(String apertoChiuso) {
		this.apertoChiuso = apertoChiuso;
	}
	public String getLogger() {
		return logger;
	}
	public void setLogger(String logger) {
		this.logger = logger;
	}
	public BitSet getSensorsArray() {
		return sensorsArray;
	}
	public void setSensorsArray(BitSet sensorsArray) {
		this.sensorsArray = sensorsArray;
	}
	public VERSION getVersione() {
		return versione;
	}
	public void setVersione(VERSION versione) {
		this.versione = versione;
	}
	
}
