package com.econorma.data;

import java.util.Date;

public class LetturaStorico {

	private String idSonda;
	
	private Date data;
 	
	private double valore;
	
	private double valore_ur;

	public String getIdSonda() {
		return idSonda;
	}

	public void setIdSonda(String idSonda) {
		this.idSonda = idSonda;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public double getValore() {
		return valore;
	}

	public void setValore(double valore) {
		this.valore = valore;
	}

	public double getValore_ur() {
		return valore_ur;
	}

	public void setValore_ur(double valore_ur) {
		this.valore_ur = valore_ur;
	}
	
}
