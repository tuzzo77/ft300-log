package com.econorma.data;

import java.util.Date;

import com.econorma.data.Sensore.Location;
import com.econorma.logic.Measurable;

public class Lettura implements Measurable {

	public static final double INVALID_MEASURE = Double.MIN_VALUE;
	private String idSonda;
	
	private String statoBatteria;

	private Date data;

	private TipoMisura tipoMisura;

	private double temperaturaGrezza = INVALID_MEASURE;
	
	private double umiditaGrezza = INVALID_MEASURE;
	
	private double milliampere = INVALID_MEASURE;
	
	private double milliampereGrezzo = INVALID_MEASURE;
	
	private double ohm = INVALID_MEASURE;
	
	private double ohmGrezzo = INVALID_MEASURE;
	
	private double volt = INVALID_MEASURE;
	
	private double voltGrezzo = INVALID_MEASURE;
	
	private String apertoChiuso;
	
	private Sensore sensore;
	
	private transient boolean _discovery;
	
	private double offset=INVALID_MEASURE;
	private double offset_urt=INVALID_MEASURE;
	private double offset_volt=INVALID_MEASURE;
	private double offset_milliampere=INVALID_MEASURE;
	private double offset_ohm=INVALID_MEASURE;
	
	private double incertezza=INVALID_MEASURE;
	private double incertezza_urt=INVALID_MEASURE;
	
	public double getTemperaturaGrezza() {
		return temperaturaGrezza;
	}

	public void setTemperaturaGrezza(double valoreOriginale) {
		this.temperaturaGrezza = valoreOriginale;
	}
	
	public double getUmiditaGrezza() {
		return umiditaGrezza;
	}

	public void setUmiditaGrezza(double valoreOriginale) {
		this.umiditaGrezza = valoreOriginale;
	}

	public String getIdSonda() {
		return idSonda.toUpperCase();
	}

	public void setIdSonda(String idSonda) {
		this.idSonda = idSonda;
	}

	public double getValore() {
		if (sensore != null)
			return temperaturaGrezza + sensore.getOffset() + sensore.getIncertezza();
		return temperaturaGrezza;
	}
	
	public double getUmidita(){
		if (sensore != null)
			return umiditaGrezza + sensore.getOffsetURT() + sensore.getIncertezzaURT();
		return umiditaGrezza;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public TipoMisura getTipoMisura() {
		return tipoMisura;
	}
	
	public String getStatoBatteria(){
		return statoBatteria;
	}

	public void setTipoMisura(TipoMisura tipoMisura) {
		this.tipoMisura = tipoMisura;
	}
	
	public void setStatoBatteria(String statoBatteria){
		this.statoBatteria=statoBatteria;
	}

	public static enum TipoMisura {
		TEMPERATURA, TEMPERATURA_UMIDITA, VOLT, MILLIAMPERE, OHM, APERTO_CHIUSO, UMIDITA, TENSIONE_CORRENTE
	}

	@Override
	public String toString() {
		return "id_sonda: " + idSonda + " temperatura:" + getValore() + " data: "+ data;
	}

	public Sensore getSensore() {
		return sensore;
	}

	public void setSensore(Sensore sensore) {
//		if (this.sensore != null)
//			throw new RuntimeException("Il sensore è già stato assegnato...");
		this.sensore = sensore;
	}
	
	
	public static Lettura createLetturaMedia(Location location, Date date,
			double media) {
		Lettura lettura = new Lettura();

		lettura.setTemperaturaGrezza(media);

		switch (location) {
		case INTERNO:
			lettura.setSensore(Sensore.MEDIA_INTERNA);
			lettura.setIdSonda(Sensore.MEDIA_INTERNA.getId_sonda());
			break;
		case ESTERNO:
			lettura.setSensore(Sensore.MEDIA_ESTERNA);
			lettura.setIdSonda(Sensore.MEDIA_ESTERNA.getId_sonda());
			break;
		case EUTETTICHE:
			break;
		default:
			throw new RuntimeException("non supportato");
		}
		

		lettura.setData(date);
		lettura.setUmiditaGrezza(0);
		lettura.setStatoBatteria("");
		lettura.setTipoMisura(Lettura.TipoMisura.TEMPERATURA);
		return lettura;
	}
	
	
	public double getValoreMisura(TipoMisura tipo){
		switch (tipo) {
		case TEMPERATURA:
			return getValore();
		case UMIDITA:
			return getUmidita();
		case TENSIONE_CORRENTE:
			return getValore();
		default:
			throw new RuntimeException("Richiesta non supportata");
		}
	}
	
	public boolean isValoreMisuraValid(TipoMisura tipo){
		switch (tipo) {
		case TEMPERATURA:
			if(TipoMisura.TEMPERATURA.equals(getTipoMisura()) || TipoMisura.TEMPERATURA_UMIDITA.equals(getTipoMisura()))  {
				return INVALID_MEASURE != getValoreMisura(tipo);
			}else{
				return false;
			}
		case UMIDITA:
			return getValoreMisura(tipo)>0;
		case TENSIONE_CORRENTE: 
			return TipoMisura.TENSIONE_CORRENTE.equals(getTipoMisura());
		default:
			throw new RuntimeException("Richiesta non supportata");
		}
	}
	
	
	public boolean isTemperaturaOutOfRange() {
		double rangeMax = sensore.getRangeMax();
		double rangeMin = sensore.getRangeMin();
		if(rangeMax!=0 || rangeMin!=0){
			return getValore()>rangeMax || getValore()<rangeMin;
		}
		return false;
	}
	
	public boolean isUmiditaOutOfRange() {
		double rangeMax = sensore.getRangeMaxURT();
		double rangeMin = sensore.getRangeMinURT();
		if(rangeMax!=0 || rangeMin!=0){
			return getUmidita()>rangeMax || getUmidita()<rangeMin;
		}
		return false;
	}
	
	public void setDiscovery(boolean discovery){
		_discovery = discovery;
	}
	
	public boolean isDiscovery(){
		return _discovery;
	}

	
	public double getOffset() {
		if (sensore != null)
			return sensore.getOffset();
		return offset;
	}
	
	public void setOffset(double offset){
		this.offset = offset;
	}
	
	public double getOffsetUrt() {
		if (sensore != null)
			return sensore.getOffsetURT();
		return offset_urt;
	}
	
	public void setOffsetUrt(double offset_urt){
		this.offset_urt = offset_urt;
	}
	
	public double getOffsetVolt() {
		if (sensore != null)
			return sensore.getOffsetVolt();
		return offset_volt;
	}
	
	public void setOffsetVolt(double offset_volt){
		this.offset_volt = offset_volt;
	}
	
	public double getOffsetMilliampere() {
		if (sensore != null)
			return sensore.getOffsetMilliampere();
		return offset_milliampere;
	}
	
	public void setOffsetMilliampere(double offset_milliampere){
		this.offset_milliampere = offset_milliampere;
	}
	
	public double getOffsetOhm() {
		if (sensore != null)
			return sensore.getOffsetOhm();
		return offset_ohm;
	}
	
	public void setOffsetOhm(double offset_ohm){
		this.offset_ohm = offset_ohm;
	}
	
 
	public double getIncertezza() {
		if (sensore != null)
			return sensore.getIncertezza();
		return incertezza;
	}
 
	public void setIncertezza(double incertezza){
		this.incertezza = incertezza;
	}
	
	public double getIncertezzaUrt() {
		if (sensore != null)
			return sensore.getIncertezzaURT();
		return incertezza_urt;
	}

	public void setIncertezza_urt(double incertezza_urt) {
		this.incertezza_urt = incertezza_urt;
	}
	
	public void setIncertezzaUrt(double incertezza_urt){
		this.incertezza_urt = incertezza_urt;
	}
	
	public double getMilliampere() {
		if (sensore != null)
			return milliampereGrezzo + sensore.getOffsetMilliampere();
		return milliampereGrezzo;
	}

	public void setMilliampere(double milliampere) {
		this.milliampere = milliampere;
	}

	public double getOhm() {
		if (sensore != null)
			return ohmGrezzo + sensore.getOffsetOhm();
		return ohmGrezzo;
	}

	public void setOhm(double ohm) {
		this.ohm = ohm;
	}

	public double getVolt() {
		if (sensore != null)
			return voltGrezzo + sensore.getOffsetVolt();
		return voltGrezzo;
	}

	public void setVolt(double volt) {
		this.volt = volt;
	}

	public String getApertoChiuso() {
		return apertoChiuso;
	}

	public void setApertoChiuso(String apertoChiuso) {
		this.apertoChiuso = apertoChiuso;
	}
	

	public double getMilliampereGrezzo() {
		return milliampereGrezzo;
	}

	public void setMilliampereGrezzo(double milliampereGrezzo) {
		this.milliampereGrezzo = milliampereGrezzo;
	}

	public double getOhmGrezzo() {
		return ohmGrezzo;
	}

	public void setOhmGrezzo(double ohmGrezzo) {
		this.ohmGrezzo = ohmGrezzo;
	}

	public double getVoltGrezzo() {
		return voltGrezzo;
	}

	public void setVoltGrezzo(double voltGrezzo) {
		this.voltGrezzo = voltGrezzo;
	}

	public Lettura duplicate(){
		Lettura lettura = new Lettura();
		lettura.setIdSonda(getIdSonda());
		lettura.setStatoBatteria(getStatoBatteria());
		lettura.setData(getData());
		lettura.setTipoMisura(getTipoMisura());
		lettura.setTemperaturaGrezza(getTemperaturaGrezza());
		lettura.setUmiditaGrezza(getUmiditaGrezza());
		lettura.setSensore(getSensore());
		lettura.setDiscovery(isDiscovery());
		lettura.setOffset(getOffset());
		lettura.setOffsetUrt(getOffsetUrt());
		lettura.setIncertezza(getIncertezza());
		lettura.setIncertezzaUrt(getIncertezzaUrt());
		lettura.setOhm(getOhm());
		lettura.setOhmGrezzo(getOhmGrezzo());
		lettura.setMilliampere(getMilliampere());
		lettura.setMilliampereGrezzo(getMilliampereGrezzo());
		lettura.setVolt(getVolt());
		lettura.setVoltGrezzo(getVoltGrezzo());
		lettura.setApertoChiuso(getApertoChiuso());
		return lettura;
	}
	
}
