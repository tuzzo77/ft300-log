package com.econorma.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.econorma.Application;
import com.econorma.logic.SensoreManager;

public class AllarmiWindow extends javax.swing.JDialog implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6746683396726822437L;
	private AllarmiPanel allarmiPanel;
	private Dimension minimumSize;
	
	//events managers
	private RowHooverAdapter rowHooverAdapter;
	

	public AllarmiWindow(java.awt.Frame parent, boolean modal) {
		    super(parent, modal);
	        initComponents();
	}
	/**
	 * Create the frame.
	 */
	private void initComponents() {
		
		setFocusable(true);
		addKeyListener(this);

		setLayout(new BorderLayout());

		
		allarmiPanel = new AllarmiPanel();
	 
 	
		minimumSize = new Dimension(1000, 500);
		allarmiPanel.setMinimumSize(minimumSize);
		allarmiPanel.setSize(minimumSize);	
		

		setTitle(Application.getInstance().getBundle().getString("allarmiWindow.titolo"));
 		setMinimumSize(minimumSize);
	
		
		setModal(true);
		 
			
	 	add(allarmiPanel);

			 
	 

		allarmiPanel.bind(SensoreManager.getInstance());

		 
		
		rowHooverAdapter = RowHooverAdapter.registerHoverManager(allarmiPanel.getLettureTables());
		
	}
	

	public AllarmiPanel getAllarmiPanel(){
		return allarmiPanel;
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE)  
		{  
			dispose(); 
		}  
		
	}
	 
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	 

}
