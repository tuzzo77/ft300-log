package com.econorma.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.econorma.logic.SensoreManager;
import com.econorma.resources.Testo;

public class ConversioniWindow extends javax.swing.JDialog implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6746683396726822437L;
	private ConversioniPanel conversioniPanel;
	private Dimension minimumSize;
	
	//events managers
	private RowHooverAdapter rowHooverAdapter;
	

	public ConversioniWindow(java.awt.Frame parent, boolean modal) {
		    super(parent, modal);
	        initComponents();
	}
	/**
	 * Create the frame.
	 */
	private void initComponents() {
		
		setFocusable(true);
		addKeyListener(this);

		setLayout(new BorderLayout());

		
		conversioniPanel = new ConversioniPanel();
	 
 	
		minimumSize = new Dimension(700, 500);
		conversioniPanel.setMinimumSize(minimumSize);
		conversioniPanel.setSize(minimumSize);	
		

		setTitle(Testo.CONVERSIONI_UM);
 		setMinimumSize(minimumSize);
	
		
		setModal(true);
		 
			
	 	add(conversioniPanel);

			 
	 

		conversioniPanel.bind(SensoreManager.getInstance());

		 
		
		rowHooverAdapter = RowHooverAdapter.registerHoverManager(conversioniPanel.getLettureTables());
		
	}
	

	public ConversioniPanel getconversioniPanel(){
		return conversioniPanel;
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE)  
		{  
			dispose(); 
		}  
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	 

}
