package com.econorma.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;

import org.apache.log4j.MDC;
import org.jdesktop.swingx.JXComboBox;
import org.jdesktop.swingx.JXDatePicker;
import org.jfree.chart.JFreeChart;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.charts.Charts;
import com.econorma.charts.ChartsPanel;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.export.ExporterLogger;
import com.econorma.export.ExporterStorico;
import com.econorma.export.pdf.ExporterLoggerPdf;
import com.econorma.logic.LettureProva;
import com.econorma.ui.Events.ExportEvent;
import com.econorma.ui.Events.TabbedPaneEvent;
import com.econorma.ui.ReportChart.REPORT;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

import net.miginfocom.swing.MigLayout;

public class SelectPanel extends JPanel implements DocumentListener {

	private static final String BTN_TEXT_VISUALIZZA = Application.getInstance().getBundle().getString("selectPanel.visualizza");
	private static final String BTN_TEXT_STAMPA = Application.getInstance().getBundle().getString("selectPanel.stampa");
	private static final String BTN_TEXT_EXPORT = "Export";
	private static final String BTN_TEXT_PDF = "PDF";
	private static final String BTN_TEXT_DELETE = Application.getInstance().getBundle().getString("selectPanel.cancella");
	private static final String BTN_TEXT_MAPPA = Application.getInstance().getBundle().getString("selectPanel.mappa");
	private static final String BTN_TEXT_STORICO = Application.getInstance().getBundle().getString("selectPanel.storico");
	private static final String TUTTI = "TUTTI";
	private JButton visualizzaButton;
	private JButton stampaButton;
	private JButton exportButton;
	private JButton pdfButton;
	private JButton deleteButton;
	private JButton storicoButton;
	private static final String TAG = "SelectPanel";
	private static final Logger logger = Logger.getLogger(SelectPanel.class);
	private HashMap<String, Sensore> sensori = new HashMap<String, Sensore>();
	private JLabel idSondaLabel;
	private WideComboBox idSondaValue;
	private JLabel dataDaLabel;
	private JXDatePicker dataDaValue;
	private JLabel dataALabel;
	private JXDatePicker dataAValue;
	private JLabel onlyInAlarmLabel;
	private JCheckBox onlyInAlarmValue;
	private JLabel medieLabel;
	private JCheckBox medieValue;
	private JLabel minutesLabel;
	private JXComboBox minutesValue;
	private boolean onlyInAlarm;
	private boolean onlyMedie;
	private JPanel infos1;
	private JPanel infos2;
	private JPanel infos3;
	private JPanel infos3a;
	private JPanel infos3b;
	private JPanel infos4;
	private JPanel infos5;
	private JPanel infos6;
	private Date dataDa;
	private Date dataA;
	private Dimension maximunSize;
	private JLabel labelSender;
	private JTextField mailSender;
	private List<ChartsPanel> charts;
	private String id_sonda;
	private String des_sonda;
	private int minutes;
	private static final DateFormat filedateformat=new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
	private List<Sensore> loadSensori;
	private String separator = "|";
	private int chartIndex;



	public SelectPanel(final List<ChartsPanel> charts) {

		this.charts = charts;
 
		setLayout(new MigLayout("insets 0", ""));


		idSondaLabel = new JLabel(Application.getInstance().getBundle().getString("selectPanel.sonda"));
		minutesLabel = new JLabel("Min: ");

		loadSensori = Application.getInstance().getDao().loadSensori();


		for (Sensore s : loadSensori) {
			switch (s.getLocation()) {
			case ESTERNO:
				addSensore(s);
				break;
			case INTERNO:
				addSensore(s);
				break;
			default:
			}	 

		}

		Map<String, Sensore> sortedMap = new TreeMap<String, Sensore>(sensori);

		final String[] valori =  new String[500];
		final String[] descrizioni =  new String[500];

		valori[0] = TUTTI;

		int i=1;
		for (Sensore s : sortedMap.values()) {
			String name = null;
			if (s.getDescrizione()!=null){
				name =  s.getId_sonda() + separator + s.getDescrizione();
			} else {
				name =  s.getId_sonda();
			}
			valori[i] = name;
			descrizioni[i] = s.getDescrizione();
			i++;
		}

		final String[] minutesCombo =  new String[]{"10", "15", "30", "60", "120", "240"};


		idSondaValue = new WideComboBox();
		idSondaValue.setPrototypeDisplayValue("Sample value");
		for (String v : valori) {
			if (v!=null)
				idSondaValue.addItem(v);	
		}
		idSondaValue.adjustDropDownMenuWidth();


		minutesValue = new JXComboBox(minutesCombo);

		dataDaLabel = new JLabel(Application.getInstance().getBundle().getString("selectPanel.dataDa"));
		dataDaValue = new JXDatePicker();
		dataDaValue.setFormats(new String[] { "E dd-MM-yyyy HH:mm" });

		dataALabel = new JLabel(Application.getInstance().getBundle().getString("selectPanel.dataA"));
		dataAValue = new JXDatePicker();
		dataAValue.setFormats(new String[] { "E dd-MM-yyyy HH:mm" });

		medieLabel = new JLabel(Application.getInstance().getBundle().getString("selectPanel.creaMedie"));
		medieValue = new JCheckBox();

		onlyInAlarmLabel = new JLabel(Application.getInstance().getBundle().getString("selectPanel.inAllarme"));
		onlyInAlarmValue = new JCheckBox();


		JPanel first = new JPanel(new MigLayout());
		add(first, "north");

		first.add(idSondaLabel, "gapleft 10px, grow");
		first.add(idSondaValue, "width 150!");

		JPanel second = new JPanel();
		second.setLayout(new MigLayout("", "", ""));
		add(second, "north, span");


		second.add(dataDaLabel, "gapleft 5px, grow");
		second.add(dataDaValue,  "width 150!, wrap");

		JPanel third = new JPanel();
		third.setLayout(new MigLayout("", "", ""));
		add(third, "north, span");


		third.add(dataALabel, "gapleft 10px, grow");
		third.add(dataAValue,  "width 150!, wrap");

		JPanel fourth = new JPanel();
		fourth.setLayout(new MigLayout("", "", ""));
		add(fourth, "north, span");


		fourth.add(medieLabel, "gapleft 10px, width 80!, grow");
		fourth.add(medieValue,  "width 23!");
		fourth.add(minutesLabel,  "width 31!");
		fourth.add(minutesValue,  "width 70!, wrap");

		JPanel fifth = new JPanel();
		fifth.setLayout(new MigLayout("", "", ""));
		add(fifth, "north, span");


		fifth.add(onlyInAlarmLabel, "gapleft 10px, grow");
		fifth.add(onlyInAlarmValue,  "width 50!, wrap");


		KeyListener listener = new TextListener();


		idSondaValue.addKeyListener(listener);


		ActionListener DateListenerDa = new DateListenerDa();  
		dataDaValue.addActionListener(DateListenerDa);
		ActionListener DateListenerA = new DateListenerA(); 
		dataAValue.addActionListener(DateListenerA);

		ItemListener inAlarmListener = new InAlarmListener(); 
		onlyInAlarmValue.addItemListener(inAlarmListener);

		ItemListener medieListener = new MedieListener(); 
		medieValue.addItemListener(medieListener);

		ItemListener minutesListener = new MinutesListener(); 
		minutesValue.addItemListener(minutesListener);


		visualizzaButton = new JButton(BTN_TEXT_VISUALIZZA);


		infos1 = createPanel();


		add(infos1, "north, span");


		infos1.add(visualizzaButton, "gapleft 10px, width 100");



		visualizzaButton.addActionListener(new ActionListener() {
 

			@Override
			public void actionPerformed(ActionEvent arg0) {


				minutes = Integer.parseInt((String) minutesValue.getSelectedItem());

				String name = (String) idSondaValue.getSelectedItem();

				String id_sonda = name.split("\\" + separator)[0];
				ExportEvent exportEvent = new Events.ExportEvent(id_sonda, dataDaValue.getDate(), dataAValue.getDate(), onlyInAlarm, onlyMedie, minutes);
				EventBusService.publish(exportEvent);
			
				List<Sensore> sensori = new ArrayList<Sensore>();
				Prova prova = null;
				Sensore s=null;


				int minutes = Integer.parseInt((String) minutesValue.getSelectedItem());

				if (id_sonda.equals(TUTTI)){

					for (Sensore se : loadSensori) {
						switch (se.getLocation()) {
						case ESTERNO:
							sensori.add(se);
							break;
						case INTERNO:
							sensori.add(se);
							break;
						default:
						}	 
					}

				}
				else {
					
					s = Sensore.newInstance(id_sonda, 0, null, null, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null, 0, 0, 0, 0, 0, null, 0, 0, 0, 0, 0, 0, null);
					sensori.add(s);
				}

				List<LetturaProva> findLettureProvaBySonda;
				if (!onlyMedie){
					findLettureProvaBySonda = Application.getInstance().getDao().findLettureProvaBySonda(id_sonda, dataDaValue.getDate(), dataAValue.getDate(), onlyInAlarm);					
				} else {
					findLettureProvaBySonda = Application.getInstance().getDao().findMedieProvaBySonda(id_sonda, dataDaValue.getDate(), dataAValue.getDate(), onlyInAlarm, minutes);					
				}

				LettureProva lettureProva = new LettureProva(prova, findLettureProvaBySonda);

				for (ChartsPanel c: charts) {
					Charts econormaChart = c.getEconormaChart();
					econormaChart.restore(lettureProva, sensori );
				}
		

			}
		});

		stampaButton = new JButton(BTN_TEXT_STAMPA);


		infos2 = createPanel();


		add(infos2, "north, span");


		infos2.add(stampaButton, "gapleft 10px, width 100");




		stampaButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {



				try{


					String targa = "";
					String responsabile = "";
					String durataProva = "";
					String numeroProva = "";
					String laboratorio = "";
					Date dataProva = new Date();
					String idprova = "";
					ReportChart reportChart=null;
					Date dataDa = dataDaValue.getDate();
					Date dataA = dataAValue.getDate();
					String name = (String) idSondaValue.getSelectedItem();

					String[] all = name.split("\\" +separator, 2);
					id_sonda = all[0];
					if (all.length > 1) {
						des_sonda = all[1];	
					} else  {
						des_sonda = "";
					}
					
					JFreeChart chart = SelectPanel.this.charts.get(chartIndex).getEconormaChart().getJFreeChart();
					
					reportChart = new ReportChart(REPORT.EXPORT, chart, responsabile, dataProva, dataDa, dataA, idprova, id_sonda, des_sonda);

					try{
						reportChart.showPrintDialog(SelectPanel.this);
					}catch (Exception e) {
						logger.error(TAG, "Errore nell'apertura del dialog report", e);
					}

				}catch(Exception e){
					logger.error(TAG, e);
					throw new RuntimeException(e);
				}


			}
		});



		exportButton = new JButton(BTN_TEXT_EXPORT);
		pdfButton = new JButton(BTN_TEXT_PDF);
	
		infos3 = createPanel();

		add(infos3, "north, span");


		infos3.add(exportButton, "gapleft 10px, width 100");



		exportButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				String name = (String) idSondaValue.getSelectedItem();
				String[] all = name.split("\\"+separator, 2);
				id_sonda = all[0];
				if (all.length > 1) {
					des_sonda = all[1];	
				} else  {
					des_sonda = "";
				}

				minutes = Integer.parseInt((String) minutesValue.getSelectedItem());

				final Date dataDa = dataDaValue.getDate();
				final Date dataA = dataAValue.getDate();

				final JFileChooser jFileChooser = new JFileChooser();
				jFileChooser.addChoosableFileFilter(new ExcelFileFilter());
				jFileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
				jFileChooser.setFileHidingEnabled(true);

				File f=null;
				try {
					f = new File(new File(".").getCanonicalPath());
				} catch (IOException e1) {

					e1.printStackTrace();
				}
				jFileChooser.setCurrentDirectory(f);



				jFileChooser.setSelectedFile(new File("Logger__"+id_sonda+"__"+filedateformat.format(dataDa)+ ".xls"));

				jFileChooser.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {

						if (arg0.getActionCommand().equals(JFileChooser.CANCEL_SELECTION))
							return;

						try{
							File selectedFile =  jFileChooser.getSelectedFile();
							if(selectedFile!=null){

								MDC.put("event_name", "Export");


								ExporterLogger exporterLogger = new ExporterLogger(id_sonda, des_sonda, dataDa, dataA, onlyInAlarm, onlyMedie, minutes);
								if(selectedFile.isDirectory()){
									logger.info(TAG, "Salvataggio su cartella selezionato: "+selectedFile.getAbsolutePath());
									logger.log(LoggerCustomLevel.AUDIT, "Export eseguito per sonda: " + id_sonda + "-" +sdf.format(dataDa) + "-" + sdf.format(dataA));
									exporterLogger.export(selectedFile);
								}else{
									logger.info(TAG, "Salvataggio su file selezionato: "+selectedFile.getAbsolutePath());
									logger.log(LoggerCustomLevel.AUDIT, "Export eseguito per sonda: " + id_sonda + "-" +sdf.format(dataDa) + "-" + sdf.format(dataA));
									File parent = selectedFile.getParentFile();
									String fileName = selectedFile.getAbsolutePath().substring(parent.getAbsolutePath().length()+1);
									exporterLogger.export(parent, fileName);
								}




							}

						}catch(Exception e){
							logger.error(TAG, e);
							throw new RuntimeException(e);
						}
					}
				});
				int returnVal = jFileChooser.showDialog(Application.getInstance().getMainFrame(), "Salva");
			}


			class ExcelFileFilter extends FileFilter {

				public boolean accept(File file) {
					if (file.isDirectory()) return true;
					String fname = file.getName().toLowerCase();
					return fname.endsWith("xls");
				}

				public String getDescription() {
					return "Documento Excel '97";
				}
			}


		});

		pdfButton = new JButton(BTN_TEXT_PDF);

		infos3a = createPanel();

		add(infos3a, "north, span");


		infos3a.add(pdfButton, "gapleft 10px, width 100");

		pdfButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				String name = (String) idSondaValue.getSelectedItem();
				String[] all = name.split("\\" +separator, 2);
				id_sonda = all[0];
				if (all.length > 1) {
					des_sonda = all[1];	
				} else  {
					des_sonda = "";
				}

				minutes = Integer.parseInt((String) minutesValue.getSelectedItem());

				final Date dataDa = dataDaValue.getDate();
				final Date dataA = dataAValue.getDate();

				final JFileChooser jFileChooser = new JFileChooser();
				jFileChooser.addChoosableFileFilter(new PdfFileFilter());
				jFileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
				jFileChooser.setFileHidingEnabled(true);

				File f=null;
				try {
					f = new File(new File(".").getCanonicalPath());
				} catch (IOException e1) {

					e1.printStackTrace();
				}
				jFileChooser.setCurrentDirectory(f);



				jFileChooser.setSelectedFile(new File("Logger__"+id_sonda+"__"+filedateformat.format(dataDa)+ ".pdf"));

				jFileChooser.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {

						if (arg0.getActionCommand().equals(JFileChooser.CANCEL_SELECTION))
							return;

						try{
							File selectedFile =  jFileChooser.getSelectedFile();
							if(selectedFile!=null){

								MDC.put("event_name", "PDF");


								ExporterLoggerPdf exporterPdf = new ExporterLoggerPdf(id_sonda, des_sonda, dataDa, dataA, onlyInAlarm, onlyMedie, minutes);
								if(selectedFile.isDirectory()){
									logger.info(TAG, "Salvataggio su cartella selezionato: "+selectedFile.getAbsolutePath());
									logger.log(LoggerCustomLevel.AUDIT, "Export eseguito per sonda: " + id_sonda + "-" +sdf.format(dataDa) + "-" + sdf.format(dataA));
									exporterPdf.export(selectedFile);
								}else{
									logger.info(TAG, "Salvataggio su file selezionato: "+selectedFile.getAbsolutePath());
									logger.log(LoggerCustomLevel.AUDIT, "Export eseguito per sonda: " + id_sonda + "-" +sdf.format(dataDa) + "-" + sdf.format(dataA));
									File parent = selectedFile.getParentFile();
									String fileName = selectedFile.getAbsolutePath().substring(parent.getAbsolutePath().length()+1);
									exporterPdf.export(parent, fileName);
								}

							}

						}catch(Exception e){
							logger.error(TAG, e);
							throw new RuntimeException(e);
						}
					}
				});
				int returnVal = jFileChooser.showDialog(Application.getInstance().getMainFrame(), "Salva");
			}


			class PdfFileFilter extends FileFilter {

				public boolean accept(File file) {
					if (file.isDirectory()) return true;
					String fname = file.getName().toLowerCase();
					return fname.endsWith("pdf");
				}

				public String getDescription() {
					return "PDF Portable Document Format";
				}
			}


		});
		
		storicoButton = new JButton(BTN_TEXT_STORICO);
		
		infos6 = createPanel();

		add(infos6, "north, span");


		infos6.add(storicoButton, "gapleft 10px, width 100");


		storicoButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				String name = (String) idSondaValue.getSelectedItem();
				String[] all = name.split("\\"+separator, 2);
				id_sonda = all[0];
				if (all.length > 1) {
					des_sonda = all[1];	
				} else  {
					des_sonda = "";
				}

				final Date dataDa = dataDaValue.getDate();
				final Date dataA = dataAValue.getDate();

				final JFileChooser jFileChooser = new JFileChooser();
				jFileChooser.addChoosableFileFilter(new TextFilter());
				jFileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
				jFileChooser.setFileHidingEnabled(true);

				File f=null;
				try {
					f = new File(new File(".").getCanonicalPath());
				} catch (IOException e1) {

					e1.printStackTrace();
				}
				jFileChooser.setCurrentDirectory(f);



				jFileChooser.setSelectedFile(new File("Storico_" +id_sonda+"__"+filedateformat.format(dataDa)+ ".csv"));

				jFileChooser.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {

						if (arg0.getActionCommand().equals(JFileChooser.CANCEL_SELECTION))
							return;

						try{
							File selectedFile =  jFileChooser.getSelectedFile();
							if(selectedFile!=null){

								MDC.put("event_name", "Export Storico");

									ExporterStorico  exporterStorico = new ExporterStorico(id_sonda, des_sonda, dataDa, dataA);
									logger.info(TAG, "Salvataggio su file selezionato: "+selectedFile.getAbsolutePath());
									logger.log(LoggerCustomLevel.AUDIT, "Export eseguito per sonda: " + id_sonda + "-" +sdf.format(dataDa) + "-" + sdf.format(dataA));
									File parent = selectedFile.getParentFile();
									String fileName = selectedFile.getAbsolutePath().substring(parent.getAbsolutePath().length()+1);
									exporterStorico.export(parent, fileName);
								

							}

						}catch(Exception e){
							logger.error(TAG, e);
							throw new RuntimeException(e);
						}
					}
				});
				int returnVal = jFileChooser.showDialog(Application.getInstance().getMainFrame(), "Salva");
			}


			class TextFilter extends FileFilter {

				public boolean accept(File file) {
					if (file.isDirectory()) return true;
					String fname = file.getName().toLowerCase();
					return fname.endsWith("csv");
				}

				public String getDescription() {
					return "Comma Separated Values";
				}
			}


		});

		deleteButton = new JButton(BTN_TEXT_DELETE);


		infos5 = createPanel();

		add(infos5, "north, span");


		infos5.add(deleteButton, "gapleft 10px, width 100");



		deleteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				final String name = (String) idSondaValue.getSelectedItem();
				id_sonda = name.split("\\" +separator)[0];

				int reply = JOptionPane.showConfirmDialog(null, "Confermi eliminazione definitiva dei dati selezionati?", "Cancella Dati", JOptionPane.YES_NO_OPTION);

				if (reply == JOptionPane.YES_OPTION) {

					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {

							MDC.put("event_name", "Cancellazione");

							ExportEvent exportEvent = new Events.ExportEvent(id_sonda, dataDaValue.getDate(), dataAValue.getDate(), onlyInAlarm, onlyMedie, minutes);
							EventBusService.publish(exportEvent);


							Application.getInstance().getDao().deleteLettureProvaBySonda(id_sonda, dataDaValue.getDate(), dataAValue.getDate());
							Application.getInstance().getDao().deleteLettureBySonda(id_sonda, dataDaValue.getDate(), dataAValue.getDate());
							JOptionPane.showMessageDialog(null, "Cancellazione eseguita con successo");
							logger.log(LoggerCustomLevel.AUDIT, "Cancellazione eseguito per sonda: " + id_sonda + "-" +sdf.format(dataDaValue.getDate()) + "-" + sdf.format(dataAValue.getDate()));
						}
					});

				}
				else {
					return;
				}

			}


		});


		
		EventBusService.subscribe(this);




	}

 

	@EventHandler
	public void handleEvent(final TabbedPaneEvent tabEvent){


		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				chartIndex = tabEvent.index;
			}
		});


	}


	@Override
	public void changedUpdate(DocumentEvent e) {

	}

	@Override
	public void insertUpdate(DocumentEvent e) {

	}

	@Override
	public void removeUpdate(DocumentEvent e) {

	}



	public class TextListener implements KeyListener
	{
		@Override
		public void keyPressed(KeyEvent e) {

		}

		@Override
		public void keyReleased(KeyEvent e) {

		}

		@Override
		public void keyTyped(KeyEvent e) {

		}
	}



	public class DateListenerDa implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			GregorianCalendar c=new GregorianCalendar() ;
			c.setTime(dataDaValue.getDate());	
			int giorno=c.get(Calendar.DAY_OF_MONTH);
			int anno=c.get(Calendar.YEAR);
			int mese=c.get(Calendar.MONTH);
			GregorianCalendar c1=new GregorianCalendar() ;
			c1.set(anno, mese, giorno);
			c1.set(Calendar.HOUR_OF_DAY, 01);
			c1.set(Calendar.MINUTE, 0);
			c1.set(Calendar.SECOND, 0);
			c1.set(Calendar.MILLISECOND, 0);
			dataDa=c1.getTime();
			dataDaValue.setDate(dataDa);

		}

	}

	public class DateListenerA implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			GregorianCalendar c=new GregorianCalendar() ;
			c.setTime(dataAValue.getDate());	
			int giorno=c.get(Calendar.DAY_OF_MONTH);
			int anno=c.get(Calendar.YEAR);
			int mese=c.get(Calendar.MONTH);
			GregorianCalendar c1=new GregorianCalendar() ;
			c1.set(anno, mese, giorno);
			c1.set(Calendar.HOUR_OF_DAY, 23);
			c1.set(Calendar.MINUTE, 59);
			c1.set(Calendar.SECOND, 0);
			c1.set(Calendar.MILLISECOND, 0);
			dataA=c1.getTime();
			dataAValue.setDate(dataA);

		}

	}

	public class InAlarmListener implements ItemListener
	{

		@Override
		public void itemStateChanged(ItemEvent e) {
			if(onlyInAlarmValue.isSelected()){
				onlyInAlarm = true;
			} else {
				onlyInAlarm = false;
			}
		}

	}

	public class MedieListener implements ItemListener
	{

		@Override
		public void itemStateChanged(ItemEvent e) {
			if(medieValue.isSelected()){
				onlyMedie = true;
			} else {
				onlyMedie = false;
			}
		}

	}

	public class MinutesListener implements ItemListener
	{

		@Override
		public void itemStateChanged(ItemEvent event) {
			if (event.getStateChange() == ItemEvent.SELECTED) {
				minutes = Integer.parseInt((String) event.getItem());
			}
		}       

	}

	private void addSensore(Sensore sensore) {
		Sensore s = sensori.get(sensore.getId_sonda());
		sensori.put(sensore.getId_sonda(), sensore);
	}

	private JPanel createPanel()
	{
		JPanel jp = new JPanel();
		jp.setLayout(new MigLayout("", "", ""));
		return jp;
	}


	 

	public Date getDateFromHour(Date date, int hours, int minutes){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, hours);
		calendar.set(Calendar.MINUTE, minutes);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}

	public File getDir(){
		String path = System.getProperty("user.dir");
		File dir = null;
		if (path!=null && path.trim().length() > 0 ){
			dir = new File(path);
		} else {
			dir = new File(".");
		}
		return dir;
	}

}
