package com.econorma.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.jdesktop.swingx.JXDatePicker;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Log;
import com.econorma.ui.Events.LogEvent;
import com.econorma.util.Logger;

import net.miginfocom.swing.MigLayout;

public class LogSelectionPanel extends JPanel implements DocumentListener {

	private static final String BTN_TEXT_VISUALIZZA = " Visualizza";
	private static final String BTN_TEXT_STAMPA = " Stampa";
	private JButton visualizzaButton;
	private JButton stampaButton;
	private static final String TAG = "LogSelectionPanel";
	private static final Logger logger = Logger.getLogger(LogSelectionPanel.class);
	private JLabel dataDaLabel;
	private JXDatePicker dataDaValue;
	private JLabel dataALabel;
	private JXDatePicker dataAValue;
	private JPanel infos1;
 	private Date dataDa;
	private Date dataA;
	private Dimension maximunSize;
  

	public LogSelectionPanel() {

	 
		dataDaLabel = new JLabel(Application.getInstance().getBundle().getString("logSelection.dataDa"));
		dataDaValue = new JXDatePicker();
		dataDaValue.setFormats(new String[] { "E dd-MM-yyyy HH:mm" });
		dataDaValue.setDate(new Date());

		dataALabel = new JLabel(Application.getInstance().getBundle().getString("logSelection.dataA"));
		dataAValue = new JXDatePicker();
		dataAValue.setFormats(new String[] { "E dd-MM-yyyy HH:mm" });
		dataAValue.setDate(new Date());



		JPanel first = new JPanel(new MigLayout());
		add(first, "north");
 
		JPanel second = new JPanel();
		second.setLayout(new MigLayout("", "", ""));
		add(second, "north, span");


		second.add(dataDaLabel, "gapleft 10px, grow");
		second.add(dataDaValue,  "width 150!, wrap");

		JPanel third = new JPanel();
		third.setLayout(new MigLayout("", "", ""));
		add(third, "north, span");


		third.add(dataALabel, "gapleft 10px, grow");
		third.add(dataAValue,  "width 150!, wrap");


		KeyListener listener = new TextListener();

 

		ActionListener DateListenerDa = new DateListenerDa();  
		dataDaValue.addActionListener(DateListenerDa);
		ActionListener DateListenerA = new DateListenerA(); 
		dataAValue.addActionListener(DateListenerA);



		visualizzaButton = new JButton(Application.getInstance().getBundle().getString("logSelectionPanel.visualizza"));
		stampaButton = new JButton(Application.getInstance().getBundle().getString("logSelectionPanel.stampa"));


		infos1 = createPanel();


		add(infos1, "north, span");


		infos1.add(visualizzaButton, "gapleft 10px, width 100");
		infos1.add(stampaButton, "gapleft 50px, width 100");



		visualizzaButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				LogEvent logEvent = new Events.LogEvent(dataDaValue.getDate(), dataAValue.getDate());
				EventBusService.publish(logEvent);
			}
		});
		
		stampaButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				 List<Log> findLogByDate = Application.getInstance().getDao().findLogByDate(dataDaValue.getDate(), dataAValue.getDate());
				
				ReportLog reportLog = new ReportLog(findLogByDate, dataDaValue.getDate(), dataAValue.getDate());
				try{
					reportLog.showPrintDialog(LogSelectionPanel.this);
				}catch (Exception e) {
					logger.error(TAG, "Errore nell'apertura del dialog report", e);
				}
				
			}
		});

		
		
		EventBusService.subscribe(this);
		
	}


	@EventHandler
	public void handleEvent(String event) {

	}




	@Override
	public void changedUpdate(DocumentEvent e) {

	}

	@Override
	public void insertUpdate(DocumentEvent e) {

	}

	@Override
	public void removeUpdate(DocumentEvent e) {

	}



	public class TextListener implements KeyListener
	{
		@Override
		public void keyPressed(KeyEvent e) {

		}

		@Override
		public void keyReleased(KeyEvent e) {

		}

		@Override
		public void keyTyped(KeyEvent e) {

		}
	}



	public class DateListenerDa implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			GregorianCalendar c=new GregorianCalendar() ;
			c.setTime(dataDaValue.getDate());	
			int giorno=c.get(Calendar.DAY_OF_MONTH);
			int anno=c.get(Calendar.YEAR);
			int mese=c.get(Calendar.MONTH);
			GregorianCalendar c1=new GregorianCalendar() ;
			c1.set(anno, mese, giorno);
			c1.set(Calendar.HOUR_OF_DAY, 01);
			c1.set(Calendar.MINUTE, 0);
			c1.set(Calendar.SECOND, 0);
			c1.set(Calendar.MILLISECOND, 0);
			dataDa=c1.getTime();
			dataDaValue.setDate(dataDa);

		}

	}

	public class DateListenerA implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			GregorianCalendar c=new GregorianCalendar() ;
			c.setTime(dataAValue.getDate());	
			int giorno=c.get(Calendar.DAY_OF_MONTH);
			int anno=c.get(Calendar.YEAR);
			int mese=c.get(Calendar.MONTH);
			GregorianCalendar c1=new GregorianCalendar() ;
			c1.set(anno, mese, giorno);
			c1.set(Calendar.HOUR_OF_DAY, 23);
			c1.set(Calendar.MINUTE, 59);
			c1.set(Calendar.SECOND, 0);
			c1.set(Calendar.MILLISECOND, 0);
			dataA=c1.getTime();
			dataAValue.setDate(dataA);

		}

	}
 

	private JPanel createPanel()
	{
		JPanel jp = new JPanel();
		jp.setLayout(new MigLayout("", "", ""));
		return jp;
	}


	 
}
