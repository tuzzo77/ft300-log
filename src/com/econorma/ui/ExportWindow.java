package com.econorma.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jdesktop.swingx.JXStatusBar;

import com.adamtaft.eb.EventBusService;
import com.econorma.Application;
import com.econorma.Application.TIPO_MISURE;
import com.econorma.charts.ChartsPanel;
import com.econorma.logic.SensoreManager;
import com.econorma.persistence.Preferences;
import com.econorma.ui.Events.TabbedPaneEvent;



public class ExportWindow extends JDialog implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6746683396726822437L;
	private SelectPanel exportPanel;
	private SelectPanel selectPanel;
	private DataPanel dataPanel;
	private JXStatusBar statusBar;
	private Dimension minimumSize;
	private Dimension entrySize;
	private Dimension panelSize;
	private Dimension chartSize;
	private List<ChartsPanel> chartsPanel;
	private Preferences preferences = Application.getInstance().getPreferences();
	private int x;
	private int y; 
	private int w; 
	private int h; 
	private int wChart;
	private int wData;


	//events managers
	private RowHooverAdapter rowHooverAdapter;
	private static Application app = Application.getInstance();


	/**
	 * Create the frame.
	 */

	public ExportWindow(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
	}


	private void initComponents() {

		loadDialog();

		setFocusable(true);
		addKeyListener(this);

		setLayout(new BorderLayout());
		
		chartsPanel = new ArrayList<ChartsPanel>();
		
		final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addChangeListener(new ChangeListener() {
		        public void stateChanged(ChangeEvent e) {
		            int index= tabbedPane.getSelectedIndex();
					TabbedPaneEvent tabEvent = new Events.TabbedPaneEvent(index);
					EventBusService.publish(tabEvent);
		        }
		    });

		switch (Application.getInstance().getTheme()) {
		case DARK:
			tabbedPane.setUI(new MyTabbedPaneUI());
			break;
		case LIGHT:
			break;
		}
		
		boolean isTemperature = false;
		
		if (Application.getInstance().isTemperature()) {
			ChartsPanel tempChartPanel = new ChartsPanel(SensoreManager.getInstance(), true, TIPO_MISURE.TEMPERATURA);
		    chartsPanel.add(tempChartPanel);
			tabbedPane.addTab(Application.getInstance().getBundle().getString("exportWindow.temperatura"), tempChartPanel );
			EventBusService.subscribe(tempChartPanel);
			isTemperature=true;
		}
		if (Application.getInstance().isTemperatureHumidity()) {
			if (!isTemperature) {
				ChartsPanel tempChartPanel = new ChartsPanel(SensoreManager.getInstance(), true, TIPO_MISURE.TEMPERATURA);
			    chartsPanel.add(tempChartPanel);
				tabbedPane.addTab(Application.getInstance().getBundle().getString("exportWindow.temperatura"), tempChartPanel );
				EventBusService.subscribe(tempChartPanel);
			}
			ChartsPanel urtChartPanel = new ChartsPanel(SensoreManager.getInstance(), true, TIPO_MISURE.TEMPERATURA_UMIDITA);
		    chartsPanel.add(urtChartPanel);
			tabbedPane.addTab(Application.getInstance().getBundle().getString("exportWindow.umidita"), urtChartPanel);
			EventBusService.subscribe(urtChartPanel);
		}
		if (Application.getInstance().isVolt()) {
			ChartsPanel voltChartPanel = new ChartsPanel(SensoreManager.getInstance(), true, TIPO_MISURE.VOLT);
		    chartsPanel.add(voltChartPanel);
			tabbedPane.addTab(Application.getInstance().getBundle().getString("exportWindow.volt"),  voltChartPanel);
			EventBusService.subscribe(voltChartPanel);
		}
		if (Application.getInstance().isMilliampere()) {
			ChartsPanel milliChartPanel = new ChartsPanel(SensoreManager.getInstance(), true, TIPO_MISURE.MILLIAMPERE);
		    chartsPanel.add(milliChartPanel);
			tabbedPane.addTab(Application.getInstance().getBundle().getString("exportWindow.milliampere"), milliChartPanel);
			EventBusService.subscribe(milliChartPanel);
		}
		if (Application.getInstance().isOhm()) {
			ChartsPanel ohmChartPanel = new ChartsPanel(SensoreManager.getInstance(), true, TIPO_MISURE.OHM);
		    chartsPanel.add(ohmChartPanel);
			tabbedPane.addTab(Application.getInstance().getBundle().getString("exportWindow.ohm"), ohmChartPanel);
			EventBusService.subscribe(ohmChartPanel);
		}
		if (Application.getInstance().isApertoChiuso()) {
			ChartsPanel acChartPanel = new ChartsPanel(SensoreManager.getInstance(), true, TIPO_MISURE.APERTO_CHIUSO);
		    chartsPanel.add(acChartPanel);
			tabbedPane.addTab(Application.getInstance().getBundle().getString("exportWindow.apertoChiuso"),  acChartPanel);
			EventBusService.subscribe(acChartPanel);
		}

		selectPanel = new SelectPanel(chartsPanel);
		dataPanel = new DataPanel();
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		entrySize = new Dimension((int) (screenSize.width/1.2), screenSize.height/2);

		
		minimumSize = new Dimension(800, 500);

		setMinimumSize(minimumSize);
		setSize(entrySize);

		if (x!=0){
			setBounds(x, y, w, h);
		}



		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {

				saveDialog();

			}  
		});

		setTitle(Application.getInstance().getBundle().getString("exportWindow.titolo"));
	 
		JSplitPane splitPane1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, 
				selectPanel, dataPanel);

		JSplitPane splitPane2 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, 
				splitPane1, tabbedPane);

		splitPane1.setOneTouchExpandable(false);
		splitPane1.setMaximumSize(new Dimension(Integer.MAX_VALUE,
				Integer.MAX_VALUE));

		splitPane2.setOneTouchExpandable(false);
		splitPane2.setMaximumSize(new Dimension(Integer.MAX_VALUE,
				Integer.MAX_VALUE));

		add(splitPane2);

//		minimumSize = new Dimension(380, 50);
		 
			minimumSize = new Dimension(600, 50);
	 
		panelSize = new Dimension(240, 50);
		chartSize = new Dimension(400, 50);
		selectPanel.setMinimumSize(panelSize);
		dataPanel.setMinimumSize(minimumSize);
//		chartPanel.setMinimumSize(chartSize);

//		if (wData!=0){
//			minimumSize = new Dimension(wData, 50);
// 			dataPanel.setMinimumSize(minimumSize);
//		}
//		if (wChart!=0){
//			chartSize = new Dimension(wChart, 50);
//			dataPanel.setMinimumSize(chartSize);
//		}
 
		splitPane1.addComponentListener(new ComponentListener() {

			@Override
			public void componentShown(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentResized(ComponentEvent event) {
				dataPanel.setMaximumSize(new Dimension());

			}

			@Override
			public void componentMoved(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentHidden(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}
		});


		splitPane2.addComponentListener(new ComponentListener() {

			@Override
			public void componentShown(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentResized(ComponentEvent event) {
				dataPanel.setMaximumSize(new Dimension());

			}

			@Override
			public void componentMoved(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentHidden(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		rowHooverAdapter = RowHooverAdapter.registerHoverManager(dataPanel.getLettureTables());

	}

	public DataPanel getDataPanel(){
		return dataPanel;
	}

	private static class MyStatusBar extends JXStatusBar {

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE)  
		{  
			saveDialog();
			dispose(); 
		}  

	}


	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	public void saveDialog()  {
		preferences.setX(getX());
		preferences.setY(getY());
		preferences.setW(getWidth());
		preferences.setH(getHeight());
		preferences.setWChart(dataPanel.getWidth());
//		preferences.setWData(chartPanel.getWidth());

	}

	public void loadDialog()  {
		x = preferences.getX();
		y = preferences.getX();
		w = preferences.getW();
		h = preferences.getH();
		wChart = preferences.getWchart();
		wData = preferences.getWData();
	}


}

