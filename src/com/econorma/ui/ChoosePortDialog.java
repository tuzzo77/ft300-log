package com.econorma.ui;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;

import org.apache.log4j.MDC;

import com.adamtaft.eb.EventBusService;
import com.econorma.Application;
import com.econorma.logic.SerialUtil;
import com.econorma.persistence.Preferences;
import com.econorma.resources.Testo;
import com.econorma.ui.MainMenuBar.PORT;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;



//http://zetcode.com/tutorials/javaswingtutorial/swingdialogs/
public class ChoosePortDialog extends JDialog implements ActionListener, KeyListener {

	private PortaSerialeWrapper oldCpi;
	private int baudRate;
	private NewJPanel panel;
	private static PORT tipo; 
	private static DefaultComboBoxModel baudRateModel;
	private static final Logger logger = Logger.getLogger(ChoosePortDialog.class);
	private static final String TAG = ChoosePortDialog.class.getSimpleName();
	
	 

	// private JComboBox selectPorte;

	public ChoosePortDialog(Window owner, String title,
			ModalityType modalityType, com.econorma.ui.MainMenuBar.PORT type) {
	
		super(owner, title, modalityType);
		this.tipo = type;
		init();
		 
		 
	     }
	
	public void init(){

		setFocusable(true);
		addKeyListener(this);

		
		super.dialogInit();
		 
		// se nel sistema non sono presenti porte viene gestito prima di aprire
		// il dialog
//		List<CommPortIdentifier> serialPorts = SerialUtil.getSerialPorts();
		
		String[] serialPorts = SerialUtil.getSerialPorts();
		int size = serialPorts.length;
		
		
 		List<PortaSerialeWrapper> wrappers = new ArrayList<PortaSerialeWrapper>(
 				size);

		Preferences preferences = Application.getInstance().getPreferences();
		String port =null;
	 
		
		switch (tipo) {
		case COM:
			port = preferences.getPort();
			baudRate = preferences.getBaudRate();
			break;
		case MODEM:
			port = preferences.getPortModem();
			baudRate=Testo.BAUDRATE_9600;
			break;
		default:
			break;
		}
		
		
		for(String s : serialPorts) {
			if (s != null) {
				PortaSerialeWrapper w = new PortaSerialeWrapper(s);
				String identifier = w.getIdentifier();
				wrappers.add(w);
				if(!Preferences.isPortDisabled(port)){
					if (port != null && w.toString() != null) {
						if (port.equals(identifier)) {
							oldCpi = w;
						}
					}
			 	}
			}
			 
		}
		
 

		panel = new NewJPanel();
		add(panel);
		
		Object[] menuChoises = new Object[wrappers.size()+1];
		
		int i = 0;
		 for( PortaSerialeWrapper pw : wrappers) {
		        menuChoises[i]=pw.getIdentifier();
		        i++;
	        }

// 		System.arraycopy(wrappers.toArray(), 0, menuChoises, 0, wrappers.size());
 	 
		menuChoises[menuChoises.length-1] = Application.getInstance().getBundle().getString("choosePortDialog.disabilita");
		
		panel.getSelectPorte().setModel(new DefaultComboBoxModel(menuChoises));

		
		if (oldCpi != null) {
//			panel.getSelectPorte().setSelectedItem(oldCpi);
 			panel.getSelectPorte().setSelectedIndex(wrappers.indexOf(oldCpi));
		}else if(Preferences.isPortDisabled(port)){
			panel.getSelectPorte().setSelectedIndex(menuChoises.length-1);
		}
		
		if (baudRate != 0) {
			int index = baudRateModel.getIndexOf(Integer.toString(baudRate));
			panel.getSelectBaudRate().setSelectedIndex(index); 
		}

		panel.getOkButton().addActionListener(this);
		panel.getCancelButton().addActionListener(this);
	}

 
	private static class PortaSerialeWrapper {

		private String cpi;

		public PortaSerialeWrapper(String cpi) {
			this.cpi = cpi;
		}
		
		public String getIdentifier() {
			return cpi;
		}

	}

	@Override
	public void actionPerformed(ActionEvent event) {

		if (event.getSource() == panel.getOkButton()) {
			Object selectedItem =   panel.getSelectPorte().getSelectedItem();
			Object selectBaudRate =   panel.getSelectBaudRate().getSelectedItem();
			
			if (selectedItem != null) {
				
				if (selectedItem.toString().equals(Application.getInstance().getBundle().getString("choosePortDialog.disabilita"))){
					Preferences preferences = Application.getInstance()
							.getPreferences();
					
					MDC.put("event_name", "COM port");
					
					switch (tipo) {
					case COM:
						preferences.setPort(Preferences.Values.PORT_DISABLED);
						logger.log(LoggerCustomLevel.AUDIT, "COM port Disabilitata");
						EventBusService.publish(new Events.PortChanged(null, baudRate));
						break;
					case MODEM:
						preferences.setPortModem(Preferences.Values.PORT_DISABLED);
						logger.log(LoggerCustomLevel.AUDIT, "COM port Modem Disabilitata");
						EventBusService.publish(new Events.PortModemChanged(null));
						
						break;
					default:
						break;
					}
				} else {
					
//					PortaSerialeWrapper w = (PortaSerialeWrapper) selectedItem;
					String w = selectedItem.toString();
					int b = Integer.parseInt(selectBaudRate.toString());
					
					MDC.put("event_name", "COM port");
					
					if (oldCpi == null	|| !w.equals(oldCpi.getIdentifier()) || (b!=baudRate)) {
						// ho selezionato una nuova porta diversa dalla precedente
						// salva
						Preferences preferences = Application.getInstance()
								.getPreferences();
						
						switch (tipo) {
						case COM:
							preferences.setPort(w);
							preferences.setBaudRate(b);
							logger.log(LoggerCustomLevel.AUDIT, "COM port Modificata: " + w);
							EventBusService.publish(new Events.PortChanged(w, b));
							break;
						case MODEM:
							preferences.setPortModem(w);
							preferences.setBaudRate(Testo.BAUDRATE_9600);
							logger.log(LoggerCustomLevel.AUDIT, "COM port Modem Modificata: " + w);
							EventBusService.publish(new Events.PortModemChanged(w));
							
							break;
						default:
							break;
						}
					}
				}
			
				
 
				
				dispose();
			} else {
				// errore è necessario selezionare una porta }
			}
		}

		if (event.getSource() == panel.getCancelButton()) {
			dispose();
		}
	}

	/**
	 * 
	 * @author marcobettiol
	 */
	private static class NewJPanel extends javax.swing.JPanel {

		/** Creates new form NewJPanel */
		public NewJPanel() {
			initComponents();
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		public void initComponents() {

			jLabel1 = new javax.swing.JLabel();
			jLabel2 = new javax.swing.JLabel();
			selectPorte = new javax.swing.JComboBox();
			selectBaudRate = new javax.swing.JComboBox();
			jButton1 = new javax.swing.JButton();
			jButton2 = new javax.swing.JButton();

			setName("Form"); // NOI18N
			
			switch (tipo) {
			case COM:
				jLabel1.setText(Application.getInstance().getBundle().getString("choosePortDialog.portaSeriale"));
				break;
			case MODEM:
				jLabel1.setText(Application.getInstance().getBundle().getString("choosePortDialog.portaSerialeModem"));	
				break;
			default:
				break;
				
			}
			jLabel2.setText(Testo.BAUDRATE);

			selectPorte.setModel(new javax.swing.DefaultComboBoxModel(
					new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
			
			baudRateModel = new DefaultComboBoxModel(new String[] { "9600", "19200", "115200"});
			
			selectBaudRate.setModel(baudRateModel);

			jButton1.setText(Application.getInstance().getBundle().getString("choosePortDialog.applica")); // NOI18N

			jButton2.setText(Application.getInstance().getBundle().getString("choosePortDialog.annulla")); // NOI18N

			org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(
					this);
			this.setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							org.jdesktop.layout.GroupLayout.LEADING)
					.add(layout
							.createSequentialGroup()
							.add(20, 20, 20)
							.add(layout
									.createParallelGroup(
											org.jdesktop.layout.GroupLayout.LEADING)
									.add(layout
											.createSequentialGroup()
											.add(jLabel1,
													org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
													100,
													org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(
													org.jdesktop.layout.LayoutStyle.RELATED)
											.add(selectPorte, 0, 281,
													Short.MAX_VALUE)
										 	)
										 	
										 	.add(layout
											.createSequentialGroup()
											.add(jLabel2,
													org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
													100,
													org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(
													org.jdesktop.layout.LayoutStyle.RELATED)
											.add(selectBaudRate, 0, 100, 100 
													 )
										 	)
								 	
									.add(org.jdesktop.layout.GroupLayout.TRAILING,
											layout.createSequentialGroup()
													.add(jButton1)
													.add(18, 18, 18)
													.add(jButton2)))
							.addContainerGap()));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							org.jdesktop.layout.GroupLayout.LEADING)
					.add(layout
							.createSequentialGroup()
							.addContainerGap()
							.add(layout
									.createParallelGroup(
											org.jdesktop.layout.GroupLayout.BASELINE)
									.add(jLabel1,
											org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
											40,
											org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
									.add(selectPorte,
											org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
											org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
											org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
						    .addPreferredGap(
									org.jdesktop.layout.LayoutStyle.RELATED)
							.add(layout
									.createParallelGroup(
											org.jdesktop.layout.GroupLayout.BASELINE)
									.add(jLabel2).add(selectBaudRate))
							.addPreferredGap(
									org.jdesktop.layout.LayoutStyle.RELATED)
							.add(layout
									.createParallelGroup(
											org.jdesktop.layout.GroupLayout.BASELINE)
									.add(jButton2).add(jButton1))
							.addContainerGap(
									org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
									Short.MAX_VALUE)));
		}// </editor-fold>
			// Variables declaration - do not modify

		private javax.swing.JButton jButton1;
		private javax.swing.JButton jButton2;
		private javax.swing.JComboBox selectPorte;
		private javax.swing.JComboBox selectBaudRate;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel2;

		// End of variables declaration

		public JComboBox getSelectPorte() {
			return selectPorte;
		}
		
		public JComboBox getSelectBaudRate() {
			return selectBaudRate;
		}

		public JButton getOkButton() {
			return jButton1;
		}

		public JButton getCancelButton() {
			return jButton2;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE)  
		{  
			dispose(); 
		}  
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	 
}
