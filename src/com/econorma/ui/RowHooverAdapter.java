package com.econorma.ui;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTable;
import javax.swing.ToolTipManager;

import com.econorma.Application;
import com.econorma.util.Logger;

public class RowHooverAdapter {
	
	private static final Logger logger = Logger.getLogger(RowHooverAdapter.class);
	private static final String TAG = RowHooverAdapter.class.getCanonicalName();
	private final  MouseAdapter mouseAdapter;
	private final JTable[] tables;
	
	private  RowHooverAdapter(JTable[] tables){
		mouseAdapter = new MyMouseAdapter();
		this.tables = tables;
		for(JTable table : tables){
			ToolTipManager.sharedInstance().unregisterComponent(table);
			ToolTipManager.sharedInstance().unregisterComponent(table.getTableHeader());
			table.addMouseListener(mouseAdapter);
		}
		Application.getInstance().getMainFrame().addMouseListener(mouseAdapter);
	}
	
	public static RowHooverAdapter registerHoverManager(JTable[] tables){
		return new RowHooverAdapter(tables);
	}

	private class MyMouseAdapter extends MouseAdapter{

		

		@Override
		public void mouseMoved(MouseEvent e) {
			logger.debug(TAG, e.toString());
			for(JTable table: tables){
				int row = table.rowAtPoint(e.getPoint());
				if(row>1){
					table.clearSelection();
					table.setRowSelectionInterval(row, row);
				}else{
					table.setSelectionBackground(Color.RED);
				}
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			logger.debug(TAG, e.toString());
		}

		@Override
		public void mouseExited(MouseEvent e) {
			logger.debug(TAG, e.toString());

		}
		
		
	}
}
