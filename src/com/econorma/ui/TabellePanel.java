package com.econorma.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DropMode;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.TransferHandler;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.Application.TIPO_MISURE;
import com.econorma.data.Lettura;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.logic.RangeCheck;
import com.econorma.logic.SensoreManager;
import com.econorma.model.LettureModel;
import com.econorma.resources.Testo;
import com.econorma.ui.Events.ChartFilterEvent;
import com.econorma.util.Logger;
import com.econorma.util.Theme;

import net.miginfocom.swing.MigLayout;


public class TabellePanel extends JPanel implements ActionListener, KeyListener {

	// http://www.chka.de/swing/table/faq.html

	/**
	 * 
	 */
	private static final long serialVersionUID = 6000355186796391145L;
	private JLabel lb_esterno;
	private JLabel lb_interno;
	private JLabel lb_unknown;
	private MyTable tableEsterno;
	private MyTable tableInterno;
	private MyTable tableEutettiche;
	private MyTable tableUnknown;
	private JScrollPane scrollEsterno;
	private JScrollPane scrollInterno;
	private JScrollPane scrollUnknown;
	private JScrollPane scrollEutettico;

	private static final Logger logger = Logger.getLogger(TabellePanel.class);
	private static final String TAG = TabellePanel.class.getSimpleName();
	private boolean configMode = Application.getInstance().isConfigMode(); 
	private static Application app = Application.getInstance();
	private static final String ACTION_DISCHARGE = "Discharge";
	private static RangeCheck rangeCheck; 

	private static long DEAD_TIME_CHECK = 1000 * 3600;
	private static Color DEFAULT_COLOR;


	// keep hard references
	LettureModel lettureModelInterno;
	LettureModel lettureModelEsterno;
	LettureModel lettureModelUnknown;
	LettureModel lettureModelEutettiche;

	private final TabelleMisureTransferHandler transferHandler = new TabelleMisureTransferHandler();
	private final Timer timer = new Timer(30 * 1000, this);


	/**
	 * Create the panel.
	 */
	public TabellePanel() {

		switch (app.getTheme()) {
		case DARK:
			DEFAULT_COLOR = Theme.WHITE;
			break;
		case LIGHT:
			DEFAULT_COLOR = Theme.BLACK;
		default:
			break;
		}

		tableInterno = new MyTable();
		tableInterno.setFillsViewportHeight(true);

		tableEsterno = new MyTable();
		tableEsterno.setFillsViewportHeight(true);

		tableUnknown = new MyTable();
		tableUnknown.setFillsViewportHeight(true);

		tableEutettiche = new MyTable();
		tableEutettiche.setFillsViewportHeight(true);

		tableEsterno.setDropMode(DropMode.ON_OR_INSERT);
		tableInterno.setDropMode(DropMode.ON_OR_INSERT);
		tableEutettiche.setDropMode(DropMode.ON_OR_INSERT);
		tableUnknown.setDropMode(DropMode.ON_OR_INSERT);

		tableEsterno.setTransferHandler(transferHandler);
		tableInterno.setTransferHandler(transferHandler);
		tableEutettiche.setTransferHandler(transferHandler);
		tableUnknown.setTransferHandler(transferHandler);

		lettureModelInterno = new LettureModel(Location.INTERNO);
		tableInterno.setModel(lettureModelInterno);

		DecimalFormat decimal = new DecimalFormat("##,##");
		JFormattedTextField formattedTextField = new JFormattedTextField(decimal);
		TableCellEditor cellEditor = new DefaultCellEditor(formattedTextField);

		tableInterno.getColumn(LettureModel.COL_OFFSET).setCellEditor(cellEditor);
		tableInterno.getColumn(LettureModel.COL_OFFSET).setCellRenderer(new CustomDoubleRenderer());
		tableInterno.getColumn(LettureModel.COL_INCERTEZZA).setCellEditor(cellEditor);
		tableInterno.getColumn(LettureModel.COL_INCERTEZZA).setCellRenderer(new CustomDoubleRenderer());	

		lettureModelEsterno = new LettureModel(Location.ESTERNO);
		tableEsterno.setModel(lettureModelEsterno);

		tableEsterno.getColumn(LettureModel.COL_OFFSET).setCellEditor(cellEditor);
		tableEsterno.getColumn(LettureModel.COL_OFFSET).setCellRenderer(new CustomDoubleRenderer());
		tableEsterno.getColumn(LettureModel.COL_INCERTEZZA).setCellEditor(cellEditor);
		tableEsterno.getColumn(LettureModel.COL_INCERTEZZA).setCellRenderer(new CustomDoubleRenderer());	

		lettureModelEutettiche = new LettureModel(Location.EUTETTICHE);
		tableEutettiche.setModel(lettureModelEutettiche);

		tableEutettiche.getColumn(LettureModel.COL_OFFSET).setCellEditor(cellEditor);
		tableEutettiche.getColumn(LettureModel.COL_OFFSET).setCellRenderer(new CustomDoubleRenderer());
		tableEutettiche.getColumn(LettureModel.COL_INCERTEZZA).setCellEditor(cellEditor);
		tableEutettiche.getColumn(LettureModel.COL_INCERTEZZA).setCellRenderer(new CustomDoubleRenderer());	

		lettureModelUnknown = new LettureModel(Location.UNKNOWN);
		tableUnknown.setModel(lettureModelUnknown);

		tableUnknown.getColumn(LettureModel.COL_OFFSET).setCellEditor(cellEditor);
		tableUnknown.getColumn(LettureModel.COL_OFFSET).setCellRenderer(new CustomDoubleRenderer());
		tableUnknown.getColumn(LettureModel.COL_INCERTEZZA).setCellEditor(cellEditor);
		tableUnknown.getColumn(LettureModel.COL_INCERTEZZA).setCellRenderer(new CustomDoubleRenderer());	

		tableInterno.setDefaultRenderer(Object.class, new CustomRenderer());
		tableEsterno.setDefaultRenderer(Object.class, new CustomRenderer());
		tableEutettiche.setDefaultRenderer(Object.class, new CustomRenderer());
		tableUnknown.setDefaultRenderer(Object.class, new CustomRenderer());

		tableInterno.getColumnExt(LettureModel.COL_ID_SONDA).setMaxWidth(60);
		tableInterno.getColumnExt(LettureModel.COL_ID_SONDA).setMinWidth(60);
		tableUnknown.getColumnExt(LettureModel.COL_ID_SONDA).setMaxWidth(60);
		tableUnknown.getColumnExt(LettureModel.COL_ID_SONDA).setMinWidth(60);

		if(Application.getInstance().getTipoMisure()==TIPO_MISURE.MISTA){
			tableInterno.getColumnExt(LettureModel.COL_TEMP_MISTA).setMaxWidth(50);
			tableInterno.getColumnExt(LettureModel.COL_TEMP_MISTA).setMinWidth(50);
			tableUnknown.getColumnExt(LettureModel.COL_TEMP_MISTA).setMaxWidth(50);
			tableUnknown.getColumnExt(LettureModel.COL_TEMP_MISTA).setMinWidth(50);
		} else {
			tableInterno.getColumnExt(LettureModel.COL_TEMP).setMaxWidth(50);
			tableInterno.getColumnExt(LettureModel.COL_TEMP).setMinWidth(50);
			tableUnknown.getColumnExt(LettureModel.COL_TEMP).setMaxWidth(50);
			tableUnknown.getColumnExt(LettureModel.COL_TEMP).setMinWidth(50);
		}

		tableInterno.getColumnExt(LettureModel.COL_DATA).setMaxWidth(130);
		tableInterno.getColumnExt(LettureModel.COL_DATA).setMinWidth(130);
		tableEsterno.getColumnExt(LettureModel.COL_DATA).setMaxWidth(130);
		tableEsterno.getColumnExt(LettureModel.COL_DATA).setMinWidth(130);
		tableUnknown.getColumnExt(LettureModel.COL_DATA).setMaxWidth(130);
		tableUnknown.getColumnExt(LettureModel.COL_DATA).setMinWidth(130);


		tableEsterno.getColumnExt(LettureModel.COL_TEMP).setMaxWidth(50);
		tableEsterno.getColumnExt(LettureModel.COL_TEMP).setMinWidth(50);
		tableEutettiche.getColumnExt(LettureModel.COL_TEMP).setMaxWidth(50);
		tableEutettiche.getColumnExt(LettureModel.COL_TEMP).setMinWidth(50);
		tableEutettiche.getColumnExt(LettureModel.COL_DATA).setMaxWidth(130);
		tableEutettiche.getColumnExt(LettureModel.COL_DATA).setMinWidth(130);

		if(Application.getInstance().getTipoMisure()==TIPO_MISURE.TEMPERATURA_UMIDITA){
			tableInterno.getColumnExt(LettureModel.COL_URT).setMaxWidth(50);
			tableInterno.getColumnExt(LettureModel.COL_URT).setMinWidth(50);
			tableUnknown.getColumnExt(LettureModel.COL_URT).setMaxWidth(50);
			tableUnknown.getColumnExt(LettureModel.COL_URT).setMinWidth(50);
			tableInterno.getColumn(LettureModel.COL_OFFSET_URT).setCellEditor(cellEditor);
			tableInterno.getColumn(LettureModel.COL_OFFSET_URT).setCellRenderer(new CustomDoubleRenderer());
			tableUnknown.getColumn(LettureModel.COL_OFFSET_URT).setCellEditor(cellEditor);
			tableUnknown.getColumn(LettureModel.COL_OFFSET_URT).setCellRenderer(new CustomDoubleRenderer());

			tableInterno.getColumn(LettureModel.COL_INCERTEZZA).setCellEditor(cellEditor);
			tableInterno.getColumn(LettureModel.COL_INCERTEZZA).setCellRenderer(new CustomDoubleRenderer());	
			tableInterno.getColumn(LettureModel.COL_INCERTEZZA_URT).setCellEditor(cellEditor);
			tableInterno.getColumn(LettureModel.COL_INCERTEZZA_URT).setCellRenderer(new CustomDoubleRenderer());


			tableInterno.setDefaultRenderer(Object.class, new CustomAlarmRenderer());
			tableInterno.getColumn(LettureModel.COL_OFFSET).setCellRenderer(new CustomAlarmDoubleRenderer());
			tableInterno.getColumn(LettureModel.COL_OFFSET_URT).setCellRenderer(new CustomAlarmDoubleRenderer());

			tableInterno.getColumn(LettureModel.COL_INCERTEZZA).setCellRenderer(new CustomAlarmDoubleRenderer());
			tableInterno.getColumn(LettureModel.COL_INCERTEZZA_URT).setCellRenderer(new CustomAlarmDoubleRenderer());	


		}




		final String sensori_interni_label;
		final String sensori_esterni_label;
		final String sensori_eutettici_label;
 
		{
			setLayout(new MigLayout("fill", "[][][]"));
			sensori_interni_label = Application.getInstance().getBundle().getString("tabellePanel.sondeInterne");

			lb_interno = new JLabel(sensori_interni_label);
			add(lb_interno, "gap 5px 5px, growprio 1, wrap");

			scrollInterno = new JScrollPane();

			new JPanel();
			add(scrollInterno, "gap 5px 5px, growy, growx, pushy, pushx, wrap");		

			scrollInterno.setViewportView(tableInterno);

			tableInterno.addMouseListener(new ClickListener());
			tableInterno.addKeyListener(new KeyListener());

		}


		tableUnknown.addMouseListener(new DeleteClickListener());



		lb_unknown = new JLabel(Application.getInstance().getBundle().getString("tabellePanel.sondeSconosciute"));
		add(lb_unknown, "gap 5px 5px, wrap, hidemode 3");

		scrollUnknown = new JScrollPane();
		add(scrollUnknown, "gap 5px 5px, growy, growx, pushy, pushx, hidemode 3, wrap");

		scrollUnknown.setViewportView(tableUnknown);

		lb_unknown.setVisible(false);
		scrollUnknown.setVisible(false);


		tableInterno.packAll();
	 
		tableUnknown.packAll();
		tableEutettiche.packAll();

 
			int timeMancataTrasmissione = 60;
			DEAD_TIME_CHECK= timeMancataTrasmissione *60 * 1000;
		 

		EventBusService.subscribe(this);

		updateStatus();
		timer.start();
	}

	public void bind(SensoreManager sensoreManager) {

		sensoreManager.addOnReadListener(lettureModelInterno);
		 
		sensoreManager.addOnReadListener(lettureModelUnknown);
	}

	private static class TabelleMisureTransferHandler extends TransferHandler {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5933360577411820687L;

		@Override
		protected Transferable createTransferable(JComponent c) {
			JTable t = (JTable) c;
			LettureModel model = (LettureModel) t.getModel();
			int[] selectedRows = t.getSelectedRows();
			List<Lettura> letture = new ArrayList<Lettura>();
			if (selectedRows != null && selectedRows.length != 0) {
				for (int i : selectedRows) {
					Lettura item = model.getItem(i);
					letture.add(item);
				}
			}
			LettureTransferable lt = new LettureTransferable(letture);
			return lt;
		}

		@Override
		protected void exportDone(JComponent c, Transferable t, int action) {
			if (action == TransferHandler.MOVE) {
				JTable sourceTable = (JTable) c;
				LettureModel letture = (LettureModel) sourceTable.getModel();
				List<Lettura> transferData = null;
				try {
					transferData = (List<Lettura>) t
							.getTransferData(LetturaDataFlavor.letture);

				} catch (UnsupportedFlavorException e) {
				} catch (IOException e) {
				}

				if (transferData != null) {
					for (Lettura l : transferData) {
						if (letture.getlocation() != l.getSensore()
								.getLocation()) // non lo rimuoviamo se è è un
							// dnd sullo stesso componente
							letture.remove(l.getSensore());
					}
				}
			}

		}

		@Override
		public int getSourceActions(JComponent jcomponent) {
			return TransferHandler.MOVE;
		}

		@Override
		public boolean importData(TransferSupport supp) {
			if (!supp.isDrop()) {
				return false;
			}

			JTable jTable = (JTable) supp.getComponent();
			LettureModel lettureModel = (LettureModel) jTable.getModel();
			JTable.DropLocation dl = (JTable.DropLocation) supp
					.getDropLocation();
			int index = dl.getRow();
			boolean insert = dl.isInsertRow();

			// Get the string that is being dropped.
			Transferable t = supp.getTransferable();
			List<Lettura> letture;
			try {
				letture = (List<Lettura>) t
						.getTransferData(LetturaDataFlavor.letture);
				Location where = lettureModel.getlocation();
				if (letture != null) {
					for (Lettura l : letture) {
						Sensore sensore = l.getSensore();
						if (sensore != null)
							sensore.setLocation(where);
					}
				}
			} catch (Exception e) {
				return false;
			}

			if (letture != null) {
				for (Lettura l : letture) {
					if (insert) {
						// sembra succedere quando si droppa tra 2 celle
						lettureModel.add(l);
					} else {
						lettureModel.add(l);
						// sembra succedere quando si droppa su una cella
					}
				}
			}

			SensoreManager.getInstance().saveState();
			return true;
		}

		@Override
		public boolean canImport(TransferSupport supp) {
			// Check for String flavor
			if (!supp.isDataFlavorSupported(LetturaDataFlavor.letture)) {
				return false;
			}

			if (!Application.getInstance().isConfigMode()) {
				return false;
			}

			// Fetch the drop location
			JTable.DropLocation loc = (JTable.DropLocation) supp
					.getDropLocation();
			// Return whether we accept the location
			// return shouldAcceptDropLocation(loc);
			return true;
		}

	}

	private static class LettureTransferable implements Transferable {

		private List<Lettura> letture;

		public LettureTransferable(List<Lettura> letture) {
			this.letture = letture;
		}

		@Override
		public Object getTransferData(DataFlavor arg0)
				throws UnsupportedFlavorException, IOException {
			return letture;
		}

		@Override
		public DataFlavor[] getTransferDataFlavors() {
			return new DataFlavor[] { new LetturaDataFlavor() };
		}

		@Override
		public boolean isDataFlavorSupported(DataFlavor df) {
			if (df instanceof LetturaDataFlavor)
				return true;
			return false;
		}

	}

	private static class LetturaDataFlavor extends DataFlavor {

		public static final LetturaDataFlavor letture = new LetturaDataFlavor();

		private LetturaDataFlavor() {
			super(Lettura.class, "Lettura");
		}
	}

	@EventHandler
	public void handleEvent(String event) {
		if (Events.APPLICATION_MODE_CHANGED.equals(event)) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {

					updateStatus();
				}
			});
		}
	}

	private void updateStatus() {
		boolean configMode = Application.getInstance().isConfigMode();

		lb_unknown.setVisible(configMode);
		scrollUnknown.setVisible(configMode);

		lettureModelEsterno.setEditMode(configMode);
		lettureModelInterno.setEditMode(configMode);
		lettureModelEutettiche.setEditMode(configMode);
		lettureModelUnknown.setEditMode(configMode);

		tableEsterno.setDragEnabled(configMode);
		tableInterno.setDragEnabled(configMode);
		tableEutettiche.setDragEnabled(configMode);
		tableUnknown.setDragEnabled(configMode);

	}


	// http://www.informit.com/articles/article.aspx?p=333472&seqNum=2
	// http://tips4java.wordpress.com/2010/01/24/table-row-rendering/




	private static class CustomRenderer extends DefaultTableCellRenderer {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6164934656361448101L;



		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			int modelRow = table.convertRowIndexToModel(row);
			Component tableCellRendererComponent = super
					.getTableCellRendererComponent(table, value, isSelected,
							hasFocus, modelRow, column);
			LettureModel lettureModel = (LettureModel) table.getModel();
			Lettura item = lettureModel.getItem(modelRow);
			if (item != null) {
				Date data = item.getData();
				if (data != null
						&& (System.currentTimeMillis() - DEAD_TIME_CHECK) > data
						.getTime()) {
					tableCellRendererComponent.setForeground(Color.BLUE);
					return this;
				}

				String battery=item.getStatoBatteria();
				if (battery.equals(Testo.BATTERIA_STATO_SCARICA)){
					tableCellRendererComponent.setForeground(Color.RED);
					return this;
				}

			}
			tableCellRendererComponent.setForeground(DEFAULT_COLOR);
			tableCellRendererComponent.setLocale(Locale.ITALY);
			return this;
		}
	}

	static class NumberRenderer extends DefaultTableCellRenderer.UIResource {
		public NumberRenderer() {
			super();
			setHorizontalAlignment(JLabel.RIGHT);
		}
	}

	static class DoubleRenderer extends NumberRenderer {
		NumberFormat formatter;
		public DoubleRenderer() { super(); }

		public void setValue(Object value) {
			if (formatter == null) {
				formatter = NumberFormat.getInstance();
			}
			setText((value == null) ? "" : formatter.format(value));
		}
	}


	private static class CustomDoubleRenderer extends DoubleRenderer {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6164934656361448101L;


		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			int modelRow = table.convertRowIndexToModel(row);
			Component tableCellRendererComponent = super
					.getTableCellRendererComponent(table, value, isSelected,
							hasFocus, modelRow, column);
			LettureModel lettureModel = (LettureModel) table.getModel();
			Lettura item = lettureModel.getItem(modelRow);
			if (item != null) {
				Date data = item.getData();
				if (data != null
						&& (System.currentTimeMillis() - DEAD_TIME_CHECK) > data
						.getTime()) {
					//  					tableCellRendererComponent.setForeground(Color.RED);
					tableCellRendererComponent.setForeground(Color.BLUE);
					return this;
				}

				String battery=item.getStatoBatteria();
				if (battery!=null){
					if (battery.equals(Testo.BATTERIA_STATO_SCARICA)){
						tableCellRendererComponent.setForeground(Color.RED);
						return this;
					}
				}

			}
			tableCellRendererComponent.setForeground(DEFAULT_COLOR);
			tableCellRendererComponent.setLocale(Locale.ITALY);
			return this;
		}
	}


	private static class CustomAlarmRenderer extends DefaultTableCellRenderer {
		/**
		 * 
		 */

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			int modelRow = table.convertRowIndexToModel(row);
			Component tableCellRendererComponent = super
					.getTableCellRendererComponent(table, value, isSelected,
							hasFocus, modelRow, column);
			LettureModel lettureModel = (LettureModel) table.getModel();
			Lettura lettura = lettureModel.getItem(modelRow);

			JLabel original =(JLabel) super.getTableCellRendererComponent(
					table, value, isSelected, hasFocus, modelRow, column);


			original.setBorder( BorderFactory.createEmptyBorder( 3, 3, 3, 3) );

			Sensore sensore = lettura.getSensore();
			if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0 || sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0) {

				if (lettura != null) {
					Double valore = Double.MIN_VALUE;
					Double umidita = Double.MIN_VALUE;


					valore = lettura.getValore();

					if (valore != null && outOfLimit(lettura))

					{
						tableCellRendererComponent.setForeground(Color.RED);
						return this;
					}



					switch (app.getTipoMisure()) {
					case TEMPERATURA_UMIDITA:

						valore = lettura.getValore();
						umidita = lettura.getUmidita();

						if (umidita != null && outOfLimitURT(lettura))
						{
							tableCellRendererComponent.setForeground(Color.RED);
							return this;
						}
					}
				}

			}




			if (lettura != null) {
				Date data = lettura.getData();
				if (data != null
						&& (System.currentTimeMillis() - DEAD_TIME_CHECK) > data
						.getTime()) {
					tableCellRendererComponent.setForeground(Color.BLUE);
					return this;
				}
			}



			tableCellRendererComponent.setForeground(DEFAULT_COLOR);
			tableCellRendererComponent.setLocale(Locale.ITALY);
			return this;


		}
	} 



	private static class CustomAlarmDoubleRenderer extends DoubleRenderer {
		/**
		 * 
		 */


		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			int modelRow = table.convertRowIndexToModel(row);
			Component tableCellRendererComponent = super
					.getTableCellRendererComponent(table, value, isSelected,
							hasFocus, modelRow, column);
			LettureModel lettureModel = (LettureModel) table.getModel();
			Lettura lettura = lettureModel.getItem(modelRow);

			JLabel original =(JLabel) super.getTableCellRendererComponent(
					table, value, isSelected, hasFocus, modelRow, column);


			original.setBorder( BorderFactory.createEmptyBorder( 3, 3, 3, 3) );


			Sensore sensore = lettura.getSensore();
			if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0 || sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0) {

				if (lettura != null) {

					Double valore = Double.MIN_VALUE;
					Double umidita = Double.MIN_VALUE;


					valore = lettura.getValore();

					if (valore != null && outOfLimit(lettura))

					{
						tableCellRendererComponent.setForeground(Color.RED);
						return this;
					}



					switch (app.getTipoMisure()) {
					case TEMPERATURA_UMIDITA:

						valore = lettura.getValore();
						umidita = lettura.getUmidita();

						if (umidita != null && outOfLimitURT(lettura))
						{
							tableCellRendererComponent.setForeground(Color.RED);
							return this;
						}
					}
				}

			}


			if (lettura != null) {
				Date data = lettura.getData();
				if (data != null
						&& (System.currentTimeMillis() - DEAD_TIME_CHECK) > data
						.getTime()) {
					tableCellRendererComponent.setForeground(Color.BLUE);
					return this;
				}
			}



			tableCellRendererComponent.setForeground(DEFAULT_COLOR);
			tableCellRendererComponent.setLocale(Locale.ITALY);
			return this;
		}
	}




	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == timer) {
			lettureModelEsterno.fireTableDataChanged();
			lettureModelInterno.fireTableDataChanged();
			lettureModelUnknown.fireTableDataChanged();

		}


	}

	public JTable[] getLettureTables(){
		return new JTable[]{tableEsterno, tableInterno, tableUnknown};
	}

	private static class MyCellEditor extends DefaultCellEditor{

		public MyCellEditor(JCheckBox checkBox) {
			super(checkBox);
		}
		public MyCellEditor(JComboBox comboBox) {
			super(comboBox);
		}

		public MyCellEditor(JTextField textField) {
			super(textField);
		}


		@Override
		public boolean stopCellEditing() {

			// TODO Auto-generated method stub
			return super.stopCellEditing();
		}
		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) {
			// TODO Auto-generated method stub
			return super.getTableCellEditorComponent(table, value, isSelected, row, column);
		}


	}


	public class ClickListener extends MouseAdapter
	{


		public void mousePressed(MouseEvent e)
		{
			JTable table = (JTable)e.getSource();
			java.awt.Point p = e.getPoint();


			if(!Application.getInstance().isConfigMode()) {



				int row = table.rowAtPoint(p);
				int col = table.columnAtPoint(p);

				if(e.getClickCount() == 2 && row!=-1 ){ 

					int modelRow = table.convertRowIndexToModel(row);

					LettureModel model = (LettureModel)table.getModel();
					Lettura lettura = model.getItem(modelRow);
					Sensore sensore = lettura.getSensore();
					ChartFilterEvent chartFilterEvent = new Events.ChartFilterEvent(sensore, Events.ChartFilterEvent.ACTION.ADD_FILTER);
					logger.debug(TAG, "double click  row "+  row+" launching event for sensor "+sensore.getId_sonda());
					EventBusService.publish(chartFilterEvent);



				}
			}

 



		}

	}

	public class DeleteClickListener extends MouseAdapter
	{

		public void mousePressed(MouseEvent e)
		{
			JTable table = (JTable)e.getSource();
			java.awt.Point p = e.getPoint();


			if(Application.getInstance().isConfigMode()) {

				if ( SwingUtilities.isLeftMouseButton( e ) ) {

				} else if ( SwingUtilities.isRightMouseButton( e ) ) {
					int row = table.rowAtPoint(p);
					int col = table.columnAtPoint(p);

					int modelRow = table.convertRowIndexToModel(row);

					LettureModel model = (LettureModel)table.getModel();


					if (model.getRowCount()>0) {

						Lettura lettura = model.getItem(modelRow);
						final Sensore sensore = lettura.getSensore();

						 
					}



				}



			}

		}

	}


	public class KeyListener extends KeyAdapter
	{
		public void keyPressed(KeyEvent e)
		{
			JTable table = (JTable)e.getSource();
			int row = table.getSelectedRow();

			int keyCode = e.getKeyCode();
			switch( keyCode ) { 
			case KeyEvent.VK_F1:

				if (row!=-1) {
					int modelRow = table.convertRowIndexToModel(row);

					LettureModel model = (LettureModel)table.getModel();
					Lettura lettura = model.getItem(modelRow);
					Sensore sensore = lettura.getSensore();

					String message = "<html> Sonda: " + lettura.getIdSonda() + " ";
					message = message + (sensore.getDescrizione() != null ? " - " + sensore.getDescrizione() : " ") + "<br>";
					if (lettura.getStatoBatteria()!=null) {
						message = message + "Stato Batteria : " + "<b>" + (lettura.getStatoBatteria().equals(Testo.BATTERIA_STATO_CARICA) ? Testo.BATTERIA_CARICA : Testo.BATTERIA_SCARICA) + "</b>" + "&nbsp" + "<br>";
					}
					message = message + "Allame °C : " + "&nbsp &nbsp &nbsp" + " min: " + "<b>" + sensore.getRangeMin() + "</b>";
					message = message + "&nbsp &nbsp &nbsp &nbsp" + " max: " + "<b>" + sensore.getRangeMax() + "</b>" + "<br>";

					switch (app.getTipoMisure()) {
					case TEMPERATURA_UMIDITA:
					case MISTA:
						message = message + "Allame UR %: " +  "&nbsp" + " min: " + "<b>" + sensore.getRangeMinURT() + "</b>";
						message = message + "&nbsp &nbsp &nbsp &nbsp" + " max: " + "<b>" + sensore.getRangeMaxURT()+ "</b>" ;
					}
					message = message + "</html>";

					JLabel label = new JLabel();
					label.setText(message);
					//    					label.setFont(new Font("sanserif", Font.PLAIN, 12));


					JOptionPane.showMessageDialog(null, label ,"Informazione Allarmi", JOptionPane.INFORMATION_MESSAGE);
				}

				break;
			}
		}
	}



	private static boolean outOfLimit(Lettura lettura){
		Sensore sensore = lettura.getSensore();
		if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0){
			return lettura.getValore() < sensore.getRangeMin() || lettura.getValore() > sensore.getRangeMax();
		}
		return false;

	}

	private static boolean outOfLimitURT(Lettura lettura){
		Sensore sensore = lettura.getSensore();
		if (sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0){
			return lettura.getUmidita() < sensore.getRangeMinURT() || lettura.getUmidita() > sensore.getRangeMaxURT();
		}
		return false;

	}
 


	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}


}
