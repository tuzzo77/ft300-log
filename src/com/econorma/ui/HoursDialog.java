package com.econorma.ui;

import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class HoursDialog {
	
//	private JTextField hours;
	private JComboBox hours;
	private JLabel label;
	private JPanel panel;
	private String[] options = {"OK"};
	private final String[] values = { "24", "12", "6", "4", "2", "1", "0" };
	private int selectedHour;

	
	public HoursDialog(){
		
//	 	 panel = new JPanel();
	 	 panel = new JPanel(new GridLayout(2,2));
		 label = new JLabel("Scegli intervallo di tempo in ore ");
		 hours = new JComboBox(values);
//		 hours = new JTextField(10);
		 panel.add(label);
		 panel.add(hours);
		
	}
	
	public void show(){
		
		 int selectedOption = JOptionPane.showOptionDialog(null, panel, "Intervallo Grafico", JOptionPane.NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options , options[0]);

		 selectedHour=0;
		 if(selectedOption == 0)
		 {
			 selectedHour = Integer.parseInt(hours.getSelectedItem().toString());
		     
		 }
		
	}
	
	public int getSelectedHour(){
		return selectedHour;
	}
	
}
