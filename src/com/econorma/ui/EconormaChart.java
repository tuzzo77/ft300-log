package com.econorma.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItem;
import org.jfree.chart.LegendItemCollection;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickMarkPosition;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.labels.XYSeriesLabelGenerator;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.Lettura.TipoMisura;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.gui.EconormaDrawingSupplier;
import com.econorma.logic.LettureProva;
import com.econorma.logic.SensoreManager;
import com.econorma.util.Logger;
import com.econorma.util.StringUtils;
import com.econorma.util.Theme;


public class EconormaChart {

	private static final String TAG = "EconormaChart";
	private static final Logger logger = Logger.getLogger(EconormaChart.class);

	private JFreeChart chart;
	private XYPlot plot; 

	private Application.THEME theme;
	private Application app;
	private boolean seriesShape;
	private boolean autoRange1;
	private boolean autoRange2;
	private int lowerRange1;
	private int lowerRange2;
	private int upperRange1;
	private int upperRange2;
	private int tickUnit1;
	private int tickUnit2;

	private EconormaChartBase umiditaChar;
	private EconormaChartBase mistaChar;

	private EconormaChartBase f0Char;
	private EconormaChartBase ELChar;

	private EconormaChartBase temperaturaChar;
	private List<EconormaChartBase> baseCharts;

	private static final int DATASET_TEMP_IDX = 0;
	private static final int DATASET_UMID_IDX = 1;
	private static final int DATASET_MISTA_IDX = 2;

	private static final int DATASET_F0_IDX = 3;
	private static final int DATASET_EL_IDX = 4;

	{
		baseCharts = new ArrayList<EconormaChartBase>();

	}

	public EconormaChart(){
		init(null, null, null);
		EventBusService.subscribe(this);
	}


	//TODO this is broken! fix restore!
	public EconormaChart(LettureProva lettureProva, List<Sensore> sensori, Prova prova){
		init(lettureProva, sensori, prova);
	}

	private void init(LettureProva lettureProva, List<Sensore> sensori, Prova prova){

		getAppDefault();
		
		EconormaChartBase ecb =null;
		if(umiditaChar==null){
			ecb = new EconormaChartBase(Lettura.TipoMisura.UMIDITA, DATASET_UMID_IDX);
			baseCharts.add(ecb);
			umiditaChar = ecb;
		}
		if(temperaturaChar==null){
			ecb = new EconormaChartBase(Lettura.TipoMisura.TEMPERATURA, DATASET_TEMP_IDX);
			baseCharts.add(ecb);
			temperaturaChar = ecb;
		}

		if(chart==null){
			chart = createOverlaidChart(app.getTipoMisure());
		}

		customizeAppearance(chart);

		for(EconormaChartBase i:baseCharts){
			i.setChart(chart);
			if(sensori!=null) {
				i.restore(lettureProva,sensori,prova);
			}
		}

	}


	public void restore(LettureProva lettureProva, List<Sensore> sensori){
		clear();
		init(lettureProva, sensori, null);
	}


	/**
	 * Creates an overlaid chart.
	 *
	 * @return The chart.
	 */
	@SuppressWarnings("deprecation")
	private JFreeChart createOverlaidChart(Application.TIPO_MISURE tipoMisure) {

		final DateAxis domainAxis = new DateAxis(Application.getInstance().getBundle().getString("econormaChart.data"));
		domainAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
		final ValueAxis rangeAxis1 = new NumberAxis(Application.getInstance().getBundle().getString("econormaChart.temperatura"));

		EconormaChartBase umiditaTmp = null;
		EconormaChartBase temperatura = null;
		for(EconormaChartBase ecb: baseCharts){
			if(ecb.getTipoLettura()==TipoMisura.TEMPERATURA)
				temperatura = ecb;
			if(ecb.getTipoLettura()==TipoMisura.UMIDITA)
				umiditaTmp = ecb;
			}

		final EconormaChartBase umidita = umiditaTmp;


		// create plot...
		final IntervalXYDataset temperature = temperatura.getDataSet();
		//         final XYItemRenderer temperatureRenderer = new DefaultXYItemRenderer();
		final XYItemRenderer temperatureRenderer = new StandardXYItemRenderer();
		plot = new XYPlot(temperature, domainAxis, rangeAxis1, temperatureRenderer){

			@Override
			public LegendItemCollection getLegendItems() {

				// se gestiamo l'umidita riordiniamo gli elementi della leggenda
				// altrimenti lasciamo come erano
				if(umidita!=null){
					LegendItemCollection legendItems = super.getLegendItems();
					Iterator iterator = legendItems.iterator();
					ArrayList<LegendItem> arrayList = new ArrayList<LegendItem>();
					while(iterator.hasNext()){
						arrayList.add((LegendItem)iterator.next());
					}
					Collections.sort(arrayList, new Comparator<LegendItem>() {

						@Override
						public int compare(LegendItem o1, LegendItem o2) {
							return o1.getLabel().compareTo(o2.getLabel());
						}

					});

					LegendItemCollection legendItemCollection = new LegendItemCollection();
					for(LegendItem i :arrayList){
						legendItemCollection.add(i);
					}
					return legendItemCollection;
				}else
					return super.getLegendItems();
			}
		};

		plot.setDrawingSupplier(new EconormaDrawingSupplier());


		
		// umidita, custom legend 
		temperatureRenderer.setLegendItemLabelGenerator(new TemperaturaUmiditaLegentItemLabelGenerator());

		// plot umidita

		final NumberAxis rangeAxis2 = new NumberAxis("Umidita Relativa %");
		rangeAxis2.setAutoRangeIncludesZero(false);

		switch (theme) {
		case DARK:
			rangeAxis2.setTickLabelPaint(Theme.WHITE); 
			rangeAxis2.setLabelPaint(Theme.WHITE);
			break;
		}

		if (!autoRange2){
			if (lowerRange2 !=0 && upperRange2 !=0) {
				rangeAxis2.setRange(lowerRange2, upperRange2);
			}
			if (tickUnit2 != 0){
				rangeAxis2.setTickUnit(new NumberTickUnit(tickUnit2)); 
			}
		}


		plot.setRangeAxis(DATASET_UMID_IDX, rangeAxis2);

		final XYDataset sondeUmidita = umidita.getDataSet();
		// 		        final XYItemRenderer renderer2A = new DefaultXYItemRenderer();
		final XYItemRenderer renderer2A = new StandardXYItemRenderer();

		renderer2A.setToolTipGenerator(
				new StandardXYToolTipGenerator(
						StandardXYToolTipGenerator.DEFAULT_TOOL_TIP_FORMAT,
						new SimpleDateFormat("d-MMM-yyyy"), new DecimalFormat("0.00")
						)
				);
		renderer2A.setLegendItemLabelGenerator(new TemperaturaUmiditaLegentItemLabelGenerator());
		plot.setDataset(DATASET_UMID_IDX, sondeUmidita);
		plot.setRenderer(DATASET_UMID_IDX, renderer2A);
		plot.mapDatasetToRangeAxis(DATASET_UMID_IDX, DATASET_UMID_IDX);

		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
		plot.setOrientation(PlotOrientation.VERTICAL);


		return new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

	}


	public void addLettura(Lettura lettura) {
		for(EconormaChartBase ecb: baseCharts){
			if(lettura.isValoreMisuraValid(ecb.getTipoLettura()))
				ecb.addLettura(lettura);
		}
	}

	public void clear() {
		for(EconormaChartBase ecb:baseCharts){
			ecb.clear();
		}
	}

	public JFreeChart getJFreeChart(){
		return chart;
	}

	public File saveAsPNG(){
		File file = null;

		try {
			file = File.createTempFile("econorma_reports_", UUID.randomUUID().toString());	
			ChartUtilities.saveChartAsPNG(file, chart, 780, 470);
			logger.info(TAG, "Grafico salvato in "+file.toString());
		} catch (IOException e) {
			logger.error(TAG, "impossibile salvare il chart in "+file.toString(),e);
		}

		return file;
	}

	public void setManageSingole(boolean value){
		for(EconormaChartBase ecb:baseCharts)
			ecb.setManageSingole(value);
	}

	@EventHandler
	public void handleEvent(final Events.ChartFilterEvent filterChartEvent){

		switch(filterChartEvent.action){
		case ADD_FILTER:
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					Sensore sensore = filterChartEvent.sensore;
					for(EconormaChartBase ecb:baseCharts){
						ecb.applyFilter(sensore);
					}
				}
			});
			break;
		case REMOVE_FILTER:
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					for(EconormaChartBase ecb:baseCharts){
						ecb.removeFilters();
					}
				}
			});
			break;
		default:
			break;
		}	
	}

	private void customizeAppearance(JFreeChart chart) {
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setBackgroundPaint(Color.WHITE);
		chart.setBackgroundPaint(UIManager.getColor("Panel.background"));

		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		switch (theme) {
		case DARK:
			plot.getRangeAxis().setTickLabelPaint(Theme.WHITE);
			plot.getDomainAxis().setTickLabelPaint(Theme.WHITE);
			plot.getRangeAxis().setLabelPaint(Theme.WHITE);
			plot.getDomainAxis().setLabelPaint(Theme.WHITE);
			plot.setBackgroundPaint(Theme.DARK);
			break;
		}


		LegendTitle legend = chart.getLegend();
		Font labelFont = legend.getItemFont().deriveFont(11f);
		legend.setItemFont(labelFont);
		legend.setBorder(0, 0, 0, 0); 
		legend.setBackgroundPaint(UIManager.getColor("Panel.background"));

		switch (theme) {
		case DARK:
			legend.setItemPaint(Theme.WHITE);
			break;
		}

		NumberAxis na = (NumberAxis)chart.getXYPlot().getRangeAxis();
		na.setNumberFormatOverride(new DecimalFormat("#0.00")); 
 
		
		plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
		plot.setDomainGridlinePaint(Color.LIGHT_GRAY);
		
		plot.setRangeGridlineStroke(new BasicStroke(0.1f));
		plot.setDomainGridlineStroke(new BasicStroke(0.1f));
 		
		DateAxis da = (DateAxis)chart.getXYPlot().getDomainAxis();  
		da.setDateFormatOverride(new SimpleDateFormat("dd-MM-yyyy HH:mm"));
		na.setAutoRange(true);	
			 
		// DA QUI //
		XYItemRenderer r = plot.getRenderer();
		r.setBaseToolTipGenerator(new StandardXYToolTipGenerator(org.jfree.chart.labels.StandardXYToolTipGenerator.DEFAULT_TOOL_TIP_FORMAT,
				new SimpleDateFormat("dd/MM/yyyy - HH:mm"), new DecimalFormat("#0.00")));
		plot.getDomainAxis().setLabelFont(new Font("Tahoma", Font.PLAIN, 11));
		plot.getRangeAxis().setLabelFont(new Font("Tahoma", Font.PLAIN, 11));
		// A QUI //


		na.setTickLabelFont(new Font("Tahoma", Font.PLAIN, 11));

		ValueAxis domainAxis = plot.getDomainAxis();
		domainAxis.setTickLabelFont(new Font("Tahoma", Font.PLAIN, 11));

		app = Application.getInstance();
		switch (app.getTipoMisure()) {
		case TEMPERATURA_UMIDITA:
			NumberAxis rangeAxis2 = (NumberAxis) plot.getRangeAxis(1);
			rangeAxis2.setAutoRange(true);
			rangeAxis2.setNumberFormatOverride(new DecimalFormat("#0.00"));
			rangeAxis2.setTickLabelFont(new Font("Tahoma", Font.PLAIN, 11));
			plot.getRangeAxis(1).setLabelFont(new Font("Tahoma", Font.PLAIN, 11));
		}

	}


	private class TemperaturaUmiditaLegentItemLabelGenerator implements XYSeriesLabelGenerator{

		@Override
		public String generateLabel(XYDataset d, int i) {
			int indexOf = plot.indexOf(d);
			String tipo = null;

			if(indexOf==DATASET_TEMP_IDX)
				tipo="(C�)";
			else if(indexOf==DATASET_UMID_IDX)
				tipo = "(UR%)";
			else if(indexOf==DATASET_MISTA_IDX)
				tipo = "(O�)";
			else if(indexOf==DATASET_F0_IDX)
				tipo = "(F�)";
			else if(indexOf==DATASET_EL_IDX)
				tipo = "(EL)";
			else
				throw new RuntimeException("Errore in sviluppo, posizione del dataset non gestita");
			String id_sonda = (String)d.getSeriesKey(i);
			Sensore sensore = SensoreManager.getInstance().getSensoreById(id_sonda);

			 
				return sensore+tipo+" - "+StringUtils.nullToEmpty(sensore.getDescrizione());
			 

		}
	}

	private class TemperaturaLegentItemLabelGenerator implements XYSeriesLabelGenerator{

		@Override
		public String generateLabel(XYDataset d, int i) {
			String id_sonda = (String)d.getSeriesKey(i);
			Sensore sensore = SensoreManager.getInstance().getSensoreById(id_sonda);

			if (sensore==null){
				return id_sonda;	
			} 

			return sensore+" - "+StringUtils.nullToEmpty(sensore.getDescrizione());
		}
	}

	public void getAppDefault(){
		app = Application.getInstance();
		theme = app.getTheme();
	}


}
