package com.econorma.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jdesktop.swingx.JXStatusBar;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.logic.SensoreManager;
import com.econorma.sinottico.PlanModel;
import com.econorma.sinottico.PlanPersistenceDelegate;
import com.econorma.sinottico.PlanPresenter;


public class MainWindow extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6746683396726822437L;
//	private TabellePanel tabellePanel;
	private MainPanel mainPanel;
	private ProvePanel provePanel;
	private JTabbedPane tabbedPane;
	private JXStatusBar statusBar;
	private Dimension minimumSize;
	private PlanPresenter planPresenter;
	private JSplitPane splitPane;
	
	//events managers
	private RowHooverAdapter rowHooverAdapter;
	private static Application app = Application.getInstance();

	private JLabel labelNotifiche;

	/**
	 * Create the frame.
	 */
	public MainWindow(Application app) {

		setLayout(new BorderLayout());
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);

		switch (Application.getInstance().getTheme()) {
		case DARK:
			tabbedPane.setUI(new MyTabbedPaneUI());
			break;
		case LIGHT:
			break;
		}
 
//		tabellePanel = new TabellePanel();
//		mainPanel = new MainPanel(app);

//		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
//				tabellePanel, mainPanel);

//		splitPane.setOneTouchExpandable(false);
//		splitPane.setMaximumSize(new Dimension(Integer.MAX_VALUE,
//				Integer.MAX_VALUE));
//		
//		tabbedPane.addTab(Application.getInstance().getBundle().getString("mainWindow.generale"), mainPanel);
		
		PlanPersistenceDelegate planPersistenceDelegate = new PlanPersistenceDelegate(Application.getInstance().getPreferences());
		PlanModel planModel = planPersistenceDelegate.load();
		if(planModel==null)
			planModel = new PlanModel();
		planPresenter = new PlanPresenter();
		planPresenter.setPersistenceDelegate(planPersistenceDelegate);
		planPresenter.bind(SensoreManager.getInstance());

		Application.getInstance().addExitListener(planPresenter);

		planPresenter.setPlanModel(planModel);
		tabbedPane.addTab(Application.getInstance().getBundle().getString("mainWindow.sinottico"), planPresenter.getDisplay());
 
	 

		provePanel = new ProvePanel();
		tabbedPane.addTab(Application.getInstance().getBundle().getString("mainWindow.storico"), provePanel);
		
		tabbedPane.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent event) {
				Component selectedComponent = tabbedPane.getSelectedComponent();
				if(provePanel.equals(selectedComponent)){
//					provePanel.forceRefresh();
				}
			}
		});
 
		add(tabbedPane, BorderLayout.CENTER);
 
		labelNotifiche = new JLabel(Application.getInstance().getBundle().getString("mainWindow.applicazionePronta"), SwingConstants.LEFT);
		labelNotifiche.setVerticalAlignment(SwingConstants.BOTTOM);
		labelNotifiche.setBorder(BorderFactory.createEmptyBorder( 1, 5, 1, 1) );

		add(labelNotifiche, BorderLayout.SOUTH);

		// // Provide minimum sizes for the two components in the split pane
	 
			minimumSize = new Dimension(420, 50);
//			tabellePanel.setMinimumSize(minimumSize);
		 

		// mainPanel.setMinimumSize(minimumSize);

//		tabellePanel.bind(SensoreManager.getInstance());
//
//		splitPane.addComponentListener(new ComponentListener() {
//
//			@Override
//			public void componentShown(ComponentEvent arg0) {
//			}
//
//			@Override
//			public void componentResized(ComponentEvent event) {
//				tabellePanel.setMaximumSize(new Dimension());
//			}
//
//			@Override
//			public void componentMoved(ComponentEvent arg0) {
//			}
//
//			@Override
//			public void componentHidden(ComponentEvent arg0) {
//			}
//		});

//		rowHooverAdapter = RowHooverAdapter.registerHoverManager(tabellePanel.getLettureTables());
		EventBusService.subscribe(this);
	}
 
	public MainPanel getMainPanel(){
		return mainPanel;
	}
	
	public JTabbedPane getTabbedPane(){
		return tabbedPane;
	}
	
	public ProvePanel getProvePanel() {
		return provePanel;
	}

	public void setProvePanel(ProvePanel provePanel) {
		this.provePanel = provePanel;
	}

	public PlanPresenter getPlanPresenter() {
		return planPresenter;
	}
 
	@EventHandler
	public void handleNotification(final Events.MainWindowInfoEvent event){
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				labelNotifiche.setText(event.toString());
			}
		});
	}
 

}
