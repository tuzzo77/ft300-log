package com.econorma.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.econorma.resources.Testo;

public class ConfigWindow extends javax.swing.JDialog implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6746683396726822437L;
	private ConfigPanel configPanel;
	private Dimension minimumSize;
	
	//events managers
	private RowHooverAdapter rowHooverAdapter;
	

	public ConfigWindow(java.awt.Frame parent, boolean modal) {
		    super(parent, modal);
	        initComponents();
	}
	/**
	 * Create the frame.
	 */
	private void initComponents() {
		
		setFocusable(true);
		addKeyListener(this);

		setLayout(new BorderLayout());

		
		configPanel = new ConfigPanel();
	 
 	
		minimumSize = new Dimension(610, 605);
		configPanel.setMinimumSize(minimumSize);
		configPanel.setSize(minimumSize);	
//		setResizable(false);
		

		setTitle(Testo.CONFIGURAZIONE_LOGGER);
 		setMinimumSize(minimumSize);
	
		
		setModal(true);
		 
			
	 	add(configPanel);

	 
		rowHooverAdapter = RowHooverAdapter.registerHoverManager(configPanel.getLettureTables());
		
	}
	

	public ConfigPanel getTrasmissionePanel(){
		return configPanel;
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE)  
		{  
			dispose(); 
		}  
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	 

}
