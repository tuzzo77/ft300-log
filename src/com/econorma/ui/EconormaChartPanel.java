package com.econorma.ui;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;

import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.NumberAxis;

import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.io.Receiver.ReadListener;
import com.econorma.logic.SensoreManager;
import com.econorma.util.Logger;

import net.miginfocom.swing.MigLayout;

public class EconormaChartPanel extends JPanel {

	private final ChartPanel chartPanel;

	private final EconormaChart chart;

	public static final String DEFAULT_TOOL_TIP_FORMAT = "{0} | {1} |  {2}";

	private boolean canRender = false;

	private final boolean isMain;


	private static final Logger logger = Logger.getLogger(EconormaChartPanel.class);
	private static final String TAG = EconormaChartPanel.class.getCanonicalName();

	private final ReadListener myReadListener = new ReadListener() {

		@Override
		public void onRead(Lettura lettura) {
			addLettura(lettura);

		}
	};

	/**
	 * Create the chart and register for events from virtual sensors
	 * MEDIA_ESTERNA, MEDIA_INTERNA
	 * 
	 * @param sensoreManager
	 */
	public EconormaChartPanel(SensoreManager sensoreManager, boolean isMain) {
		this.isMain = isMain;
		setLayout(new MigLayout("insets 0", "[grow]"));

		chart = new EconormaChart();

		boolean properties = true;
		boolean save = true;
		boolean print = true;
		boolean zoom = true;
		boolean tooltips = true;
		chartPanel = new ChartPanel(chart.getJFreeChart(), properties, save, print, zoom,
				tooltips);
		chartPanel.addMouseListener(new MouseAdapter() {


			@Override
			public void mouseClicked(MouseEvent e) {


				if(e.getClickCount()==2){
					NumberAxis na = (NumberAxis)chart.getJFreeChart().getXYPlot().getRangeAxis();
					na.setNumberFormatOverride(new DecimalFormat("#0.00")); 
					na.setRange(-50.00, 40.00);
				}

			}
		});

		chartPanel.setPreferredSize(new Dimension(2000, 2000));
		chartPanel.setMouseWheelEnabled(true);
		add(chartPanel);

		if(sensoreManager!=null){
			sensoreManager.addOnReadListener(myReadListener);
		}
	}

	public void addLettura(Lettura lettura) {

		if (canRender && lettura.getSensore() != null) {
			if(lettura.isDiscovery())
				return;
			else
				chart.addLettura(lettura);
		}
	}

	public void clear() {
		chart.clear();
	}
 


	@EventHandler
	public void handleEvent(String event) {

		if(Events.SENSORI_RESTORE_COMPLETED.equals(event)){
			canRender = true;
		}

		if(Application.getInstance().isConfigMode()){
			canRender = false;
		} else {
			canRender = true;
		}

	}


	public EconormaChart getEconormaChart(){
		return chart;
	}

}