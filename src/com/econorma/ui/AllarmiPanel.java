package com.econorma.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DropMode;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;

import org.jdesktop.swingx.JXTable;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.Sensore.Location;
import com.econorma.logic.SensoreManager;
import com.econorma.model.AllarmiModel;
import com.econorma.util.Logger;
import com.econorma.util.Theme;

import net.miginfocom.swing.MigLayout;


public class AllarmiPanel extends JPanel implements ActionListener, TableModelListener {

	// http://www.chka.de/swing/table/faq.html

	/**
	 * 
	 */
	private static final long serialVersionUID = 6000355186796391145L;
	private JLabel lb_allarmi;
	private MyTable tableAllarmi;
	private JScrollPane scrollAllarmi;

	private static final Logger logger = Logger.getLogger(AllarmiPanel.class);
	private static final String TAG = AllarmiPanel.class.getSimpleName();

	private static Application app = Application.getInstance();
	private static Color DEFAULT_COLOR;

	// keep hard references
	AllarmiModel allarmiModel;




	private final Timer timer = new Timer(30 * 1000, this);

	/**
	 * Create the panel.
	 */
	public AllarmiPanel() {

		switch (app.getTheme()) {
		case DARK:
			DEFAULT_COLOR = Theme.WHITE;
			break;
		case LIGHT:
			DEFAULT_COLOR = Theme.BLACK;
		default:
			break;
		}

		tableAllarmi = new MyTable();
		tableAllarmi.setFillsViewportHeight(true);

		tableAllarmi.setDropMode(DropMode.ON_OR_INSERT);

		//		allarmiModel = new AllarmiModel(Application.getInstance().getDao());
		allarmiModel = new AllarmiModel(Location.INTERNO, SensoreManager.getInstance().getUltimeLetture());

		tableAllarmi.setModel(allarmiModel);

		DecimalFormat decimal = new DecimalFormat("##,##");
		JFormattedTextField formattedTextField = new JFormattedTextField(decimal);
		TableCellEditor cellEditor = new DefaultCellEditor(formattedTextField);


		tableAllarmi.getColumn(allarmiModel.COL_TEMP_MIN).setCellEditor(cellEditor);
		tableAllarmi.getColumn(allarmiModel.COL_TEMP_MAX).setCellEditor(cellEditor);


		tableAllarmi.setDefaultRenderer(Object.class, new CustomRenderer());


		final String sensori_interni_label;

		tableAllarmi.getModel().addTableModelListener(this);


		//         	setLayout(new MigLayout("fill", "[][][]"));
		setLayout(new MigLayout("fill"));
		sensori_interni_label = Application.getInstance().getBundle().getString("allarmiPanel.titolo");

		lb_allarmi = new JLabel(sensori_interni_label);
		add(lb_allarmi, "gap 5px 5px, growprio 1, wrap");

		scrollAllarmi = new JScrollPane();

		new JPanel();
		add(scrollAllarmi, "gap 5px 5px, growy, growx, pushy, pushx, wrap");

		scrollAllarmi.setViewportView(tableAllarmi);

		tableAllarmi.packAll();


		EventBusService.subscribe(this);


		timer.start();
	}



	public void bind(SensoreManager sensoreManager) {

		sensoreManager.addOnReadListener(allarmiModel);

	}




	private static class LetturaDataFlavor extends DataFlavor {

		public static final LetturaDataFlavor letture = new LetturaDataFlavor();

		private LetturaDataFlavor() {
			super(Lettura.class, "Lettura");
		}
	}

	@EventHandler
	public void handleEvent(String event) {
		if (Events.APPLICATION_MODE_CHANGED.equals(event)) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {

					updateStatus();
				}
			});
		}
	}

	private void updateStatus() {
		boolean configMode = Application.getInstance().isConfigMode();

		allarmiModel.setEditMode(configMode);

	}


	private static class MyTable extends JXTable {

		/**
		 *
		 */
		private static final long serialVersionUID = -7663077065549587996L;

		public MyTable() {
			super();
			setColumnControlVisible(true);
		}

		@Override
		protected void createDefaultEditors() {
			// TODO Auto-generated method stub
			super.createDefaultEditors();
		}

		@Override
		public Component prepareEditor(TableCellEditor arg0, int arg1, int arg2) {
			// TODO Auto-generated method stub
			return super.prepareEditor(arg0, arg1, arg2);
		}

		@Override
		public TableCellEditor getCellEditor() {
			// TODO Auto-generated method stub
			return super.getCellEditor();
		}

		@Override
		public TableCellEditor getCellEditor(int arg0, int arg1) {
			// TODO Auto-generated method stub
			return super.getCellEditor(arg0, arg1);
		}

		@Override
		public void setCellEditor(TableCellEditor arg0) {
			// TODO Auto-generated method stub
			super.setCellEditor(arg0);
		}


	}

	// http://www.informit.com/articles/article.aspx?p=333472&seqNum=2
	// http://tips4java.wordpress.com/2010/01/24/table-row-rendering/




	@Override
	public void actionPerformed(ActionEvent event) {

		allarmiModel.fireTableDataChanged();

		//		if (event.getSource() == timer) {
		//			allarmiModel.fireTableDataChanged();
		//		 
		//		}

	}

	public JTable[] getLettureTables(){
		return new JTable[]{tableAllarmi };
	}

	private static class MyCellEditor extends DefaultCellEditor{

		public MyCellEditor(JCheckBox checkBox) {
			super(checkBox);
		}
		public MyCellEditor(JComboBox comboBox) {
			super(comboBox);
		}

		public MyCellEditor(JTextField textField) {
			super(textField);
		}


		@Override
		public boolean stopCellEditing() {

			// TODO Auto-generated method stub
			return super.stopCellEditing();
		}
		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) {
			// TODO Auto-generated method stub
			return super.getTableCellEditorComponent(table, value, isSelected, row, column);
		}


	}

	@Override
	public void tableChanged(TableModelEvent e) {

		int row = e.getFirstRow();
		int column = e.getColumn();
		if(column!=-1 ){

			String columnName = allarmiModel.getColumnName(column);
			Object data = allarmiModel.getValueAt(row, column);

			Double rangeMin=Double.MIN_VALUE;
			Double rangeMax=Double.MIN_VALUE;  
			Double rangeMinURT=Double.MIN_VALUE;
			Double rangeMaxURT=Double.MIN_VALUE;


			//				rangeMin = (Double) allarmiModel.getValueAt(row,5);
			//				rangeMax = (Double) allarmiModel.getValueAt(row,6);
			//
			//				rangeMinURT = (Double) allarmiModel.getValueAt(row,8);
			//				rangeMaxURT = (Double) allarmiModel.getValueAt(row,9);
			//
			//
			//				if (rangeMax > 0 && rangeMin >0 && rangeMax < rangeMin || rangeMax > 0 && rangeMin >0 && rangeMin > rangeMax){
			//					JOptionPane.showMessageDialog(null, "Range allarmi temperature non inseriti correttamente" ,"Errore", JOptionPane.ERROR_MESSAGE);
			//					logger.error(TAG, "Errore in inserimento Range allarmi temperature");
			//				}
			//				if (rangeMaxURT > 0 && rangeMinURT >0 && rangeMaxURT < rangeMinURT || rangeMaxURT > 0 && rangeMinURT >0 && rangeMinURT > rangeMaxURT){
			//					JOptionPane.showMessageDialog(null, "Range allarmi umidità non inseriti correttamente" ,"Errore", JOptionPane.ERROR_MESSAGE);
			//					logger.error(TAG, "Errore in inserimento Range allarmi umidità");
			//				}



		}

	}

	private static class CustomRenderer extends DefaultTableCellRenderer {
		/**
		 * 
		 */

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			int modelRow = table.convertRowIndexToModel(row);
			Component tableCellRendererComponent = super
					.getTableCellRendererComponent(table, value, isSelected,
							hasFocus, modelRow, column);


			JLabel original =(JLabel) super.getTableCellRendererComponent(
					table, value, isSelected, hasFocus, modelRow, column);


			original.setBorder( BorderFactory.createEmptyBorder( 3, 3, 3, 3) );

			tableCellRendererComponent.setForeground(DEFAULT_COLOR);
			tableCellRendererComponent.setLocale(Locale.ITALY);
			return this;
		}
	} 


}
