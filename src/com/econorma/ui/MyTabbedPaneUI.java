package com.econorma.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.plaf.basic.BasicGraphicsUtils;
import javax.swing.text.View;

import com.econorma.util.Theme;

public class MyTabbedPaneUI extends javax.swing.plaf.basic.BasicTabbedPaneUI {
 
	
	 @Override
	 protected void paintText(Graphics g, int tabPlacement, Font font, FontMetrics metrics,
	                          int tabIndex, String title, Rectangle textRect, boolean isSelected) {

	     g.setFont(font);

	     View v = getTextViewForTab(tabIndex);
	     if (v != null) {
	         v.paint(g, textRect);
	     } else {
	         int mnemIndex = tabPane.getDisplayedMnemonicIndexAt(tabIndex);

	         g.setColor(Theme.SMOKE_WHITE);

	         String text = title;
	         int rectX = textRect.x;
	         
	         if (tabPane.isEnabled() && tabPane.isEnabledAt(tabIndex)) {
//	        	 BasicGraphicsUtils.drawStringUnderlineCharAt(g, text, mnemIndex, rectX,
//                         textRect.y + metrics.getAscent());
	             BasicGraphicsUtils.drawString(g, text, mnemIndex, rectX,
	                                                          textRect.y + metrics.getAscent());
	         } else {  
	             Color bg = tabPane.getBackgroundAt(tabIndex);
	             g.setColor(bg.brighter());
	             BasicGraphicsUtils.drawString(g, text, mnemIndex, rectX,
	                                                          textRect.y + metrics.getAscent());
	             g.setColor(bg.darker());
	             BasicGraphicsUtils.drawString(g, text, mnemIndex, rectX - 1,
	                                                          textRect.y + metrics.getAscent() - 1);
	         }
	     }
	 }
   
     
 }
