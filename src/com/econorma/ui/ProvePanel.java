package com.econorma.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.log4j.MDC;
import org.jdesktop.swingx.JXTable;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.export.ExporterDat;
import com.econorma.export.ExporterProva;
import com.econorma.gui.ButtonCellEditor;
import com.econorma.gui.ButtonCellRenderer;
import com.econorma.logic.LettureProva;
import com.econorma.model.ProveModel;
import com.econorma.resources.Testo;
import com.econorma.ui.ReportChart.REPORT;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;
import com.econorma.util.SwingUtils;

public class ProvePanel extends JPanel implements ActionListener {

	private static final String TAG = "ProvePanel";
	private static final Logger logger = Logger.getLogger(ProvePanel.class);
	private static final DateFormat filedateformat=new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");

	private JXTable tabellaProve;
	private ProveModel proveModel;

	private DetailPanel detailPanel;
	private JLabel labelSender;
	private JTextField mailSender;
	private boolean cancel=true;
	private final Timer timer = new Timer(30 * 1000, this);

	public ProvePanel(){
		initCompontens();
	}


	protected void initCompontens(){


		JScrollPane jScrollPane = new JScrollPane();
		tabellaProve = new MyTable();
		tabellaProve.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		proveModel = new ProveModel(Application.getInstance().getDao()); 

		jScrollPane.setViewportView(tabellaProve);

		setLayout(new GridLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		add(jScrollPane);


		tabellaProve.setModel(proveModel);

		tabellaProve.getColumnExt(ProveModel.P_ID).setMaxWidth(80);
		tabellaProve.getColumnExt(ProveModel.P_SONDA).setMinWidth(200);
		tabellaProve.getColumnExt(ProveModel.P_SONDA).setMaxWidth(200);
		tabellaProve.getColumnExt(ProveModel.P_DATA_SCARICO).setMaxWidth(300);
		tabellaProve.getColumnExt(ProveModel.P_DATA_SCARICO).setMinWidth(300);
		//			tabellaProve.getColumnExt(ProveModel.P_RESPONSABILE).setMaxWidth(300);
		tabellaProve.getColumnExt(ProveModel.P_DATA_PROVA).setMaxWidth(200);
		tabellaProve.getColumnExt(ProveModel.P_DATA_PROVA).setMinWidth(200);
		tabellaProve.getColumnExt(ProveModel.P_DATA_FINE_PROVA).setMinWidth(200);
		tabellaProve.getColumnExt(ProveModel.P_DATA_FINE_PROVA).setMaxWidth(200);
		tabellaProve.getColumnExt(ProveModel.P_EXEC_DETTAGLI).setMaxWidth(80);
		tabellaProve.getColumnExt(ProveModel.P_EXEC_ESPORTA_EXCEL).setMaxWidth(80);
		tabellaProve.getColumnExt(ProveModel.P_EXEC_REPORT).setMaxWidth(80);
		tabellaProve.getColumnExt(ProveModel.C_EXEC_CANCELLA).setMaxWidth(80);
		tabellaProve.getColumnExt(ProveModel.C_EXEC_EXPORT).setMaxWidth(80);

		tabellaProve.getColumnExt(ProveModel.C_EXEC_DETTAGLI).setCellRenderer(new ButtonCellRenderer());
		tabellaProve.getColumnExt(ProveModel.C_EXEC_DETTAGLI).setCellEditor(new DetailCellEditor(new JCheckBox()));

		tabellaProve.getColumnExt(ProveModel.C_EXEC_ESPORTA_EXCEL).setCellRenderer(new ButtonCellRenderer());
		tabellaProve.getColumnExt(ProveModel.C_EXEC_ESPORTA_EXCEL).setCellEditor(new ExcellCellEditor(new JCheckBox()));

		tabellaProve.getColumnExt(ProveModel.C_EXEC_REPORT).setCellRenderer(new ButtonCellRenderer());
		tabellaProve.getColumnExt(ProveModel.C_EXEC_REPORT).setCellEditor(new ReportCellEditor(new JCheckBox()));

		tabellaProve.getColumnExt(ProveModel.C_EXEC_CANCELLA).setCellRenderer(new ButtonCellRenderer());
		tabellaProve.getColumnExt(ProveModel.C_EXEC_CANCELLA).setCellEditor(new CancelCellEditor(new JCheckBox()));
		
		tabellaProve.getColumnExt(ProveModel.C_EXEC_EXPORT).setCellRenderer(new ButtonCellRenderer());
		tabellaProve.getColumnExt(ProveModel.C_EXEC_EXPORT).setCellEditor(new ExportCellEditor(new JCheckBox()));

		EventBusService.subscribe(this);

	}


	class ExcelCellImageRenderer extends DefaultTableCellRenderer { 
		/**
		 * 
		 */
		private static final long serialVersionUID = -7746205491190661955L;
		JLabel lbl = new JLabel(); 
		JButton btn = new JButton(); 
		ImageIcon icon = null; 

		int width = 24;
		int height = 24;
		ExcelCellImageRenderer() { 
			BufferedImage original = null;
			try {
				original = ImageIO.read((InputStream) Thread.currentThread().getContextClassLoader().getResourceAsStream("com/econorma/ui/resources/excel_icon.gif"));
				//			original = ImageIO.read(getClass().getResourceAsStream("excel_icon.gif"));
			} catch (IOException e) {
				logger.error(TAG, e);
			}
			// Create new (blank) image of required (scaled) size

			BufferedImage scaledImage = new BufferedImage(
					width, height, BufferedImage.TYPE_INT_ARGB);

			// Paint scaled version of image to new image

			Graphics2D graphics2D = scaledImage.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			graphics2D.drawImage(original, 0, 0, width, height, null);


			// clean up

			graphics2D.dispose();
			icon = new ImageIcon(scaledImage);
		} 

		public Component getTableCellRendererComponent(JTable table, Object value, 
				boolean isSelected, boolean hasFocus,  
				int row, int column) {                   
			//lbl.setText((String)value); 
			lbl.setIcon(icon); 
			btn.setIcon(icon);

			//	        return lbl; 
			return btn; 
		}

		private Dimension d = new Dimension(75, 25);

		@Override
		public Dimension getPreferredSize() {
			// TODO Auto-generated method stub
			return d;
		}

		@Override
		public void setPreferredSize(Dimension preferredSize) {
			// TODO Auto-generated method stub
			super.setPreferredSize(d);
		}



	} 


	private class ExcellCellEditor extends ButtonCellEditor implements ActionListener{

		private Prova prova;


		public ExcellCellEditor(JCheckBox checkBox) {
			super(checkBox);
		}

		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int col) {

			JButton component = (JButton) super.getTableCellEditorComponent(table, value, isSelected, row, col);
			component.removeActionListener(this);
			component.addActionListener(this);

			prova = proveModel.getProva(row);

			return component;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			//			JFrame frame = new JFrame("Esporta Prova in Excel");
			final JFileChooser jFileChooser = new JFileChooser();
			jFileChooser.addChoosableFileFilter(new ExcelFileFilter());
			jFileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
			jFileChooser.setFileHidingEnabled(true);

			File f=null;
			try {
				f = new File(new File(".").getCanonicalPath());
			} catch (IOException e1) {

				e1.printStackTrace();
			}
			jFileChooser.setCurrentDirectory(f);


			File file = null;

			file = new File("Prova__"+prova.getId()+"__"+filedateformat.format(prova.getData_inizio()));


			jFileChooser.setSelectedFile(file);

			jFileChooser.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {

					if (arg0.getActionCommand().equals(JFileChooser.CANCEL_SELECTION))
						return;

					try{
						File selectedFile =  jFileChooser.getSelectedFile();
						int status = 0;

						if (status == JFileChooser.CANCEL_OPTION)  
							return;

						MDC.put("event_name", "Export");

						if(selectedFile!=null){
							ExporterProva exporterProva = new ExporterProva(prova);

							if(selectedFile.isDirectory()){
								logger.info(TAG, "Salvataggio su cartella selezionato: "+selectedFile.getAbsolutePath());
								logger.log(LoggerCustomLevel.AUDIT, "Export Prova: " + prova.getId());
								exporterProva.export(selectedFile);
							}else{
								logger.info(TAG, "Salvataggio su file selezionato: "+selectedFile.getAbsolutePath());
								logger.log(LoggerCustomLevel.AUDIT, "Export Prova: " + prova.getId());
								File parent = selectedFile.getParentFile();
								String fileName = selectedFile.getAbsolutePath().substring(parent.getAbsolutePath().length()+1);
								exporterProva.exporter(parent, fileName);
							}


						} else {

							if(selectedFile!=null){
								ExporterProva exporterProva = new ExporterProva(prova);
								if(selectedFile.isDirectory()){
									logger.info(TAG, "Salvataggio su cartella selezionato: "+selectedFile.getAbsolutePath());
									logger.log(LoggerCustomLevel.AUDIT, "Export Registrazione: " + prova.getId());
									exporterProva.export(selectedFile);
								}else{
									logger.info(TAG, "Salvataggio su file selezionato: "+selectedFile.getAbsolutePath());
									logger.log(LoggerCustomLevel.AUDIT, "Export Registrazione: " + prova.getId());
									File parent = selectedFile.getParentFile();
									String fileName = selectedFile.getAbsolutePath().substring(parent.getAbsolutePath().length()+1);
									exporterProva.exporter(parent, fileName);
								}
							}

						}





					}catch(Exception e){
						logger.error(TAG, e);
						throw new RuntimeException(e);
					}
				}
			});
			int returnVal = jFileChooser.showDialog(Application.getInstance().getMainFrame(), "Salva");
		}


		private class ExcelFileFilter extends FileFilter {

			public boolean accept(File file) {
				if (file.isDirectory()) return true;
				String fname = file.getName().toLowerCase();
				return fname.endsWith("xls");
			}

			public String getDescription() {
				return "Documento Excel '97";
			}
		}

	}





	private class ReportCellEditor extends ButtonCellEditor implements ActionListener{

		private Prova prova;

		public ReportCellEditor(JCheckBox checkBox) {
			super(checkBox);
		}

		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int col) {

			JButton component = (JButton) super.getTableCellEditorComponent(table, value, isSelected, row, col);
			component.removeActionListener(this);
			component.addActionListener(this);

			prova = proveModel.getProva(row);

			return component;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			try{

				EconormaChart econormaChart=null;
				List<Sensore> sensori = new ArrayList<Sensore>(); 
				Sensore s = Sensore.newInstance(prova.getId_sensore(), 0, null, null, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null, 0, 0, 0, 0, 0, null, 0, 0, 0, 0, 0, 0, null);
				sensori.add(s);

				LettureProva lettureProva = null;

				lettureProva = new LettureProva.LettureProvaCreator().loadAll(prova);


				econormaChart = new EconormaChart(lettureProva, sensori, prova);


				String responsabile = prova.getResponsabile();
				if (responsabile== null){
					responsabile="";
				}

				Date dataInizioProva = prova.getData_inizio();
				Date dataFineProva = prova.getData_fine();
				String idprova = "" + prova.getId();
				ReportChart reportChart=null;
				Date dataProva = prova.getData_scarico();
				Date dataA = new Date();
				int interval=0;

				reportChart = new ReportChart(REPORT.RIEPILOGO_PROVA, econormaChart.getJFreeChart(), responsabile, dataProva, dataInizioProva, dataFineProva, idprova, null, null);

				try{
					reportChart.showPrintDialog(ProvePanel.this);
				}catch (Exception e) {
					logger.error(TAG, "Errore nell'apertura del dialog report", e);
				}

			}catch(Exception e){
				logger.error(TAG, e);
				throw new RuntimeException(e);
			}
		}


	}


	class BorderCellRenderer extends DefaultTableCellRenderer { 
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {


			JLabel original =(JLabel) super.getTableCellRendererComponent(
					table, value, isSelected, hasFocus, row, column);


			original.setBorder( BorderFactory.createEmptyBorder( 3, 3, 3, 3) );



			return original;
		}
	}


	private class DetailCellEditor extends ButtonCellEditor implements ActionListener{

		private Prova prova;

		public DetailCellEditor(JCheckBox checkBox) {
			super(checkBox);
		}

		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int col) {

			JButton component = (JButton) super.getTableCellEditorComponent(table, value, isSelected, row, col);
			component.removeActionListener(this);
			component.addActionListener(this);

			prova = proveModel.getProva(row);


			return component;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			try{


				DetailWindow dettagli = new DetailWindow
						(Application
								.getInstance().getMainFrame(), false, prova);
				SwingUtils.showCenteredDialog(dettagli);



			}catch(Exception e){
				logger.error(TAG, e);
				throw new RuntimeException(e);
			}
		}


	}

	private class CancelCellEditor extends ButtonCellEditor implements ActionListener{

		private Prova prova;
		private int riga;

		public CancelCellEditor(JCheckBox checkBox) {
			super(checkBox);
		}

		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int col) {

			JButton component = (JButton) super.getTableCellEditorComponent(table, value, isSelected, row, col);
			component.removeActionListener(this);
			component.addActionListener(this);


			prova = proveModel.getProva(row);


			riga=row;

			return component;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			try{

				if (riga==0){

					JOptionPane.showMessageDialog(null, "Impossibile cancellare la memorizzazione in corso" ,"Errore Cancellazione", JOptionPane.ERROR_MESSAGE);
					return;

				}



				int reply = JOptionPane.showConfirmDialog(null, Application.getInstance().getBundle().getString("provePanel.confermiCancellazione"), Application.getInstance().getBundle().getString("provePanel.cancellaDati"), JOptionPane.YES_NO_OPTION);

				if (reply == JOptionPane.YES_OPTION) {

					MDC.put("event_name", "Cancellazione");

					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							Application.getInstance().getDao().deleteLettureProvaByProva(prova);
							Application.getInstance().getDao().deleteProvaByProva(prova);
							JOptionPane.showMessageDialog(null, "Cancellazione eseguita con successo");

							logger.log(LoggerCustomLevel.AUDIT, "Cancellazione Id: " + prova.getId());

							EventBusService.publish(Events.CANCEL_PROVA);

						}
					});

				}
				else {
					return;
				}



			}catch(Exception e){
				logger.error(TAG, e);
				throw new RuntimeException(e);
			}
		}


	}
	
	
	private class ExportCellEditor extends ButtonCellEditor implements ActionListener{

		private Prova prova;


		public ExportCellEditor(JCheckBox checkBox) {
			super(checkBox);
		}

		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int col) {

			JButton component = (JButton) super.getTableCellEditorComponent(table, value, isSelected, row, col);
			component.removeActionListener(this);
			component.addActionListener(this);

			prova = proveModel.getProva(row);

			return component;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (!Application.getInstance().isTemperature()){
				JOptionPane.showMessageDialog(null, "Impossibile Salvare file se tipo DataLogger non di Temperatura" ,"Errore Cancellazione", JOptionPane.ERROR_MESSAGE);
				return;

			}
			 
			final JFileChooser jFileChooser = new JFileChooser();
			jFileChooser.addChoosableFileFilter(new ExcelFileFilter());
			jFileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
			jFileChooser.setFileHidingEnabled(true);

			File f=null;
			try {
				f = new File(new File(".").getCanonicalPath());
			} catch (IOException e1) {

				e1.printStackTrace();
			}
			jFileChooser.setCurrentDirectory(f);


			File file = null;

			file = new File("Export__"+prova.getId()+"__"+filedateformat.format(prova.getData_inizio()));


			jFileChooser.setSelectedFile(file);

			jFileChooser.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {

					if (arg0.getActionCommand().equals(JFileChooser.CANCEL_SELECTION))
						return;

					try{
						File selectedFile =  jFileChooser.getSelectedFile();
						int status = 0;

						if (status == JFileChooser.CANCEL_OPTION)  
							return;

						MDC.put("event_name", "Export");

						if(selectedFile!=null){
							ExporterDat exporterDat = new ExporterDat(prova);

							if(selectedFile.isDirectory()){
								logger.info(TAG, "Salvataggio su cartella selezionato: "+selectedFile.getAbsolutePath());
								logger.log(LoggerCustomLevel.AUDIT, "Export Prova: " + prova.getId());
								exporterDat.export(selectedFile);
							}else{
								logger.info(TAG, "Salvataggio su file selezionato: "+selectedFile.getAbsolutePath());
								logger.log(LoggerCustomLevel.AUDIT, "Export Prova: " + prova.getId());
								File parent = selectedFile.getParentFile();
								String fileName = selectedFile.getAbsolutePath().substring(parent.getAbsolutePath().length()+1);
								exporterDat.exporter(parent, fileName);
							}


						}  



					}catch(Exception e){
						logger.error(TAG, e);
						throw new RuntimeException(e);
					}
				}
			});
			int returnVal = jFileChooser.showDialog(Application.getInstance().getMainFrame(), "Salva");
		}


		private class ExcelFileFilter extends FileFilter {

			public boolean accept(File file) {
				if (file.isDirectory()) return true;
				String fname = file.getName().toLowerCase();
				return fname.endsWith("dat");
			}

			public String getDescription() {
				return "Generic Data File";
			}
		}

	}


	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == timer) {
			proveModel.fireTableDataChanged();
		}

	}

	@EventHandler
	public void handleEvent(final Events.DischargeEvent dischargeEvent){
		switch(dischargeEvent.action){
		case RUNNING:
			break;
		case COMPLETE:
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					if(Testo.LOGGER_AUTOMATICO.equals(dischargeEvent.type)){
						proveModel.fireTableDataChanged();
						refresh();

					}else if (Testo.LOGGER_MANUALE.equals(dischargeEvent.type)){
						proveModel.fireTableDataChanged();
						refresh();
					}		 			
				}
			});
		}
	}


	public ProveModel getProveModel() {
		return proveModel;
	}


	public void setProveModel(ProveModel proveModel) {
		this.proveModel = proveModel;
	}

	public void refresh() {
		tabellaProve.revalidate();
		tabellaProve.repaint();
	}


	public JXTable getTabellaProve() {
		return tabellaProve;
	}


	public void setTabellaProve(JXTable tabellaProve) {
		this.tabellaProve = tabellaProve;
	}

}
