package com.econorma.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.AbstractButton;
import javax.swing.plaf.basic.BasicButtonUI;

public class MyButtonUI extends BasicButtonUI {

    private static final Color BACKGROUND_COLOR = Color.YELLOW;
    private static final Color SELECT_COLOR = Color.RED;
    
    public MyButtonUI() {
        super();
    }

    @Override
    protected void paintText(Graphics g, AbstractButton b, Rectangle r, String t) {
        super.paintText(g, b, r, t);
        g.setColor(SELECT_COLOR);
        g.drawRect(r.x, r.y, r.width, r.height);
    }

    @Override
    protected void paintFocus(Graphics g, AbstractButton b,
        Rectangle viewRect, Rectangle textRect, Rectangle iconRect) {
        super.paintFocus(g, b, viewRect, textRect, iconRect);
        g.setColor(Color.blue.darker());
        g.drawRect(viewRect.x, viewRect.y, viewRect.width, viewRect.height);
    }

    @Override
    protected void paintButtonPressed(Graphics g, AbstractButton b) {
        if (b.isContentAreaFilled()) {
            g.setColor(SELECT_COLOR);
            g.fillRect(0, 0, b.getWidth(), b.getHeight());
        }
    }

    @Override
    protected void installDefaults(AbstractButton b) {
        super.installDefaults(b);
        b.setFont(b.getFont().deriveFont(11f));
        b.setBackground(BACKGROUND_COLOR);
    }

   
}
