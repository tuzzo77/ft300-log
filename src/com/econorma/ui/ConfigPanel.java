package com.econorma.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.Lettura.TipoMisura;
import com.econorma.data.LoggerResponse;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.io.CancellaLogger;
import com.econorma.io.ConfiguraLogger;
import com.econorma.io.ParsersLogger.StatusResponse.VERSION;
import com.econorma.io.VerificaLogger;
import com.econorma.logic.SensoreManager;
import com.econorma.logic.logger.DownloadLoggerSingoloDialogListener;
import com.econorma.model.ConfigModel;
import com.econorma.resources.Testo;
import com.econorma.ui.Events.VerificaLoggerEvent;
import com.econorma.util.DateUtil;
import com.econorma.util.Logger;
import com.econorma.util.Theme;

import net.miginfocom.swing.MigLayout;


public class ConfigPanel extends JPanel implements ActionListener {

	// http://www.chka.de/swing/table/faq.html

	/**
	 * 
	 */
	private static final long serialVersionUID = 6000355186796391145L;
	private JLabel lb_allarmi;
	private JButton writeButton;
	private JButton resetButton;
	private JButton readButton;
	private JButton downloadButton;
	private MyTable tableConfig;
	private JScrollPane scrollConfig;

	private static final Logger logger = Logger.getLogger(ConfigPanel.class);
	private static final String TAG = ConfigPanel.class.getSimpleName();

	private static Application app = Application.getInstance();
	private static Color DEFAULT_COLOR;

	// keep hard references
	ConfigModel configModel;


	/**
	 * Create the panel.
	 */
	public ConfigPanel() {
		
		switch (app.getTheme()) {
		case DARK:
			DEFAULT_COLOR = Theme.WHITE;
			break;
		case LIGHT:
			DEFAULT_COLOR = Theme.BLACK;
		default:
			break;
		}

		tableConfig = new MyTable();
		tableConfig.setFillsViewportHeight(true);

		tableConfig.setDropMode(DropMode.ON_OR_INSERT);

		//		allarmiModel = new AllarmiModel(Application.getInstance().getDao());
		configModel = new ConfigModel(null);

		tableConfig.setModel(configModel);
 
 
		tableConfig.setDefaultRenderer(Object.class, new CustomRenderer());
		
		tableConfig.getColumn(configModel.COL_VALORE).setCellRenderer(new MyRenderer());
//		tableConfig.getColumn(configModel.COL_VALORE).setCellEditor(new MyRenderer());
		
		
		tableConfig.setRowHeight(22);
		 
		tableConfig.getColumnExt(ConfigModel.COL_VALORE).setMaxWidth(200);
		 

		final String sensori_interni_label;
 
		setLayout(new MigLayout("fill"));
		sensori_interni_label = Testo.CONFIGURA_PARAMETRI;
		
		lb_allarmi = new JLabel(sensori_interni_label);
		writeButton = new JButton(Testo.WRITE_CONFIG);
		resetButton = new JButton(Testo.RESET_CONFIG);
		readButton = new JButton(Testo.READ_CONFIG);
		downloadButton = new JButton(Testo.DOWNLOAD_SAMPLES);
		add(lb_allarmi, "gap 5px 5px, growprio 1, wrap");

		scrollConfig = new JScrollPane();
		new JPanel();
		add(scrollConfig, "gap 5px 5px, growy, growx, pushy, pushx, wrap");
		scrollConfig.setViewportView(tableConfig);
		
		JPanel bottom = new JPanel(new MigLayout("align 50% 50%"));
		add(bottom, "south, span");
		bottom.add(readButton, "width 100");
		bottom.add(writeButton, "width 100");
		bottom.add(downloadButton, "width 100");
		bottom.add(resetButton, "width 100");
		
		readButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				VerificaLogger verificaLogger = new VerificaLogger(null);
				try {
					
					LoggerResponse response = verificaLogger.verifica();
					if(response.isValid()){
						
						Sensore oldSensor = SensoreManager.getInstance().getSensoreById(response.getIdSondaComplete());
						
						String idSondaComplete = response.getIdSondaComplete();
						int trasmission = response.getTrasmission();
						Date data = response.getData();
						String statoBatteria = response.getStatoBatteria();
						Double temperature = response.getTemperature();
						Double humidity = response.getHumidity();
						int samples = response.getSamples();
						Double volt = response.getVolt();
						Double milliampere = response.getMilliampere();
						Double ohm = response.getOhm();
						String apertoChiuso = response.getApertoChiuso();
						VERSION version = response.getVersione();
						
						if (!Application.getInstance().isOhm()) {
							ohm = 0.0;	
						}
						if (!Application.getInstance().isVolt()) {
							volt = 0.0;
						}
						if (!Application.getInstance().isApertoChiuso()) {
							apertoChiuso = "0";
						}
						if (!Application.getInstance().isMilliampere()) {
							milliampere = 0.0;
						}
						
						
						Sensore sensore = Sensore.newInstance(idSondaComplete, 0, Testo.DESCRIZIONE_LOGGER_NEW, Location.INTERNO, 0, 0, 0, 0, 0, 0, 0, 
								trasmission, data, TipoMisura.TEMPERATURA_UMIDITA, statoBatteria, temperature, humidity, volt, milliampere, ohm, 
								apertoChiuso, 0, 0, 0, 0, 0, samples, version);
						
						if (oldSensor != null) {
							sensore.setDescrizione(oldSensor.getDescrizione());
							sensore.setOffset(oldSensor.getOffset());
							sensore.setRangeMin(oldSensor.getRangeMin());
							sensore.setRangeMax(oldSensor.getRangeMax());
							sensore.setRangeMinURT(oldSensor.getRangeMinURT());
							sensore.setRangeMaxURT(oldSensor.getRangeMaxURT());
						}
						
						final Lettura lettura = new Lettura();
						lettura.setTipoMisura(sensore.getTipoMisura());
						lettura.setStatoBatteria(sensore.getStatoBatteria());
						lettura.setTemperaturaGrezza(sensore.getTemperature());
						lettura.setUmiditaGrezza(sensore.getHumidity());
						lettura.setVolt(sensore.getVolt());
						lettura.setMilliampere(sensore.getMilliampere());
						lettura.setOhmGrezzo(sensore.getOhm());
						lettura.setApertoChiuso(sensore.getApertoChiuso());
						lettura.setIdSonda(sensore.getId_sonda());
						lettura.setData(sensore.getData());
						lettura.setOffset(sensore.getOffset());
//						lettura.setSensore(sensore);
						
						SensoreManager.getInstance().saveSensore(sensore);
						SensoreManager.getInstance().onRead(lettura);
						
						JOptionPane.showMessageDialog(null, Testo.LETTURA_PARAMETRI,
								"Econorma", JOptionPane.INFORMATION_MESSAGE);
						
						configModel = new ConfigModel(sensore);

						tableConfig.setModel(configModel);
						
						configModel.fireTableDataChanged();
						
						VerificaLoggerEvent verificaLoggerEvent = new Events.VerificaLoggerEvent(sensore.getId_sonda());
						EventBusService.publish(verificaLoggerEvent);
						
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, Testo.LETTURA_PARAMETRI_ERRORI,
							"Econorma", JOptionPane.ERROR_MESSAGE);
				}
				
				
				
			}
		});
		
		writeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				ConfiguraLogger configuraLogger = new ConfiguraLogger(null);
				try {
					
					int trasmission = 0;
					
					int rowCount = configModel.getRowCount();
					if (rowCount == 0) {
//						JOptionPane.showMessageDialog(null, Testo.DOWNLOAD_ERRORE,
//								"Econorma", JOptionPane.ERROR_MESSAGE);
//						return;
					} else {
						String value = (String) configModel.getValueAt(3, 1);
						trasmission = Integer.parseInt(value);
						
					}
					
					if (trasmission ==  0) {
						trasmission = 60;
					}
					
					boolean isValid = configuraLogger.write(trasmission);
					if(isValid){
						JOptionPane.showMessageDialog(null, Testo.CONFIGURA_PARAMETRI_CORRETTA,
								"Econorma", JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, Testo.CONFIGURA_PARAMETRI_ERRORI,
								"Econorma", JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, Testo.CONFIGURA_PARAMETRI_ERRORI,
							"Econorma", JOptionPane.ERROR_MESSAGE);
					logger.error(TAG, e);
				}

			}
		});
		
		resetButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				CancellaLogger cancellaLogger = new CancellaLogger(null);
				try {
					
					boolean isValid = cancellaLogger.write();
					if(isValid){
						JOptionPane.showMessageDialog(null, Testo.CANCELLAZIONE_CORRETTA,
								"Econorma", JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, Testo.CANCELLAZIONE_ERRORI,
								"Econorma", JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, Testo.CANCELLAZIONE_ERRORI,
							"Econorma", JOptionPane.ERROR_MESSAGE);
					logger.error(TAG, e);
				}

			}
		});
		
		downloadButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				int rowCount = configModel.getRowCount();
				if (rowCount == 0) {
					JOptionPane.showMessageDialog(null, Testo.DOWNLOAD_ERRORE,
							"Econorma", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				String idSonda = (String) configModel.getValueAt(0, 1);
				 
				
//				Sensore sensore = Sensore.newInstance(idSonda, 0, null, null, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null, 0, 0, 0, 0, 0, null, 0,0,0, 0, 0, 0, null);
				Sensore sensore = SensoreManager.getInstance().getSensoreById(idSonda);
				
				try {
					boolean scarico = Application.getInstance().getScaricoLoggerManager().scaricoSingolo(sensore, new DownloadLoggerSingoloDialogListener());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, Testo.CONFIGURA_PARAMETRI_ERRORI,
							"Econorma", JOptionPane.ERROR_MESSAGE);
					logger.error(TAG, e);
				}

			}
		});
	 

		tableConfig.packAll();


		EventBusService.subscribe(this);
	}

	@EventHandler
	public void handleEvent(String event) {
		if (Events.APPLICATION_MODE_CHANGED.equals(event)) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {

					updateStatus();
				}
			});
		}
	}
	
	@EventHandler
	public void handleEvent(final Events.DischargeEvent dischargeEvent){
		switch(dischargeEvent.action){
		case RUNNING:
			break;
		case COMPLETE:
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					JDialog dialog = (JDialog) getRootPane().getParent();
					dialog.dispose();
				}
			});
		}
	}

	private void updateStatus() {
		boolean configMode = Application.getInstance().isConfigMode();

		configModel.setEditMode(configMode);

	}
 

	@Override
	public void actionPerformed(ActionEvent event) {
		configModel.fireTableDataChanged();
	}

	public JTable[] getLettureTables(){
		return new JTable[]{tableConfig };
	}

	private static class CustomRenderer extends DefaultTableCellRenderer {
		/**
		 * 
		 */

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			int modelRow = table.convertRowIndexToModel(row);
			Component tableCellRendererComponent = super
					.getTableCellRendererComponent(table, value, isSelected,
							hasFocus, modelRow, column);


			JLabel original =(JLabel) super.getTableCellRendererComponent(
					table, value, isSelected, hasFocus, modelRow, column);


			original.setBorder( BorderFactory.createEmptyBorder( 3, 3, 3, 3) );
			
			tableCellRendererComponent.setForeground(DEFAULT_COLOR);
			tableCellRendererComponent.setLocale(Locale.ITALY);
			return this;
		}
	} 
	
	 
	 


	static class MyRenderer extends DefaultTableCellRenderer {
	    public MyRenderer() { 
	    	
	    	super();
 	    	setHorizontalAlignment(JLabel.RIGHT);
 	   
	    		
	    }

	    public void setValue(Object value) {
	    	Date d = (Date) value;
	    	if(d==null){
	    		 setText("0");
	    	}else{
	    		 setText(Integer.toString(DateUtil.toDelaySeconds(d)));
	    	}
	    }
	}
}
