package com.econorma.ui;

import java.util.Arrays;

import javax.swing.JOptionPane;

import org.apache.commons.codec.digest.DigestUtils;

import com.adamtaft.eb.EventBusService;
import com.econorma.Application;
import com.econorma.data.User;
import com.econorma.ui.Events.UserEvent;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

public class CreateUser extends javax.swing.JDialog {

	private static final String TAG = CreateUser.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(CreateUser.class);
	 
	public CreateUser(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
		EventBusService.subscribe(this);
	}

	public void initComponents(){

		jNomeCompleto = new javax.swing.JTextField();
		jLabel2 = new javax.swing.JLabel();
		jLabel1 = new javax.swing.JLabel();
		jNomeUtente = new javax.swing.JTextField();
		jLabel5 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jTipoUtente = new javax.swing.JComboBox();
		jPassword = new javax.swing.JPasswordField();
		jPasswordRepeat = new javax.swing.JPasswordField();
		jCrea = new javax.swing.JButton();
		jAnulla = new javax.swing.JButton();
		jLabel6 = new javax.swing.JLabel();
        jLanguage = new javax.swing.JComboBox();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
 
		jLabel2.setText("Nome utente");

		jLabel1.setText("Tipo utente");
 
		jLabel5.setText("Ripeti password");

		jLabel4.setText("Password");

		jLabel3.setText("Nome completo");
 	 
		jTipoUtente.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Administrator", "Standard" }));
		jTipoUtente.setEditable(false);

		jCrea.setText("Crea Utente");

		jAnulla.setText("Annulla");

		jLabel6.setText("Lingua");

        jLanguage.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Italiano", "English" }));

		 
		jCrea.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbtn_okActionPerformed(evt);
			}
		});


	 	jAnulla.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbtn_annullaActionPerformed(evt);
			}
		});

		    org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(22, 22, 22)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .add(jCrea)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jAnulla, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 125, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(layout.createSequentialGroup()
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jLabel5)
                                .add(jLabel4)
                                .add(jLabel3))
                            .add(18, 18, 18)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jNomeCompleto)
                                .add(jPassword, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                                .add(jPasswordRepeat)))
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jLabel2)
                                .add(jLabel1))
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                .add(jNomeUtente, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 186, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jTipoUtente, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 186, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(layout.createSequentialGroup()
                            .add(jLabel6)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jLanguage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 186, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(43, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(16, 16, 16)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(jTipoUtente, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(27, 27, 27)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel2)
                    .add(jNomeUtente, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(29, 29, 29)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel3)
                    .add(jNomeCompleto, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(29, 29, 29)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel4)
                    .add(jPassword, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(33, 33, 33)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel5)
                    .add(jPasswordRepeat, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(28, 28, 28)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel6)
                    .add(jLanguage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 35, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jCrea)
                    .add(jAnulla))
                .add(15, 15, 15))
        );

        pack();
	}

	private void jbtn_okActionPerformed(java.awt.event.ActionEvent evt) { 
		
		char[] password = jPassword.getPassword();
		char[] passwordR = jPasswordRepeat.getPassword();
		
		boolean equals = Arrays.equals(password, passwordR);
		
		if (!equals) {
			JOptionPane.showMessageDialog (null, "Password digitata non corretta", "Errore", JOptionPane.ERROR_MESSAGE);
			return;
		}

		User user = new User();
		user.setUser(jNomeUtente.getText());
		user.setPassword(getSHA1(jPassword.getText()));
		user.setUser_complete(jNomeCompleto.getText());
		user.setType(jTipoUtente.getSelectedItem().toString());
		user.setLanguage(jLanguage.getSelectedItem().toString());
		Application.getInstance().getDao().insertUser(user);
		
		logger.log(LoggerCustomLevel.AUDIT, "Nuovo utente creato: " + user.getUser());
		
		UserEvent userEvent = new Events.UserEvent(user.getUser());
		EventBusService.publish(userEvent);
		
		dispose();
		
	}           

	private void jbtn_annullaActionPerformed(java.awt.event.ActionEvent evt) {   
		dispose();
	}

	public String getSHA1(String text){
		return  DigestUtils.shaHex(text);
	}
	
	
	// Variables declaration - do not modify                     
	private javax.swing.JButton jAnulla;
	private javax.swing.JButton jCrea;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
    private javax.swing.JComboBox jLanguage;
	private javax.swing.JTextField jNomeCompleto;
	private javax.swing.JTextField jNomeUtente;
	private javax.swing.JPasswordField jPassword;
	private javax.swing.JPasswordField jPasswordRepeat;
	private javax.swing.JComboBox jTipoUtente;
	// End of variables declaration                      
}
