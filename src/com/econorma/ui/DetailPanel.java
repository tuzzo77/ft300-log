package com.econorma.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DropMode;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;

import org.jdesktop.swingx.JXDatePicker;

import com.adamtaft.eb.EventBusService;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.logic.SensoreManager;
import com.econorma.model.DetailModel;
import com.econorma.util.Logger;
import com.econorma.util.Theme;

import net.miginfocom.swing.MigLayout;


public class DetailPanel extends JPanel implements ActionListener, DocumentListener {

	// http://www.chka.de/swing/table/faq.html

	/**
	 * 
	 */
	private static final long serialVersionUID = 6000355186796391145L;
	private JLabel lb_allarmi;
	private MyTable tableDetail;
	private JScrollPane scrollDetail;

	private static final Logger logger = Logger.getLogger(DetailPanel.class);
	private static final String TAG = DetailPanel.class.getSimpleName();
	private static Application app = Application.getInstance();

	private List<LetturaProva> singole=null;

	private JLabel sondaLabel;
	private JTextField sondaValue;
	private JLabel descrizioneSondaLabel;
	private JTextField descrizioneSondaValue;
	private JLabel dataRegistrazioneLabel;
	private JXDatePicker dataRegistrazioneValue;
	private JSeparator separator;

	private static Color DEFAULT_COLOR;

	// keep hard references
	DetailModel detailModel;




	private final Timer timer = new Timer(30 * 1000, this);

	/**
	 * Create the panel.
	 */
	public DetailPanel(Prova prova) {

		switch (app.getTheme()) {
		case DARK:
			DEFAULT_COLOR = Theme.WHITE;
			break;
		case LIGHT:
			DEFAULT_COLOR = Theme.BLACK;
		default:
			break;
		}

		//     	setLayout(new MigLayout("fill", "[][][]"));
		setLayout(new MigLayout("fill"));


		dataRegistrazioneLabel = new JLabel(Application.getInstance().getBundle().getString("detailPanel.data"));
		dataRegistrazioneValue = new JXDatePicker();
		dataRegistrazioneValue.setFormats(new String[] { "E dd-MM-yyyy" });

		Application.getInstance().getGui().registerDataRegistrazioneField(dataRegistrazioneValue);


		sondaLabel = new JLabel(Application.getInstance().getBundle().getString("detailPanel.idSonda"));
		sondaValue = new JTextField();
		Application.getInstance().getGui().registerIdSondaField(sondaValue);


		descrizioneSondaLabel = new JLabel(Application.getInstance().getBundle().getString("detailPanel.descrizione"));
		descrizioneSondaValue = new JTextField();
		Application.getInstance().getGui().registerDescrizioneSondaField(descrizioneSondaValue);

		separator = new JSeparator();


		JPanel top = new JPanel(new MigLayout());
		add(top, "north");
		top.add(dataRegistrazioneLabel, "grow");
		top.add(dataRegistrazioneValue, "width 110!");
		top.add(sondaLabel, "gapleft 10px, grow");
		top.add(sondaValue, "width 100!");
		top.add(descrizioneSondaLabel, "gapleft 10px, grow");
		top.add(descrizioneSondaValue,  "width 180!, wrap");

		KeyListener listener = new TextListener();

		sondaValue.getDocument().addDocumentListener(this);
		sondaValue.getDocument().putProperty("market", "sonda");

		sondaValue.addKeyListener(listener);


		descrizioneSondaValue.getDocument().addDocumentListener(this);
		descrizioneSondaValue.getDocument().putProperty("market", "descrizioneSonda");

		descrizioneSondaValue.addKeyListener(listener);

		ActionListener DateListener = new DateListener();  
		dataRegistrazioneValue.addActionListener(DateListener);


		add(separator,  "growx, wrap, gap 5px 5px");


		scrollDetail = new JScrollPane();

		new JPanel();
		add(scrollDetail, "gap 5px 5px 5px 5px, growy, growx, pushy, pushx, wrap");


		tableDetail = new MyTable();
		tableDetail.setFillsViewportHeight(true);

		tableDetail.setDropMode(DropMode.ON_OR_INSERT);

		//allarmiModel = new AllarmiModel(Application.getInstance().getDao());
		boolean filter = true;
		singole = Application.getInstance().getDao().findLettureProvaByProva(prova, filter);

		detailModel = new DetailModel(Location.INTERNO, singole, prova);

		tableDetail.setModel(detailModel);


		tableDetail.getColumnExt(detailModel.POS_DATA).setMaxWidth(150);
		tableDetail.getColumnExt(detailModel.POS_DATA).setMinWidth(100);


		DecimalFormat decimal = new DecimalFormat("##,##");
		JFormattedTextField formattedTextField = new JFormattedTextField(decimal);
		TableCellEditor cellEditor = new DefaultCellEditor(formattedTextField);


		tableDetail.setDefaultRenderer(Object.class, new CustomRenderer());


		scrollDetail.setViewportView(tableDetail);

		tableDetail.packAll();


		EventBusService.subscribe(this);


		timer.start();
	}




	private static class LetturaDataFlavor extends DataFlavor {

		public static final LetturaDataFlavor letture = new LetturaDataFlavor();

		private LetturaDataFlavor() {
			super(Lettura.class, "Lettura");
		}
	}

 

	// http://www.informit.com/articles/article.aspx?p=333472&seqNum=2
	// http://tips4java.wordpress.com/2010/01/24/table-row-rendering/




	@Override
	public void actionPerformed(ActionEvent event) {

		detailModel.fireTableDataChanged();

		//		if (event.getSource() == timer) {
		//			allarmiModel.fireTableDataChanged();
		//		 
		//		}

	}

	public JTable[] getLettureTables(){
		return new JTable[]{tableDetail };
	}

	private static class MyCellEditor extends DefaultCellEditor{

		public MyCellEditor(JCheckBox checkBox) {
			super(checkBox);
		}
		public MyCellEditor(JComboBox comboBox) {
			super(comboBox);
		}

		public MyCellEditor(JTextField textField) {
			super(textField);
		}


		@Override
		public boolean stopCellEditing() {

			// TODO Auto-generated method stub
			return super.stopCellEditing();
		}
		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) {
			// TODO Auto-generated method stub
			return super.getTableCellEditorComponent(table, value, isSelected, row, column);
		}


	}

	private static class CustomRenderer extends DefaultTableCellRenderer {
		/**
		 * 
		 */

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			int modelRow = table.convertRowIndexToModel(row);
			Component tableCellRendererComponent = super
					.getTableCellRendererComponent(table, value, isSelected,
							hasFocus, modelRow, column);
			DetailModel detailModel = (DetailModel) table.getModel();
			LetturaProva letturaProva = detailModel.getItem(modelRow);



			JLabel original =(JLabel) super.getTableCellRendererComponent(
					table, value, isSelected, hasFocus, modelRow, column);


			original.setBorder( BorderFactory.createEmptyBorder( 3, 3, 3, 3) );


			Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());

			if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0 || sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0) {

				if (letturaProva != null) {


					Double valore = Double.MIN_VALUE;
					Double umidita = Double.MIN_VALUE;
					Double ohm = Double.MIN_VALUE;

					valore = letturaProva.getValore();

					if (valore != null && outOfLimit(letturaProva))	{
						tableCellRendererComponent.setForeground(Color.RED);
						return this;
					}


					umidita = letturaProva.getUmidita();

					if (umidita != null && outOfLimitURT(letturaProva))	{
						tableCellRendererComponent.setForeground(Color.RED);
						return this;
					}

					ohm = letturaProva.getOhm();

					if (ohm != null && outOfLimitOhm(letturaProva))	{
						tableCellRendererComponent.setForeground(Color.RED);
						return this;
					}

				}

			}
			tableCellRendererComponent.setForeground(DEFAULT_COLOR);
			tableCellRendererComponent.setLocale(Locale.ITALY);
			return this;
		}
	} 


	private static boolean outOfLimit(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0){
			return letturaProva.getValore() < sensore.getRangeMin() || letturaProva.getValore() > sensore.getRangeMax();
		}
		return false;

	}

	private static boolean outOfLimitURT(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0){
			return letturaProva.getUmidita() < sensore.getRangeMinURT() || letturaProva.getUmidita() > sensore.getRangeMaxURT();
		}
		return false;

	}
	
	private static boolean outOfLimitOhm(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMinOhm()!=0 || sensore.getRangeMaxOhm()!= 0){
			return letturaProva.getOhm() < sensore.getRangeMinOhm() || letturaProva.getOhm() > sensore.getRangeMaxOhm();
		}
		return false;

	}


	public class TextListener implements KeyListener
	{
		@Override
		public void keyPressed(KeyEvent e) {

		}

		@Override
		public void keyReleased(KeyEvent e) {

		}

		@Override
		public void keyTyped(KeyEvent e) {
			EventBusService.publish(Events.REFRESH_QUERY_DETAIL);
		}
	}



	public class DateListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {

			EventBusService.publish(Events.REFRESH_QUERY_DETAIL);

		}

	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		// TODO Auto-generated method stub

	}



	@Override
	public void removeUpdate(DocumentEvent e) {
		// TODO Auto-generated method stub

	}



	@Override
	public void changedUpdate(DocumentEvent e) {
		// TODO Auto-generated method stub

	}


}
