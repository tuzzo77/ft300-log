package com.econorma.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.econorma.data.LetturaProva;
import com.econorma.data.Sensore;
import com.econorma.util.DateUtil;

import gnu.io.SerialPort;
 

public class Events {

	private Events() { /* private */}
	
	public static final String EXIT = "Exit";
	public static final String APPLICATION_MODE_CHANGED = "ApplicationModeChanged";
	public static final String APPLICATION_SCREEN_CHANGED = "ApplicationScreenChanged";
	public static final String CLEAR_CHART = "ClearChart";
	public static final String VISUALIZZA_SENSORI_SINGOLI_CHANGED = "VisualizzaSensoriSingoliChanged";
	public static final String SENSORI_RESTORE_COMPLETED = "SensoriRestoreCompleted";
	public static final Object REFRESH_QUERY_DETAIL = "RefreshQueryDetail";
	public static final Object REFRESH_CHART_EXPORT = "RefreshChartExport";
	public static final Object CANCEL_PROVA = "CancelProva";

	
	public static class PortChanged {

		private String port;
		private int baudRate;

		public PortChanged(String cpi, int baudRate) {
			port = cpi;
			this.baudRate=baudRate;
		}

		public String getPort() {
			return port;
		}

		public int getBaudRate() {
			return baudRate;
		}

	}

	public static class PortModemChanged {

		private String port;

		public PortModemChanged(String cpi) {
			port = cpi;
		}

		public String getModemPort() {
			return port;
		}
	}


	public enum Status {
		AVAILABLE, CONNECTED, BUSY, ERROR
	}

	public static class PortEvent {

		public final SerialPort port;
		public final int type;
		public final Exception exception;

		public static final int ERROR_GENERIC = 0;
		public static final int ERROR_OCCUPIED = 1;
		public static final int ERROR_OPEN_FAILED = 2;
		public static final int CONNECTED = 3;
		public static final int READ_EVENT = 4;

		public PortEvent(SerialPort port, int type) {
			this(port, type, null);
		}

		public PortEvent(SerialPort port, int type, Exception e) {
			this.type = type;
			this.port = port;
			this.exception = e;
		}
	}

  

	public static class ChartFilterEvent {

		public enum ACTION {
			ADD_FILTER,
			REMOVE_FILTER
		}
		final public Sensore sensore;
		final public ACTION  action;


		public  ChartFilterEvent(Sensore sensore, ACTION a) {
			this.sensore = sensore;
			this.action = a;
		}

	}

	public static class DischargeEvent {

		public enum ACTION {
			RUNNING,
			COMPLETE
		}

		final public Sensore sensore;
		final public String type;
		final public ACTION  action;

		public DischargeEvent(Sensore sensore, ACTION a, String type) {
			this.sensore = sensore;
			this.action = a;
			this.type=type;

		}
	}


	public static class NuovaLetturaProva{

		public final LetturaProva l;

		public NuovaLetturaProva(LetturaProva letturaProva){
			this.l = letturaProva;
		}
	}

	public static class ExportEvent {


		final public String id_sonda;
		final public Date Da;
		final public Date A;
		final public boolean onlyInAlarm;
		final public boolean onlyMedie;
		final public int minutes;

		public ExportEvent(String id_sonda, Date Da, Date A, boolean onlyInAlarm, boolean onlyMedie, int minutes) {
			this.id_sonda = id_sonda;
			this.Da = Da;
			this.A = A;
			this.onlyInAlarm = onlyInAlarm;
			this.onlyMedie = onlyMedie;
			this.minutes = minutes;
		}
	}
	
	public static class LogEvent {

		final public Date Da;
		final public Date A;

		public LogEvent(Date Da, Date A) {
			this.Da = Da;
			this.A = A;

		}
	}
	
	public static class UserEvent {

		final public String user;
			public UserEvent(String user) {
			this.user = user;
		}
	}

	public static class ChangeSinotticoEvent {


		final public String sinottico;

		public ChangeSinotticoEvent(String sinottico) {
			this.sinottico = sinottico;

		}
	}
	
	public static class VerificaLoggerEvent {


		final public String idSonda;

		public VerificaLoggerEvent(String idSonda) {
			this.idSonda = idSonda;

		}
	}
	
	public static class TabbedPaneEvent {


		final public int index;

		public TabbedPaneEvent(int index) {
			this.index = index;

		}
 
	}


	public static class DiscoveryEndEvent{

		public final boolean ok;
		public final  List<String> sensoriTrovati;

		public DiscoveryEndEvent(boolean ok, List<String> sensoriTrovati){
			this.ok = ok;
			this.sensoriTrovati = sensoriTrovati;
		}

		@Override
		public String toString() {
			return "DiscoveryEndEvent [ok=" + ok + ", sensoriTrovati="
					+ sensoriTrovati + "]";
		}
	}
	
	public static class MainWindowInfoEvent{
		
		private String message;
		private Type type; 
		private long startTime;
		private long endTime;
		
		public enum Type {
			START, RUNNING, END
		}
		
		public MainWindowInfoEvent(Type type, long startDate, long endDate, String message){
			setMessage(message);
			setType(type);
			setStartTime(startDate);
			setEndTime(endDate);
		}
		
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public Type getType() {
			return type;
		}
		public void setType(Type type) {
			this.type = type;
		}
		public long getStartTime() {
			return startTime;
		}
		public void setStartTime(long startTime) {
			this.startTime = startTime;
		}
		public long getEndTime() {
			return endTime;
		}
		public void setEndTime(long endTime) {
			this.endTime = endTime;
		}

		/**
		 * usato per scrivere sulla barra delle notifiche
		 */
		@Override
		public String toString() {
			SimpleDateFormat FORMATTER_DATE = new SimpleDateFormat("HH:mm:ss");
			
			switch (type) {
			case START:
				break;
			case END:
				long elapsedTime = System.currentTimeMillis() - startTime;
				int t = (int) (elapsedTime / 1000);
				message = message + " Durata: " + DateUtil.formatElapsedTime(t);
				break;

			default:
				break;
			}
			
			return FORMATTER_DATE.format(startTime) + " - " + message;
		}
		
	}
}
