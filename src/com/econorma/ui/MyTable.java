package com.econorma.ui;

import java.awt.Component;

import javax.swing.table.TableCellEditor;

import org.jdesktop.swingx.JXTable;

import com.econorma.Application;
import com.econorma.util.Theme;

public class MyTable extends JXTable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7663077065549587996L;

	public MyTable() {
		super();
	
			setColumnControlVisible(true);
			setShowGrid(true);
			switch (Application.getInstance().getTheme()) {
			case DARK:
				setGridColor(Theme.MY_GRAY);
				break;
			case LIGHT:
				setGridColor(Theme.LIGHT_GRAY);
				break;
			}
		
		
		switch (Application.getInstance().getTheme()) {
		case DARK:
			getTableHeader().setOpaque(false);
			getTableHeader().setBackground(Theme.DARK);
			getTableHeader().setForeground(Theme.SMOKE_WHITE);
			break;
		case LIGHT:
			break;
		}
	
	}

	@Override
	protected void createDefaultEditors() {
		// TODO Auto-generated method stub
		super.createDefaultEditors();
	}

	@Override
	public Component prepareEditor(TableCellEditor arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		return super.prepareEditor(arg0, arg1, arg2);
	}

	@Override
	public TableCellEditor getCellEditor() {
		// TODO Auto-generated method stub
		return super.getCellEditor();
	}

	@Override
	public TableCellEditor getCellEditor(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return super.getCellEditor(arg0, arg1);
	}

	@Override
	public void setCellEditor(TableCellEditor arg0) {
		// TODO Auto-generated method stub
		super.setCellEditor(arg0);
	}

	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return super.getColumnName(column).toLowerCase();
	}
 
}

