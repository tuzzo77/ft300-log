package com.econorma.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

import org.jdesktop.swingx.JXStatusBar;

import com.econorma.Application;
import com.econorma.persistence.Preferences;



public class LogWindow extends JDialog implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6746683396726822437L;
	private LogSelectionPanel logSelectionPanel;
	private LogPanel logPanel;
	private JXStatusBar statusBar;
	private Dimension minimumSize;
	private Dimension entrySize;
	private Dimension panelSize;
	private Preferences preferences = Application.getInstance().getPreferences();
	private int wData;


	//events managers
	private RowHooverAdapter rowHooverAdapter;
	private static Application app = Application.getInstance();


	/**
	 * Create the frame.
	 */

	public LogWindow(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
	}


	private void initComponents() {
 
		setFocusable(true);
		addKeyListener(this);

		setLayout(new BorderLayout());


		final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
 
		logSelectionPanel = new LogSelectionPanel();
		logPanel = new LogPanel();
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		entrySize = new Dimension((int) (screenSize.width/1.5), (int) (screenSize.height/1.5));

		
//		minimumSize = new Dimension(600, 500);

//		setMinimumSize(minimumSize);
	 
 		setSize(entrySize);
 


		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
 
			}  
		});

		setTitle(Application.getInstance().getBundle().getString("logWindow.titolo"));


		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, 
				logSelectionPanel, logPanel);
 

		splitPane.setOneTouchExpandable(false);
		splitPane.setMaximumSize(new Dimension(Integer.MAX_VALUE,
				Integer.MAX_VALUE));

		
		add(splitPane);

		minimumSize = new Dimension(420, 50);
		panelSize = new Dimension(230, 50);
		logSelectionPanel.setMinimumSize(panelSize);
		logPanel.setMinimumSize(minimumSize);
 
		splitPane.addComponentListener(new ComponentListener() {

			@Override
			public void componentShown(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentResized(ComponentEvent event) {
				logPanel.setMaximumSize(new Dimension());

			}

			@Override
			public void componentMoved(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentHidden(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		rowHooverAdapter = RowHooverAdapter.registerHoverManager(logPanel.getLettureTables());

	}

	public LogPanel getLogPanel(){
		return logPanel;
	}

	private static class MyStatusBar extends JXStatusBar {

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE)  
		{  
			 
			dispose(); 
		}  

	}


	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	 

}

