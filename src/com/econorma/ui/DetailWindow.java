package com.econorma.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.econorma.Application;
import com.econorma.data.Prova;

public class DetailWindow extends javax.swing.JDialog  implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6746683396726822437L;
	private DetailPanel detailPanel;
	private Dimension minimumSize;
	private Prova prova;
	private static Application app = Application.getInstance();
	//events managers
	private RowHooverAdapter rowHooverAdapter;


	public DetailWindow(java.awt.Frame parent, boolean modal, Prova prova) {
		super(parent, modal);
		this.prova=prova;
		initComponents();


	}
	/**
	 * Create the frame.
	 */
	private void initComponents() {

		setFocusable(true);
		addKeyListener(this);


		setLayout(new BorderLayout());

		detailPanel = new DetailPanel(prova);

	 
		minimumSize = new Dimension(700, 500);
	 
		detailPanel.setMinimumSize(minimumSize);
		detailPanel.setSize(minimumSize);	
 
		setTitle(Application.getInstance().getBundle().getString("detailWindow.dettagliProva"));
		 

		setMinimumSize(minimumSize);


		setModal(true);


		add(detailPanel);



		rowHooverAdapter = RowHooverAdapter.registerHoverManager(detailPanel.getLettureTables());

	}


	public DetailPanel getDetailPanel(){
		return detailPanel;
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE)  
		{  
			dispose(); 
		} 

	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}



}
