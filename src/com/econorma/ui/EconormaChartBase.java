package com.econorma.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItem;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.IntervalXYDataset;

import com.econorma.data.Lettura;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.logic.LettureProva;
import com.econorma.logic.SensoreManager;
import com.econorma.util.Logger;
import com.econorma.util.StringUtils;
 

public class EconormaChartBase {

	private static final String TAG = "EconormaChartBase";
	private static final Logger logger = Logger.getLogger(EconormaChartBase.class);
	 
	private ImprovedTimeSeriesCollection dataset = new ImprovedTimeSeriesCollection();
	private HashMap<String, TimeSeries> serieses = new HashMap<String, TimeSeries>();
	
//	private final JFreeChart chart;
	
	private boolean manageMedie = false;
	private boolean manageSingole = false;
	
	
	private Sensore filterSensor = null;
	
	private HashMap<String , LegendItem> legendStyles = new HashMap<String, LegendItem>();
	
	private Lettura.TipoMisura tipoLettura;
	private int dataSetIndx; // myself
	
	public EconormaChartBase(Lettura.TipoMisura tipoLettura, int dataSetPosition){

		this.tipoLettura = tipoLettura;
		this.dataSetIndx = dataSetPosition;
		manageMedie = false;
		manageSingole = true;
		 
	}
	
	public void restore(LettureProva lettureProva, List<Sensore> sensori, Prova prova){
		dataset.removeAllSeries();

		if(manageMedie){
			
			List<String> idSensoriSorted = lettureProva.getIdSensoriSorted();
			for(String idSensore : idSensoriSorted){
				if(Sensore.isMedia(idSensore)){
					for(LetturaProva l:lettureProva.lettureDelSensore(idSensore)){
						TimeSeries timeSeries = getTimeSeries(idSensore);
						timeSeries.addOrUpdate(new Second(l.getData()),
								l.getValore());
						makeVisible(idSensore);
					}
				}
			}
		}
	
		if(manageSingole){

			if(sensori!=null){

				List<Sensore> sensoriDaRipristinare = new ArrayList<Sensore>();
				for(int ii=0; ii<sensori.size();ii++){
					Sensore sensore = sensori.get(ii);
					String id_sonda = sensore.getId_sonda();
	
					
					
					
					if (id_sonda.equals(Sensore.MEDIA_INTERNA.getId_sonda())){
						continue; // skip
					}
					
					List<LetturaProva> singole;
					
					if(prova!=null)
 						singole = new LettureProva.LettureProvaCreator().loadProvaIdSonda(prova, id_sonda).lettureDelSensore(id_sonda);
					else{
						singole = lettureProva.lettureDelSensore(sensore.getId_sonda());
					}
					
					TimeSeries timeSeries = getTimeSeries(sensore);
					
					if (singole !=null){
						for(LetturaProva l:singole){
							if(l.isValoreMisuraValid(getTipoLettura())){
								timeSeries.addOrUpdate(new Second(l.getData()),
										l.getValoreMisura(getTipoLettura()));
							}
						}
						
						sensoriDaRipristinare.add(sensore);
					}
					
				}
				
				{
					XYPlot plot = (XYPlot) chart.getPlot();
					
					if (plot.getRenderer(dataSetIndx)!=null) {
						StandardXYItemRenderer r = (StandardXYItemRenderer) plot.getRenderer(dataSetIndx);
						r.clearSeriesPaints(true);	
					}
					
					
					for (Sensore s : sensoriDaRipristinare){
						makeVisible(s);
					}
				}
				
			}
			
		}
			
	}
	
	

	
	public void addLettura(Lettura lettura) {

		if(!lettura.isValoreMisuraValid(getTipoLettura()))
			return;
		
		double valore = lettura.getValoreMisura(getTipoLettura());
		
		if(manageMedie){
			if (lettura.getSensore() != null) {
				Sensore sensore = lettura.getSensore();
				if (sensore.getLocation() == Location.VIRTUAL) {
					
						if(sensore.isMedia()){
							TimeSeries timeSeries = getTimeSeries(sensore);
							timeSeries.addOrUpdate(new Second(lettura.getData()),
									valore);
						}
				}
				
				
				
				
				makeVisible(sensore);
				
			}
		}

		if(manageSingole){
			if (lettura.getSensore() != null) {
				Sensore sensore = lettura.getSensore();
				if(!Sensore.Location.VIRTUAL.equals(sensore.getLocation()) &&
						!Sensore.Location.UNKNOWN.equals(sensore.getLocation()) ){


					
					
					
				 
					// tutti i sensori non virtuali e noti

					//real time logic
					
			      
		           TimeSeries timeSeries = getTimeSeries(sensore);
					
					
					timeSeries.addOrUpdate(new Second(lettura.getData()),
							valore);
					
					if(filterSensor==null || StringUtils.equals(filterSensor.getId_sonda(), sensore.getId_sonda()))
						makeVisible(sensore);

				}
			}
		}
		

	}
	
	public void clear() {
		
		//TODO i sensori aggiunti rimangono, semplicemente svuotiamo le letture di tutti
		
		for(TimeSeries s : serieses.values()){
			s.clear();
		}
	}
 
	
	private TimeSeries getTimeSeries(String id_sonda){

		TimeSeries timeSeries = serieses.get(id_sonda);
		if(timeSeries==null){
//			timeSeries = new TimeSeries(id_sonda,
//					RegularTimePeriod.class); 
			
			timeSeries = new TimeSeries(id_sonda,
 					Second.class); 
			serieses.put(id_sonda, timeSeries);
		}
		
		return timeSeries;
	}
	private TimeSeries getTimeSeries(Sensore sensore){
		return getTimeSeries(sensore.getId_sonda());
	}
	
	
	
	private void makeVisible(Sensore sensore){
		makeVisible(sensore.getId_sonda());
	}
	
	private void makeVisible(String idSonda){
		TimeSeries timeSeries = getTimeSeries(idSonda);
		if(!dataset.contains(timeSeries)){
			List<TimeSeries> visibleSeries = dataset.getSeries();
			boolean visible = false;
			if(visibleSeries!=null){
				for(TimeSeries s: visibleSeries){
					if(idSonda.equals(s.getKey()))
						visible = true;
				}
			}
			if(!visible){
				if(timeSeries.isEmpty())
					return;
				dataset.addSeries(timeSeries);
 				restoreLegend(idSonda);
			}
		}
		
	}
	
	
	
	public static class ImprovedTimeSeriesCollection extends TimeSeriesCollection{

		private HashSet<TimeSeries> contained = new HashSet<TimeSeries>();
		
		@Override
		public void addSeries(TimeSeries series) {
			super.addSeries(series);
			contained.add(series);
		}

		@Override
		public void removeSeries(TimeSeries series) {
			super.removeSeries(series);
			contained.clear();
			
		}

		@Override
		public void removeSeries(int index) {
			throw new UnsupportedOperationException("Operazione Non supportata");
		}

		@Override
		public void removeAllSeries() {
			super.removeAllSeries();
			contained.clear();
		}

		public boolean contains(TimeSeries series){
			return contained.contains(series);
		}
		
		
	}
	
	
	public void setManageSingole(boolean value){
		manageSingole = value;
	}
	
	public void applyFilter(Sensore sensore){
		setFilterSensor(sensore);
		//dataset.removeAllSeries();
		XYItemRenderer renderer = chart.getXYPlot().getRenderer(dataSetIndx);
		List<TimeSeries> series = dataset.getSeries();
		for(TimeSeries ts: series){
			int idx = dataset.indexOf(ts);
			if(ts.getKey().equals(sensore.getId_sonda())){
				renderer.setSeriesVisible(idx, Boolean.TRUE);
			}else
				renderer.setSeriesVisible(idx, Boolean.FALSE);
		}

		makeVisible(sensore);
		
	}
	
	public void removeFilters(){
		filterSensor=null;
		// dataset.removeAllSeries();
		XYItemRenderer renderer = chart.getXYPlot().getRenderer(dataSetIndx);
		List<TimeSeries> series = dataset.getSeries();
		for(TimeSeries ts: series){
			int idx = dataset.indexOf(ts);
			renderer.setSeriesVisible(idx, Boolean.TRUE);
		}
		
		//TODO location can change!
		//TODO manage lista sensori dedicata...
		for(Sensore s: SensoreManager.getInstance().retrieveSensors(Sensore.Location.INTERNO)){
			makeVisible(s);
		}
	}
	
	public void setFilterSensor(Sensore s){
		filterSensor= s;
	}
	
	
	private void restoreLegend(String id_sonda){
		
		logger.debug(TAG,"restoreLegend "+id_sonda+" BEGIN");
		if(logger.isDebugEnabled())	logger.debug(TAG,"\t dataset : "+System.identityHashCode(dataset));
		XYPlot plot = (XYPlot) chart.getPlot();
		XYItemRenderer r = plot.getRenderer(dataSetIndx);
							if(logger.isDebugEnabled())	logger.debug(TAG,"\t renderer : "+System.identityHashCode(r));
		LegendItem item = legendStyles.get(id_sonda);
							if(logger.isDebugEnabled())	logger.debug(TAG,"\t legend item : "+((item!=null ) ? System.identityHashCode(item)  : "----"));
		TimeSeries timeSeries = serieses.get(id_sonda);
							if(logger.isDebugEnabled())	logger.debug(TAG,"\t timeSeries : "+((timeSeries!=null ) ? System.identityHashCode(timeSeries)  : "----"));
		if(timeSeries!=null){
			//sto facendo il restore per cui avevo ricevuto delle letture ok!
			int index = dataset.indexOf(timeSeries);
								if(logger.isDebugEnabled())	logger.debug(TAG,"\t  posizione timeseries nel dataset : "+ index);
			if(index==-1){
				throw new RuntimeException(" non posso restorare un dataset di cui non ho la timeseries!");
			}
			if(item!=null){
				// lo avevo già aggiunto al grafico, restore
				if(index!=-1){
					r.setSeriesShape(index, item.getShape());
					r.setSeriesPaint(index, item.getFillPaint());
				}
			}else{
				
				if (r!=null) {
					item =  r.getLegendItem(dataSetIndx, index);
					legendStyles.put(id_sonda, item);	
				}
		 		
				if(logger.isDebugEnabled())	logger.debug(TAG,"\t  save for restore : la time series ha posizone"+ index+" nel dataset, la salvo");
				if(logger.isDebugEnabled())	logger.debug(TAG,"\t legend item per restore : "+((item!=null ) ? System.identityHashCode(item)  : "----"));
			}
			
			if(logger.isDebugEnabled())	logger.debug(TAG,"\t paint obj  : "+((item!=null ) ?  System.identityHashCode(item.getFillPaint())  : "----"));
			if(logger.isDebugEnabled())	logger.debug(TAG,"\t item color  : "+((item!=null ) ? item.getFillPaint()  : "----"));
		}
		
		logger.debug(TAG,"restoreLegend "+id_sonda+" END");
		
	}
	
	public Lettura.TipoMisura getTipoLettura(){
		return tipoLettura;
	}
	
	public IntervalXYDataset getDataSet(){
		return dataset;
	}
	
	private JFreeChart chart;
	
	public void setChart(JFreeChart chart){
		this.chart = chart;
	}
	
	 
}
