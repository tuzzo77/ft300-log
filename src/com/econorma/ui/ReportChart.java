package com.econorma.ui;

 
import java.awt.Component;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.commons.io.IOUtils;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import com.econorma.Application;
import com.econorma.logic.SensoreManager;
import com.econorma.print.ReportChartDetail;
import com.econorma.util.Logger;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.WhenNoDataTypeEnum;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;


public class ReportChart {
	
	public enum REPORT{
		RIEPILOGO_PROVA,
		RIEPILOGO_REALTIME, EXPORT	}
	
	private static final String TAG = ReportChart.class.getCanonicalName();
	
	private static final Logger logger = Logger.getLogger(ReportChart.class);
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	
	    public static void main(String[] args) {  
	     
	 
	        }
	    
	 
	private final REPORT reportType;
	private final JFreeChart chart;
	private final String responsabile;
	private final Date dataInizio;
	private final Date dataFine;
	private final Date dataProva;
	private final String id;
	private final String idSonda;
	private final String descrizioneSonda;
	
	public ReportChart(REPORT reportType, JFreeChart chart, String responsabile, Date dataProva, Date dataInizio, Date dataFine, String id, String idSonda, String descrizioneSonda){
		this.reportType = reportType;
		this.chart = chart;
		this.responsabile = responsabile;
		this.dataInizio = dataInizio;
		this.dataProva=dataProva;
		this.dataFine = dataFine;
		this.id = id;
		this.idSonda=idSonda;
		this.descrizioneSonda=descrizioneSonda;
	}
	
	public PrepraredPrint preparePrint(){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat simpleDateFormatExport = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		
		String provaDuration = SensoreManager.getInstance().provaDuration(dataInizio, dataFine);
		
        Map parameters = new HashMap(); 
        parameters.put("RESPONSABILE",responsabile);  
        parameters.put("ID_PROVA", id);
        parameters.put("DATA_PROVA", simpleDateFormat.format(dataProva));  
        parameters.put("DATA_INIZIO_PROVA", simpleDateFormatExport.format(dataInizio));
        parameters.put("DATA_FINE_PROVA", simpleDateFormatExport.format(dataFine));
        parameters.put("DURATA_PROVA", provaDuration);
        
        parameters.put("DATA_DA", simpleDateFormat.format(dataInizio));
        parameters.put("DATA_A", simpleDateFormat.format(dataFine));
        parameters.put("ID_SONDA", idSonda);
        parameters.put("DES_SONDA", descrizioneSonda);
        
        
        parameters.put("PAGINA", Application.getInstance().getBundle().getString("reportChart.pagina"));
        parameters.put("TITOLO", Application.getInstance().getBundle().getString("reportChart.titolo"));
        parameters.put("DATA_INIZIO_PROVA_DESC", Application.getInstance().getBundle().getString("reportChart.dataInizio"));
        parameters.put("DATA_FINE_PROVA_DESC", Application.getInstance().getBundle().getString("reportChart.dataFine"));
        parameters.put("RESPONSABILE_DESC", Application.getInstance().getBundle().getString("reportChart.responsabile"));
        parameters.put("PROVA_DESC", Application.getInstance().getBundle().getString("reportChart.prova"));
        parameters.put("DURATA_DESC", Application.getInstance().getBundle().getString("reportChart.durata"));
        parameters.put("NUMERO_DESC", Application.getInstance().getBundle().getString("reportChart.numero"));
        
        
     //
        // GENERO IL GRAFICO GLOBALE DELLA PROVA
        //
        
//    	XYPlot plot1 = (XYPlot) chart.getPlot();
//		DateAxis dateAxis1 = (DateAxis) plot1.getDomainAxis();
//		ValueAxis rangeAxis1 = plot1.getRangeAxis();
//		dateAxis1.setTickLabelsVisible(false);
//		rangeAxis1.setTickLabelsVisible(false);
		
		File chartFile = null;
		File chartFileSVG = null;
		Rectangle bounds=new Rectangle(0, 0, 1800, 800);
		try {
			chartFile = File.createTempFile("econorma_reports_", UUID.randomUUID().toString());
 			chartFileSVG = File.createTempFile("econorma_reports_SVG", UUID.randomUUID().toString());
 			chartFile.deleteOnExit(); // just in case
			saveAsPNG(chart, chartFile);
			exportChartAsSVG(chart, bounds, chartFileSVG);
		 } catch (IOException e) {
			//ignore
			logger.error(TAG, "impossibile generare il file temporaneo "+chartFile,e);
		}
		
		
		//
		// IMPOSTO SEMPRE L'IMMAGINE GLOBALE COME PARAMETRO PER RETRO COMPATIBILITA SU TUTTI I REPORT
		//
		parameters.put("CHART_IMAGE", chartFile.toString());
		
		
		//
		// SPLIT GRAFICO PER INTERVALLI
		//
		JRDataSource dataSource = null;
		{
			ArrayList<ReportChartDetail> splitCharts = new ArrayList<ReportChartDetail>();
			

			XYPlot plot = (XYPlot) chart.getPlot();
			DateAxis dateAxis = (DateAxis) plot.getDomainAxis();
			Date minimumDate = dateAxis.getMinimumDate();
			Date maximumDate = dateAxis.getMaximumDate();
//			int span = 0;//1000*60*2;// 1000*60*60*24;
			int span = 1000*60*60*0;
			long leftDuration = maximumDate.getTime() - minimumDate.getTime();
			if(leftDuration<span || span<=0){
				//do nothing, ci sta tutto su una sola immagine 
				//oppure lo span non e' definito
				
				// USO L'IMMAGINE GLOBALE NELLA STAMPA
 			splitCharts.add(new ReportChartDetail(chartFile.toString()));
//			splitCharts.add(new ReportChartDetail(chartFileSVG.toString()));
				
			}else{
				// devo dividere 
				
				// USO UN'IMMAGINE PER OGNI INTERVALLO
				int n = (int) Math.ceil((double)leftDuration / (double)span); //numero di grarfici necessari per coprire l'intervallo di tempo di n*span
				try{
					for(int i=0;i<n;i++){
						
						Date startDate = new Date(minimumDate.getTime()+i*span);
						Date endDate = new Date(minimumDate.getTime()+(i+1)*span);
						dateAxis.setMinimumDate(startDate);
						dateAxis.setMaximumDate(endDate);
						
						String uuid = UUID.randomUUID().toString();
						try {
							File splitChartFile = File.createTempFile("econorma_reports_",uuid+"_"+(i+1));
							saveAsPNG(chart, splitChartFile);
							splitCharts.add(new ReportChartDetail(splitChartFile.toString()));
						} catch (IOException e) {
						}
					}
				}finally{
					//restore just for kindess
					dateAxis.setMinimumDate(minimumDate);
					dateAxis.setMaximumDate(maximumDate);
				}
			}
			dataSource = new JRBeanCollectionDataSource(splitCharts);
		}
		
		return new PrepraredPrint(parameters, chartFile,dataSource);
		
	}
	
	
	
	
	private void saveAsPNG(JFreeChart chart, File file){
		try {
			ChartUtilities.saveChartAsPNG(file, chart, 900, 500);
			
//			MI CRASHA QUANDO HA STAMPO PIU' GRAFICI
			
//			OutputStream outputStream = new FileOutputStream(file);
//			ChartUtilities.writeScaledChartAsPNG(outputStream, chart, 2000, 1000, 2, 2);
			  
			
			logger.info(TAG, "Grafico salvato in "+file.toString());
		} catch (IOException e) {
			logger.error(TAG, "impossibile salvare il chart in "+file.toString(),e);
		}
	}
	
	
	public void exportChartAsSVG(JFreeChart chart, Rectangle bounds, File svgFile) throws IOException {
		
        // Get a DOMImplementation and create an XML document
        DOMImplementation domImpl =
            GenericDOMImplementation.getDOMImplementation();
        Document document = domImpl.createDocument(null, "svg", null);

        // Create an instance of the SVG Generator
        SVGGraphics2D svgGenerator = new SVGGraphics2D(document);

        // draw the chart in the SVG generator
        chart.draw(svgGenerator, bounds);
        
        // Write svg file
        OutputStream outputStream = new FileOutputStream(svgFile);
        Writer out = new OutputStreamWriter(outputStream, "UTF-8");
        svgGenerator.stream(out, true /* use css */);						
        outputStream.flush();
        outputStream.close();
      
        
        logger.info(TAG, "Grafico salvato in "+svgFile.toString());
	}	
	
	
	
	private static class PrepraredPrint {
		
		public final Map<String,Object> params;
		public final File chartFile;
		public JRDataSource dataSource;
		
		public PrepraredPrint(Map<String,Object> params, File chartFile,JRDataSource dataSource){
			this.params = params;
			this.chartFile = chartFile;
			this.dataSource = dataSource;
		}
	}
	
	public void showPrintDialog(Component parent) throws JRException {
		
		PrepraredPrint preparePrint = preparePrint();
		JasperReport template = getTemplate(reportType);
		template.setWhenNoDataType(WhenNoDataTypeEnum.ALL_SECTIONS_NO_DETAIL);
		 
        JasperPrint jasperPrint = JasperFillManager.fillReport(template, preparePrint.params,preparePrint.dataSource);
        
        
        JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
        jasperViewer.setTitle(Application.getInstance().getBundle().getString("reportChart.stampa"));

        jasperViewer.setSize(800, 600);
        jasperViewer.setLocation(500, 100);
        jasperViewer.setResizable(true); 
        jasperViewer.setVisible(true);
        jasperViewer.setAlwaysOnTop(true);
       
	}
	
	private JasperReport getTemplate(REPORT reportType){
		String  reportTemplateFile = null;
		switch (reportType) {
		case RIEPILOGO_PROVA:
			reportTemplateFile =  "ReportChart.jrxml";
			break;
		case EXPORT:
			reportTemplateFile =  "ExportChart.jrxml";
			break;

		default:
			throw new IllegalArgumentException("tipo di report sconosciuto");
		}
		
		URL resource = Thread.currentThread().getContextClassLoader().getResource(reportTemplateFile);
		InputStream reportTemplateStream = null; 
		JasperDesign jasperDesign = null;
		 JasperReport compiledTemplate = null;
		try{
				reportTemplateStream = 	resource.openStream();
			    jasperDesign = JRXmlLoader.load(reportTemplateStream);  
			    compiledTemplate = JasperCompileManager.compileReport(jasperDesign);
		}catch (Exception e) {
			logger.error(TAG, "Impossibile compilare/trovare il template"+resource.toString(),e);
		}
		finally{
			IOUtils.closeQuietly(reportTemplateStream);
		}
        return compiledTemplate;
	}
	
	
	
}
