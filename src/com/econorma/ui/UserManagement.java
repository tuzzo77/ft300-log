package com.econorma.ui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.adamtaft.eb.EventBusService;
import com.econorma.Application;
import com.econorma.data.User;
import com.econorma.model.UserModel;
import com.econorma.resources.Testo;
import com.econorma.ui.Events.UserEvent;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;
import com.econorma.util.SwingUtils;

public class UserManagement  extends javax.swing.JDialog implements KeyListener{

	private static final String TAG = UserManagement.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(UserManagement.class);

	private UserModel userModel;

	public UserManagement(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
		EventBusService.subscribe(this);
	}


	private void initComponents() {

		setTitle(Application.getInstance().getBundle().getString("userManagement.gestioneUtenti"));
		setFocusable(true);
		addKeyListener(this);

		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jNomeUtente = new javax.swing.JTextField();
		jNomeCompleto = new javax.swing.JTextField();
		jLabel6 = new javax.swing.JLabel();
		jSeparator2 = new javax.swing.JSeparator();
		jAdd = new javax.swing.JButton();
		jRemove = new javax.swing.JButton();
		jSeparator3 = new javax.swing.JSeparator();
		jScrollPane1 = new javax.swing.JScrollPane();
		jUserTable = new javax.swing.JTable();
		jTipoUtente = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jLanguage = new javax.swing.JComboBox();
        jAggiorna = new javax.swing.JButton();
        jCancel = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		jLabel1.setText(Application.getInstance().getBundle().getString("userManagement.tipoUtente"));

		jLabel2.setText(Application.getInstance().getBundle().getString("userManagement.nomeUtente"));

		jLabel3.setText(Application.getInstance().getBundle().getString("userManagement.nomeCompleto"));

		jNomeUtente.setEditable(false);
		jNomeUtente.setFocusable(false);

		jNomeCompleto.setEditable(false);
		jNomeCompleto.setFocusable(false);

		jAdd.setText("+");

		jRemove.setText("-");
		
		jAggiorna.setText(Application.getInstance().getBundle().getString("userManagement.aggiorna"));
		jCancel.setText(Application.getInstance().getBundle().getString("userManagement.cancella"));


		jAdd.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbtn_Add(evt);
			}
		});


		jRemove.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbtn_Remove(evt);
			}
		});
		
		jAggiorna.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbtn_Aggiorna(evt);
			}
		});
		
		jCancel.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbtn_Cancel(evt);
			}
		});

		jSeparator3.setForeground(new java.awt.Color(204, 204, 204));
		jSeparator3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
		jSeparator3.setPreferredSize(new java.awt.Dimension(0, 2));

		userModel = new UserModel();

		jUserTable.setModel(userModel);

		jUserTable.setFocusTraversalKeysEnabled(false);
		jUserTable.setFocusable(false);
		jUserTable.setFillsViewportHeight(true);
		jUserTable.setShowHorizontalLines(false);
		jUserTable.setShowVerticalLines(false);
		jScrollPane1.setViewportView(jUserTable);


		jTipoUtente.setModel(new javax.swing.DefaultComboBoxModel(new String[] { Testo.ADMINISTRATOR, Testo.STANDARD }));
		jTipoUtente.setEnabled(false);
		
		jLabel4.setText("Lingua");

        jLanguage.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Italiano", "English" }));
        jLanguage.setEnabled(true);

		jUserTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {            
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int row = jUserTable.getSelectedRow();

				if (row!=-1) {
					int modelRow = jUserTable.convertRowIndexToModel(row);
					UserModel model = (UserModel)jUserTable.getModel();
					User user = model.getItem(modelRow);
					jNomeUtente.setText(user.getUser());
					jNomeCompleto.setText(user.getUser_complete());
 					
					if (user.getType().equals(Testo.ADMINISTRATOR)){
						jTipoUtente.setSelectedItem(Testo.ADMINISTRATOR);
					} else {
						jTipoUtente.setSelectedItem(Testo.STANDARD);
					}
					
					if (user.getLanguage().equals(Testo.ITALIAN)){
						jLanguage.setSelectedItem(Testo.ITALIAN);
					} else {
						jLanguage.setSelectedItem(Testo.ENGLISH);
					}
 
				}

			}
		});

		 org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 172, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 166, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(layout.createSequentialGroup()
                                .add(jAdd, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 46, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jRemove, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSeparator3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel2)
                            .add(jLabel1)
                            .add(jLabel3))
                        .add(19, 19, 19)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jNomeCompleto, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 187, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jNomeUtente, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 186, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jTipoUtente, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 186, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(jLabel4)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jLanguage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 186, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(layout.createSequentialGroup()
                                .add(jAggiorna)
                                .add(30, 30, 30)
                                .add(jCancel)))))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 21, Short.MAX_VALUE)
                .add(jLabel6)
                .addContainerGap(26, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 311, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 12, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jRemove)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jAdd))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(37, 37, 37)
                        .add(jLabel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 289, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(17, 17, 17)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel1)
                            .add(jTipoUtente, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(27, 27, 27)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel2)
                            .add(jNomeUtente, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(29, 29, 29)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel3)
                            .add(jNomeCompleto, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(31, 31, 31)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel4)
                            .add(jLanguage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jAggiorna)
                    .add(jCancel))
                .add(20, 20, 20))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jSeparator3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
	}// </editor-fold>    


	// Variables declaration - do not modify                     
	private javax.swing.JButton jAdd;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JTextField jNomeCompleto;
	private javax.swing.JTextField jNomeUtente;
	private javax.swing.JButton jRemove;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JSeparator jSeparator2;
	private javax.swing.JSeparator jSeparator3;
	private javax.swing.JComboBox jTipoUtente;
	private javax.swing.JTable jUserTable;
    private javax.swing.JLabel jLabel4;
	private javax.swing.JComboBox jLanguage;
	private javax.swing.JButton jAggiorna;
	private javax.swing.JButton jCancel;
	// End of variables declaration    

	private void jbtn_Add(java.awt.event.ActionEvent evt) {                                        

		CreateUser create = new CreateUser
				(Application
						.getInstance().getMainFrame(), false);
		SwingUtils.showCenteredDialog(create);
	}           

	private void jbtn_Remove(java.awt.event.ActionEvent evt) {   

		int i = JOptionPane.showConfirmDialog(
				null,
				Application.getInstance().getBundle().getString("userManagement.confermiCancellazione"),
				Application.getInstance().getBundle().getString("userManagement.errore"),
				JOptionPane.YES_NO_OPTION);

		if(i == JOptionPane.YES_OPTION) {

			int row = jUserTable.getSelectedRow();

			if (row!=-1) {
				int modelRow = jUserTable.convertRowIndexToModel(row);
				UserModel model = (UserModel)jUserTable.getModel();
				User user = model.getItem(modelRow);
				User findByUser = Application.getInstance().getDao().findByUser(user.getUser()); 
				Application.getInstance().getDao().deleteUser(user);

				logger.log(LoggerCustomLevel.AUDIT, "Utente cancellato: " + user.getUser());

				UserEvent userEvent = new Events.UserEvent(user.getUser());
				EventBusService.publish(userEvent);
			}

		}
	
	}
	
	private void jbtn_Cancel(java.awt.event.ActionEvent evt) { 
		dispose();
	}
	
	private void jbtn_Aggiorna(java.awt.event.ActionEvent evt) { 
		String utente = jNomeUtente.getText();
		User findByUser = Application.getInstance().getDao().findByUser(utente); 
		findByUser.setLanguage(jLanguage.getSelectedItem().toString());
		Application.getInstance().getDao().updateUser(findByUser);
		logger.log(LoggerCustomLevel.AUDIT, "Utente aggiornato: " + utente);
		dispose();
	}



	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE)  
		{  
			dispose(); 
		}  

	}


	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public JTable getTable(){
		return jUserTable;
	}

	
}
