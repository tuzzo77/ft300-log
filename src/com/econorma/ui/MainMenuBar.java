package com.econorma.ui;


import java.awt.Dialog;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.util.WindowUtils;
import org.jfree.chart.JFreeChart;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.Main;
import com.econorma.db.util.BackupDatabase;
import com.econorma.db.util.ImportaStorico;
import com.econorma.db.util.ShrinkDatabase;
import com.econorma.gui.EconormaLoginDialog;
import com.econorma.gui.GUI;
import com.econorma.io.Receiver;
import com.econorma.io.ReceiverLogger;
import com.econorma.logic.SerialUtil;
import com.econorma.resources.Testo;
import com.econorma.ui.ReportChart.REPORT;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;
import com.econorma.util.SwingUtils;
import com.econorma.util.Theme;


public class MainMenuBar extends JMenuBar implements ActionListener {

	private static final String TAG = MainMenuBar.class.getCanonicalName();
	private static final Logger logger = Logger.getLogger(MainMenuBar.class);

	private static final String ACTION_EXIT = "Exit";
	private static final String ACTION_CHANGE_MODE = "ChangeMode";
	private static final String ACTION_SELECT_PORTA = "SelectPorta";
	private static final String ACTION_SELECT_PORTA_MODEM = "SelectPortaModem";
	private static final String ACTION_SELECT_FILE_SINOTTICO = "SelectFileSinottico";
	private static final String ACTION_SELECT_SCANSIONE_LOGGER = "ScansioneLogger";
	private static final String ACTION_PULISCI_GRAFICO = "PulisciGrafico";
	private static final String ACTION_SHOW_ABOUT = "ShowAbout";
	private static final String ACTION_REPORT = "Report";
	private static final String ACTION_PARAMETRI_PASTORIZZAZIONE = "Parametri di Pastorizzazione";
	private static final String ACTION_VISUALIZZA_SENSORI_SINGOLI = "Visualizza Sensori Singoli";
	private static final String ACTION_CONFIGURA = "Impostazioni Logger";
	private static final String ACTION_ALLARMI = "Configura Allarmi";
	private static final String ACTION_ALLARMI_MAIL = "Mail Allarmi";
	private static final String ACTION_EXPORT = "Export";
	private static final String ACTION_USER = "Utenti";
	private static final String ACTION_LOG = "Log";
	private static final String ACTION_BACKUP = "Backup";
	private static final String ACTION_INVIA_DB = "Invia DB";
	private static final String ACTION_SHRINK = "Compatta DB";
	private static final String ACTION_IMPORTA_STORICO = "Importa da CSV";
	private static final String ACTION_OPZIONI = "Opzioni";
	private static final String ACTION_CONVERSIONI_UM = "Conversioni UM";
	private static final String ACTION_TEMPI_TRASMISSIONE = "Tempi di trasmissione";
	private static final String ACTION_CONFIGURAZIONE = "Configura Data-Logger";
	private static final String ACTION_FULL_SCREEN = "Visualizza schermo intero";
	private static final String ACTION_DELETE_UNKNOWN = "Cancella sonde sconosciute";
	private static final String ACTION_ABILITA_ALLARMI = "Abilita allarmi";
	private static final String ACTION_PULLDOWN = "Pulldown";
	private static final String ACTION_REFRESH_STORICO = "Aggiorna Storico";

	private Receiver receiver;
	private ReceiverLogger receiverLogger;

	enum PORT { COM, MODEM };


	JCheckBoxMenuItem configModeMenuItem;
	JMenuItem selezionaPorta;
	JMenuItem selezionaPortaModem;
	JMenuItem selezionaFileSinottico;
	JMenuItem scasioneBidirezionale;
	JMenuItem conversioniUM;
	JMenuItem aboutItem;
	JMenuItem allarmiItem;
	JMenuItem exportItem;
	JMenuItem backupItem;
	JMenuItem deleteUnknown;
	JMenuItem abilitaAllarmi;
	JMenuItem shrinkItem;
	JMenuItem inviaDbItem;
	JMenuItem importaStoricoItem;
	JMenuItem refreshStoricoItem;
	JMenuItem reportItem;
	JMenuItem opzioniItem;
	JMenuItem trasmissioneItem;
	JMenuItem configurazioneItem;
	JMenuItem pastorizzazioneItem;
	JMenuItem listaItem;
	JMenuItem userManagementItem;
	JMenuItem pulldownItem;
	JCheckBoxMenuItem fullScreen;

	private JMenu principale;
	private JMenu configurazione;
	private JMenu preferenze;
	private JMenu about;
	private JMenu export;
	private JMenu allarmi;
	private JMenu conversioni;
	private JMenu utility;
	private JMenu scansione;
	private JMenu trasmissione;
	private JMenu pastorizzazione;
	private JMenu log;
	private JMenu user;

	MainWindow mainWindow;

	public MainMenuBar(MainWindow window) {


		JMenu menu = new JMenu(Application.getInstance().getBundle().getString("mainMenuBar.file"), true);
		principale = menu;
		mainWindow = window;

		add(menu);
		
		configurazioneItem = new JMenuItem(Testo.CONFIGURA);
		configurazioneItem.setActionCommand(ACTION_CONFIGURAZIONE);
		configurazioneItem.addActionListener(this);
		menu.add(configurazioneItem);



		JSeparator separator = new JSeparator(SwingConstants.HORIZONTAL);
		menu.add(separator);
		{
			JMenuItem esci = new JMenuItem(Application.getInstance().getBundle().getString("mainMenuBar.esci"));
			menu.add(esci);
			esci.setActionCommand(ACTION_EXIT);
			esci.addActionListener(this);

		}

		add(menu);

		menu = new JMenu(Application.getInstance().getBundle().getString("mainMenuBar.preferenze"), true);
		preferenze = menu;
		{
			
			if (Main.getConsoleMode() || EconormaLoginDialog.getTypeUser().equals(Testo.ADMINISTRATOR)) {

				selezionaPorta = new JMenuItem(Application.getInstance().getBundle().getString("mainMenuBar.selezionaPorta"));
				selezionaPorta.setActionCommand(ACTION_SELECT_PORTA);
				selezionaPorta.addActionListener(this);
				menu.add(selezionaPorta);

			}
			

			selezionaFileSinottico = new JMenuItem(Application.getInstance().getBundle().getString("mainMenuBar.selezioneFileSinottico"));
			selezionaFileSinottico.setActionCommand(ACTION_SELECT_FILE_SINOTTICO);
			selezionaFileSinottico.addActionListener(this);
			menu.add(selezionaFileSinottico);
			
			
			if (Main.getConsoleMode() || EconormaLoginDialog.getTypeUser().equals(Testo.ADMINISTRATOR)) {
				opzioniItem = new JMenuItem(Application.getInstance().getBundle().getString("mainMenuBar.opzioni"));
				opzioniItem.setActionCommand(ACTION_OPZIONI);
				opzioniItem.addActionListener(this);
				menu.add(opzioniItem);

			}
		}
 
		add(menu);

		
		menu = new JMenu(Application.getInstance().getBundle().getString("mainMenuBar.allarmi"), true);
		allarmi = menu;
		{
			allarmiItem = new JMenuItem(Application.getInstance().getBundle().getString("mainMenuBar.allarmi"));
			allarmiItem.setActionCommand(ACTION_ALLARMI);
			allarmiItem.addActionListener(this);
			menu.add(allarmiItem);

			add(menu);

		}
		
 
		menu = new JMenu(Testo.MENU_EXPORT, true);
		export = menu;
		{
			exportItem = new JMenuItem(Testo.MENU_EXPORT);
			exportItem.setActionCommand(ACTION_EXPORT);
			exportItem.addActionListener(this);
			menu.add(exportItem);

		}

		add(menu);



		if (Main.getConsoleMode() || EconormaLoginDialog.getTypeUser().equals(Testo.ADMINISTRATOR)) {

			menu = new JMenu(Application.getInstance().getBundle().getString("mainMenuBar.utenti"), true);
			user = menu;
			{
				userManagementItem = new JMenuItem(Application.getInstance().getBundle().getString("mainMenuBar.gestioneUtenti"));
				userManagementItem.setActionCommand(ACTION_USER);
				userManagementItem.addActionListener(this);
				menu.add(userManagementItem);

			}

			add(menu);

			menu = new JMenu(Application.getInstance().getBundle().getString("mainMenuBar.log"), true);
			log = menu;
			{
				listaItem = new JMenuItem(Application.getInstance().getBundle().getString("mainMenuBar.listaAttivita"));
				listaItem.setActionCommand(ACTION_LOG);
				listaItem.addActionListener(this);
				menu.add(listaItem);

			}

			add(menu);

		}


		menu = new JMenu(Application.getInstance().getBundle().getString("mainMenuBar.utilita"), true);
		utility = menu;
		{
			
			if (Main.getConsoleMode() || EconormaLoginDialog.getTypeUser().equals(Testo.ADMINISTRATOR)) {

				backupItem = new JMenuItem(Application.getInstance().getBundle().getString("mainMenuBar.backupDatabase"));
				backupItem.setActionCommand(ACTION_BACKUP);
				backupItem.addActionListener(this);
				menu.add(backupItem);

				shrinkItem = new JMenuItem(Application.getInstance().getBundle().getString("mainMenuBar.compattaDatabase"));
				shrinkItem.setActionCommand(ACTION_SHRINK);
				shrinkItem.addActionListener(this);
				menu.add(shrinkItem);

				inviaDbItem = new JMenuItem(Application.getInstance().getBundle().getString("mainMenuBar.inviaDatabase"));
				inviaDbItem.setActionCommand(ACTION_INVIA_DB);
				inviaDbItem.addActionListener(this);
				menu.add(inviaDbItem);

			}

		}

		add(menu);



		menu = new JMenu(Testo.MENU_ABOUT, true);
		about = menu;
		{
			aboutItem = new JMenuItem(Testo.MENU_ABOUT);
			aboutItem.setActionCommand(ACTION_SHOW_ABOUT);
			aboutItem.addActionListener(this);
			menu.add(aboutItem);

		}

		add(menu);

		EventBusService.subscribe(this);
	}



	@Override
	public void actionPerformed(ActionEvent event) {
		if (ACTION_EXIT.equals(event.getActionCommand())) {
			EventBusService.publish(Events.EXIT);
		}

		if (ACTION_CHANGE_MODE.equals(event.getActionCommand())) {
			boolean configMode = Application.getInstance().isConfigMode();
			Application.getInstance().setConfigMode(!configMode);
		}


		if (ACTION_SELECT_PORTA.equals(event.getActionCommand())) {


			String[] serialPorts = SerialUtil.getSerialPorts();

			if (serialPorts == null || serialPorts.length<=0) {
 				
				JOptionPane.showMessageDialog(Application.getInstance().getMainFrame(),
						Application.getInstance().getBundle().getString("mainMenuBar.nessunaPortaSeriale"),
						Application.getInstance().getBundle().getString("mainMenuBar.errorePortaSeriale"), JOptionPane.ERROR_MESSAGE);
				return;
			}
			// apri il dialog al centro
			ChoosePortDialog dialog = new ChoosePortDialog(
					WindowUtils.findWindow(Application.getInstance()
							.getMainFrame()),
					Application.getInstance().getBundle().getString("mainMenuBar.selezionaPorta"),
					Dialog.ModalityType.APPLICATION_MODAL, PORT.COM);
			dialog.pack();
			SwingUtils.showCenteredDialog(dialog);


		}

		if (ACTION_SELECT_SCANSIONE_LOGGER.equals(event.getActionCommand())) {
			DiscoveryLoggerDialog dialog = new DiscoveryLoggerDialog(
					WindowUtils.findWindow(Application.getInstance()
							.getMainFrame()),
					Testo.SCANSIONE_LOGGER,
					Dialog.ModalityType.APPLICATION_MODAL);
			dialog.pack();
			SwingUtils.showCenteredDialog(dialog);
		}


		if (ACTION_CONVERSIONI_UM.equals(event.getActionCommand())) {
			ConversioniWindow conversioni = new ConversioniWindow
					(Application
							.getInstance().getMainFrame(), false);
			SwingUtils.showCenteredDialog(conversioni);
		}

		if (ACTION_CONFIGURAZIONE.equals(event.getActionCommand())) {
			ConfigWindow trasmissione = new ConfigWindow
					(Application
							.getInstance().getMainFrame(), false);
			SwingUtils.showCenteredDialog(trasmissione);
		}

		if (ACTION_SELECT_PORTA_MODEM.equals(event.getActionCommand())) {


			String[] serialPorts = SerialUtil.getSerialPorts();

			if (serialPorts == null || serialPorts.length<=0) {

				JOptionPane.showMessageDialog(this,
						Application.getInstance().getBundle().getString("mainMenuBar.nessunaPortaSeriale"),
						Application.getInstance().getBundle().getString("mainMenuBar.errorePortaSeriale"), JOptionPane.ERROR_MESSAGE);
				return;
			}
			// apri il dialog al centro
			ChoosePortDialog dialog = new ChoosePortDialog(
					WindowUtils.findWindow(Application.getInstance()
							.getMainFrame()),
					Application.getInstance().getBundle().getString("mainMenuBar.selezionaPortaModem"),
					Dialog.ModalityType.APPLICATION_MODAL, PORT.MODEM);
			dialog.pack();
			SwingUtils.showCenteredDialog(dialog);

		}

		if (ACTION_SELECT_FILE_SINOTTICO.equals(event.getActionCommand())) {
			ChooseFileSinottico dialog = new ChooseFileSinottico(Application.getInstance()
					.getMainFrame(), Testo.SELEZIONA_FILE_SINOTTICO, Dialog.ModalityType.APPLICATION_MODAL);
			SwingUtils.showCenteredDialog(dialog);

		}

		if (ACTION_REPORT.equals(event.getActionCommand())) {
			String idprova;

			try{
				idprova = Long.toString( Application.getInstance().getDao().findUltimaProva().getId());
			}catch (Exception e) {
				logger.info(TAG, "nessuna prova in corso... Non posso aprire il dialog di stampa");
				JOptionPane.showMessageDialog(null, Testo.ERRORE_NESSUNA_PROVA_AVVIATA,
						"Econorma", JOptionPane.ERROR_MESSAGE);
				return;
			}


			ReportChart reportChart = null;
			GUI gui=null;
			JFreeChart chart=null;
			String targa=null;
			String responsabile=null;
			String durataProva=null;
			Date dataProva=null;
			Date dataDa=new Date();
			Date dataA=new Date();
			String id_sonda=null;
			String des_sonda=null;
			String numeroProva=null;
			String laboratorio=null;

			gui = Application.getInstance().getGui();
			chart =gui.getEconormaChartProvider().getValue().getJFreeChart();


			targa = gui.getTargaProvider().getValue();
			responsabile = gui.getResponsabileProvider().getValue();
			durataProva = gui.getElapsedTimeProvider().getValue();
			dataProva = gui.getDataProvider().getValue();
			numeroProva = gui.getNumeroProvaProvider().getValue();
			laboratorio = gui.getLaboratorioProvider().getValue();
			reportChart = new ReportChart(REPORT.RIEPILOGO_PROVA, chart, responsabile, dataProva, dataProva, dataProva, idprova, null, null);



			try{
				reportChart.showPrintDialog(this);
			}catch (Exception e) {
				logger.error(TAG, "Errore nell'apertura del dialog report", e);
			}
			//			ReportChart.main(null);
		}

		if (ACTION_PULISCI_GRAFICO.equals(event.getActionCommand())) {
			EventBusService.publish(Events.CLEAR_CHART);
		}

		if (ACTION_SHOW_ABOUT.endsWith(event.getActionCommand())) {
			AboutDialog dialog = new AboutDialog(Application.getInstance()
					.getMainFrame());
			SwingUtils.showCenteredDialog(dialog);
		}


		if (ACTION_CONFIGURA.endsWith(event.getActionCommand())) {
			//			 com.econorma.pulse.Application.launch(com.econorma.pulse.Application.class, null);
			JOptionPane.showMessageDialog(null, "Funzione non disponibile" ,"Errore", JOptionPane.ERROR_MESSAGE);

		}
		
		if(ACTION_ALLARMI.equals(event.getActionCommand())){
			AllarmiWindow allarmi = new AllarmiWindow
					(Application
							.getInstance().getMainFrame(), false);
			SwingUtils.showCenteredDialog(allarmi);
		}


		if(ACTION_EXPORT.equals(event.getActionCommand())){


			ExportWindow export = new ExportWindow
					(Application
							.getInstance().getMainFrame(), false);
			SwingUtils.showCenteredDialog(export);

		}



		if(ACTION_USER.equals(event.getActionCommand())){


			UserManagement userManagement = new UserManagement
					(Application
							.getInstance().getMainFrame(), false);
			SwingUtils.showCenteredDialog(userManagement);

		}

		if(ACTION_LOG.equals(event.getActionCommand())){


			LogWindow export = new LogWindow
					(Application
							.getInstance().getMainFrame(), false);
			SwingUtils.showCenteredDialog(export);

		}

		if(ACTION_IMPORTA_STORICO.equals(event.getActionCommand())){

			ImportaStorico importa = new ImportaStorico();
			importa.run();

		}
		
		if(ACTION_REFRESH_STORICO.equals(event.getActionCommand())){
			
			Application.getInstance().getMainWindow().getProvePanel().getProveModel().refresh();

		}

		if(ACTION_BACKUP.equals(event.getActionCommand())){

			BackupDatabase backup = new BackupDatabase();
			backup.copy();


		}

		if(ACTION_SHRINK.equals(event.getActionCommand())){

			ShrinkDatabase shrink = new ShrinkDatabase();
			shrink.compress();


		}

		if(ACTION_DELETE_UNKNOWN.equals(event.getActionCommand())){

			Application.getInstance().getDao().deleteSondeUnknown();
			JOptionPane.showMessageDialog(null,Application.getInstance().getBundle().getString("mainMenuBar.cancellaSondeSconosciute"), Application.getInstance().getBundle().getString("mainMenuBar.informazione"), JOptionPane.INFORMATION_MESSAGE);
			logger.log(LoggerCustomLevel.AUDIT, "Cancellazione Sonde Sconosciute");


		}

		if(ACTION_ABILITA_ALLARMI.equals(event.getActionCommand())){

			Application.getInstance().getDao().enableAlarms();
			JOptionPane.showMessageDialog(null,Application.getInstance().getBundle().getString("mainMenuBar.abilitaAllarmi"), Application.getInstance().getBundle().getString("mainMenuBar.informazione"), JOptionPane.INFORMATION_MESSAGE);
			logger.log(LoggerCustomLevel.AUDIT, "Abilitazione totale allarmi");


		}


		if(ACTION_OPZIONI.equals(event.getActionCommand())){

			OptionDialog dialog = new OptionDialog(
					WindowUtils.findWindow(Application.getInstance()
							.getMainFrame()),
					Testo.OPZIONI,
					Dialog.ModalityType.APPLICATION_MODAL);


			dialog.pack();
			SwingUtils.showCenteredDialog(dialog);	 


		}



	}

	@EventHandler
	public void handleEvent(String event) {
		if (Events.APPLICATION_MODE_CHANGED.equals(event)) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					boolean configMode = Application.getInstance()
							.isConfigMode();
					configModeMenuItem.setSelected(configMode);
				}
			});
		}
	}

	public void loseFocusHack() {
		principale.setPopupMenuVisible(false);
		about.setPopupMenuVisible(false);
		principale.setSelected(false);
		about.setSelected(false);
	}


	@Override
	protected void paintComponent(Graphics g)   {

		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		switch (Application.getInstance().getTheme()) {
		case DARK:
			g2d.setColor(Theme.DARK);
			g2d.fillRect(0, 0, getWidth() - 1, getHeight() - 1);
			break;
		case LIGHT:
			g2d.setColor(Theme.WHITE);
			g2d.fillRect(0, 0, getWidth() - 1, getHeight() - 1);
			break;
		}
	}


}

