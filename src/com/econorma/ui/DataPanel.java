package com.econorma.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DropMode;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.LetturaProva;
import com.econorma.data.Sensore;
import com.econorma.logic.SensoreManager;
import com.econorma.model.DataModel;
import com.econorma.util.Logger;
import com.econorma.util.Theme;

import net.miginfocom.swing.MigLayout;

public class DataPanel extends JPanel implements ActionListener, TableModelListener {

	// http://www.chka.de/swing/table/faq.html

	/**
	 * 
	 */
	private static final long serialVersionUID = 6000355186796391145L;
	private JLabel lb_data;
	private MyTable tableData;
	private JScrollPane scrollData;

	private static final Logger logger = Logger.getLogger(DataPanel.class);
	private static final String TAG = DataPanel.class.getSimpleName();

	private static Application app = Application.getInstance();
	private static Color DEFAULT_COLOR;

	// keep hard references
	DataModel dataModel;




	private final Timer timer = new Timer(30 * 1000, this);

	/**
	 * Create the panel.
	 */
	public DataPanel() {

		switch (app.getTheme()) {
		case DARK:
			DEFAULT_COLOR = Theme.WHITE;
			break;
		case LIGHT:
			DEFAULT_COLOR = Theme.BLACK;
		default:
			break;
		}

		tableData = new MyTable();
		tableData.setFillsViewportHeight(true);

		tableData.setDropMode(DropMode.ON_OR_INSERT);

		dataModel = new DataModel();

		tableData.setModel(dataModel);

		DecimalFormat decimal = new DecimalFormat("##,##");
		JFormattedTextField formattedTextField = new JFormattedTextField(decimal);
		TableCellEditor cellEditor = new DefaultCellEditor(formattedTextField);


		//		tableData.getColumn(dataModel.COL_TEMP).setCellEditor(cellEditor);

		switch (app.getTipoMisure()) {
		case TEMPERATURA_UMIDITA:
			//		tableData.getColumn(dataModel.COL_UMI).setCellEditor(cellEditor);
		}

		tableData.setDefaultRenderer(Object.class, new CustomRenderer());

		tableData.getColumnExt(dataModel.COL_DATA).setMaxWidth(150);
		tableData.getColumnExt(dataModel.COL_DATA).setMinWidth(100);

		final String sensori_interni_label;

		tableData.getModel().addTableModelListener(this);


		setLayout(new MigLayout("fill"));
		sensori_interni_label =  Application.getInstance().getBundle().getString("dataPanel.movimenti");

		lb_data = new JLabel(sensori_interni_label);
		add(lb_data, "gap 5px 5px, growprio 1, wrap");

		scrollData = new JScrollPane();

		new JPanel();
		add(scrollData, "gap 5px 5px, growy, growx, pushy, pushx, wrap");

		scrollData.setViewportView(tableData);

		tableData.packAll();


		EventBusService.subscribe(this);


		timer.start();
	}




	private static class LetturaDataFlavor extends DataFlavor {

		public static final LetturaDataFlavor letture = new LetturaDataFlavor();

		private LetturaDataFlavor() {
			super(Lettura.class, "Lettura");
		}
	}

	@EventHandler
	public void handleEvent(String event) {

	}



	@Override
	public void actionPerformed(ActionEvent event) {

		dataModel.fireTableDataChanged();

		//		if (event.getSource() == timer) {
		//			allarmiModel.fireTableDataChanged();
		//		 
		//		}

	}

	public JTable[] getLettureTables(){
		return new JTable[]{tableData };
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		// TODO Auto-generated method stub

	}

	private static class CustomRenderer extends DefaultTableCellRenderer {
		/**
		 * 
		 */

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			int modelRow = table.convertRowIndexToModel(row);
			Component tableCellRendererComponent = super
					.getTableCellRendererComponent(table, value, isSelected,
							hasFocus, modelRow, column);
			DataModel dataModel = (DataModel) table.getModel();
			LetturaProva letturaProva = dataModel.getItem(modelRow);



			JLabel original =(JLabel) super.getTableCellRendererComponent(
					table, value, isSelected, hasFocus, modelRow, column);


			original.setBorder( BorderFactory.createEmptyBorder( 3, 3, 3, 3) );


			Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
			if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0 || sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0) {

				if (letturaProva != null) {


					Double valore = Double.MIN_VALUE;
					Double umidita = Double.MIN_VALUE;
					Double ohm = Double.MIN_VALUE;

					valore = letturaProva.getValore();

					if (valore != null && outOfLimit(letturaProva)) {
						tableCellRendererComponent.setForeground(Color.RED);
						return this;
					}

					umidita = letturaProva.getUmidita();

					if (umidita != null && outOfLimitURT(letturaProva))	{
						tableCellRendererComponent.setForeground(Color.RED);
						return this;
					}

					ohm = letturaProva.getOhm();

					if (ohm != null && outOfLimitOhm(letturaProva))	{
						tableCellRendererComponent.setForeground(Color.RED);
						return this;
					}

				}


			}
			tableCellRendererComponent.setForeground(DEFAULT_COLOR);
			tableCellRendererComponent.setLocale(Locale.ITALY);
			return this;
		}
	} 


	private static boolean outOfLimit(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0){
			return letturaProva.getValore() < sensore.getRangeMin() || letturaProva.getValore() > sensore.getRangeMax();
		}
		return false;

	}

	private static boolean outOfLimitURT(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0){
			return letturaProva.getUmidita() < sensore.getRangeMinURT() || letturaProva.getUmidita() > sensore.getRangeMaxURT();
		}
		return false;

	}

	private static boolean outOfLimitOhm(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMinOhm()!=0 || sensore.getRangeMaxOhm()!= 0){
			return letturaProva.getOhm() < sensore.getRangeMinOhm() || letturaProva.getOhm() > sensore.getRangeMaxOhm();
		}
		return false;

	}



}
