package com.econorma.ui;
 
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import com.econorma.data.Log;
import com.econorma.util.Logger;


public class ReportLog {
	
	 
	
	private static final String TAG = ReportLog.class.getCanonicalName();
	
	private static final Logger logger = Logger.getLogger(ReportLog.class);
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	
	private final List<Log> log;
	private final Date dataDa;
	private final Date dataA;
	
	
	public ReportLog(List<Log> log, Date dataDa, Date dataA){
		this.log = log;
		this.dataDa = dataDa;
		this.dataA = dataA;
	}
	
	    
	 
	
	public PrepraredPrint preparePrint(){
	
        Map parameters = new HashMap(); 
        parameters.put("DATA_DA", sdf.format(dataDa));
        parameters.put("DATA_A", sdf.format(dataA));
        
		
		return new PrepraredPrint(parameters);
		
	}
 
	 
	
	
	private static class PrepraredPrint {
		
		public final Map<String,Object> params;
	 	
		public PrepraredPrint(Map<String,Object> params){
			this.params = params;
		}
	}
	
	public void showPrintDialog(Component parent) throws JRException {
		PrepraredPrint preparePrint = preparePrint();
		JasperReport template = getTemplate();
//		template.setWhenNoDataType(template.WHEN_NO_DATA_TYPE_ALL_SECTIONS_NO_DETAIL);  
		
		JRBeanCollectionDataSource beanColDataSource =  new JRBeanCollectionDataSource(log);

		 
        JasperPrint jasperPrint = JasperFillManager.fillReport(template, preparePrint.params, beanColDataSource);
        JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
        jasperViewer.setTitle("Stampa");

        jasperViewer.setSize(800, 600);
        jasperViewer.setLocation(500, 100);
        jasperViewer.setResizable(true); 
        jasperViewer.setVisible(true);
        jasperViewer.setAlwaysOnTop(true);
       
	}
	
	private JasperReport getTemplate(){
		
		String  reportTemplateFile = "ReportLog.jrxml";
		 
		
		URL resource = Thread.currentThread().getContextClassLoader().getResource(reportTemplateFile);
		InputStream reportTemplateStream = null; 
		JasperDesign jasperDesign = null;
		 JasperReport compiledTemplate = null;
		try{
				reportTemplateStream = 	resource.openStream();
			    jasperDesign = JRXmlLoader.load(reportTemplateStream);  
			    compiledTemplate = JasperCompileManager.compileReport(jasperDesign);
		}catch (Exception e) {
			logger.error(TAG, "Impossibile compilare/trovare il template"+resource.toString(),e);
		}
		finally{
 
		}
        return compiledTemplate;
	}
	
	
}
