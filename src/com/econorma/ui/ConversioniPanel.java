package com.econorma.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DropMode;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.MaskFormatter;

import org.jdesktop.swingx.JXTable;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.Sensore.Location;
import com.econorma.logic.SensoreManager;
import com.econorma.model.ConversioniModel;
import com.econorma.resources.Testo;
import com.econorma.util.Logger;

import net.miginfocom.swing.MigLayout;


public class ConversioniPanel extends JPanel implements ActionListener, TableModelListener {

	// http://www.chka.de/swing/table/faq.html

	/**
	 * 
	 */
	private static final long serialVersionUID = 6000355186796391145L;
	private JLabel lb_allarmi;
	private MyTable tableConversioni;
	private JScrollPane scrollAllarmi;

	private static final Logger logger = Logger.getLogger(ConversioniPanel.class);
	private static final String TAG = ConversioniPanel.class.getSimpleName();

	private static Application app = Application.getInstance();
	

	// keep hard references
	ConversioniModel conversioniModel;




	private final Timer timer = new Timer(30 * 1000, this);

	/**
	 * Create the panel.
	 */
	public ConversioniPanel() {

		tableConversioni = new MyTable();
		tableConversioni.setFillsViewportHeight(true);

		tableConversioni.setDropMode(DropMode.ON_OR_INSERT);

		conversioniModel = new ConversioniModel(Location.INTERNO, SensoreManager.getInstance().getUltimeLetture());

		tableConversioni.setModel(conversioniModel);

		DecimalFormat decimal = new DecimalFormat("##,##");
		JFormattedTextField formattedTextField = new JFormattedTextField(decimal);
		TableCellEditor cellEditor = new DefaultCellEditor(formattedTextField);

		tableConversioni.setDefaultRenderer(Object.class, new CustomRenderer());
		 
		
		tableConversioni.getColumn(conversioniModel.COL_VALORE_MINIMO_SONDA).setCellEditor(cellEditor);
		tableConversioni.getColumn(conversioniModel.COL_VALORE_MASSIMO_SONDA).setCellEditor(cellEditor);
		tableConversioni.getColumn(conversioniModel.COL_VALORE_MINIMO_UM).setCellEditor(cellEditor);
		tableConversioni.getColumn(conversioniModel.COL_VALORE_MASSIMO_UM).setCellEditor(cellEditor);
		
		tableConversioni.setRowHeight(22);
		

		MaskFormatter formatter =null;
		try {
			formatter = new MaskFormatter("##:##:##");
		}catch (Exception e) {
			e.printStackTrace();
		}

				
		final String sensori_interni_label;

		tableConversioni.getModel().addTableModelListener(this);


		setLayout(new MigLayout("fill"));
		sensori_interni_label = Testo.SENSORI_CONOSCIUTI;

		lb_allarmi = new JLabel(sensori_interni_label);
		add(lb_allarmi, "gap 5px 5px, growprio 1, wrap");

		scrollAllarmi = new JScrollPane();

		new JPanel();
		add(scrollAllarmi, "gap 5px 5px, growy, growx, pushy, pushx, wrap");

		scrollAllarmi.setViewportView(tableConversioni);

		tableConversioni.packAll();


		EventBusService.subscribe(this);


		timer.start();
	}



	public void bind(SensoreManager sensoreManager) {

		sensoreManager.addOnReadListener(conversioniModel);

	}




	private static class LetturaDataFlavor extends DataFlavor {

		public static final LetturaDataFlavor letture = new LetturaDataFlavor();

		private LetturaDataFlavor() {
			super(Lettura.class, "Lettura");
		}
	}

	@EventHandler
	public void handleEvent(String event) {
		if (Events.APPLICATION_MODE_CHANGED.equals(event)) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {

					updateStatus();
				}
			});
		}
	}

	private void updateStatus() {
		boolean configMode = Application.getInstance().isConfigMode();

		conversioniModel.setEditMode(configMode);

	}


	private static class MyTable extends JXTable {

		/**
		 *
		 */
		private static final long serialVersionUID = -7663077065549587996L;

		public MyTable() {
			super();
			setColumnControlVisible(true);
		}

		@Override
		protected void createDefaultEditors() {
			// TODO Auto-generated method stub
			super.createDefaultEditors();
		}

		@Override
		public Component prepareEditor(TableCellEditor arg0, int arg1, int arg2) {
			// TODO Auto-generated method stub
			return super.prepareEditor(arg0, arg1, arg2);
		}

		@Override
		public TableCellEditor getCellEditor() {
			// TODO Auto-generated method stub
			return super.getCellEditor();
		}

		@Override
		public TableCellEditor getCellEditor(int arg0, int arg1) {
			// TODO Auto-generated method stub
			return super.getCellEditor(arg0, arg1);
		}

		@Override
		public void setCellEditor(TableCellEditor arg0) {
			// TODO Auto-generated method stub
			super.setCellEditor(arg0);
		}


	}
 

	@Override
	public void actionPerformed(ActionEvent event) {

		conversioniModel.fireTableDataChanged();
 
	}

	public JTable[] getLettureTables(){
		return new JTable[]{tableConversioni };
	}

	private static class MyCellEditor extends DefaultCellEditor{

		public MyCellEditor(JCheckBox checkBox) {
			super(checkBox);
		}
		public MyCellEditor(JComboBox comboBox) {
			super(comboBox);
		}

		public MyCellEditor(JTextField textField) {
			super(textField);
		}


		@Override
		public boolean stopCellEditing() {

			// TODO Auto-generated method stub
			return super.stopCellEditing();
		}
		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) {
			// TODO Auto-generated method stub
			return super.getTableCellEditorComponent(table, value, isSelected, row, column);
		}


	}

	@Override
	public void tableChanged(TableModelEvent e) {

		int row = e.getFirstRow();
		int column = e.getColumn();
		if(column!=-1 ){

			String columnName = conversioniModel.getColumnName(column);
		 

		}

	}
 
	public class MyComboBoxRenderer extends JComboBox implements TableCellRenderer {
		  
		public MyComboBoxRenderer(String[] items) {
		    super(items);
		  }

		  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
		      boolean hasFocus, int row, int column) {
		    if (isSelected) {
		      setForeground(table.getSelectionForeground());
		      super.setBackground(table.getSelectionBackground());
		    } else {
		      setForeground(table.getForeground());
		      setBackground(table.getBackground());
		    }
		    setSelectedItem(value);
		    return this;
		  }
		  
		}

	public class MyComboBoxEditor extends DefaultCellEditor {
		
		  public MyComboBoxEditor(String[] items) {
		    super(new JComboBox(items));
		  }
		  
	}
	
	private static class CustomRenderer extends DefaultTableCellRenderer {
		/**
		 * 
		 */
		
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			int modelRow = table.convertRowIndexToModel(row);
			Component tableCellRendererComponent = super
					.getTableCellRendererComponent(table, value, isSelected,
							hasFocus, modelRow, column);
			 

			JLabel original =(JLabel) super.getTableCellRendererComponent(
					table, value, isSelected, hasFocus, modelRow, column);


			original.setBorder( BorderFactory.createEmptyBorder( 3, 3, 3, 3) );
	   
 			tableCellRendererComponent.setForeground(Color.BLACK);
			tableCellRendererComponent.setLocale(Locale.ITALY);
			return this;
		}
		
	} 
	
	 
}
