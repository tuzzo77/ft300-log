package com.econorma.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.DropMode;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;

import net.miginfocom.swing.MigLayout;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.Log;
import com.econorma.model.LogModel;
import com.econorma.util.Logger;
import com.econorma.util.Theme;

public class LogPanel extends JPanel implements ActionListener, TableModelListener {

	// http://www.chka.de/swing/table/faq.html

	/**
	 * 
	 */
	private static final long serialVersionUID = 6000355186796391145L;
	private JLabel lb_data;
	private MyTable tableData;
	private JScrollPane scrollData;

	private static final Logger logger = Logger.getLogger(LogPanel.class);
	private static final String TAG = LogPanel.class.getSimpleName();

	private static Application app = Application.getInstance();

	private static Color DEFAULT_COLOR;

	// keep hard references
	LogModel logModel;




	private final Timer timer = new Timer(30 * 1000, this);

	/**
	 * Create the panel.
	 */
	public LogPanel() {
		
		switch (app.getTheme()) {
		case DARK:
			DEFAULT_COLOR = Theme.WHITE;
			break;
		case LIGHT:
			DEFAULT_COLOR = Theme.BLACK;
		default:
			break;
		}

		tableData = new MyTable();
		tableData.setFillsViewportHeight(true);

		tableData.setDropMode(DropMode.ON_OR_INSERT);

		logModel = new LogModel();

		tableData.setModel(logModel);
 
		tableData.setDefaultRenderer(Object.class, new CustomRenderer());

		tableData.getColumnExt(logModel.COL_DATA).setMaxWidth(120);
		tableData.getColumnExt(logModel.COL_DATA).setMinWidth(100);
		tableData.getColumnExt(logModel.COL_ORA).setMaxWidth(120);
		tableData.getColumnExt(logModel.COL_ORA).setMinWidth(100);
		tableData.getColumnExt(logModel.COL_USER).setMaxWidth(80);
		tableData.getColumnExt(logModel.COL_USER).setMinWidth(50);
		tableData.getColumnExt(logModel.COL_NAME).setMaxWidth(120);
		tableData.getColumnExt(logModel.COL_NAME).setMinWidth(100);
		tableData.getColumnExt(logModel.COL_HOST).setMaxWidth(300);
		tableData.getColumnExt(logModel.COL_HOST).setMinWidth(150);
		tableData.getColumnExt(logModel.COL_MESSAGGE).setMaxWidth(600);
		tableData.getColumnExt(logModel.COL_MESSAGGE).setMinWidth(500);

		final String sensori_interni_label;

		tableData.getModel().addTableModelListener(this);

		setLayout(new MigLayout("fill"));
		/*sensori_interni_label = Testo.MOVIMENTI;

		lb_data = new JLabel(sensori_interni_label);
		add(lb_data, "gap 5px 5px, growprio 1, wrap");*/

		scrollData = new JScrollPane();

		new JPanel();
		add(scrollData, "gap 5px 5px, growy, growx, pushy, pushx, wrap");

		scrollData.setViewportView(tableData);

		tableData.packAll();


		EventBusService.subscribe(this);


		timer.start();
	}




	private static class LetturaDataFlavor extends DataFlavor {

		public static final LetturaDataFlavor letture = new LetturaDataFlavor();

		private LetturaDataFlavor() {
			super(Lettura.class, "Lettura");
		}
	}

	@EventHandler
	public void handleEvent(String event) {

	}

 
 

	@Override
	public void actionPerformed(ActionEvent event) {

		logModel.fireTableDataChanged();
 

	}

	public JTable[] getLettureTables(){
		return new JTable[]{tableData };
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		// TODO Auto-generated method stub

	}

	private static class CustomRenderer extends DefaultTableCellRenderer {
		/**
		 * 
		 */

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			int modelRow = table.convertRowIndexToModel(row);
			Component tableCellRendererComponent = super
					.getTableCellRendererComponent(table, value, isSelected,
							hasFocus, modelRow, column);
			LogModel logModel = (LogModel) table.getModel();
			Log log = logModel.getItem(modelRow);



			JLabel original =(JLabel) super.getTableCellRendererComponent(
					table, value, isSelected, hasFocus, modelRow, column);


			original.setBorder( BorderFactory.createEmptyBorder( 3, 3, 3, 3) );


			 
			tableCellRendererComponent.setForeground(DEFAULT_COLOR);
			tableCellRendererComponent.setLocale(Locale.ITALY);
			return this;
		}
	} 


}
