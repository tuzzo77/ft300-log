package com.econorma.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.jdesktop.swingx.JXDatePicker;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.FieldsWidth;
import com.econorma.logic.SensoreManager;
import com.econorma.persistence.Preferences;

import net.miginfocom.swing.MigLayout;

public class MainPanel extends JPanel implements DocumentListener {

	//	private static final String BTN_TEXT_INIZIO = " Inizio";
	//	private static final String BTN_TEXT_FINE = " Termina";

	private static final String BTN_TEXT_INIZIO = Application.getInstance().getBundle().getString("mainPanel.inizio");
	private static final String BTN_TEXT_FINE = Application.getInstance().getBundle().getString("mainPanel.termina");
	private EconormaChartPanel chartPanel;
	private JButton actionButton;

	private JLabel elapsedTimeLabel;
	private JTextField elapsedTimeValue;
	private JTextField remainingTime;
	private JLabel remainingTimeLabel;
	private JLabel mediaInternaLabel;
	private JLabel intervalLabel;
	private JTextField intervalValue;
	private JLabel nextRilevationLabel;
	private JTextField nextRilevationValue;
	private JTextField mediaInterna;
	private JLabel mediaEsternaLabel;
	private JTextField mediaEsterna;
	private JLabel scartoLabel;
	private JTextField scarto;
	private JLabel targaLabel;
	private JTextField targaValue;
	private JLabel lottoLabel;
	private JTextField lottoValue;
	private JLabel responsabileLabel;
	private JTextField responsabileValue;
	private JLabel laboratorioLabel;
	private JTextField laboratorioValue;
	private JLabel numeroProvaLabel;
	private JTextField numeroProvaValue;
	private JLabel dataProvaLabel;
	private JTextField f0Value;
	private JLabel f0Label;
	private JLabel classeALabel;
	private JTextField classeAValue;
	private JLabel classeBLabel;
	private JTextField classeBValue;
	private JLabel classeCLabel;
	private JTextField classeCValue;


	private int width;
	private int height;

	private int targaWidth;
	private int responsabileWidth;
	private int laboratorioWidth;
	private int numeroProvaWidth;
	private int dataProvaWidth;

	private boolean isPulldown;
	private int step = 1;

	private JXDatePicker dataProvaValue;



	private final Application app;

	public MainPanel(Application app) {

		this.app = app;


		FieldsWidth fields = new FieldsWidth();


		setLayout(new MigLayout("insets 0", ""));


		targaLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.targa"));
		targaValue = new JTextField();
		Application.getInstance().getGui().registerTargaField(targaValue);
		responsabileLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.responsabile"));
		responsabileValue = new JTextField();
		Application.getInstance().getGui().registerResponsabileField(responsabileValue);

		classeALabel = new JLabel("Classe A");
		classeAValue = new JTextField();
		classeBLabel = new JLabel("Classe B");
		classeBValue = new JTextField();
		classeCLabel = new JLabel("Classe C");
		classeCValue = new JTextField();

		mediaEsternaLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.mediaEsterna"));
		mediaEsterna = new JTextField();
		mediaInternaLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.mediaInterna"));
		mediaInterna = new JTextField();
		scartoLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.scarto"));
		scarto = new JTextField();

		dataProvaLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.dataProva"));

		dataProvaValue = new JXDatePicker();
		dataProvaValue.setFormats(new String[] { "E dd-MM-yyyy" });
		dataProvaValue.setDate(new Date());

		Application.getInstance().getGui().registerDataField(dataProvaValue);

		laboratorioLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.laboratorio"));
		laboratorioValue = new JTextField();
		Application.getInstance().getGui().registerLaboratorioField(laboratorioValue);

		numeroProvaLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.numeroVerbale"));
		numeroProvaValue = new JTextField();
		Application.getInstance().getGui().registerNumeroProvaField(numeroProvaValue);


		chartPanel = new EconormaChartPanel(SensoreManager.getInstance(), true);
		EventBusService.subscribe(chartPanel);

		Application.getInstance().getGui().registerEconormaChartProvider(chartPanel.getEconormaChart());

		JPanel top = new JPanel(new MigLayout());
		add(top, "north");


		top.add(targaLabel, "grow");
		top.add(targaValue, "width " + fields.getTargaWidth() + "!");
		top.add(responsabileLabel, "gapleft 10px, grow");
		top.add(responsabileValue, "width " + fields.getResponsabileWidth() + "!");
		top.add(laboratorioLabel, "gapleft 10px, grow");
		top.add(laboratorioValue,  "width " + fields.getLaboratorioWidth() + "!");
		top.add(numeroProvaLabel, "gapleft 10px, grow");
		top.add(numeroProvaValue,  "width "+ fields.getNumeroProvaWidth() + "!");
		top.add(dataProvaLabel, "gapleft 10px, grow");


		top.add(mediaInternaLabel, "sg topLabel");
		top.add(mediaInterna, "width 100!");
		top.add(mediaEsternaLabel, "gapleft 10px");
		top.add(mediaEsterna, "width 100!");
		top.add(scartoLabel, "gapleft 10px");
		top.add(scarto, "width 100!");

		actionButton = new JButton(BTN_TEXT_INIZIO);

		elapsedTimeValue = new JTextField();
		Application.getInstance().getGui().registerElaspedTimeField(elapsedTimeValue);
		elapsedTimeLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.tempoTrascorso"));
		remainingTimeLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.tempoRimanente"));
		intervalLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.rilevazione"));
		nextRilevationLabel = new JLabel(Application.getInstance().getBundle().getString("mainPanel.prossimaRilevazione"));
		nextRilevationValue = new JTextField();
		nextRilevationValue.setEditable(false);
		remainingTime = new JTextField();
		elapsedTimeValue.setEditable(false);
		remainingTime.setEditable(false);
		intervalValue = new JTextField();
		intervalValue.setEditable(false);



		JPanel infos = new JPanel();
		infos.setLayout(new MigLayout("", "", ""));
		add(chartPanel, "span, growx, pushx, growy, pushy, wrap");

		add(infos, "south, span");


		infos.add(actionButton, "width 100");
		infos.add(elapsedTimeLabel, "sg infoLabel");
		infos.add(elapsedTimeValue, "sg infoValue, width 100");
		infos.add(remainingTimeLabel, "sg infoLabel");
		infos.add(remainingTime, "sg infoValue");
		infos.add(intervalLabel, "sg infoLabel");
		infos.add(intervalValue, "width 80");
		infos.add(nextRilevationLabel, "sg infoLabel");
		infos.add(nextRilevationValue, "width 100");


		actionButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

			 

			}
		});


		targaValue.getDocument().addDocumentListener(this);
		targaValue.getDocument().putProperty("market", "targa");
		responsabileValue.getDocument().addDocumentListener(this);
		responsabileValue.getDocument().putProperty("market", "responsabile");
		laboratorioValue.getDocument().addDocumentListener(this);
		laboratorioValue.getDocument().putProperty("market", "laboratorio");
		numeroProvaValue.getDocument().addDocumentListener(this);
		numeroProvaValue.getDocument().putProperty("market", "numeroProva");

		classeAValue.setEditable(false);
		classeBValue.setEditable(false);
		classeCValue.setEditable(false);

		mediaEsterna.setEditable(false);
		mediaInterna.setEditable(false);
		scarto.setEditable(false);


		restore();

		chartPanel.clear();

		EventBusService.subscribe(this);
		updateStatus();




	}



	@EventHandler
	public void handleEvent(String event) {
		if (event == Events.CLEAR_CHART) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					chartPanel.clear();
				}
			});
		}

		if (Events.APPLICATION_MODE_CHANGED.equals(event)) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {

					updateStatus();
				}
			});
		}



	}

	private void restore() {

		Preferences preferences = Application.getInstance().getPreferences();
		responsabileValue.setText(preferences.getResponsabile());
		laboratorioValue.setText(preferences.getLaboratorio());
		numeroProvaValue.setText(preferences.getNumeroProva());
		targaValue.setText(preferences.getTargaAutomezzo());
		preferences.getDataProva();
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
		saveState(e);
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		saveState(e);
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		saveState(e);
	}

	private void saveState(DocumentEvent e) {
		Preferences preferences = Application.getInstance().getPreferences();
		Object source = e.getDocument().getProperty("market");
		if ("targa".equals(source))
			preferences.setTargaAutomezzo(targaValue.getText());

		if ("responsabile".equals(source))
			preferences.setResponsabile(responsabileValue.getText());

		if ("laboratorio".equals(source))
			preferences.setLaboratorio(laboratorioValue.getText());

		if ("numeroProva".equals(source))
			preferences.setNumeroProva(numeroProvaValue.getText());

		if("dataProva".equals(source)){
			preferences.setDataProva(dataProvaValue.getDate());
		}
	}


	private void updateStatus() {

		actionButton.setEnabled(true); 	
		if(Application.getInstance().isConfigMode())
			actionButton.setEnabled(false); 	 

	}



}
