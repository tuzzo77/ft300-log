package com.econorma.print;

public class ReportChartDetail {
	
	private String imagePath;

	
	public ReportChartDetail(String imagePath) {
		super();
		this.imagePath = imagePath;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	
}