package com.econorma.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.econorma.Application;
import com.econorma.data.Sensore;
import com.econorma.logic.SensoreManager;
import com.econorma.resources.Testo;
import com.econorma.util.Logger;

public class ConfigModel extends AbstractTableModel  {

	private static final long serialVersionUID = 1750252628241166572L;

	private static final Logger logger = Logger.getLogger(ConfigModel.class);
	private static final String TAG = ConfigModel.class.getSimpleName();

	private static Application app = Application.getInstance();

	public static final String COL_DESCRIZIONE = "Descrizione       ";
	public static final String COL_VALORE = " Valore ";
	public static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	public static final NumberFormat nf = new DecimalFormat("#####.##");


	private static final int POS_DESCRIZIONE = 0;

	private static final int POS_VALORE = 1;

	private static final String[] COLUMN_NAMES = new String[] {COL_DESCRIZIONE, COL_VALORE};

	private ArrayList<String> parameters = new ArrayList<String>();

	private boolean editMode;
	private Sensore sensore;

	public ConfigModel(Sensore sensore) {
		this.sensore=sensore;
		if (sensore!=null) {
			parameters = sensore.getParameter().toList();	
		}
	}

	@Override
	public int getColumnCount() {
		return COLUMN_NAMES.length;
	}

	@Override
	public int getRowCount() {
		return parameters.size();
	}


	@Override
	public Object getValueAt(int row, int column) {
		String parameter = parameters.get(row);

		switch (row) {
		case 0:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.ID_LOGGER;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}
		case 1:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.DESCRIZIONE_LOGGER;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}

		case 2:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.DATA_LOGGER;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}

		case 3:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.TRASMISSION_LOGGER;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}

		case 4:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.TIPO_LOGGER;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}

		case 5:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.STATO_BATTERIA;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}	


		case 6:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.TEMPERATURA;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}

		case 7:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.RANGE_MIN_LOGGER;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}

		case 8:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.RANGE_MAX_LOGGER;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}

		case 9:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.OFFSET_LOGGER;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}	

		case 10:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.UMIDITA;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}


		case 11:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.RANGE_MIN_LOGGER_URT;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}

		case 12:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.RANGE_MAX_LOGGER_URT;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}

		case 13:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.OFFSET_LOGGER_URT;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}	

		case 14:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.OHM;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}	

		case 15:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.OFFSET_LOGGER_OHM;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}	

		case 16:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.VOLT;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}	


		case 17:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.OFFSET_LOGGER_VOLT;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}	
			
		case 18:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.MILLIAMPERE;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}	

		case 19:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.OFFSET_LOGGER_MA;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}	

		case 20:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.APERTO_CHIUSO;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}	


		case 21:
			switch (column) {
			case POS_DESCRIZIONE:
				return Testo.CAMPIONI;
			case POS_VALORE:
				return parameter;
			default:
				return null;
			}	


		default:
			break;
		}

		return null;

	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {

		switch (columnIndex) {
		case POS_DESCRIZIONE:
			return String.class;
		case POS_VALORE:
			return String.class;
		default:
			break;
		}

		return super.getColumnClass(columnIndex);
	}

	public String getItem(int row) {
		if (parameters != null)
			return parameters.get(row);
		return null;
	}



	@Override
	public String getColumnName(int pos) {
		return COLUMN_NAMES[pos];
	}

	@Override
	public boolean isCellEditable(int row, int col) {

		if (col==POS_VALORE && row!=0 && row != 4 && row != 5 && row != 6 && row != 10 && row != 14 && row != 16 && row != 18 && row != 20 && row != 21)
			return true;
		return false;

	}


	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		boolean changed = isEditMode() != editMode;
		this.editMode = editMode;
		if (changed) {
			if (isEditMode()) {
			} else {

			}
		}
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {

		switch (rowIndex) {
		case 0:
			break;
		case 1:
			switch (columnIndex) {
			case POS_VALORE:
				String descrizione = (String) value;
				sensore.setDescrizione(descrizione);
				parameters = sensore.getParameter().toList();
				SensoreManager.getInstance().saveSensore(sensore);
				fireTableCellUpdated(rowIndex, columnIndex);
			}
			break;
		case 2:
			switch (columnIndex) {
			case POS_VALORE:
				String data = (String) value;
			}
			break; 
		case 3:
			switch (columnIndex) {
			case POS_VALORE:
				String trasmission = (String) value;
				sensore.setTrasmission(Integer.parseInt(trasmission));
				parameters = sensore.getParameter().toList();
				SensoreManager.getInstance().saveSensore(sensore);
				fireTableCellUpdated(rowIndex, columnIndex);
			}
			break;	
		case 4:
			switch (columnIndex) {
			case POS_VALORE:
				String tipo = (String) value;
			}
			break;
		case 7:
			switch (columnIndex) {
			case POS_VALORE:
				String rangeMin = (String) value;
				sensore.setRangeMin(Double.parseDouble(rangeMin));
				parameters = sensore.getParameter().toList();
				SensoreManager.getInstance().saveSensore(sensore);
				fireTableCellUpdated(rowIndex, columnIndex);
			}
			break;
		case 8:
			switch (columnIndex) {
			case POS_VALORE:
				String rangeMax = (String) value;
				sensore.setRangeMax(Double.parseDouble(rangeMax));
				parameters = sensore.getParameter().toList();
				SensoreManager.getInstance().saveSensore(sensore);
				fireTableCellUpdated(rowIndex, columnIndex);
			}
			break;
		case 9:
			switch (columnIndex) {
			case POS_VALORE:
				String offset = (String) value;
				sensore.setOffset(Double.parseDouble(offset));
				parameters = sensore.getParameter().toList();
				SensoreManager.getInstance().saveSensore(sensore);
				fireTableCellUpdated(rowIndex, columnIndex);
			}
			break;
		case 10:
			switch (columnIndex) {
			case POS_VALORE:
				String rangeMinUrt = (String) value;
				sensore.setRangeMinURT(Double.parseDouble(rangeMinUrt));
				SensoreManager.getInstance().saveSensore(sensore);
				fireTableCellUpdated(rowIndex, columnIndex);
			}
			break;
		case 11:
			switch (columnIndex) {
			case POS_VALORE:
				String rangeMaxUrt = (String) value;
				sensore.setRangeMaxURT(Double.parseDouble(rangeMaxUrt));
				parameters = sensore.getParameter().toList();
				SensoreManager.getInstance().saveSensore(sensore);
				fireTableCellUpdated(rowIndex, columnIndex);
			}
			break;
		case 12:
			switch (columnIndex) {
			case POS_VALORE:
				String offsetUrt = (String) value;
				sensore.setOffsetURT(Double.parseDouble(offsetUrt));
				parameters = sensore.getParameter().toList();
				SensoreManager.getInstance().saveSensore(sensore);
				fireTableCellUpdated(rowIndex, columnIndex);
			}
			break;
		case 13:
			switch (columnIndex) {
			case POS_VALORE:
				String offsetURT = (String) value;
				sensore.setOffsetURT(Double.parseDouble(offsetURT));
				parameters = sensore.getParameter().toList();
				SensoreManager.getInstance().saveSensore(sensore);
				fireTableCellUpdated(rowIndex, columnIndex);
			}
			break;

		default:
			break;
		}


	}


}
