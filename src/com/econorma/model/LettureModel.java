package com.econorma.model;

import java.awt.EventQueue;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import org.apache.log4j.MDC;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.io.Receiver.ReadListener;
import com.econorma.logic.SensoreManager;
import com.econorma.resources.Testo;
import com.econorma.ui.Events;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;
import com.econorma.util.StringUtils;

public class LettureModel extends AbstractTableModel implements ReadListener {

	private static final long serialVersionUID = 1750252628241166572L;

	private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
			"dd/MM/yy HH:mm:ss");
	public static final NumberFormat nf = new DecimalFormat("#####.##");
	public static final NumberFormat nfATP = new DecimalFormat("####0.00");
	public static final NumberFormat nf0 = new DecimalFormat("####0.00");

	private static Application app = Application.getInstance();

	public static final String COL_ID_SONDA = Application.getInstance().getBundle().getString("lettureModel.idSonda");
	public static final String COL_DESCRIZIONE = Application.getInstance().getBundle().getString("lettureModel.descrizione");
	public static final String COL_TEMP = "C°   ";
	public static final String COL_TEMP_MISTA = " Val ";
	public static final String COL_URT = "UR%   ";
	public static final String COL_UM = " Um  ";
	public static final String COL_OFFSET = "Offset";
	public static final String COL_INCERTEZZA = Application.getInstance().getBundle().getString("lettureModel.incertezza");
	public static final String COL_DATA = Application.getInstance().getBundle().getString("lettureModel.data");
	public static final String COL_OFFSET_URT = "Offset UR%";
	public static final String COL_INCERTEZZA_URT = Application.getInstance().getBundle().getString("lettureModel.incertezzaUr");

	public static final int POS_ID_SONDA = 0;

	public static final int POS_DESCRIZIONE = 1;

	public static final int POS_TEMPERATURA = 2;

	public static final int POS_UM = 3;

	public static final int POS_OFFSET = 3;

	public static final int POS_INCERTEZZA = 5;

	public static final int POS_DATA = 4;


	public static final int POS_OFFSET_TEMP_URT = 3;
	public static final int POS_UMIDITA = 4;
	public static final int POS_OFFSET_UMI_URT = 5;
	public static final int POS_DATA_URT = 6;
	public static final int POS_INCERTEZZA_TEMP_URT = 7;
	public static final int POS_INCERTEZZA_UMI_URT = 8;

	public static final int POS_UMIDITA_MISTA = 4;
	public static final int POS_OFFSET_MISTA = 5;
	public static final int POS_DATA_MISTA = 6;

	private static final Logger logger = Logger.getLogger(LettureModel.class);
	private static final String TAG = LettureModel.class.getSimpleName();



	private static final String[] COLUMN_NAMES = new String[] { COL_ID_SONDA,
			COL_DESCRIZIONE, COL_TEMP, COL_OFFSET, COL_DATA };

	private static final String[] COLUMN_NAMES_INCERTEZZA = new String[] { COL_ID_SONDA,
			COL_DESCRIZIONE, COL_TEMP, COL_OFFSET, COL_DATA, COL_INCERTEZZA };

	private static final String[] COLUMN_NAMES_URT = new String[] { COL_ID_SONDA,
			COL_DESCRIZIONE, COL_TEMP, COL_OFFSET, COL_URT, COL_OFFSET_URT, COL_DATA };

	private static final String[] COLUMN_NAMES_URT_INCERTEZZA = new String[] { COL_ID_SONDA,
			COL_DESCRIZIONE, COL_TEMP, COL_OFFSET, COL_URT, COL_OFFSET_URT, COL_DATA, COL_INCERTEZZA, COL_INCERTEZZA_URT };

	private static final String[] COLUMN_NAMES_MISTA = new String[] { COL_ID_SONDA,
			COL_DESCRIZIONE, COL_TEMP_MISTA, COL_UM,  COL_URT, COL_OFFSET, COL_DATA };

	private List<Lettura> ultimeLetture = new ArrayList<Lettura>();

	private Sensore.Location where;

	private boolean editMode;

	public LettureModel(Sensore.Location where) {
		this.where = where;
		EventBusService.subscribe(this);
	}

	@Override
	public int getColumnCount() {

		switch (app.getTipoMisure()) {
		case TEMPERATURA:

			return COLUMN_NAMES.length;

		case TEMPERATURA_UMIDITA:

			return COLUMN_NAMES_URT.length;

		case MISTA:
			return COLUMN_NAMES_MISTA.length;
		default:
			return COLUMN_NAMES.length;
		}

	}

	@Override
	public int getRowCount() {
		return ultimeLetture.size();
	}

	@Override
	public Object getValueAt(int row, int column) {

		Lettura lettura = ultimeLetture.get(row);

		switch (app.getTipoMisure()) {
		case TEMPERATURA:

			switch (column) {
			case POS_ID_SONDA:
				return lettura.getIdSonda();
			case POS_TEMPERATURA:
				double valore = lettura.getValore();


				return nfATP.format(valore);


			case POS_DATA:
				if (lettura.getData() != null) {
					return DATE_FORMAT.format(lettura.getData());
				}
				return null;
			case POS_OFFSET:
				double offset = lettura.getSensore().getOffset();
				if (offset != 0)
					return offset;
				else
					return null;
			case POS_DESCRIZIONE:
				return lettura.getSensore().getDescrizione();
			case POS_INCERTEZZA:
				double incertezza = lettura.getSensore().getIncertezza();
				if (incertezza != 0)
					return incertezza;
				else
					return null;
			default:
				throw new RuntimeException("Campo sconosciuto");
			}


		case TEMPERATURA_UMIDITA:

			switch (column) {
			case POS_ID_SONDA:
				return lettura.getIdSonda();
			case POS_TEMPERATURA:
				double valore = lettura.getValore();

				// TODO convertire a Double
				return nf.format(valore);
			case POS_UMIDITA:
				double valore_umi = lettura.getUmidita();

				// TODO convertire a Double
				return nf.format(valore_umi);
			case POS_DATA_URT:
				if (lettura.getData() != null) {
					return DATE_FORMAT.format(lettura.getData());
				}
				return null;
			case POS_OFFSET_TEMP_URT:
				double offset = lettura.getSensore().getOffset();
				if (offset != 0)
					return offset;
				else
					return null;
			case POS_OFFSET_UMI_URT:
				double offsetURT = lettura.getSensore().getOffsetURT();
				if (offsetURT != 0)
					return offsetURT;
				else
					return null;
			case POS_DESCRIZIONE:
				return lettura.getSensore().getDescrizione();
			case POS_INCERTEZZA_TEMP_URT:
				double incertezza = lettura.getSensore().getIncertezza();
				if (incertezza != 0)
					return incertezza;
				else
					return null;
			case POS_INCERTEZZA_UMI_URT:
				double incertezzaURT = lettura.getSensore().getIncertezzaURT();
				if (incertezzaURT != 0)
					return incertezzaURT;
				else
					return null;
			default:
				throw new RuntimeException("Campo sconosciuto");
			}

		case MISTA:
			switch (column) {
			case POS_ID_SONDA:
				return lettura.getIdSonda();
			case POS_TEMPERATURA:
				double valore = lettura.getValore();
				return nf.format(valore);
			case POS_UM:
				Sensore sensore =lettura.getSensore();
				return sensore.toString();
			case POS_UMIDITA_MISTA:
				double valore_umi = lettura.getUmidita();

				// TODO convertire a Double
				return nf.format(valore_umi);
			case POS_DATA_MISTA:
				if (lettura.getData() != null) {
					return DATE_FORMAT.format(lettura.getData());
				}
				return null;
			case POS_OFFSET_MISTA:
				double offset = lettura.getSensore().getOffset();
				if (offset != 0)
					return offset;
				else
					return null;
			case POS_DESCRIZIONE:
				return lettura.getSensore().getDescrizione();
			default:
				throw new RuntimeException("Campo sconosciuto");
			}


		default:
			return null;

		}





	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {


		switch (app.getTipoMisure()) {
		case TEMPERATURA:

			switch (columnIndex) {
			case POS_DESCRIZIONE:
				return String.class;
			case POS_OFFSET:
				return Double.class;
			case POS_INCERTEZZA:
				return Double.class;
				//				case POS_OFFSET:
				//					return String.class;
			case POS_TEMPERATURA:
				return String.class;
			default:
				break;
			} 
			break;


		case TEMPERATURA_UMIDITA:

			switch (columnIndex) {
			case POS_DESCRIZIONE:
				return String.class;
			case POS_OFFSET_TEMP_URT:
				return Double.class;
			case POS_OFFSET_UMI_URT:
				return Double.class;
			case POS_INCERTEZZA_TEMP_URT:
				return Double.class;
			case POS_INCERTEZZA_UMI_URT:
				return Double.class;
				//				case POS_OFFSET:
				//					return String.class;
			case POS_TEMPERATURA:
				return String.class;
			case POS_UMIDITA:
				return String.class;
			default:
				break;
			} 
			break;


		case MISTA:
			switch (columnIndex) {
			case POS_DESCRIZIONE:
				return String.class;
			case POS_OFFSET_MISTA:
				return Double.class;
				//				case POS_OFFSET:
				//					return String.class;
			case POS_TEMPERATURA:
				return String.class;
			case POS_UMIDITA_MISTA:
				return String.class;
			default:
				break;
			} 
			break;

		default:
			break;
		}		



		return super.getColumnClass(columnIndex);
	}

	public Lettura getItem(int row) {
		if (ultimeLetture != null)
			return ultimeLetture.get(row);
		return null;
	}

	public Location getlocation() {
		return where;
	}

	@Override
	public String getColumnName(int pos) {

		switch (app.getTipoMisure()) {
		case TEMPERATURA:
			return COLUMN_NAMES_INCERTEZZA[pos];

		case TEMPERATURA_UMIDITA:

			return COLUMN_NAMES_URT_INCERTEZZA[pos];


		case MISTA:
			return COLUMN_NAMES_MISTA[pos];

		default:
			return COLUMN_NAMES[pos];
		}

		//		return COLUMN_NAMES[pos];
	}

	@Override
	public boolean isCellEditable(int row, int col) {

		switch (app.getTipoMisure()) {
		case TEMPERATURA:
			if (isEditMode() && (col == POS_OFFSET || col == POS_DESCRIZIONE || col == POS_INCERTEZZA))
				return true;
			else
				return false;
		case TEMPERATURA_UMIDITA:

			if (isEditMode() && (col == POS_OFFSET_TEMP_URT || col == POS_OFFSET_UMI_URT || col == POS_DESCRIZIONE || col == POS_INCERTEZZA_TEMP_URT || col == POS_INCERTEZZA_UMI_URT))
				return true;
			else
				return false; 
		case MISTA:
			if (isEditMode() && (col == POS_OFFSET_MISTA || col == POS_DESCRIZIONE))
				return true;
			else
				return false;

		default:
			return false; 
		}
	}

	@Override
	public void onRead(final Lettura lettura) {

		Sensore sensore = lettura.getSensore();
		Sensore.Location whereLettura;
		if (sensore == null)
			whereLettura = Sensore.Location.UNKNOWN;
		else {
			switch (sensore.getLocation()) {
			case ESTERNO:
				whereLettura = Location.ESTERNO;
				break;
			case INTERNO:
				whereLettura = Location.INTERNO;
				break;
			case UNKNOWN:
				whereLettura = Location.UNKNOWN;
				break;
			case EUTETTICHE:
				whereLettura = Location.EUTETTICHE;
				break;
			case VIRTUAL:
				whereLettura = Location.VIRTUAL;
				break;
			default:
				throw new RuntimeException("caso non gestito");
			}
		}

		if (whereLettura != where)
			return;

		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				add(lettura);
			}
		});
	}

	public void add(Lettura lettura) {
		int idx = -1;
		for (int i = 0; i < ultimeLetture.size(); i++) {
			Lettura ll = ultimeLetture.get(i);
			if (StringUtils.equals(lettura.getIdSonda(), ll.getIdSonda())) {
				idx = i;
				break;
			}
		}

		if (idx >= 0) {
			ultimeLetture.set(idx, lettura);
			fireTableRowsUpdated(idx, idx);
		} else
			ultimeLetture.add(lettura);

		fireTableDataChanged();
	}

	public void remove(Sensore sensore) {
		if (ultimeLetture != null) {
			for (int i = 0; i < ultimeLetture.size(); i++) {
				Lettura l = ultimeLetture.get(i);
				if (l == null)
					continue;
				if (StringUtils.equals(l.getIdSonda(), sensore.getId_sonda())) {
					ultimeLetture.remove(i);
					fireTableRowsDeleted(i, i);
				}
			}
		}
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		boolean changed = isEditMode() != editMode;
		this.editMode = editMode;
		if (changed) {
			if (isEditMode()) {
			} else {

			}
		}
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		Lettura item = getItem(rowIndex);

		switch (app.getTipoMisure()) {
		case TEMPERATURA:

			switch (columnIndex) {
			case POS_DESCRIZIONE:
				item.getSensore().setDescrizione((String) value);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Sensori");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica descrizione sensore: " + item.getSensore().getId_sonda() + " - " +  (String) value);
				break;
			case POS_OFFSET:
				if (value != null && !(value.equals(""))) {

					String offsetStr=((String) value);
					offsetStr=offsetStr.replaceAll(",", ".");
					Double offset=Double.parseDouble(offsetStr);
					item.getSensore().setOffset(offset);
					SensoreManager.getInstance().saveState();
					MDC.put("event_name", "Sensori");
					logger.log(LoggerCustomLevel.AUDIT, "Modifica offset sensore: " + item.getSensore().getId_sonda() +  " - " +  offset);
				}

				break;

			case POS_INCERTEZZA:
				if (value != null && !(value.equals(""))) {

					String incertezzaStr=((String) value);
					incertezzaStr=incertezzaStr.replaceAll(",", ".");
					Double incertezza=Double.parseDouble(incertezzaStr);
					item.getSensore().setIncertezza(incertezza);
					SensoreManager.getInstance().saveState();
					MDC.put("event_name", "Sensori");
					logger.log(LoggerCustomLevel.AUDIT, "Modifica offset sensore: " + item.getSensore().getId_sonda() +  " - " +  incertezza);
				}

				break;

			default:
				break;
			}


			break;
		case TEMPERATURA_UMIDITA:

			switch (columnIndex) {
			case POS_DESCRIZIONE:
				item.getSensore().setDescrizione((String) value);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Sensori");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica descrizione sensore: " + item.getSensore().getId_sonda() + " - " +  (String) value);
				break;
			case POS_OFFSET_TEMP_URT:
				if (value != null && !(value.equals(""))) {

					String offsetStr=((String) value);
					offsetStr=offsetStr.replaceAll(",", ".");
					Double offset=Double.parseDouble(offsetStr);
					item.getSensore().setOffset(offset);
					SensoreManager.getInstance().saveState();
					MDC.put("event_name", "Sensori");
					logger.log(LoggerCustomLevel.AUDIT, "Modifica offset sensore: " + item.getSensore().getId_sonda() + " - " + offset);
				}

				break;

			case POS_OFFSET_UMI_URT:
				if (value != null && !(value.equals(""))) {

					String offsetStr=((String) value);
					offsetStr=offsetStr.replaceAll(",", ".");
					Double offset=Double.parseDouble(offsetStr);
					item.getSensore().setOffsetURT(offset);
					SensoreManager.getInstance().saveState();
					MDC.put("event_name", "Sensori");
					logger.log(LoggerCustomLevel.AUDIT, "Modifica offset sensore: " + item.getSensore().getId_sonda() + " - " + offset);
				}

				break;

			case POS_INCERTEZZA_TEMP_URT:
				if (value != null && !(value.equals(""))) {

					String incertezzaStr=((String) value);
					incertezzaStr=incertezzaStr.replaceAll(",", ".");
					Double incertezza=Double.parseDouble(incertezzaStr);
					item.getSensore().setIncertezza(incertezza);
					SensoreManager.getInstance().saveState();
					MDC.put("event_name", "Sensori");
					logger.log(LoggerCustomLevel.AUDIT, "Modifica offset sensore: " + item.getSensore().getId_sonda() +  " - " +  incertezza);
				}

				break;

			case POS_INCERTEZZA_UMI_URT:
				if (value != null && !(value.equals(""))) {

					String incertezzaUrtStr=((String) value);
					incertezzaUrtStr=incertezzaUrtStr.replaceAll(",", ".");
					Double incertezzaUrt=Double.parseDouble(incertezzaUrtStr);
					item.getSensore().setIncertezzaURT(incertezzaUrt);
					SensoreManager.getInstance().saveState();
					MDC.put("event_name", "Sensori");
					logger.log(LoggerCustomLevel.AUDIT, "Modifica offset sensore: " + item.getSensore().getId_sonda() +  " - " +  incertezzaUrt);
				}

				break;


			default:
				break;
			}

			break;

		case MISTA:
			switch (columnIndex) {
			case POS_DESCRIZIONE:
				item.getSensore().setDescrizione((String) value);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Sensori");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica descrizione sensore: " + item.getSensore().getId_sonda()  + " - " +  (String) value);
				break;
			case POS_OFFSET_MISTA:
				if (value != null && !(value.equals(""))) {

					String offsetStr=((String) value);
					offsetStr=offsetStr.replaceAll(",", ".");
					Double offset=Double.parseDouble(offsetStr);
					item.getSensore().setOffset(offset);
					SensoreManager.getInstance().saveState();
				}

				break;


			default:
				break;
			}


			break;
		default:
			break;
		}


	}

	@EventHandler
	public void handleEvent(final Events.DischargeEvent dischargeEvent){
		switch(dischargeEvent.action){
		case RUNNING:
			break;
		case COMPLETE:
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					if(Testo.LOGGER_AUTOMATICO.equals(dischargeEvent.type)){
						//carico tutto
						List<Lettura> tmp =  new ArrayList<Lettura>(ultimeLetture);
						for(Lettura l : tmp){
							Lettura ultimaLettura = app.getDao().getUltimaLettura(l.getSensore());
							ultimaLettura.setSensore(l.getSensore());
							add(ultimaLettura);
						}
					}else if (Testo.LOGGER_MANUALE.equals(dischargeEvent.type)){
						//carico 1 solo sensore
						Sensore s = dischargeEvent.sensore;
						Lettura ultimaLettura = app.getDao().getUltimaLettura(s);
						ultimaLettura.setSensore(s);
						add(ultimaLettura);
					}		 			
				}
			});
		}
	}
}
