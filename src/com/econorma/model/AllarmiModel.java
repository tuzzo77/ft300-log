package com.econorma.model;

import java.awt.EventQueue;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.apache.log4j.MDC;

import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.io.Receiver.ReadListener;
import com.econorma.logic.SensoreManager;
import com.econorma.persistence.DAO;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;
import com.econorma.util.StringUtils;

public class AllarmiModel extends AbstractTableModel implements ReadListener {

	private static final long serialVersionUID = 1750252628241166572L;

	private static final Logger logger = Logger.getLogger(AllarmiModel.class);
	private static final String TAG = AllarmiModel.class.getSimpleName();

	public static final NumberFormat nf = new DecimalFormat("#####.##");
	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"HH:mm:ss");

	private static Application app = Application.getInstance();

	public static final String COL_ID_SONDA = Application.getInstance().getBundle().getString("allarmiModel.idSonda");
	public static final String COL_DESCRIZIONE = Application.getInstance().getBundle().getString("allarmiModel.descrizione");
	public static final String COL_TEMP = " C�   ";
	public static final String COL_TEMP_MISTA = " Val ";
	public static final String COL_TEMP_MIN = "Min C�   ";
	public static final String COL_TEMP_MAX = "Max C�   ";
	public static final String COL_UM = " Um ";

	public static final String COL_UMI = " UR%   ";
	public static final String COL_UMI_MIN = "Min UR%   ";
	public static final String COL_UMI_MAX = "Max UR%   ";

	public static final String COL_OHM_MIN = "Min Pt100  ";
	public static final String COL_OHM_MAX = "Max Pt100  ";

	public static final String COL_OFFSET = " Offset ";
	public static final String COL_OFFSET_URT = " Offset UR% ";
	public static final String COL_OFFSET_VOLT = " Offset Volt ";
	public static final String COL_OFFSET_MILLIAMPERE = " Offset mA ";
	public static final String COL_OFFSET_OHM = " Offset Pt100 ";
	public static final String COL_VOLT = " Volt ";
	public static final String COL_MILLIAMPERE = " mA ";
	public static final String COL_OHM = " Pt100 ";

	public static final int POS_ID_SONDA = 0;
	public static final int POS_DESCRIZIONE = 1;

	public static int POS_TEMPERATURA;
	public static int POS_TEMP_MIN;
	public static int POS_TEMP_MAX;
	public static int POS_UMI_MIN;
	public static int POS_UMI_MAX;
	public static int POS_OHM_MIN;
	public static int POS_OHM_MAX;
	public static int POS_OFFSET;
	public static int POS_UMIDITA;
	public static int POS_OFFSET_URT;
	public static int POS_OHM;
	public static int POS_OFFSET_OHM;


	private static final List<String> COLUMN_NAMES = new ArrayList<String>();

	private List<Lettura> ultimeLetture = new ArrayList<Lettura>();

	private Sensore.Location where;
	private DAO dao;

	private boolean editMode;

	public AllarmiModel(Sensore.Location where, List<Lettura> letture ) {

		createColumns();

		this.where = where;
		if(letture!=null && !letture.isEmpty()){
			for(Lettura l:letture){
				add(l);
			}
		}
	}

	public void createColumns() {
		boolean isTemperature=false;
		COLUMN_NAMES.clear();
		COLUMN_NAMES.add(COL_ID_SONDA);
		COLUMN_NAMES.add(COL_DESCRIZIONE);

		if (Application.getInstance().isTemperature()) {
			COLUMN_NAMES.add(COL_TEMP);
			POS_TEMPERATURA = COLUMN_NAMES.indexOf(COL_TEMP);
			COLUMN_NAMES.add(COL_TEMP_MIN);
			POS_TEMP_MIN = COLUMN_NAMES.indexOf(COL_TEMP_MIN);
			COLUMN_NAMES.add(COL_TEMP_MAX);
			POS_TEMP_MAX = COLUMN_NAMES.indexOf(COL_TEMP_MAX);
			COLUMN_NAMES.add(COL_OFFSET);
			POS_OFFSET = COLUMN_NAMES.indexOf(COL_OFFSET);
			isTemperature = true;
		}
		if (Application.getInstance().isTemperatureHumidity()) {
			if (!isTemperature) {
				COLUMN_NAMES.add(COL_TEMP);
				POS_TEMPERATURA = COLUMN_NAMES.indexOf(COL_TEMP);
				COLUMN_NAMES.add(COL_TEMP_MIN);
				POS_TEMP_MIN = COLUMN_NAMES.indexOf(COL_TEMP_MIN);
				COLUMN_NAMES.add(COL_TEMP_MAX);
				POS_TEMP_MAX = COLUMN_NAMES.indexOf(COL_TEMP_MAX);
				COLUMN_NAMES.add(COL_OFFSET);
				POS_OFFSET = COLUMN_NAMES.indexOf(COL_OFFSET);
			}
			COLUMN_NAMES.add(COL_UMI);
			POS_UMIDITA = COLUMN_NAMES.indexOf(COL_UMI);
			COLUMN_NAMES.add(COL_UMI_MIN);
			POS_UMI_MIN = COLUMN_NAMES.indexOf(COL_UMI_MIN);
			COLUMN_NAMES.add(COL_UMI_MAX);
			POS_UMI_MAX = COLUMN_NAMES.indexOf(COL_UMI_MAX);
			COLUMN_NAMES.add(COL_OFFSET_URT);
			POS_OFFSET_URT = COLUMN_NAMES.indexOf(COL_OFFSET_URT);
		}
		if (Application.getInstance().isOhm()) {
			COLUMN_NAMES.add(COL_OHM);
			POS_OHM = COLUMN_NAMES.indexOf(COL_OHM);
			COLUMN_NAMES.add(COL_OHM_MIN);
			POS_OHM_MIN = COLUMN_NAMES.indexOf(COL_OHM_MIN);
			COLUMN_NAMES.add(COL_OHM_MAX);
			POS_OHM_MAX = COLUMN_NAMES.indexOf(COL_OHM_MAX);
			COLUMN_NAMES.add(COL_OFFSET_OHM);
			POS_OFFSET_OHM = COLUMN_NAMES.indexOf(COL_OFFSET_OHM);
		}

	}


	@Override
	public int getColumnCount() {
		return COLUMN_NAMES.size();
	}

	@Override
	public int getRowCount() {
		return ultimeLetture.size();
	}


	@Override
	public Object getValueAt(int row, int column) {

		Lettura lettura = ultimeLetture.get(row);

		Sensore sensore = lettura.getSensore();

		if (column==POS_ID_SONDA) {
			return sensore.getId_sonda();
		} 

		if (column==POS_DESCRIZIONE) {
			if (sensore==null){
				return "";
			} else{ 
				return sensore.getDescrizione();
			} 
		}
		if (column==POS_TEMPERATURA) {
			double valore = lettura.getValore();
			return nf.format(valore);
		}
		if (column==POS_UMIDITA) {
			double umidita = lettura.getUmidita();
			return nf.format(umidita);
		}
		if (column==POS_OFFSET) {
			double offset = 0.0;
			if (sensore.getOffset() != 0.0){
				offset = sensore.getOffset();
			}  
			if (offset!=0.0)
				return nf.format(offset);
			else
				return "0.0";
		}

		if (column==POS_OFFSET_URT) {
			double offsetURT=0.0;
			if (sensore.getOffsetURT() != 0.0){
				offsetURT = sensore.getOffsetURT();
			}  
			if (offsetURT!=0.0)
				return nf.format(offsetURT);
			else
				return "0.0";
		}


		if (column==POS_OHM) {
			double ohm = lettura.getOhm();
			return nf.format(ohm);
		}


		if (column==POS_OFFSET_OHM) {
			double offsetURT=0.0;
			if (sensore.getOffsetOhm() != 0.0){
				offsetURT = sensore.getOffsetOhm();
			} 
			if (offsetURT!=0.0)
				return nf.format(offsetURT);
			else
				return "0.0";
		}

		if (column==POS_TEMP_MIN) {
			double rangeTempMin=0.0;
			if (sensore.getRangeMin() != 0.0){
				rangeTempMin = sensore.getRangeMin();
			} 
			if (rangeTempMin!=0.0)
				return nf.format(rangeTempMin);
			else
				return "0.0";
		}

		if (column==POS_TEMP_MAX) {
			double rangeTempMax=0.0;
			if (sensore.getRangeMax() != 0.0){
				rangeTempMax = sensore.getRangeMax();
			} 
			if (rangeTempMax!=0.0)
				return nf.format(rangeTempMax);
			else
				return "0.0";
		}

		if (column==POS_UMI_MIN) {
			double rangeMinUrt=0.0;
			if (sensore.getRangeMinURT()!= 0.0){
				rangeMinUrt = sensore.getRangeMinURT();
			} 
			if (rangeMinUrt!=0.0)
				return nf.format(rangeMinUrt);
			else
				return "0.0";
		}

		if (column==POS_UMI_MAX) {
			double rangeMaxUrt=0.0;
			if (sensore.getRangeMaxURT()!= 0.0){
				rangeMaxUrt = sensore.getRangeMaxURT();
			} 
			if (rangeMaxUrt!=0.0)
				return nf.format(rangeMaxUrt);
			else
				return "0.0";
		}

		if (column==POS_OHM_MIN) {
			double rangeMinOhm=0.0;
			if (sensore.getRangeMinOhm()!= 0.0){
				rangeMinOhm = sensore.getRangeMinOhm();
			} 
			if (rangeMinOhm!=0.0)
				return nf.format(rangeMinOhm);
			else
				return "0.0";
		}

		if (column==POS_OHM_MAX) {
			double rangeMaxOhm=0.0;
			if (sensore.getRangeMaxOhm()!= 0.0){
				rangeMaxOhm = sensore.getRangeMaxOhm();
			} 
			if (rangeMaxOhm!=0.0)
				return nf.format(rangeMaxOhm);
			else
				return "0.0";
		}


		return null;

	}


	@Override
	public Class<?> getColumnClass(int columnIndex) {


		if (columnIndex==POS_DESCRIZIONE) {
			return String.class; 
		}

		if (columnIndex==POS_TEMPERATURA) {
			return String.class;
		}
		if (columnIndex==POS_UMIDITA) {
			return String.class; 
		}
		if (columnIndex==POS_OFFSET) {
			return String.class;
		}

		if (columnIndex==POS_OFFSET_URT) {
			return String.class;
		}

		if (columnIndex==POS_OFFSET_OHM) {
			return String.class; 
		}
		if (columnIndex==POS_TEMP_MIN) {
			return String.class; 
		}
		if (columnIndex==POS_TEMP_MAX) {
			return String.class; 
		}
		if (columnIndex==POS_UMI_MIN) {
			return String.class;
		}

		if (columnIndex==POS_UMI_MAX) {
			return String.class;
		}
		if (columnIndex==POS_OHM_MIN) {
			return String.class;
		}
		if (columnIndex==POS_OHM_MAX) {
			return String.class;
		}

		return super.getColumnClass(columnIndex);
	}

	public Lettura getItem(int row) {
		if (ultimeLetture != null)
			return ultimeLetture.get(row);
		return null;
	}



	@Override
	public String getColumnName(int pos) {
		return COLUMN_NAMES.get(pos);
	}

	@Override
	public boolean isCellEditable(int row, int col) {

		if ( (col == POS_OFFSET || col == POS_OFFSET_URT || col==POS_OFFSET_OHM || col==POS_TEMP_MIN || col==POS_TEMP_MAX
				|| col==POS_UMI_MIN || col==POS_UMI_MAX || col==POS_OHM_MIN || col==POS_OHM_MAX)) {
			return true;
		}


		return false;

	}

	@Override
	public void onRead(final Lettura lettura) {


		Sensore sensore = lettura.getSensore();
		Sensore.Location whereLettura;
		if (sensore == null)
			whereLettura = Sensore.Location.UNKNOWN;
		else {
			switch (sensore.getLocation()) {
			case ESTERNO:
				whereLettura = Location.ESTERNO;
				break;
			case INTERNO:
				whereLettura = Location.INTERNO;
				break;
			case UNKNOWN:
				whereLettura = Location.UNKNOWN;
				break;
			case VIRTUAL:
				whereLettura = Location.VIRTUAL;
				break;
			default:
				throw new RuntimeException("caso non gestito");
			}
		}

		if (whereLettura != where)
			return;

		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				add(lettura);
			}
		});
	}

	public void add(Lettura lettura) {
		int idx = -1;
		for (int i = 0; i < ultimeLetture.size(); i++) {
			Lettura ll = ultimeLetture.get(i);
			if (StringUtils.equals(lettura.getIdSonda(), ll.getIdSonda())) {
				idx = i;
				break;
			}
		}

		if (idx >= 0) {
			ultimeLetture.set(idx, lettura);
			fireTableRowsUpdated(idx, idx);
		} else
			ultimeLetture.add(lettura);

		fireTableDataChanged();
	}

	public void remove(Sensore sensore) {
		if (ultimeLetture != null) {
			for (int i = 0; i < ultimeLetture.size(); i++) {
				Lettura l = ultimeLetture.get(i);
				if (l == null)
					continue;
				if (StringUtils.equals(l.getIdSonda(), sensore.getId_sonda())) {
					ultimeLetture.remove(i);
					fireTableRowsDeleted(i, i);
				}
			}
		}
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		boolean changed = isEditMode() != editMode;
		this.editMode = editMode;
		if (changed) {
			if (isEditMode()) {
			} else {

			}
		}
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		Lettura item = getItem(rowIndex);

		if (columnIndex==POS_OFFSET) {
			if (value != null && !(value.equals(""))) {


				String offsetStr=((String) value);
				offsetStr=offsetStr.replaceAll(",", ".");
				double offset=Double.parseDouble(offsetStr);

				item.getSensore().setOffset(offset);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Soglie Allarmi");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica Offset Temperatura" + item.getSensore().getId_sonda() + " - " + Double.toString(offset));
			}

			fireTableCellUpdated(rowIndex, columnIndex);

		}
		
		if (columnIndex==POS_OFFSET_URT) {
			if (value != null && !(value.equals(""))) {


				String offsetStr=((String) value);
				offsetStr=offsetStr.replaceAll(",", ".");
				double offset=Double.parseDouble(offsetStr);

				item.getSensore().setOffsetURT(offset);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Soglie Allarmi");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica Offset Umidita" + item.getSensore().getId_sonda() + " - " + Double.toString(offset));
			}

			fireTableCellUpdated(rowIndex, columnIndex);

		}
		
		if (columnIndex==POS_OFFSET_OHM) {
			if (value != null && !(value.equals(""))) {


				String offsetStr=((String) value);
				offsetStr=offsetStr.replaceAll(",", ".");
				double offset=Double.parseDouble(offsetStr);

				item.getSensore().setOffsetOhm(offset);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Soglie Allarmi");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica Offset Ohm" + item.getSensore().getId_sonda() + " - " + Double.toString(offset));
			}

			fireTableCellUpdated(rowIndex, columnIndex);

		}

		if (columnIndex==POS_TEMP_MIN) {
			if (value != null && !(value.equals(""))) {


				String minStr=((String) value);
				minStr=minStr.replaceAll(",", ".");
				double min=Double.parseDouble(minStr);

				item.getSensore().setRangeMin(min);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Soglie Allarmi");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica allarme Temperatura Minima " + item.getSensore().getId_sonda() + " - " + Double.toString(min));
			}

			fireTableCellUpdated(rowIndex, columnIndex);

		}

		if (columnIndex==POS_TEMP_MAX) {
			if (value != null  && !(value.equals(""))) {


				String maxStr=((String) value);
				maxStr=maxStr.replaceAll(",", ".");
				double max=Double.parseDouble(maxStr);

				item.getSensore().setRangeMax(max);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Soglie Allarmi");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica allarme Temperatura Massima " + item.getSensore().getId_sonda() + " - " + Double.toString(max));
			}

			fireTableCellUpdated(rowIndex, columnIndex);


		}
		
		if (columnIndex==POS_UMI_MIN) {
			if (value != null  && !(value.equals(""))) {


				String minStr=((String) value);
				minStr=minStr.replaceAll(",", ".");
				double min=Double.parseDouble(minStr);

				item.getSensore().setRangeMinURT(min);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Soglie Allarmi");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica allarme Umidita Minima " + item.getSensore().getId_sonda() + " - " + Double.toString(min));
			}

			fireTableCellUpdated(rowIndex, columnIndex);


		}
		
		if (columnIndex==POS_UMI_MAX) {
			if (value != null  && !(value.equals(""))) {


				String maxStr=((String) value);
				maxStr=maxStr.replaceAll(",", ".");
				double max=Double.parseDouble(maxStr);

				item.getSensore().setRangeMaxURT(max);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Soglie Allarmi");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica allarme Umidita Massima " + item.getSensore().getId_sonda() + " - " + Double.toString(max));
			}

			fireTableCellUpdated(rowIndex, columnIndex);


		}
		
		if (columnIndex==POS_OHM_MIN) {
			if (value != null  && !(value.equals(""))) {


				String minStr=((String) value);
				minStr=minStr.replaceAll(",", ".");
				double min=Double.parseDouble(minStr);

				item.getSensore().setRangeMinOhm(min);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Soglie Allarmi");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica allarme Ohm Minima " + item.getSensore().getId_sonda() + " - " + Double.toString(min));
			}

			fireTableCellUpdated(rowIndex, columnIndex);


		}
		
		if (columnIndex==POS_OHM_MAX) {
			if (value != null  && !(value.equals(""))) {


				String maxStr=((String) value);
				maxStr=maxStr.replaceAll(",", ".");
				double max=Double.parseDouble(maxStr);

				item.getSensore().setRangeMaxOhm(max);
				SensoreManager.getInstance().saveState();
				MDC.put("event_name", "Soglie Allarmi");
				logger.log(LoggerCustomLevel.AUDIT, "Modifica allarme Ohm Massima " + item.getSensore().getId_sonda() + " - " + Double.toString(max));
			}

			fireTableCellUpdated(rowIndex, columnIndex);


		}


	}



}



