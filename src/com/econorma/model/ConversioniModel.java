package com.econorma.model;

import java.awt.EventQueue;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.io.Receiver.ReadListener;
import com.econorma.logic.SensoreManager;
import com.econorma.persistence.DAO;
import com.econorma.util.Logger;
import com.econorma.util.StringUtils;

public class ConversioniModel extends AbstractTableModel implements ReadListener {

	private static final long serialVersionUID = 1750252628241166572L;

	private static final Logger logger = Logger.getLogger(ConversioniModel.class);
	private static final String TAG = ConversioniModel.class.getSimpleName();

	public static final NumberFormat nf = new DecimalFormat("#####");
	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"HH:mm:ss");

	private static Application app = Application.getInstance();

	public static final String COL_ID_SONDA = "Id Sonda";
	public static final String COL_DESCRIZIONE = "Descrizione       ";
	public static final String COL_UM = "  Um  ";
	public static final String COL_VALORE_MINIMO_SONDA = " Minimo (Sonda)  ";
	public static final String COL_VALORE_MASSIMO_SONDA = " Massimo (Sonda)  ";
	public static final String COL_VALORE_MINIMO_UM = " Minimo (Um)  ";
	public static final String COL_VALORE_MASSIMO_UM = " Massimo (Um)  ";


	private static final int POS_ID_SONDA = 0;

	private static final int POS_DESCRIZIONE = 1;

	private static final int POS_UM = 2;

	private static final int POS_VALORE_MINIMO_SONDA = 3;

	private static final int POS_VALORE_MASSIMO_SONDA = 4;

	private static final int POS_VALORE_MINIMO_UM = 5;

	private static final int POS_VALORE_MASSIMO_UM = 6;
 

	private static final String[] COLUMN_NAMES = new String[] { COL_ID_SONDA,
		COL_DESCRIZIONE, COL_UM, COL_VALORE_MINIMO_SONDA, COL_VALORE_MASSIMO_SONDA, COL_VALORE_MINIMO_UM, COL_VALORE_MASSIMO_UM };

	 
	private List<Lettura> ultimeLetture = new ArrayList<Lettura>();

	private Sensore.Location where;
	private DAO dao;

	private boolean editMode;

	public ConversioniModel(Sensore.Location where, List<Lettura> letture ) {
		this.where = where;
		if(letture!=null && !letture.isEmpty()){
			for(Lettura l:letture){
				add(l);
			}
		}
	}
 
	@Override
	public int getColumnCount() {

		switch (app.getTipoMisure()) {
		 
		case MISTA:	
			return COLUMN_NAMES.length;

		default:
			return COLUMN_NAMES.length;
		}


	}

	@Override
	public int getRowCount() {
		return ultimeLetture.size();
	}


 

	@Override
	public Object getValueAt(int row, int column) {
		Lettura lettura = ultimeLetture.get(row);

		Sensore sensore = lettura.getSensore();


		switch (app.getTipoMisure()) {
		 
		case MISTA:

			switch (column) { 
			case POS_ID_SONDA:
				return lettura.getIdSonda();
			case POS_UM:
				
			case POS_VALORE_MINIMO_SONDA:
			
			case POS_VALORE_MASSIMO_SONDA:
			
			case POS_VALORE_MINIMO_UM:
			
			case POS_VALORE_MASSIMO_UM:
			
			case POS_DESCRIZIONE:
				return lettura.getSensore().getDescrizione();
			
			default:
				logger.error(TAG, "case : Ricevuto un valore sconosicuto: "+sensore+"  pos:"+column);

			}
		default:
			return null;

		}

	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {


		switch (app.getTipoMisure()) {
		case MISTA:


			switch (columnIndex) {
			case POS_DESCRIZIONE:
				return String.class;
			case POS_UM:
				return String.class;
		 	case POS_VALORE_MINIMO_SONDA:
				return String.class;
			case POS_VALORE_MASSIMO_SONDA:
				return String.class;
			case POS_VALORE_MINIMO_UM:
				return String.class;
			case POS_VALORE_MASSIMO_UM:
				return String.class;
			default:
				break;
			}

		 

		default:
			break;

		}

		return super.getColumnClass(columnIndex);
	}

	public Lettura getItem(int row) {
		if (ultimeLetture != null)
			return ultimeLetture.get(row);
		return null;
	}



	@Override
	public String getColumnName(int pos) {

		switch (app.getTipoMisure()) {
		 
		case MISTA:
			return COLUMN_NAMES[pos];

		default:
			return COLUMN_NAMES[pos];
		}
 
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		if (Application.getInstance().isConfigMode() && (col == POS_UM || col == POS_VALORE_MINIMO_SONDA || col == POS_VALORE_MASSIMO_SONDA || col==POS_VALORE_MINIMO_UM || col==POS_VALORE_MASSIMO_UM))
			return true;
		else
			return false;
	}

	@Override
	public void onRead(final Lettura lettura) {


		Sensore sensore = lettura.getSensore();
		Sensore.Location whereLettura;
		if (sensore == null)
			whereLettura = Sensore.Location.UNKNOWN;
		else {
			switch (sensore.getLocation()) {
			case ESTERNO:
				whereLettura = Location.ESTERNO;
				break;
			case INTERNO:
				whereLettura = Location.INTERNO;
				break;
			case UNKNOWN:
				whereLettura = Location.UNKNOWN;
				break;
			case VIRTUAL:
				whereLettura = Location.VIRTUAL;
				break;
			default:
				throw new RuntimeException("caso non gestito");
			}
		}

		if (whereLettura != where)
			return;

		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				add(lettura);
			}
		});
	}

	public void add(Lettura lettura) {
		int idx = -1;
		for (int i = 0; i < ultimeLetture.size(); i++) {
			Lettura ll = ultimeLetture.get(i);
			if (StringUtils.equals(lettura.getIdSonda(), ll.getIdSonda())) {
				idx = i;
				break;
			}
		}

		if (idx >= 0) {
			ultimeLetture.set(idx, lettura);
			fireTableRowsUpdated(idx, idx);
		} else
			ultimeLetture.add(lettura);

		fireTableDataChanged();
	}

	public void remove(Sensore sensore) {
		if (ultimeLetture != null) {
			for (int i = 0; i < ultimeLetture.size(); i++) {
				Lettura l = ultimeLetture.get(i);
				if (l == null)
					continue;
				if (StringUtils.equals(l.getIdSonda(), sensore.getId_sonda())) {
					ultimeLetture.remove(i);
					fireTableRowsDeleted(i, i);
				}
			}
		}
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		boolean changed = isEditMode() != editMode;
		this.editMode = editMode;
		if (changed) {
			if (isEditMode()) {
			} else {

			}
		}
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		Lettura item = getItem(rowIndex);

		switch (app.getTipoMisure()) {
		case MISTA:


			switch (columnIndex) {
			
			case POS_UM:
				if (value != null && !(value.equals(""))) {

					String um=((String) value);

			 		
					SensoreManager.getInstance().saveState();
				}

				fireTableCellUpdated(rowIndex, columnIndex);

				break;
			 

			case POS_VALORE_MINIMO_SONDA:
				if (value != null && !(value.equals(""))) {


					String minStr=((String) value);
					minStr=minStr.replaceAll(",", ".");
					double min=Double.parseDouble(minStr);

					
					SensoreManager.getInstance().saveState();
				}

				fireTableCellUpdated(rowIndex, columnIndex);

				break;
				
			case POS_VALORE_MASSIMO_SONDA:
				if (value != null  && !(value.equals(""))) {


					String maxStr=((String) value);
					maxStr=maxStr.replaceAll(",", ".");
					double max=Double.parseDouble(maxStr);

					
					SensoreManager.getInstance().saveState();
				}

				fireTableCellUpdated(rowIndex, columnIndex);

				break;
				
				
			case POS_VALORE_MINIMO_UM:
				if (value != null && !(value.equals(""))) {


					String minStr=((String) value);
					minStr=minStr.replaceAll(",", ".");
					double min=Double.parseDouble(minStr);

					
					SensoreManager.getInstance().saveState();
				}

				fireTableCellUpdated(rowIndex, columnIndex);

				break;
			case POS_VALORE_MASSIMO_UM:
				if (value != null  && !(value.equals(""))) {


					String maxStr=((String) value);
					maxStr=maxStr.replaceAll(",", ".");
					double max=Double.parseDouble(maxStr);


					SensoreManager.getInstance().saveState();
				}

				fireTableCellUpdated(rowIndex, columnIndex);

				break;

			default:
				break;
			}


		 

			default:
				break;
			}


		}


 



}
