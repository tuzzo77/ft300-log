package com.econorma.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.LetturaProva;
import com.econorma.data.Sensore;
import com.econorma.logic.SensoreManager;
import com.econorma.ui.Events.ExportEvent;

public class DataModel extends AbstractTableModel {

	private static final long serialVersionUID = 1750252628241166572L;
	private static Application app = Application.getInstance();

	public static final NumberFormat nf = new DecimalFormat("#####.##");
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

	public static final String COL_DATA = Application.getInstance().getBundle().getString("dataModel.data");
	public static final String COL_ID_SONDA = Application.getInstance().getBundle().getString("dataModel.idSonda");
	public static final String COL_DESCRIZIONE = Application.getInstance().getBundle().getString("dataModel.descrizione");
	public static final String COL_TEMP = " C°   ";
	public static final String COL_UMI = " UR%   ";
	public static final String COL_OFFSET = " Offset ";
	public static final String COL_OFFSET_URT = " Offset UR% ";
	public static final String COL_OFFSET_VOLT = " Offset Volt ";
	public static final String COL_OFFSET_MILLIAMPERE = " Offset mA ";
	public static final String COL_OFFSET_OHM = " Offset Pt100 ";
	public static final String COL_VOLT = " Volt ";
	public static final String COL_MILLIAMPERE = " mA ";
	public static final String COL_OHM = " Pt100 ";
	public static final String COL_APERTO_CHIUSO = " Aperto/Chiuso ";
 
	public static final int POS_ID_SONDA = 0;
	public static final int POS_DESCRIZIONE = 1;
	public static final int POS_DATA = 2;
	
	public static int POS_TEMPERATURA;
	public static int POS_OFFSET;
	public static int POS_UMIDITA;
	public static int POS_OFFSET_URT;
	public static int POS_VOLT;
	public static int POS_OFFSET_VOLT;
	public static int POS_MILLIAMPERE;
	public static int POS_OFFSET_MILLIAMPERE;
	public static int POS_OHM;
	public static int POS_OFFSET_OHM;
	public static int POS_APERTO_CHIUSO;


	private static final List<String> COLUMN_NAMES = new ArrayList<String>();

	private List<LetturaProva> singole=new ArrayList<LetturaProva>();

	private String id_sonda;
	private Date Da;
	private Date A;
	private boolean onlyInAlarm;
	private boolean onlyMedie;
	private int minutes;


	public DataModel() {
		createColumns();
		EventBusService.subscribe(this);
	}

	public void createColumns() {
		boolean isTemperature = false;
		COLUMN_NAMES.clear();
		COLUMN_NAMES.add(COL_ID_SONDA);
		COLUMN_NAMES.add(COL_DESCRIZIONE);
		COLUMN_NAMES.add(COL_DATA);
		
		if (Application.getInstance().isTemperature()) {
			COLUMN_NAMES.add(COL_TEMP);
			POS_TEMPERATURA = COLUMN_NAMES.indexOf(COL_TEMP);
			COLUMN_NAMES.add(COL_OFFSET);
			POS_OFFSET = COLUMN_NAMES.indexOf(COL_OFFSET);
			isTemperature = true;
		}
		if (Application.getInstance().isTemperatureHumidity()) {
			if (!isTemperature) {
				COLUMN_NAMES.add(COL_TEMP);
				POS_TEMPERATURA = COLUMN_NAMES.indexOf(COL_TEMP);
				COLUMN_NAMES.add(COL_OFFSET);
				POS_OFFSET = COLUMN_NAMES.indexOf(COL_OFFSET);
			}
			COLUMN_NAMES.add(COL_UMI);
			POS_UMIDITA = COLUMN_NAMES.indexOf(COL_UMI);
			COLUMN_NAMES.add(COL_OFFSET_URT);
			POS_OFFSET_URT = COLUMN_NAMES.indexOf(COL_OFFSET_URT);
		}
		if (Application.getInstance().isVolt()) {
			COLUMN_NAMES.add(COL_VOLT);
			POS_VOLT = COLUMN_NAMES.indexOf(COL_VOLT);
			COLUMN_NAMES.add(COL_OFFSET_VOLT);
			POS_OFFSET_VOLT = COLUMN_NAMES.indexOf(COL_OFFSET_VOLT);
		}
		if (Application.getInstance().isMilliampere()) {
			COLUMN_NAMES.add(COL_MILLIAMPERE);
			POS_MILLIAMPERE = COLUMN_NAMES.indexOf(COL_MILLIAMPERE);
			COLUMN_NAMES.add(COL_OFFSET_MILLIAMPERE);
			POS_OFFSET_MILLIAMPERE = COLUMN_NAMES.indexOf(COL_OFFSET_MILLIAMPERE);
		}
		if (Application.getInstance().isOhm()) {
			COLUMN_NAMES.add(COL_OHM);
			POS_OHM = COLUMN_NAMES.indexOf(COL_OHM);
			COLUMN_NAMES.add(COL_OFFSET_OHM);
			POS_OFFSET_OHM = COLUMN_NAMES.indexOf(COL_OFFSET_OHM);
		}
		if (Application.getInstance().isApertoChiuso()) {
			COLUMN_NAMES.add(COL_APERTO_CHIUSO);
			POS_APERTO_CHIUSO = COLUMN_NAMES.indexOf(COL_APERTO_CHIUSO);
		}

	}


	@Override
	public int getColumnCount() {
		return COLUMN_NAMES.size();
	}
	

	@Override
	public int getRowCount() {

		if(singole.isEmpty()){
			singole.clear();  

			List<LetturaProva> letturaProva=new ArrayList<LetturaProva>();

			if (!onlyMedie){
				letturaProva = Application.getInstance().getDao().findLettureProvaBySonda(id_sonda, Da, A, onlyInAlarm);
			} else {
				letturaProva = Application.getInstance().getDao().findMedieProvaBySonda(id_sonda, Da, A, onlyInAlarm, minutes);
			}

			singole.addAll(letturaProva);

		}


		return singole.size(); 
	}



	@Override
	public Object getValueAt(int row, int column) {
		LetturaProva letturaProva = singole.get(row);

		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());

		if (column==POS_ID_SONDA) {
			return letturaProva.getId_sonda();
		} 
		if (column==POS_DATA) {
			return sdf.format(letturaProva.getData());	
		} 
		if (column==POS_DESCRIZIONE) {
			if (sensore==null){
				return "";
			} else{ 
				return sensore.getDescrizione();
			} 
		}
		if (column==POS_TEMPERATURA) {
			double valore = letturaProva.getValore();
			return nf.format(valore);
		}
		if (column==POS_UMIDITA) {
			double umidita = letturaProva.getUmidita();
			return nf.format(umidita);
		}
		if (column==POS_OFFSET) {
			double offset;
			if (letturaProva.getOffset() != null){
				offset = letturaProva.getOffset();
			} else {
				offset = letturaProva.getValore()-letturaProva.getValore_grezzo();
			}
			if (offset!=0.0)
				return nf.format(offset);
			else
				return "";
		}

		if (column==POS_OFFSET_URT) {
			double offsetURT;
			if (letturaProva.getOffsetUrt() != null){
				offsetURT = letturaProva.getOffsetUrt();
			} else {
				offsetURT = letturaProva.getUmidita()-letturaProva.getUmidita_grezzo();
			}
			if (offsetURT!=0.0)
				return nf.format(offsetURT);
			else
				return "";
		}

		if (column==POS_VOLT) {
			double volt = letturaProva.getVolt();
			return nf.format(volt);
		}
		if (column==POS_MILLIAMPERE) {
			double milliampere = letturaProva.getMilliampere();
			return nf.format(milliampere);
		}
		if (column==POS_OHM) {
			double ohm = letturaProva.getOhm();
			return nf.format(ohm);
		}
		if (column==POS_APERTO_CHIUSO) {
			return letturaProva.getApertoChiuso();
		}

		if (column==POS_OFFSET_VOLT) {
			double offsetURT;
			if (letturaProva.getOffsetUrt() != null){
				offsetURT = letturaProva.getOffsetUrt();
			} else {
				offsetURT = letturaProva.getUmidita()-letturaProva.getUmidita_grezzo();
			}
			if (offsetURT!=0.0)
				return nf.format(offsetURT);
			else
				return "";
		}
		if (column==POS_OFFSET_MILLIAMPERE) {
			double offsetURT;
			if (letturaProva.getOffsetUrt() != null){
				offsetURT = letturaProva.getOffsetUrt();
			} else {
				offsetURT = letturaProva.getUmidita()-letturaProva.getUmidita_grezzo();
			}
			if (offsetURT!=0.0)
				return nf.format(offsetURT);
			else
				return "";
		}
		if (column==POS_OFFSET_OHM) {
			double offsetURT;
			if (letturaProva.getOffsetUrt() != null){
				offsetURT = letturaProva.getOffsetUrt();
			} else {
				offsetURT = letturaProva.getUmidita()-letturaProva.getUmidita_grezzo();
			}
			if (offsetURT!=0.0)
				return nf.format(offsetURT);
			else
				return "";
		}
		
		
		return null;
		

	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex==POS_DESCRIZIONE) {
			return String.class; 
		}
		if (columnIndex==POS_DATA) {
			return String.class;
		}
		if (columnIndex==POS_TEMPERATURA) {
			return String.class;
		}
		if (columnIndex==POS_UMIDITA) {
			return String.class; 
		}
		if (columnIndex==POS_OFFSET) {
			return String.class;
		}

		if (columnIndex==POS_OFFSET_URT) {
			return String.class;
		}

		if (columnIndex==POS_VOLT) {
			return String.class; 
		}
		if (columnIndex==POS_MILLIAMPERE) {
			return String.class; 
		}
		if (columnIndex==POS_OHM) {
			return String.class; 
		}
		if (columnIndex==POS_APERTO_CHIUSO) {
			return String.class;
		}

		if (columnIndex==POS_OFFSET_VOLT) {
			return String.class;
		}
		if (columnIndex==POS_OFFSET_MILLIAMPERE) {
			return String.class;
		}
		if (columnIndex==POS_OFFSET_OHM) {
			return String.class;
		}

		return super.getColumnClass(columnIndex);
 
	}

	public LetturaProva getItem(int row) {
		if (singole != null)
			return singole.get(row);
		return null;
	}



	@Override
	public String getColumnName(int pos) {
		return COLUMN_NAMES.get(pos);
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return false;
	}



	public void add(LetturaProva letturaProva) {

		singole.add(letturaProva);

		fireTableDataChanged();
	}


	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {

	}



	@EventHandler
	public void handleEvent(final ExportEvent exportEvent){


		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				id_sonda = exportEvent.id_sonda;
				Da = exportEvent.Da;
				A = exportEvent.A;
				onlyInAlarm = exportEvent.onlyInAlarm;
				onlyMedie = exportEvent.onlyMedie;
				minutes = exportEvent.minutes;
				singole.clear();
				getRowCount();
				fireTableDataChanged();

			}
		});


	}


}

