package com.econorma.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Log;
import com.econorma.ui.Events.LogEvent;

public class LogModel extends AbstractTableModel {

	private static final long serialVersionUID = 1750252628241166572L;
	private static Application app = Application.getInstance();

	private static final SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
	private static final SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");

	public static final String COL_DATA = Application.getInstance().getBundle().getString("logModel.data");
	public static final String COL_ORA = Application.getInstance().getBundle().getString("logModel.ora");
	public static final String COL_HOST = Application.getInstance().getBundle().getString("logModel.host");
	public static final String COL_USER = Application.getInstance().getBundle().getString("logModel.utente");
	public static final String COL_NAME = Application.getInstance().getBundle().getString("logModel.tipo");
	public static final String COL_MESSAGGE = Application.getInstance().getBundle().getString("logModel.messaggio");
	public static final String COL_CLASS = Application.getInstance().getBundle().getString("logModel.pgm");;
	 

	public static final int POS_DATA = 0;
	public static final int POS_ORA = 1;
	public static final int POS_HOST = 2;
	public static final int POS_USER = 3;
	public static final int POS_NAME = 4;
	public static final int POS_MESSAGGE = 5;
	public static final int POS_CLASS = 6;

 


	private static final String[] COLUMN_NAMES = new String[] {COL_DATA,
		COL_ORA, COL_HOST, COL_USER, COL_NAME, COL_MESSAGGE, COL_CLASS };
 


	private List<Log> log=new ArrayList<Log>();

 
	private Date Da;
	private Date A;



	public LogModel() {

		EventBusService.subscribe(this);
	}



	@Override
	public int getColumnCount() {
 		return COLUMN_NAMES.length;
	 }

	@Override
	public int getRowCount() {

		if(log.isEmpty()){
			log.clear();  

			List<Log> logLista=new ArrayList<Log>();

			logLista = Application.getInstance().getDao().findLogByDate(Da, A);


	 		log.addAll(logLista);

		}


		return log.size(); 
	}



	@Override
	public Object getValueAt(int row, int column) {
		
		Log logDati = log.get(row);
 
				
				switch (column) {
				case POS_DATA:
					return sdfDate.format(logDati.getData());
				case POS_ORA:
					return sdfTime.format(logDati.getData());
				case POS_USER:
					return logDati.getUser();
				case POS_HOST:
					return logDati.getHost();
				case POS_NAME:
					return logDati.getType();
				case POS_MESSAGGE:
					return logDati.getMessagge();
				case POS_CLASS:
					 return logDati.getJavaClass();
				default:
					throw new RuntimeException("Campo sconosciuto");
				}
		 
 
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {

 

			switch (columnIndex) {
			case POS_HOST:
				return String.class;
			case POS_NAME:
				return String.class;
			case POS_MESSAGGE:
				return String.class;
			case POS_DATA:
				return String.class;
			case POS_ORA:
				return String.class;
			case POS_CLASS:
				return String.class;
		 	default:
				break;
			}
			 
  
		return super.getColumnClass(columnIndex);
	}

	
	
	public Log getItem(int row) {
		if (log != null)
			return log.get(row);
		return null;
	}



	@Override
	public String getColumnName(int pos) {
		 		return COLUMN_NAMES[pos];
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return false;
	}



	public void add(Log logData) {

		log.add(logData);

		fireTableDataChanged();
	}


	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {

	}



	@EventHandler
	public void handleEvent(final LogEvent logEvent){


		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				Da = logEvent.Da;
				A = logEvent.A;
				log.clear();
				getRowCount();
				fireTableDataChanged();

			}
		});


	}
	
 


}

