package com.econorma.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.Prova;
import com.econorma.logic.SensoreManager;
import com.econorma.persistence.DAO;
import com.econorma.resources.Testo;
import com.econorma.ui.Events;
import com.econorma.util.Logger;

public class ProveModel extends AbstractTableModel{

	private static final Logger logger = Logger.getLogger(ProveModel.class);

	private static final String TAG = ProveModel.class.getCanonicalName();

	private static final String C_ID = Application.getInstance().getBundle().getString("proveModel.idProva");
	private static final String C_ID_SONDA= Application.getInstance().getBundle().getString("proveModel.idSonda");
	private static final String C_DATA_SCARICO= Application.getInstance().getBundle().getString("proveModel.dataScarico");
	private static final String C_RESPONSABILE = Application.getInstance().getBundle().getString("proveModel.responsabile");
	private static final String C_DATA_PROVA = Application.getInstance().getBundle().getString("proveModel.inizioProva");
	private static final String C_DATA_FINE_PROVA = Application.getInstance().getBundle().getString("proveModel.fineProva");
	public static final String C_EXEC_ESPORTA_EXCEL = Application.getInstance().getBundle().getString("proveModel.excel");
	public static final String C_EXEC_REPORT = Application.getInstance().getBundle().getString("proveModel.report");
	public static final String C_EXEC_CANCELLA = Application.getInstance().getBundle().getString("proveModel.cancella");
	public static final String C_EXEC_DETTAGLI = Application.getInstance().getBundle().getString("proveModel.dettagli");
	public static final String C_EXEC_EXPORT = Application.getInstance().getBundle().getString("proveModel.export");


	public static final int P_ID = 0;
	public static final int P_SONDA = 1;
	public static final int P_DATA_SCARICO = 2;
	public static final int P_RESPONSABILE = 3;
	public static final int P_DATA_PROVA = 4;
	public static final int P_DATA_FINE_PROVA =5;
	public static final int P_EXEC_DETTAGLI = 6;
	public static final int P_EXEC_ESPORTA_EXCEL = 7;
	public static final int P_EXEC_REPORT = 8;
	public static final int P_EXEC_CANCELLA = 9;
	public static final int P_EXEC_EXPORT = 10;

	private final DAO dao;

	private final List<Prova> prove = new ArrayList<Prova>();

	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - HH:mm");

	public ProveModel(DAO dao){
		this.dao = dao;
		EventBusService.subscribe(this);
	}

	@Override
	public int getColumnCount() {
		return 11;
	}

	@Override
	public String getColumnName(int col) {
		switch (col) {
		case P_ID:
			return	C_ID;
		case P_SONDA:
			return C_ID_SONDA;
		case P_DATA_SCARICO:
			return C_DATA_SCARICO;
		case P_RESPONSABILE:
			return C_RESPONSABILE;
		case P_DATA_PROVA:
			return C_DATA_PROVA;
		case P_DATA_FINE_PROVA:
			return C_DATA_FINE_PROVA;
		case P_EXEC_ESPORTA_EXCEL:
			return C_EXEC_ESPORTA_EXCEL;
		case P_EXEC_REPORT:
			return C_EXEC_REPORT;
		case P_EXEC_CANCELLA:
			return C_EXEC_CANCELLA;
		case P_EXEC_DETTAGLI:
			return C_EXEC_DETTAGLI;
		case P_EXEC_EXPORT:
			return C_EXEC_EXPORT;

		default:
			throw new IllegalArgumentException("campo sconosciuto");
		}
	}

	@Override
	public int getRowCount() {
		if(prove.isEmpty()){
			prove.clear();
			List<Prova> findAllProve = dao.findAllProve();
			prove.addAll(findAllProve);
		}
		return prove.size();
	}

	@Override
	public Object getValueAt(int row, int col) {

		Prova prova = prove.get(row);
		switch (col) {
		case P_ID:
			return prova.getId();
		case P_DATA_SCARICO:
			return sdf.format(prova.getData_scarico());
		case P_DATA_PROVA:
			return sdf.format(prova.getData_inizio());
		case P_DATA_FINE_PROVA:
			return sdf.format(prova.getData_fine());

		case P_RESPONSABILE:
			return prova.getResponsabile();

		case P_SONDA:
			return prova.getId_sensore();

		case P_EXEC_ESPORTA_EXCEL:
			return Application.getInstance().getBundle().getString("proveModel.salva");
		case P_EXEC_REPORT:
			return Application.getInstance().getBundle().getString("proveModel.stampa");
		case P_EXEC_CANCELLA:
			return Application.getInstance().getBundle().getString("proveModel.cancella");
		case P_EXEC_EXPORT:
			return Application.getInstance().getBundle().getString("proveModel.export");
		case P_EXEC_DETTAGLI:
			return Application.getInstance().getBundle().getString("proveModel.dettagli");
		default:
			break;
		}
		return null;
	}

	public Prova getProva(int row){
		return prove.get(row);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if(columnIndex == P_EXEC_ESPORTA_EXCEL || columnIndex == P_EXEC_REPORT || columnIndex == P_EXEC_DETTAGLI || columnIndex == P_EXEC_CANCELLA || columnIndex == P_EXEC_EXPORT || columnIndex == P_RESPONSABILE)
			return true;
		return false;
	}

	@EventHandler
	public void handleEvent(final Events.DischargeEvent dischargeEvent){
		switch(dischargeEvent.action){
		case RUNNING:
			break;
		case COMPLETE:
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					if(Testo.LOGGER_AUTOMATICO.equals(dischargeEvent.type)){
						refresh(); 
					}else if (Testo.LOGGER_MANUALE.equals(dischargeEvent.type)){
						refresh();  
					}		 			
				}
			});
		}
	}

	public void refresh() {
		prove.clear();
		fireTableDataChanged();
	}


	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		Prova prova = getItem(rowIndex);
		switch (columnIndex) {
		case P_RESPONSABILE:
			prova.setResponsabile((String) value);
			SensoreManager.getInstance().saveNote(prova, false);
			break;

		case P_DATA_PROVA:
			String newDate = (String) value;
			try {
				prova.setData_inizio(sdf.parse(newDate));
				prova.setOra_inizio(sdf.parse(newDate));
				prova.setOra_fine(sdf.parse(newDate));
			} catch (Exception e) {
				logger.error(TAG, "Data inizio prova formato errato");
			}
			;
			SensoreManager.getInstance().saveNote(prova, true);
			break;
		default:
			break;
		}
	}


	@EventHandler
	public void handleEvent(String event) {
		if (event == Events.CANCEL_PROVA) {
			prove.clear();
			fireTableDataChanged();
		}
	} 

	public Prova getItem(int row) {
		if (prove != null)
			return prove.get(row);
		return null;
	}


}
