package com.econorma.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.Application;
import com.econorma.data.User;
import com.econorma.ui.Events.UserEvent;

public class UserModel extends AbstractTableModel {

	private static final long serialVersionUID = 1750252628241166572L;
	private static Application app = Application.getInstance();
 
	public static final String COL_USER= " Nome ";
	public static final String COL_USER_COMPLETE = " Nome Completo ";
	 
 
	public static final int POS_USER = 0;
	public static final int POS_USER_COMPLETE = 1;
	 

	private static final String[] COLUMN_NAMES = new String[] {COL_USER,
		COL_USER_COMPLETE};
 


	private List<User> user=new ArrayList<User>();

  

	public UserModel() {

		EventBusService.subscribe(this);
	}



	@Override
	public int getColumnCount() {
 		return COLUMN_NAMES.length;
	 }

	@Override
	public int getRowCount() {

		if(user.isEmpty()){
			user.clear();  

			List<User> userLista=new ArrayList<User>();

			userLista = Application.getInstance().getDao().findAllUser();


	 		user.addAll(userLista);

		}


		return user.size(); 
	}



	@Override
	public Object getValueAt(int row, int column) {
		
		User userDati = user.get(row);
 
				
				switch (column) {
				case POS_USER:
					return userDati.getUser();
				case POS_USER_COMPLETE:
					return userDati.getUser_complete();
			 	default:
					throw new RuntimeException("Campo sconosciuto");
				}
		 
 
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {

 

			switch (columnIndex) {
			case POS_USER:
				return String.class;
			case POS_USER_COMPLETE:
			default:
				break;
			}
			 
  
		return super.getColumnClass(columnIndex);
	}

	
	
	public User getItem(int row) {
		if (user != null)
			return user.get(row);
		return null;
	}



	@Override
	public String getColumnName(int pos) {
		 		return COLUMN_NAMES[pos];
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return false;
	}



	public void add(User userData) {

		user.add(userData);

		fireTableDataChanged();
	}


	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {

	}



	@EventHandler
	public void handleEvent(final UserEvent userEvent){


		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				user.clear();
				getRowCount();
				fireTableDataChanged();

			}
		});


	}
	
 


}

