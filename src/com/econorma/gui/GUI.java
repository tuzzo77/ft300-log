package com.econorma.gui;

import java.util.Date;

import javax.swing.JTextField;

import org.jdesktop.swingx.JXDatePicker;
import org.jfree.chart.JFreeChart;

import com.econorma.ui.EconormaChart;

public class GUI {

	public static final String TARGA = "targa";
	public static final String LOTTO = "lotto";
	public static final String RESPONSABILE = "responsabile";
	public static final String DATA = "data";
	public static final String IDSONDA = "id sonda";
	public static final String DESCRIZIONESONDA = "descrizione sonda";
	public static final String DATAREGISTRAZIONE = "data registrazione";
	
	private ValueProvider<String> targaProvider;
	private ValueProvider<String> lottoProvider;
	private ValueProvider<String> responsabileProvider;
	private ValueProvider<String> laboratorioProvider;
	private ValueProvider<String> numeroProvaProvider;
	private ValueProvider<Date> dataProvider;
	private ValueProvider<String> elapsedTimeProvider;
	private ValueProvider<JFreeChart> chartProvider;
	private ValueProvider<EconormaChart> econormaChartProvider;
	
	private ValueProvider<Date> dataRegistrazioneProvider;
	private ValueProvider<String> idSondaProvider;
	private ValueProvider<String> descrizioneSondaProvider;
	
	public ValueProvider<String> getTargaProvider() {
		return targaProvider;
	}
	
	public ValueProvider<String> getLottoProvider() {
		return lottoProvider;
	}

	public ValueProvider<String> getResponsabileProvider() {
		return responsabileProvider;
	}
	
	public ValueProvider<String> getLaboratorioProvider() {
		return laboratorioProvider;
	}
	
	public ValueProvider<String> getNumeroProvaProvider() {
		return numeroProvaProvider;
	}
	
	public ValueProvider<Date> getDataProvider() {
		return dataProvider;
	}
	
	public ValueProvider<String> getIdSondaProvider() {
		return idSondaProvider;
	}

	public ValueProvider<String> getDescrizioneSondaProvider() {
		return descrizioneSondaProvider;
	}

	public ValueProvider<Date> getDataRegistrazioneProvider() {
		return dataRegistrazioneProvider;
	}
	
	public void registerTargaField(JTextField targaField){
		targaProvider = new ValueStringProvider(targaField);
		
	}
	
	public void registerLottoField(JTextField lottoField){
		lottoProvider= new ValueStringProvider(lottoField);
		
	}
	
	public void registerResponsabileField(JTextField responsabileField){
		responsabileProvider = new ValueStringProvider(responsabileField);
	}
	
	public void registerLaboratorioField(JTextField laboratorioField){
		laboratorioProvider = new ValueStringProvider(laboratorioField);
	}
	
	public void registerNumeroProvaField(JTextField numeroProvaField){
		numeroProvaProvider = new ValueStringProvider(numeroProvaField);
	}
	
	public void registerDataField(JXDatePicker dataField){
		dataProvider = new ValueDateProvider(dataField);
	}
	
	public void registerElaspedTimeField(JTextField elapsedTimeField){
		elapsedTimeProvider = new ValueStringProvider(elapsedTimeField);
	}

	public ValueProvider<String> getElapsedTimeProvider() {
		return elapsedTimeProvider;
	}
	
	public void registerEconormaChartProvider(EconormaChart econormaChart){
		econormaChartProvider = new ValueEconormaChartProvider(econormaChart);
	}

	
	public ValueProvider<EconormaChart> getEconormaChartProvider(){
		return econormaChartProvider;
	}
	
	public void registerIdSondaField(JTextField idSondaField){
		idSondaProvider = new ValueStringProvider(idSondaField);
	}
	
	public void registerDescrizioneSondaField(JTextField descrizioneSondaField){
		descrizioneSondaProvider = new ValueStringProvider(descrizioneSondaField);
	}
	
	public void registerDataRegistrazioneField(JXDatePicker dataRegistrazioneField){
		dataRegistrazioneProvider = new ValueDateProvider(dataRegistrazioneField);
	}
	
 
	
}
