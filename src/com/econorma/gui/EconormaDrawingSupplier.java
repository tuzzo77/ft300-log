package com.econorma.gui;

import java.awt.Color;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;

import org.jfree.chart.ChartColor;
import org.jfree.chart.plot.DefaultDrawingSupplier;
 

public class EconormaDrawingSupplier extends DefaultDrawingSupplier{

    public EconormaDrawingSupplier()
    {
        super(createDefaultPaintArray(), DefaultDrawingSupplier.DEFAULT_FILL_PAINT_SEQUENCE, DefaultDrawingSupplier.DEFAULT_OUTLINE_PAINT_SEQUENCE,
        		DefaultDrawingSupplier.DEFAULT_STROKE_SEQUENCE, 
        		DefaultDrawingSupplier.DEFAULT_OUTLINE_STROKE_SEQUENCE, 
        		DefaultDrawingSupplier.DEFAULT_SHAPE_SEQUENCE);
    }
    
   
    
    public static Paint[] createDefaultPaintArray()
    {
        return (new Paint[] {
            new Color(255, 85, 85),
            ChartColor.DARK_GREEN,
            ChartColor.DARK_BLUE,
            new Color(85, 85, 255),
            ChartColor.VERY_DARK_RED, 
            new Color(85, 255, 85), 
            new Color(85, 255, 255),
            Color.pink,
            Color.gray, 
            ChartColor.DARK_RED, 
            ChartColor.DARK_CYAN, 
            ChartColor.DARK_MAGENTA,
            ChartColor.DARK_CYAN, 
            Color.darkGray, 
            ChartColor.LIGHT_RED, 
            ChartColor.LIGHT_BLUE, 
            ChartColor.LIGHT_GREEN, 
            ChartColor.LIGHT_MAGENTA, 
            ChartColor.LIGHT_CYAN, 
            Color.lightGray, 
            ChartColor.VERY_DARK_RED, 
            ChartColor.VERY_DARK_BLUE, 
            ChartColor.VERY_DARK_GREEN,
            ChartColor.VERY_DARK_YELLOW,
            Color.darkGray, 
            ChartColor.VERY_DARK_MAGENTA,
            ChartColor.VERY_DARK_CYAN, 
            ChartColor.VERY_LIGHT_RED, 
            ChartColor.VERY_LIGHT_BLUE, 
            ChartColor.VERY_LIGHT_GREEN, 
            ChartColor.VERY_LIGHT_YELLOW, 
            ChartColor.VERY_LIGHT_MAGENTA, 
            ChartColor.VERY_LIGHT_CYAN
        });
    }



	@Override
	public Paint getNextPaint() {
		Paint nextPaint = super.getNextPaint();
		return nextPaint;
	}



	@Override
	public Paint getNextOutlinePaint() {
		 Paint nextOutlinePaint = super.getNextOutlinePaint();
		return nextOutlinePaint;
	}



	@Override
	public Stroke getNextStroke() {
		 Stroke nextStroke = super.getNextStroke();
		return nextStroke;
	}



	@Override
	public Stroke getNextOutlineStroke() {
		Stroke nextOutlineStroke = super.getNextOutlineStroke();
		return nextOutlineStroke;
	}



	@Override
	public Shape getNextShape() {
		Shape nextShape = super.getNextShape();
		return nextShape;
	}



	@Override
	public Paint getNextFillPaint() {
		Paint nextFillPaint = super.getNextFillPaint();
		return nextFillPaint;
	}
    
	
    
    
    
    
}
