package com.econorma.gui;

public interface ValueProvider<T> {

	public T getValue();
}
