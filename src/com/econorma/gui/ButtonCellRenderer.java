package com.econorma.gui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

import com.econorma.Application;


/**
 * @version 1.0 11/09/98
 */
public class ButtonCellRenderer extends JButton implements TableCellRenderer {

	public ButtonCellRenderer() {
		setOpaque(true);
	}
 

public Component getTableCellRendererComponent(JTable table, Object value,
		boolean isSelected, boolean hasFocus, int row, int column) {




	if (isSelected) {

		switch (Application.getInstance().getTheme()) {
		case DARK:
			setForeground(new Color(0, 0, 0));
			break;
		case LIGHT:
			setForeground(table.getSelectionForeground());
			setBackground(table.getSelectionBackground());
			break;
		}	


	} else{

		switch (Application.getInstance().getTheme()) {
		case DARK:
			setForeground(new Color(0, 0, 0));
			break;
		case LIGHT:
			setForeground(table.getForeground());
			setBackground(UIManager.getColor("Button.background"));
			break;
		}


	}
	setText( (value ==null) ? "" : value.toString() );
	return this;
}
}
