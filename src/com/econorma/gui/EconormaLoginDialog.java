package com.econorma.gui;

import java.sql.Connection;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.codec.digest.DigestUtils;
import org.jdesktop.swingx.JXLoginDialog;
import org.jdesktop.swingx.auth.LoginListener;
import org.jdesktop.swingx.auth.LoginService;

import com.econorma.Application;
import com.econorma.Application.INTERFACE;
import com.econorma.Application.THEME;
import com.econorma.data.User;
import com.econorma.logic.DatabaseManager;
import com.econorma.persistence.AppConfChanger;
import com.econorma.persistence.AppConfChanger.AppCfg;
import com.econorma.persistence.DAO;
import com.econorma.persistence.DAOUser;
import com.econorma.resources.Testo;
import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;


public class EconormaLoginDialog extends javax.swing.JDialog {

	private static List<User> findAll;
	private static boolean find=false;
	private static boolean findAdmin=false;
	private static String userLogged;
	private static String userType;
	private static String language;
	private static User user;

	public EconormaLoginDialog(){

	}

	public static void show(final AppConfChanger appConfChanger, final ResourceBundle bundle, LoginListener listener){


		LoginService loginService = new LoginService() {

			@Override
			public boolean authenticate(String name, char[] password, String server)
					throws Exception {

				if (findAll!=null) {

					for(User u: findAll){

						String sha = getSHA1(String.valueOf(password));

						char[] pwd = sha.toCharArray(); 
						char[] pwdUser = u.getPassword().toCharArray();
						
						if (name!=null && name.equals(u.getUser())){
							findAdmin = true;
						}
								

						if (name!=null && name.equals(u.getUser()) && password!=null && Arrays.equals(pwdUser, pwd)){
							find=true;
							userType=u.getType();
							language=u.getLanguage();
							user = u;
						}

					}
				}

				if (!findAdmin){
					char pwdAdmin[] = new char[]{'e','c','o','n','o','r','m','a'};

					if(!find && name!=null && name.equals("admin") && password!=null && Arrays.equals(password, pwdAdmin)) {
						find=true;
						userType=Testo.ADMINISTRATOR;
						language=bundle.getString("econormaLoginDialog.lingua");
						user = new User();
						user.setUser("admin");
						user.setPassword(getSHA1("econorma"));
						user.setType(Testo.ADMINISTRATOR);
						user.setLanguage(bundle.getString("econormaLoginDialog.lingua"));
					}
				}

				if (find){
					userLogged = name;
					return true;
				} else {
					userLogged = null;
					return false;
				}


			}

		};

		String tema = appConfChanger.getValue(AppCfg.THEME);
		Application.THEME theme = THEME.valueOf(tema);
		
		String myInterface = appConfChanger.getValue(AppCfg.INTERFACE);
		Application.INTERFACE interfaccia = INTERFACE.valueOf(myInterface);
		
		switch (theme) {
		case LIGHT:
			switch (interfaccia) {
			case OLD:
				try {
					UIManager.setLookAndFeel(UIManager
							.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException e) {
				} catch (InstantiationException e) {
				} catch (IllegalAccessException e) {
				} catch (UnsupportedLookAndFeelException e) {
				}
				break;
			case NEW:
				try {
				    UIManager.setLookAndFeel(new FlatLightLaf());
				} catch( Exception ex ) {
				     
				}
				break;
			}
			
			break;
		case DARK:
			switch (interfaccia) {
			case OLD:
				try {
					UIManager.setLookAndFeel(UIManager
							.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException e) {
				} catch (InstantiationException e) {
				} catch (IllegalAccessException e) {
				} catch (UnsupportedLookAndFeelException e) {
				}
				break;
			case NEW:
				try {
					 UIManager.setLookAndFeel(new FlatDarkLaf());
				} catch( Exception ex ) {
				     
				}
				break;
			}
			
			break;

		default:
			break;
		}
 
		JXLoginDialog jxLoginDialog = new JXLoginDialog(loginService,null,null);
 		jxLoginDialog.setVisible(true);
		jxLoginDialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		loginService.addLoginListener(listener);
		 

		DatabaseManager.getInstance().init();
		Connection connection = null;

		findAll=null;
	
		try{
			connection = DatabaseManager.getInstance().getConnection();
			if (DAOUser.exist(connection)) {
				findAll = DAOUser.findAll(connection);
			} 
		}catch (Exception e) {
		}finally {
			DAO.silentClose(connection);
		}
				
	
	}

	private static String getSHA1(String text){
		return  DigestUtils.shaHex(text);
	}

	public static String getUserLogged() {
		return userLogged;
	}

	public static String getTypeUser() {
		return userType;
	}
	
	public static String getLanguage() {
		return language;
	}

	public static User getUser() {
		return user;
	}
 

}
