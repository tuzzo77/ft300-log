package com.econorma.gui;

import java.util.Date;

import org.jdesktop.swingx.JXDatePicker;

public class ValueDateProvider implements ValueProvider<Date> {

	private final JXDatePicker field;
	
	public ValueDateProvider(JXDatePicker field){
		this.field = field;
	}
	
	@Override
	public Date getValue() {
		return field.getDate();
	}
}
