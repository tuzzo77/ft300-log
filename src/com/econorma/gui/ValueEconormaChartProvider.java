package com.econorma.gui;

 
import com.econorma.ui.EconormaChart;

public class ValueEconormaChartProvider implements ValueProvider<EconormaChart>{

	private EconormaChart chart;
	public ValueEconormaChartProvider(EconormaChart chart){
		this.chart = chart;
	}
	@Override
	public EconormaChart getValue() {
		return chart;
	}

}
