package com.econorma.gui;

import org.jfree.chart.JFreeChart;

public class ValueJFreeChartProvider implements ValueProvider<JFreeChart>{

	private JFreeChart chart;
	public ValueJFreeChartProvider(JFreeChart chart){
		this.chart = chart;
	}
	@Override
	public JFreeChart getValue() {
		return chart;
	}

}
