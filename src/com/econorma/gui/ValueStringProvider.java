package com.econorma.gui;

import javax.swing.JTextField;

public class ValueStringProvider implements ValueProvider<String>{

	private final JTextField field;
	
	public ValueStringProvider(JTextField field){
		this.field = field;
	}
	@Override
	public String getValue() {
		return field.getText();
	}

}
