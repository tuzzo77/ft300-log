package com.econorma;

import java.net.InetAddress;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.log4j.MDC;
import org.jdesktop.swingx.auth.LoginEvent;
import org.jdesktop.swingx.auth.LoginListener;

import com.econorma.gui.EconormaLoginDialog;
import com.econorma.language.Language;
import com.econorma.license.License;
import com.econorma.license.ShowLicenseDialog;
import com.econorma.license.Sinottico;
import com.econorma.logic.SerialUtil;
import com.econorma.persistence.AppConfChanger;
import com.econorma.resources.Testo;
import com.econorma.util.JustOneLock;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;
import com.econorma.util.OS;
 

public class Main {

	private static final String TAG = Main.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(Main.class);
	private static String hostName;
	private static boolean consoleMode;
	private static boolean freeLicense;
	private static boolean freeIstances;

	public static void main(final String[] args) {
		
		Language language = new Language();
		final ResourceBundle bundle = language.getDefault();
		
		AppConfChanger appConfChanger = new AppConfChanger(null);
	     
		//args
		// 0 = console mode | value = console
		// 1 = free license | value = nolic
		// 2 = multiple instances| value = noone
	
		try {
			hostName = InetAddress.getLocalHost().getHostName();
		} catch (Exception e) {
			logger.error(TAG, e);
		}

		MDC.put("hostname", hostName);
		MDC.put("event_name", "Login");


		if (args.length == 0) {
			logger.info(TAG, "No arguments passed");
		} else {
			for (int i = 0; i < args.length; i++) {
				logger.info(TAG, "Arguments passed: " + args[i]);
			}
		}

		JustOneLock ua = new JustOneLock(Main.class.getName());
		License	license = new License();
		
		Sinottico sinottico = new Sinottico();

		Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler());


		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (OS.isWindows()) {

			if (!freeLicense(args)) {
				if (!license.compareLicense()){
					String number = license.getNumberLicense();
					ShowLicenseDialog sd = new ShowLicenseDialog(number);
					logger.error(TAG, bundle.getString("main.licenza"));
					System.exit(1);
				}
			}
			
			

		}


		try{

			String[] serialPorts = SerialUtil.getSerialPorts();
			if (serialPorts == null || serialPorts.length<=0) {
				JOptionPane.showMessageDialog(null, bundle.getString("main.serialPort") ,bundle.getString("main.errore"), JOptionPane.ERROR_MESSAGE);
				logger.error(TAG, bundle.getString("main.serialPort"));
//				System.exit(1);

			}

		}catch (UnsatisfiedLinkError e) {
			JOptionPane.showMessageDialog(null, bundle.getString("main.driversPort") ,bundle.getString("main.errore"), JOptionPane.ERROR_MESSAGE);
			logger.error(TAG, bundle.getString("main.driversPort"));
//			System.exit(1);
		}
 


		if (!freeInstances(args) && ua.isAppActive()) {
			logger.info(TAG, bundle.getString("main.inEsecuzione"));
			JOptionPane.showMessageDialog(null, bundle.getString("main.inEsecuzione"),
					"Econorma", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		} else {

			if (!consoleMode(args)) {

				logger.info(TAG, "No other instance running");
			 
				EconormaLoginDialog.show(appConfChanger, bundle , new LoginListener() {

					@Override
					public void loginSucceeded(LoginEvent arg0) {
						
						MDC.put("hostname", hostName);
						MDC.put("event_name", "Login");
						MDC.put("user", EconormaLoginDialog.getUserLogged());
						MDC.put("language", EconormaLoginDialog.getLanguage());
						logger.log(LoggerCustomLevel.AUDIT, bundle.getString("main.login"));

						Application.launch(Application.class, args);
					}

					@Override
					public void loginFailed(LoginEvent arg0) {

					}

					@Override
					public void loginStarted(LoginEvent arg0) {

					}


					@Override
					public void loginCanceled(LoginEvent arg0) {
						System.exit(0);
					}
				});
			}

			else {
				Application.launch(Application.class, args);
			}
		}


	}


	public static boolean consoleMode(final String[] args){
 		
		if (args.length == 0) {
			consoleMode= false;
		}

		if (args.length>0  && args[0].equals(Testo.CONSOLE.trim())){
			consoleMode= true; 
		}

 		return consoleMode;
	}
	
	public static boolean freeLicense(final String[] args){
 		
		freeLicense= false;
	
		if (args.length>1  && args[1].equals(Testo.FREE_LICENSE.trim())){
			freeLicense= true; 
		}

 		return freeLicense;
	}
	
	public static boolean freeInstances(final String[] args){
 		
		freeIstances= false;
	
		if (args.length>2  && args[2].equals(Testo.FREE_INSTANCES.trim())){
			freeIstances= true; 
		}

 		return freeIstances;
	}
	
	public static boolean getConsoleMode() {
		return consoleMode;
	}

	public static boolean getFreeInstances() {
		return freeIstances;
	}

}
