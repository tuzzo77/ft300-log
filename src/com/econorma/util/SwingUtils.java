package com.econorma.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ColorUIResource;

import org.jdesktop.swingx.util.WindowUtils;

import com.econorma.Application;
import com.econorma.Application.THEME;
import com.econorma.resources.Testo;
import com.econorma.ui.MyTableHeaderUI;
import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;

public class SwingUtils {

	private static final Logger logger = Logger.getLogger(SwingUtils.class);

	public static void showCenteredDialog(JDialog dialog) {
		Window window = WindowUtils.findWindow(Application.getInstance()
				.getMainFrame());
		if (window != null) {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension dialogSize = dialog.getSize();

			if (dialogSize.height > screenSize.height) {
				dialogSize.height = screenSize.height;
			}
			if (dialogSize.width > screenSize.width) {
				dialogSize.width = screenSize.width;
			}

			dialog.setLocation((screenSize.width - dialogSize.width) / 2,
					(screenSize.height - dialogSize.height) / 2);

			if (dialog.getTitle()!=null && dialog.getTitle().equals(Testo.OPZIONI)){
				dialog.setLocation((screenSize.width - dialogSize.width) / 2,
						(int) ((screenSize.height - dialogSize.height) / 5));
			}

			dialog.setVisible(true);
		}
	}


	public static void checkEDT() throws RuntimeException {
		if (!SwingUtilities.isEventDispatchThread()) {
			throw new RuntimeException("Must be called from the main thread");
		}
	}

	public static void integrateWithSystem() {


		if (OS.isMacOsX()) {
			// take the menu bar off the jframe
			System.setProperty("apple.laf.useScreenMenuBar", "true");

			// set the name of the application menu item
			System.setProperty(
					"com.apple.mrj.application.apple.menu.about.name",
					"Econorma");

			//			 set the look and feel
			try {
				UIManager.setLookAndFeel(UIManager
						.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedLookAndFeelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// impostiamo il tema
			// JFrame.setDefaultLookAndFeelDecorated(true);


		}

		switch (Application.getInstance().getTheme()) {
		case LIGHT:
			
		
			switch (Application.getInstance().getInterfaccia()) {
			case OLD:
				try {
					Color color = UIManager.getColor("Panel.background");
					Object default_table_color = UIManager
							.getColor("Table.background");
					UIManager.put("Table.background_default", default_table_color);
					UIManager.put("Table.gridColor", Theme.LIGHT_GRAY);
					UIManager.put("Table.disabled", false);
					UIManager.put("Table.showGrid", true);
					UIManager.put("Table.intercellSpacing", new Dimension(1, 1));
				} catch (Exception e) {

				}
				break;
			case NEW:
				
				try {
				    UIManager.setLookAndFeel(new FlatLightLaf() );
					UIManager.put("Table.gridColor", Theme.LIGHT_GRAY);
					UIManager.put("Table.disabled", false);
					UIManager.put("Table.showGrid", true);
					UIManager.put("Table.intercellSpacing", new Dimension(1, 1));
				} catch( Exception ex ) {
				     
				}
				break;
			}
			
			break;
	
		case DARK:
		 
			switch (Application.getInstance().getInterfaccia()) {
			case OLD:
				try {
					 
					UIManager.put("Table.background", Theme.DARK);
					UIManager.put("Table.foreground", Theme.WHITE);
					UIManager.put("Panel.background", Theme.DARK);
					UIManager.put("Panel.foreground", Theme.DARK);
					UIManager.put("Label.foreground", Theme.WHITE);
					UIManager.put("OptionPane.border", Theme.DARK);
					UIManager.put("OptionPane.messageForeground", Theme.WHITE);
					UIManager.put("RadioButton.foreground", Theme.WHITE);
	 				UIManager.put("CheckBox.foreground", Theme.WHITE);
					UIManager.put("TabbedPane.background", Theme.DARK);
					UIManager.put("SplitPane.background", Theme.GREEN);
					
	 				UIManager.put("MenuItem.background", Theme.DARK);
		            UIManager.put("MenuItem.foreground", Theme.WHITE);
		            UIManager.put("MenuItem.opaque", true);
		            
		            Color background = UIManager.getColor("Button.background").darker();
		            UIManager.put("Button.background", new ColorUIResource(background));
		            UIManager.put("Button.foreground", new ColorUIResource(background));
//		            UIManager.put("ButtonUI", MyButtonUI.class.getName());
		            
		            UIManager.put("Menu.foreground", Color.WHITE);
		            UIManager.put("PopupMenu.border",  BorderFactory.createLineBorder(Color.GRAY, 1));
		            UIManager.put("TableHeaderUI", MyTableHeaderUI.class.getName());
		         	 
		         	UIManager.put("Separator.background", Theme.DARK);
	 	         	UIManager.put("Separator.foreground", Color.GRAY);
	 	         	
	 	         	UIManager.put("OptionPane.border", new EmptyBorder(5, 5, 5, 5));
	 	         	UIManager.put("OptionPane.background", THEME.DARK);
	 	         	
	 				 EventQueue.invokeLater(new Runnable() {
	 			            @Override
	 			            public void run() {
	 			            	UIManager.put("CheckBox.background", Theme.DARK);
	 			 				UIManager.put("CheckBox.foreground", Theme.WHITE);
	 			 				UIManager.put("RadioButton.background", Theme.DARK);
	 			 				UIManager.put("RadioButton.foreground", Theme.WHITE);
	 			 	        }
	 			        });
	 
			
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				break;
			case NEW:
				try {
				    UIManager.setLookAndFeel(new FlatDarkLaf() );
				    UIManager.put("Table.gridColor", Theme.LIGHT_GRAY);
					UIManager.put("Table.disabled", false);
					UIManager.put("Table.showGrid", true);
					UIManager.put("Table.intercellSpacing", new Dimension(1, 1));
					
				} catch( Exception ex ) {
				     
				}
				break;
			}
			
			
			break;
		}
		
	            

		UIManager.getInstalledLookAndFeels();


	}
 
}
