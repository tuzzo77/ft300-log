package com.econorma.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import org.apache.commons.io.IOUtils;

public class JarToClassath {
	
	private static final String TAG = JarToClassath.class.getSimpleName();

	private static final Logger logger = Logger.getLogger(JarToClassath.class);
	
	public static void addJarToClasspath(File jarFile) 
	{ 
	   try 
	   {  
		   
 	       URL url = jarFile.toURI().toURL();
	       URLClassLoader urlClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader(); 
	       Class<?> urlClass = URLClassLoader.class; 
	       Method method = urlClass.getDeclaredMethod("addURL", new Class<?>[] { URL.class }); 
	       method.setAccessible(true);         
	       method.invoke(urlClassLoader, new Object[] { url });
	       
  	       
	       logger.info(TAG, "SWT Libreria caricata " + jarFile.toString());
	       
	   } 
	   catch (Throwable t) 
	   { 
	       t.printStackTrace(); 
	       logger.info(TAG, "SWT error " + t.toString());
	   } 
	}
	
	public static void createSWTJar(){
	
		
//		if (new File("swt").mkdir()) {
//			logger.info(TAG, "Creata cartella swt");
//		}
		
		
		File f = new File("swt_win_32.jar");
		FileInputStream fis=null;
		
		boolean load=false;
		
		
		try {
			if (f.exists()){
				load=true;
			}
			
		} catch (Exception e) {
			

		}finally{
			IOUtils.closeQuietly(fis);
		}
		
		
		if (!load){
			InputStream is=null;
			OutputStream out =null;
			
			try {
			 	
				is = getSWTStream();
				
				 
				    int readBytes;
				    byte[] buffer = new byte[4096];
				    try {
				    	out = new FileOutputStream(f);
				        while ((readBytes = is.read(buffer)) > 0) {
				            out.write(buffer, 0, readBytes);
				        }
				    } catch (IOException e) {
				    	logger.debug(TAG, "InputStream Exception : "+  e.toString());
				    }
		  
				
			} catch (Exception e) {

			} finally{
				IOUtils.closeQuietly(is);
				IOUtils.closeQuietly(out);
			}
		}
	 	        
	}
	
	
	
	private static InputStream getSWTStream(){
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("com/econorma/maps/resources/swt_win_32.jar");
		logger.debug(TAG, "Resource stream : "+ (in!=null));
		return in;
	}
	
}
