package com.econorma.util;

import org.apache.log4j.Level;

import com.econorma.Main;


public class LoggerCustomLevel extends Level 
	{ 
	
		public LoggerCustomLevel(int level, String levelStr, int syslogEquivalent) 
		{ 
			super(level, levelStr, syslogEquivalent); 
		} 
	
	
	
		public static LoggerCustomLevel toLevel(int val, Level defaultLevel) 
		{ 
			return AUDIT; 
		} 
	
		public static LoggerCustomLevel toLevel(String sArg, Level defaultLevel) 
		{ 
	
			return AUDIT; 
	
	
		} 
	
	
	 	public static final LoggerCustomLevel AUDIT = new LoggerCustomLevel(60000, "AUDIT", 0); 
	 
	
	} 
 
