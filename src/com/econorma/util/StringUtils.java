package com.econorma.util;

public class StringUtils {

	public static boolean equals(String s1, String s2) {
		if (s1 == null && s2 == null)
			return true;
		if (s1 != null && s2 != null) {
			return s1.equals(s2);
		}

		return false;
	}

	public static boolean nullOrEmpty(String v) {
		return v == null || v.isEmpty();
	}
	
	public static String nullToEmpty(String value){
		if(value==null)
			return "";
		else 
			return value;
	}
	
	public static boolean isSome(String s){
		return s!=null && s.trim().length()>0; 
	}
}
