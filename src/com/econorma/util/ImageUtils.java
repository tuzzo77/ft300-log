package com.econorma.util;

import java.awt.Image;
import java.awt.image.BufferedImage;

public class ImageUtils {
	
	

	public static Image scale(final Image image, final int maxWidth, final int maxHeight ){
		BufferedImage sourceImage = (BufferedImage) image;
		
		double imgWidth = (double) sourceImage.getWidth();
		double imgHeight = (double) sourceImage.getHeight();
		
		final double ratio = imgWidth/imgHeight;
		
		imgHeight  = maxHeight;
		imgWidth = ratio*imgHeight;
		
		if(imgWidth>maxWidth){
			imgWidth = maxWidth;
			imgHeight = imgWidth/ratio;
		}

		
		int newWidth = (int)Math.floor(imgWidth);
		int newHeight = (int)Math.floor(imgHeight);
		Image thumbnail = sourceImage.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
		
		BufferedImage bufferedThumbnail = new BufferedImage(newWidth,
				newHeight,
                BufferedImage.TYPE_INT_RGB);
bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);
		return bufferedThumbnail;
	}
}
