package com.econorma.util;

import java.awt.Color;


public class Theme {
	
	public static final Color OTHER_DARK = new Color(32, 32, 32);
	public static final Color SECOND_DARK = new Color(32, 32, 32);
	public static final Color DARK = new Color(47, 47, 47);
	public static final Color WHITE = new Color(255, 255, 255);
	public static final Color BLACK = new Color(0, 0, 0);
	public static final Color BLUE = new Color(217, 232, 247);
	public static final Color GREEN = new Color(62, 81, 93);
	public static final Color LIGHT_GRAY = new Color(211, 211, 211);
	public static final Color SMOKE_WHITE = new Color(245, 245, 245);
	public static final Color ANOTHER_WHITE = new Color(220, 220, 220);
	public static final Color MY_GRAY = new Color(90, 90, 90);
	 
}
