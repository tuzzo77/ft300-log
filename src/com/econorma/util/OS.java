package com.econorma.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

//http://www.rgagnon.com/javadetails/java-0150.html
public class OS {
 
	public static boolean isWindows() {
		 
		String os = System.getProperty("os.name");
		return (os!=null && os.toLowerCase().indexOf("win") >= 0);
 
	}
 
	public static boolean isOsInformationAvailable(){
		String os = System.getProperty("os.name");
		if(os!=null && os.length()!=0)
			return true;
		return false;
	}
	
	public static boolean isMacOsX() {
 
		String os = System.getProperty("os.name");
		return (os!=null && os.toLowerCase().indexOf("mac os x") >= 0);
 
	}
 
	public static boolean isLinux() {
 
		String os = System.getProperty("os.name");
		return (os!=null && os.toLowerCase().indexOf("linux") >= 0);
 
	}
 
	public static boolean isSolaris() {
 
		String os = System.getProperty("os.name");
		return (os!=null && os.toLowerCase().indexOf("sunos") >= 0);
 
	}
	
	public static boolean is64bit(){
		if(isWindows()){
			if(System.getenv("ProgramW6432") != null){
				return true;
			}else if(new File("C:\\Program Files (x86)").exists()){
				// use some euristics...
				return true;
			}else{
				Runtime r = Runtime.getRuntime();
				Process p;
				BufferedReader br = null;
				try {
					p = r.exec( "cmd.exe /c set" );
					br = new BufferedReader
						     ( new InputStreamReader( p.getInputStream() ) );
						  String line;
						  while( (line = br.readLine()) != null ) {
						   int idx = line.indexOf( '=' );
						   if(idx>0){
							   String key = line.substring( 0, idx );
							   String value = line.substring( idx+1 );
							   if(key.contains("ProgramFiles(x86)"))
								   return true;
						   }
						   
						   }
				} catch (IOException e) {
					// TODO log
					e.printStackTrace();
					
				}finally{
					if(br!=null){
						try {
							br.close();
						} catch (IOException e) {
						}
					}
				}
				 
			}
			
			//seems to be windows 32-bit
			return false;
		}
		
		//on *nix
		Runtime r = Runtime.getRuntime();
		BufferedReader br = null;
		try {
			Process p = r.exec("getconf LONG_BIT");
			br = null;
			br = new BufferedReader
				     ( new InputStreamReader( p.getInputStream() ) );
				  String line;
				  while( (line = br.readLine()) != null ){
					  if(line.contains("64"))
						  return true;
				  }
		} catch (IOException e) {
			//TODO log
		}finally{
			if(br!=null){
				try {
					br.close();
				} catch (IOException e) {
				}
			}
		}
		
		try {
			Process p = r.exec("uname -m");
			br = null;
			br = new BufferedReader
				     ( new InputStreamReader( p.getInputStream() ) );
				  String line;
				  while( (line = br.readLine()) != null ){
					  if(line.contains("64"))
						  return true;
				  }
		} catch (IOException e) {
		}finally{
			if(br!=null){
				try {
					br.close();
				} catch (IOException e) {
				}
			}
		}
		
		//fallback to vm 
		return isVM86bit();
		
	}
	
	
	public static boolean isVM86bit(){
		String arch = System.getProperty("os.arch");
		return arch!=null && arch.indexOf("64")>0;
	}
	
	public static String archFileName(String prefix){
		
		String osName=null;
		String osArch=null;
		
		if (isWindows()) {
			osName="win";
		} else if
		  (isLinux()) {
			osName="linux";
		}else if
		   (isMacOsX()) {
			osName="mac";
		}
		
		if (OS.isVM86bit()) {
			osArch="64";
		} else {
			osArch="32";
		}
		
		if (osName==null || osArch==null)
			return null;
		
		 return prefix + "_" + osName + "_" + osArch + ".jar"; 
		
	}
	
}
