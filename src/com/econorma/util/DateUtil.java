package com.econorma.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {

	private static final double MINUTE_IN_SECONDS = 60;
	private static final double HOUR_IN_SECONDS = MINUTE_IN_SECONDS * 60; // double
																			// to

	private static final NumberFormat nf = new DecimalFormat("####");

	static {
		nf.setMinimumIntegerDigits(2);
	}

	// avoid
	// casting

	public static String formatElapsedTime(int durationInSeconds) {
		StringBuilder sb = new StringBuilder();
		int hours = (int) (durationInSeconds / HOUR_IN_SECONDS);
		int minutes = (int) ((((double) durationInSeconds) - hours
				* HOUR_IN_SECONDS) / MINUTE_IN_SECONDS);
		int seconds = (int) (((double) durationInSeconds) - hours
				* HOUR_IN_SECONDS - minutes * MINUTE_IN_SECONDS);
		sb.append(nf.format(hours)).append(":").append(nf.format(minutes))
				.append(":").append(nf.format(seconds));
		return sb.toString();
	}
	
	public static int toDelayMillis(Date tempo){
		return 1000 * toDelaySeconds(tempo);

	}
	
	public static int toDelaySeconds(Date tempo){
		if(tempo!=null){
			Calendar instance = Calendar.getInstance();
			instance.setTime(tempo);
			instance.setTimeZone(TimeZone.getTimeZone("UTC"));
			int tempoOra = instance.get(Calendar.HOUR_OF_DAY);
			int tempoMinuti = instance.get(Calendar.MINUTE);
			int tempoSecondi = instance.get(Calendar.SECOND);
			int delay = (tempoSecondi + 60 * (tempoMinuti + 60 * tempoOra));
			return delay;
		}else
			return 0;
	}
	
	public static Date fromDelaySeconds(int timeInSeconds){
		Date date = new Date();
		date.setTime(timeInSeconds * 1000);
		return date;
	}

}
