package com.econorma.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ListWrapper<T> implements List<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	final private List<T> wrapped;
	
	protected List<T> getWrapped(){
		return wrapped;
	}
	public ListWrapper(List<T> letture){
		this.wrapped = letture;
	}
	@Override
	public boolean add(T e) {
		return wrapped.add(e);
	}

	@Override
	public void add(int index, T element) {
		wrapped.add(index, element);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		return wrapped.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		return wrapped.addAll(index, c);
	}

	@Override
	public void clear() {
		wrapped.clear();
	}

	@Override
	public boolean contains(Object o) {
		return wrapped.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return wrapped.containsAll(c);
	}

	@Override
	public T get(int index) {
		return wrapped.get(index);
	}

	@Override
	public int indexOf(Object o) {
		return wrapped.indexOf(o);
	}

	@Override
	public boolean isEmpty() {
		return wrapped.isEmpty();
	}

	@Override
	public Iterator<T> iterator() {
		return wrapped.iterator();
	}

	@Override
	public int lastIndexOf(Object o) {
		return wrapped.lastIndexOf(o);
	}

	@Override
	public ListIterator<T> listIterator() {
		return listIterator();
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		return listIterator(index);
	}

	@Override
	public boolean remove(Object o) {
		return remove(o);
	}

	@Override
	public T remove(int index) {
		return remove(index);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return retainAll(c);
	}

	@Override
	public T set(int index, T element) {
		return set(index, element);
	}

	@Override
	public int size() {
		return size();
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		return subList(fromIndex, toIndex);
	}

	@Override
	public Object[] toArray() {
		return toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return toArray(a);
	}

	
}
