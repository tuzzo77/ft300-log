package com.econorma.db.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.Lettura.TipoMisura;
import com.econorma.data.LetturaProva;
import com.econorma.data.LetturaStorico;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.resources.Testo;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

public class ImportaStorico extends JPanel {

	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	private static final Logger logger = Logger.getLogger(ImportaStorico.class);
	private static final String TAG = "ImportaStorico";

	private List<String> sensori = new ArrayList<String>();
	private List<LetturaStorico> lettureStorico = new ArrayList<LetturaStorico>();
	private String separator = ";";
	private BufferedReader br = null;
	String line = "";

	public ImportaStorico() {

	}


	public void run() {

		String percorso = (new File(".")).getAbsolutePath();
		final JFileChooser jFileChooser = new JFileChooser(percorso);

		final FileNameExtensionFilter filter = new FileNameExtensionFilter("*.csv", "csv");
		jFileChooser.setFileFilter(filter);

		int n = jFileChooser.showOpenDialog(this);
		if (n == jFileChooser.APPROVE_OPTION) {

			File csvFile = new File(jFileChooser.getCurrentDirectory().toString()
					+"/"+jFileChooser.getSelectedFile().getName());

			readCsv(csvFile);
			if (lettureStorico != null) {

				Date dateFrom = lettureStorico.get(0).getData();
				Date dateTo = lettureStorico.get(lettureStorico.size()-1).getData();
				
				Prova prova = Application.getInstance().getDao().findUltimaProva();
				if (prova == null) {
					prova = new Prova();
					prova.setResponsabile(null);
					prova.setData_inizio(new Date());
					prova.setData_fine(new Date());
					prova.setOra_inizio(new Date(System.currentTimeMillis()));
					prova.setOra_fine(new Date(System.currentTimeMillis()));
					prova.setId_sensore(lettureStorico.get(0).getIdSonda());
					prova.setResponsabile(Testo.ADMINISTRATOR);
					Application.getInstance().getDao().insertOrUpdateProva(prova);
				}

				for (String sonda: sensori){
					Application.getInstance().getDao().deleteLettureBySonda(sonda, dateFrom, dateTo);
					Application.getInstance().getDao().deleteLettureProvaBySonda(sonda, dateFrom, dateTo);
				}
				
				for (LetturaStorico ls : lettureStorico){
					Lettura lettura = new Lettura();
					Sensore s = new Sensore(ls.getIdSonda(), Location.INTERNO);
					lettura.setIdSonda(ls.getIdSonda());
					lettura.setSensore(s); 
					lettura.setData(ls.getData());
					lettura.setTemperaturaGrezza(ls.getValore());
					lettura.setUmiditaGrezza(ls.getValore_ur());
					lettura.setTipoMisura(TipoMisura.TEMPERATURA);
					lettura.setStatoBatteria("B");
					Application.getInstance().getDao().insertLettura(lettura);
					LetturaProva letturaProva = new LetturaProva(prova, lettura);
					Application.getInstance().getDao().insertLetturaProva(letturaProva);
				}

				JOptionPane.showMessageDialog(null,"Import da Storico CSV completato correttamente", "Informazione", JOptionPane.INFORMATION_MESSAGE);
				logger.log(LoggerCustomLevel.AUDIT, "Import da Storico CSV completato correttamente");

			} else {

				JOptionPane.showMessageDialog(null,"Import da Storico CSV con Errori", "Errore", JOptionPane.ERROR_MESSAGE);
				logger.log(LoggerCustomLevel.AUDIT, "Import da Storico CSV con Errori");

			}
 
		}


	}

	public void readCsv(File csvFile){

		try {

			br = new BufferedReader(new FileReader(csvFile));
			int i = 0;
			while ((line = br.readLine()) != null) {
				i++;
				if (i==1 || line.trim().length()==0){
					continue;
				}
				String[] columns = line.split(separator);
				String idSonda = columns[0];
				Date data = sdf.parse(columns[1] + " " + columns[2]);
				double valore = Double.parseDouble(columns[3]);
				double valore_ur = Double.parseDouble(columns[4]);
				LetturaStorico ls = new LetturaStorico();
				ls.setIdSonda(idSonda);
				ls.setData(data);		
				ls.setValore(valore);
				ls.setValore_ur(valore_ur);
				lettureStorico.add(ls);
				if (!sensori.contains(idSonda)){
					sensori.add(idSonda);
				}
				logger.debug(TAG,"Data: " +  columns[0] + "|" + columns[1] + "|" + columns[2] + "|" + columns[3]+ "|" + columns[4]);
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.error(TAG, e);
				}
			}
		}

	}


}
