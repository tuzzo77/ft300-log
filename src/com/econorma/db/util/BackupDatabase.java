package com.econorma.db.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;



public class BackupDatabase {
	
	  private static final Logger logger = Logger.getLogger(BackupDatabase.class);
	  private static final String TAG = "BackupDatabase";
	  private static final DateFormat filedateformat=new SimpleDateFormat("_yyyy_MM_dd__HH_mm_ss");
	 


	public BackupDatabase(){
	}

	public void copy() {
		
		if (new File("backup").mkdir()) {
			logger.info(TAG, "Creata cartella backup");
		}
		
		
			
		String backupName = ("backup/database"+filedateformat.format(new Date())+".db");
		
		
		File inputFile = new File("database.db");
		File outputFile = new File(backupName);
		
		InputStream finput = null;
		OutputStream foutput = null;
		
		
		try {
			finput = new BufferedInputStream(new FileInputStream(inputFile));
		} catch (FileNotFoundException e) {
	 		logger.error(TAG, "Errore backup database", e);
		}
		
		
		try {
			foutput = new BufferedOutputStream( new FileOutputStream(outputFile));
		} catch (FileNotFoundException e) {
			logger.error(TAG, "Errore backup database", e);
		}
		
		
		byte[] buffer = new byte[1024 * 500];
		int bytes_letti = 0;
		try {
			while((bytes_letti = finput.read(buffer)) > 0)
				foutput.write(buffer, 0, bytes_letti);
		} catch (IOException e) {
			logger.error(TAG, "Errore backup database", e);
			
		}
		try {
			finput.close();
		} catch (IOException e) {
			logger.error(TAG, "Errore backup database", e);
		}
		try {
			foutput.close();
		} catch (IOException e) {
			logger.error(TAG, "Errore backup database", e);
		}
 		
		
		 JOptionPane.showMessageDialog(null,"Backup Database eseguito correttamente", "Informazione", JOptionPane.INFORMATION_MESSAGE);
		 
		 logger.log(LoggerCustomLevel.AUDIT, "Backup Database");

	}


}
