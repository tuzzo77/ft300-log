package com.econorma.db.util;

 

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JOptionPane;

import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

public class CompressDatabase {

	 private static final Logger logger = Logger.getLogger(CompressDatabase.class);
	 private static final String TAG = "CompressDatabase";
	 private static final String zipFile = "backup/database.zip";
	 private static final String dbFile = "database.db";
	
	public CompressDatabase(){
		
	}
	
	 
	
	public void compress()  {
	    try
        {
	    		if (new File("backup").mkdir()) {
	    			logger.info(TAG, "Creata cartella backup");
	    		}
               
                
                byte[] buffer = new byte[1024];
                
                 FileOutputStream fout = new FileOutputStream(zipFile);
                 
                
                 ZipOutputStream zout = new ZipOutputStream(fout);
                 
                
                 FileInputStream fin = new FileInputStream(dbFile);
                
                 zout.putNextEntry(new ZipEntry(dbFile));
                 
                 int length;
                 
                 while((length = fin.read(buffer)) > 0)
                 {
                        zout.write(buffer, 0, length);
                 }
                 
                
                  zout.closeEntry();
                 
                
                  fin.close();
                 
                
                  zout.close();
                  
                  logger.log(LoggerCustomLevel.AUDIT, "Compressione Database");
                 
//                  JOptionPane.showMessageDialog(null,"Compressione eseguita correttamente",
//  		  			 	"Informazione", JOptionPane.INFORMATION_MESSAGE);
       
        }
        catch(IOException e)
        {
        	logger.error(TAG, "Errore in compressione database", e);
	    	JOptionPane.showMessageDialog(null, "Errore in compressione database" ,"Errore", JOptionPane.ERROR_MESSAGE);
        }
       
	}
		
}
	
 
