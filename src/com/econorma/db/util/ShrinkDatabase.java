package com.econorma.db.util;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.econorma.Application;
import com.econorma.logic.DatabaseManager;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

public class ShrinkDatabase {
	
	private DatabaseManager databaseManager;
	
	 private static final Logger logger = Logger.getLogger(ShrinkDatabase.class);
	 private static final String TAG = "ShrinkDatabase";

	
	public ShrinkDatabase(){
		 
	}
	
	 
	
	public void compress(){
		
		

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				
				
				 Application.getInstance().getDao().shrinkDatabase();
				
				 JOptionPane.showMessageDialog(null,"Compattamento Database eseguito correttamente", "Informazione", JOptionPane.INFORMATION_MESSAGE);
				 
				 logger.log(LoggerCustomLevel.AUDIT, "Compattamento Database");
				
			}
		});
		
		
		
	}
	
}
