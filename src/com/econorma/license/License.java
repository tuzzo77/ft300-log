package com.econorma.license;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

import com.econorma.resources.Testo;
import com.econorma.util.Logger;

public class License {

	private static Logger logger = Logger.getLogger(License.class);
	private static final String TAG = "License";
	private File f;
 
	
	public License(){

		f = new File("application.lic");
		
		boolean load=false;
 
		if (f.exists()){
			load=true;
		}
 
		if (!load){
			InputStream is=null;
			OutputStream out =null;

			try {

				is = getStream();
		
				out= new FileOutputStream( f );
				
				IOUtils.copy(is,out);
				
			} catch (Exception e) {

			} finally{
				IOUtils.closeQuietly(is);
				IOUtils.closeQuietly(out);
			}
		}


	}


	private InputStream getStream(){
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("com/econorma/resources/Application.lic");
		logger.debug(TAG, "Resource stream : "+ (in!=null));
		return in;
	}

  
	public String getSHA1(String text){
		return  DigestUtils.shaHex(text);
	}
	

	public boolean compareLicense(){

		String number = getNumberLicense();
		
		String key = getSHA1(Testo.PGM_LICENZA + number);
		
		String keyFile=getValue();

		if (keyFile!=null && keyFile.trim().equals(key.trim()))
			return true;

		return false;

	}
	
	public String getNumberLicense(){
		
		String cpuId = MiscUtils.getMotherboardSN();
		String disk = DiskUtils.getSerialNumber("C");
		
		return cpuId.trim()+disk.trim();
	}

 
	public String getValue(){
		
		InputStream fis=null;
		String keyLic=null;
 
		try {
			if (f.exists()){
				fis = new FileInputStream(f);
			}
		
			try {
	 			
	 			keyLic = IOUtils.toString(fis, "UTF-8");
			}
			catch (Exception e) {}
			

		} catch (Exception e) {


		}finally{
			IOUtils.closeQuietly(fis);
		}
		

		return keyLic;
	}


}
