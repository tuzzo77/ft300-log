package com.econorma.license;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import com.econorma.util.Logger;

public class Sinottico {

	private static Logger logger = Logger.getLogger(Sinottico.class);
	private static final String TAG = "License";
	private File f;


	public Sinottico(){

		f = new File("home.jpg");

		boolean load=false;

		if (f.exists()){
			load=true;
		}

		if (!load){
			InputStream is=null;
			OutputStream out =null;

			try {

				is = getStream();

				out= new FileOutputStream( f );

				IOUtils.copy(is,out);

			} catch (Exception e) {

			} finally{
				IOUtils.closeQuietly(is);
				IOUtils.closeQuietly(out);
			}
		}


	}


	private InputStream getStream(){
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("com/econorma/resources/home.jpg");
		logger.debug(TAG, "Resource stream : "+ (in!=null));
		return in;
	}


}
