package com.econorma.license;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class ShowLicenseDialog  implements ActionListener {

	private final JPopupMenu cutpasteMenu;
	private JMenuItem cutMenuItem;
	private JMenuItem copyMenuItem;
	private JMenuItem pasteMenuItem;
	private JLabel label;
	//	private JTextArea licKey;
	private JTextField licKey;
	private JLabel labelSender;
	private JTextField mailSender;
	private boolean show;


	public ShowLicenseDialog(String number){

		cutpasteMenu = new JPopupMenu();
		cutMenuItem = new JMenuItem("Taglia");
		copyMenuItem = new JMenuItem("Copia");
		pasteMenuItem = new JMenuItem("Incolla");

		label= new JLabel("Codice di attivazione: ");
		//		licKey= new JTextArea(number);
		licKey= new JTextField(number);
		licKey.setEditable(false);

		labelSender= new JLabel("Mail Mittente: ");
		mailSender= new JTextField();
		mailSender.setEditable(true);

		show = true;

		show(number);

	}

	public void show(String number){

		JPanel panel = new JPanel(new GridLayout(2,2));
		panel.add(label);
		panel.add(licKey);

		panel.add(labelSender);
		panel.add(mailSender);

		cutMenuItem.addActionListener(this);
		copyMenuItem.addActionListener(this);
		pasteMenuItem.addActionListener(this);

		cutpasteMenu.add(cutMenuItem);
		cutpasteMenu.add(copyMenuItem);
		cutpasteMenu.add(pasteMenuItem);

		licKey.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if(SwingUtilities.isRightMouseButton(e)){
					cutpasteMenu.show(e.getComponent(), e.getX(), e.getY());
				}

			}
		});

		while (show){

			int result = JOptionPane.showOptionDialog(new JFrame(),
					panel,
					"Attivazione Licenza", 
					JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE,
					null, 
					new String[] { "OK", "ANNULLA", "INVIA" }, 
					JOptionPane.NO_OPTION); 
			
			if (result==1) {
				show=false;
			}

			if (result==2) {
				System.out.println(mailSender.getText());
				if (mailSender.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Inserire mail mittente" ,"Errore", JOptionPane.ERROR_MESSAGE);
				} else {
					
					show=false;
				}
			}

		}
	}


	@Override
	public void actionPerformed(ActionEvent evt) {

		Object source = evt.getSource();

		if (source == cutMenuItem) {
			JTextField jte = (JTextField)cutpasteMenu.getInvoker();
			jte.cut();
		}
		if (source == copyMenuItem) {
			JTextField jte = (JTextField)cutpasteMenu.getInvoker();
			jte.copy();
		}
		if (source == pasteMenuItem) {
			JTextField jte = (JTextField)cutpasteMenu.getInvoker();
			jte.paste();
		}

	}


}
