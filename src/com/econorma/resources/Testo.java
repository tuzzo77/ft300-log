package com.econorma.resources;



public class Testo {

	public static final String ECONORMA = "Econorma";
	public static final String PULISCI_GRAFICO = "Pulisci Grafico";
	public static final String ESCI = "Esci";
	public static final String ADMINISTRATOR = "Administrator";
	public static final String STANDARD = "Standard";
	public static final String LOGGER = "DTL";
	public static final String DESCRIZIONE_LOGGER_NEW = "Logger new da configurare";
	
	public static final String ID_LOGGER="Id Sonda";
	public static final String DESCRIZIONE_LOGGER="Descrizione";
	public static final String TIPO_LOGGER="Tipo Lettura";
	public static final String DATA_LOGGER="Data";
	public static final String TRASMISSION_LOGGER="Intervallo misure (sec)";
	public static final String OFFSET_LOGGER="Offset";
	public static final String OFFSET_LOGGER_URT="Offset UR%";
	public static final String OFFSET_LOGGER_VOLT="Offset Volt";
	public static final String OFFSET_LOGGER_MA="Offset mA";
	public static final String OFFSET_LOGGER_OHM="Offset Pt100";
	public static final String RANGE_MIN_LOGGER="Soglia °C Min";
	public static final String RANGE_MAX_LOGGER="Soglia °C Max";
	public static final String RANGE_MIN_LOGGER_URT="Soglia Min UR%";
	public static final String RANGE_MAX_LOGGER_URT="Soglia Max UR%";
	public static final String STATO_BATTERIA="Stato Batteria";
	public static final String CAMPIONI="Numero campioni";
	
	public static final String VOLT="Volt";
	public static final String MILLIAMPERE="Milliampere";
	public static final String OHM="Temperatura Pt100 °C";
	public static final String APERTO_CHIUSO="Aperto/Chiuso";
	
	public static final String GRADI=" �C";
	public static final String PERCENTO=" UR%";
	
	public static final String READ_CONFIG = "Leggi Parametri";
	public static final String DOWNLOAD_SAMPLES = "Scarica letture";
	public static final String SET_DATE = "Imposta Data";
	public static final String WRITE_CONFIG = "Scrivi Parametri";
	public static final String RESET_CONFIG = "Reset";
	public static final String LETTURA_PARAMETRI = "Lettura parametri completata";
	public static final String LETTURA_PARAMETRI_ERRORI = "Lettura parametri con errori";
	public static final String CONFIGURA_PARAMETRI_CORRETTA = "Configurazione eseguita con successo";
	public static final String CONFIGURA_PARAMETRI_ERRORI = "Configurazione eseguita con errori";
	public static final String CANCELLAZIONE_CORRETTA = "Cancellazione eseguita con successo";
	public static final String CANCELLAZIONE_ERRORI = "Cancellazione eseguita con errori";
	public static final String DOWNLOAD_ERRORE = "Lettura parametri da eseguire prima di scaricare i dati";
	
	public static final String ITALIAN = "Italiano";
	public static final String IT = "IT";
	public static final String ENGLISH = "English";

	public static final String PERMETTI_MODIFICA_SONDE = "Permetti Modifica Sonde";
	public static final String SELEZIONA_PORTA = "Seleziona porta";
	public static final String SELEZIONA_PORTA_MODEM = "Seleziona porta Modem";
	public static final String SELEZIONA_FILE_LOGGER = "Seleziona File Logger";
	public static final String SCEGLI_LA_PORTA_DA_UTILIZZARE = "Scegli la porta da utilizzare";
	public static final String SCANSIONE_LOGGER = "Sensori Logger";
	public static final String CONVERSIONI_UM = "Conversioni UM";
	public static final String TEMPI_TRASMISSIONE = "Tempi Trasmissione";
	public static final String CONFIGURAZIONE_LOGGER = "Configurazione Logger";
	public static final String PARAMETRI_PASTORIZZAZIONE = "Parametri di calcolo";
	public static final String SCEGLI_LA_PORTA_MODEM_DA_UTILIZZARE = "Scegli la porta Modem da utilizzare";
	public static final String PORTA_SERIALE = "Porta Seriale";
	public static final String PORTA_SERIALE_MODEM = "Porta Seriale Modem";
	public static final String BAUDRATE = "Baudrate";
	public static final int    BAUDRATE_9600 = 9600;
	public static final int    BAUDRATE_115200 = 115200;
	public static final String DISCOVERY_LOGGER = "Discovery Sensori";
	public static final String DISCOVERY_LOGGER_COMPLETATO = "Scansione Sensori Completato";
	public static final String DISCOVERY_NUMERO_SENSORI_LOGGER = "Nuovi Sensori Trovati:";
	public static final String REPORT = "Stampa";
	public static final String CONFIGURA = "Impostazioni Logger";
	public static final String ALLARMI = "Allarmi";
	public static final String ALLARMI_MAIL = "Configura";
	public static final String OPZIONI = "Opzioni";
	public static final String MAPPA = "Mappa";
	
	public static final String CHART_SERVLET = "Chart";
	public static final String SINOTTICO_SERVLET = "Sinottico";

	public static final String ERROR_PORTA_SERIALE_TITLE = "Errore porta seriale";
	public static final String ERROR_NESSUNA_PORTA_SERIALE_MESSAGE = "Non è stata rilevata nessuna porta seriale nel sistema.\nInstallare o configurare correttamente il sistema e quindi riprovare";
	public static final String RESTART_APPLICATION = "Le modifiche apportate diverranno operative solo dopo che l'applicazione sarà riavviata.";

	public static final String ANNULLA = "Annulla";
	public static final String APPLICA = "Applica";

	public static final String MENU_PRINCIPALE = "Principale";
	public static final String MENU_PREFERENZE = "Preferenze";
	public static final String MENU_ABOUT = "About";
	public static final String MENU_ALLARMI = "Allarmi";
	public static final String MENU_CONVERSIONI= "Conversioni";
	public static final String MENU_SCANSIONE = "Scansione";
	public static final String MENU_TRASMISSIONE = "Trasmissione";
	public static final String MENU_CONFIGURAZIONE = "Configurazione";
	public static final String MENU_PASTORIZZAZIONE = "Pastorizzazione";
	public static final String MENU_AUDIT_LOG = "Log";
	public static final String MENU_USER = "Utenti";
	
	public static final String LISTA_ATTIVITA = "Lista Attivita'";
	public static final String LISTA_MINUTES_PULLDOWN = "Minuti per Pulldown";
	public static final String USER_MANAGEMENT = "Gestione Utenti";
	 
	public static final String ERRORE_NESSUNA_PROVA_AVVIATA = "Nessuna Prova Avviata"; 
	
	public static final String VISUALIZZA_SENSORI_SINGOLI = "Visualizza Sensori Singoli";
	
	public static final String SENSORI_CONOSCIUTI = "Sonde Conosciute";
	public static final String CONFIGURA_PARAMETRI = "Configura Parametri";
	
	public static final String SENSORI_INTERNI = "Sonde Interne";
	public static final String SENSORI_ESTERNI = "Sonde Esterne";
	public static final String SENSORI_EUTETTICI = "Sonde Eutettiche";
	
	public static final String SOGLIE_ALLARMI = "Soglie Allarmi";
	
	public static final String VISUALIZZA_DETTAGLI = "Dettagli Registrazione";
	public static final String VISUALIZZA_DETTAGLI_PROVA = "Dettagli Prova";
	
	public static final String ELENCO_LETTURE = "Elenco Letture";
	
	public static final String DISCHARGE_COMPLETATO = "Importazione dati terminata con successo";
	
	public static final String INDIRIZZO_INESISTENTE = "Indirizzo Logger non risponde";
	public static final String MENU_EXPORT = "Export";
	public static final String SELEZIONE = "Selezione";
	public static final String MOVIMENTI = "Movimenti";
	public static final String SELEZIONA_FILE_SINOTTICO = "Seleziona File Sinottico";
	public static final String MENU_UTILITY = "Utilità";
	public static final String BACKUP = "Backup database";
	public static final String INVIA_DB = "Invia database ad Econorma Sas";
	public static final String IMPORTA_STORICO = "Importa Storico da CSV";
	
	public static final String ANOMALIES = "Anomalies";
	public static final String ANOMALIES_APERTURA = "Anomalies_Apertura";
	public static final String ANOMALIES_CHIUSURA = "Anomalies_Chiusura";
	public static final String DB_SEND = "sendDB";
	public static final String BATTERY_CHARGE_SEND = "sendBatteryCharge";
	public static final String NO_TRASMISSION_SEND = "noTrasmission";
	public static final String AUTO_DAILY_FILE_SEND = "autoDailyFile";
	public static final String EXPORT_SEND = "sendExport";
	public static final String LICENSE_ACTIVATION = "License";
	public static final String CONSOLE = "console";
	public static final String FREE_LICENSE = "nolic";
	public static final String FREE_INSTANCES = "noone";
	
	public static final String UM_EXPORT = "UM_Export";
	public static final String UM_WEB = "UM_Web";
	public static final String UM_GUI = "UM_Gui";
	
	public static final String DISABILITA = "disabilita";
	public static final String SHRINK = "Compatta database";
	public static final String CHECK_DB_STRUCT = "Controllo struttura database";
	public static final String DELETE_UNKNOWN = "Cancella sonde sconosciute";
	
	public static final String ALLARME_SONDA = "Sonda: ";
	public static final String ALLARME_DESCRIZIONE = "Descrizione: ";
	public static final String ALLARME_SOGLIA_MINIMA = "Soglia minima: ";
	public static final String ALLARME_SOGLIA_MASSIMA = "Soglia massima: ";
	public static final String ALLARME_TEMPERATURA = "Temperatura allarme: ";
	public static final String ALLARME_UMIDITA = "Umidita allarme: ";
	public static final String ALLARME_C = " C";
	public static final String ALLARME_URT = " URT";
	
	public static final String APERTURA_ALLARME = "Apertura allarme";
	public static final String CHIUSURA_ALLARME = "Chiusura allarme ";
	
	public static final String BATTERIA_STATO_CARICA = "B";
	public static final String BATTERIA_CARICA = "CARICA";
	public static final String BATTERIA_STATO_SCARICA = "b";
	public static final String BATTERIA_SCARICA = "SCARICA";
	public static final String STATO_BATTERIA_SCARICA = "Stato Batteria Scarica";
	public static final String STATO_BATTERIA_CARICA = "Stato Batteria carica";
	
	public static final String REAL_TIME = "Tempo Reale";
	public static final String ATP = "Collaudi ATP";
	public static final String PASTORIZZAZIONE = "Pastorizzazione";
	public static final String MONODIREZIONALE = "Monodirezionale";
	public static final String BIDIREZIONALE = "Bidirezionale";
	public static final String GSM = "GSM/GPS";
	public static final String TEMPERATURA = "Temperatura °C";
	public static final String UMIDITA = "Umidita UR%";
	public static final String TEMPERATURA_UMIDITA = "Temperatura/Umidita'";
	public static final String MISTA = "Mista";
	public static final String LIGHT = "Light";
	public static final String DARK = "Dark";
	public static final String NEW = "New";
	public static final String OLD = "Old";
	
	
	public static final String MANCATA_TRASMISSIONE = "Mancata Trasmissione";
	
	public static final String PGM_LICENZA = "FT105RF";
	public static final String SMTP_INVIO_LICENZA = "smtp.crismatica.com";
	public static final String USER_LOGIN_INVIO_LICENZA = "alessandro.mattiuzzi@econorma.com";
	public static final String PWD_LOGIN_INVIO_LICENZA = "patitaga";
	public static final String OGGETTO_INVIO_LICENZA = "Invio Licenza per attivazione";
	public static final String SENDER_INVIO_LICENZA = "FT105RF_licenza@econorma.com";
	public static final String RECIPIENT_INVIO_LICENZA = "FT105RF_licenza@econorma.com";
	public static final String RECEIVER_INVIO_LICENZA = "info@econorma.com";
	
	public static final String SCARICO_LOGGER="Scarico dati ";
	public static final String LOGGER_AUTOMATICO="Automatico";
	public static final String LOGGER_MANUALE="Manuale";
	
	public static final String FULL_SCREEN="Visualizza Schermo Intero";
	public static final String MEDIE="Medie";
	public static final String SENSORI="Sensori";
	
	public static final String PULLDONW="Pulldown";
	public static final String MINUTES_PULLDONW="Minuti per pulldown";
	public static final String DELIMITATORE="|";
	
}

