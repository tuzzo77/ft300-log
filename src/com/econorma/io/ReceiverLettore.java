package com.econorma.io;

public abstract class ReceiverLettore extends Thread {

	
	public ReceiverLettore() {
		super();
	}

	public ReceiverLettore(Runnable target, String name) {
		super(target, name);
	}

	public ReceiverLettore(Runnable target) {
		super(target);
	}

	public ReceiverLettore(String name) {
		super(name);
	}

	public ReceiverLettore(ThreadGroup group, Runnable target, String name,
			long stackSize) {
		super(group, target, name, stackSize);
	}

	public ReceiverLettore(ThreadGroup group, Runnable target, String name) {
		super(group, target, name);
	}

	public ReceiverLettore(ThreadGroup group, Runnable target) {
		super(group, target);
	}

	public ReceiverLettore(ThreadGroup group, String name) {
		super(group, name);
	}

	public abstract void die() ;
}
