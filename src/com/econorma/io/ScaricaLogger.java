package com.econorma.io;
 

import java.awt.Dialog;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import org.apache.commons.io.FileUtils;

import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.util.Logger;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;
 


public class ScaricaLogger {

	private static long startTime;
	private static long current;
	private static long elapsed;
	private static final long DURATA = 700000;
	private static final long DURATA_CANC = 66000;
	private static final int MESSAGE_SIZE = 500;
	private static InputStream input = null;
	private static OutputStream output = null;
	private static String port = null;
	
	private static final Logger logger = Logger.getLogger(ScaricaLogger.class);
	private static final String TAG = ScaricaLogger.class.getSimpleName();

	
	public static boolean Scarica(final Runnable callback, final Sensore sensore, final int indirizzo, final Prova prova) throws Exception  {

		new Thread() {
			public void run() {
				
				
				 
				SerialPort serialPort = null;
				boolean reply=false; 
				startTime = System.currentTimeMillis();
				int y=0;

				try {
					
				 
					port = Application.getInstance().getPreferences().getPort();

					CommPortIdentifier portId = CommPortIdentifier
							.getPortIdentifier(port);
					serialPort = (SerialPort) portId.open("SimpleReadApp", 2000);
					
					 

					int n;

					input = serialPort.getInputStream();
					output = serialPort.getOutputStream();


					Thread readThread;
// 					serialPort.notifyOnDataAvailable(true);

					try {
						serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, 
								SerialPort.STOPBITS_1, 
								SerialPort.PARITY_NONE);
					}
					catch (UnsupportedCommOperationException e) {}
					Thread.sleep(2000);
 
					File f = new File("out.txt");
					FileUtils.deleteQuietly(f);
					

					final JProgressBar progressBar = new JProgressBar(0,100);  
					progressBar.setPreferredSize(new Dimension(300,20));
					progressBar.setString("In esecuzione");
					progressBar.setStringPainted(true);
					JDialog dialog = null;

  
					int i=1;
					int start=0;
					String hexCampio =null;
					String hexStart =null;
					char digit = (char) indirizzo;

				 	while (i<=5000) {
				 		
				 		current = System.currentTimeMillis();
						elapsed = (current - startTime);


						Runnable runme = new Runnable() {

							public void run() {
								progressBar.setValue((int)((elapsed)/((double)DURATA)*100));
								int proc = (int) Math
										.round(progressBar.getPercentComplete() * 100);
								progressBar.setString(proc + " %");
							}
						};
						SwingUtilities.invokeLater(runme);
 

						if(dialog==null){
							dialog = new JDialog(Application.getInstance().getMainFrame(), sensore.getId_sonda() + " - " + "Importazione in corso....", true);
							dialog.setContentPane(progressBar);
							dialog.setResizable(false);
							dialog.pack();
							dialog.setLocationRelativeTo(null);

							class DialogWorker extends SwingWorker<Void,Void>{

								Dialog d;
								public DialogWorker(Dialog d){
									this.d = d;
								}
								@Override
								protected Void doInBackground()
										throws Exception {
									d.setVisible(true);
									return null;
								}

							}

							new DialogWorker(dialog).execute();

						}
				 
						
						String data = writeAndRead(indirizzo, 2000, start);
					 
//						System.out.print(new String(buffer));	
				 	 
							
							int x=0;
							String check1 = "------------/---/---";
							String check=null;
							boolean exit = false;
							boolean failed = false;
							
							if(data!=null){
							
							if (!(data.trim().isEmpty())) {
							 
								
								StringTokenizer st = new StringTokenizer(data.trim(), "_");
								while (st.hasMoreTokens()){
									x++;
									String riga = st.nextToken();
									
									
									if (x==2){
									
										if (riga.equals(check1)) {
											
											failed = true;
									 	 
											if (y>1){
									 		exit=true;
										 	}
										  	y++;
										 	
										}
										 
										
										break;
										
									}
										
									if (data.trim().length()<100) {
										failed=true;	
									}
									
								}
								 
					 	
							} else {
								failed=true;
							}   
							} else {
								failed=true;
				 	        }
							
							
							
							try {
   						 		FileUtils.write(f, data, true);
   						 		List<Lettura> letture = ParsersLogger.createLettura(data);
   						 	
   							
   						 		
	   						 	if (letture!=null) {
	   						 		
	   						 	for (Lettura l : letture) {
	   						 		
	   						 		if (l.getTemperaturaGrezza()==0){
	   						 			continue;
	   						 		}
	   						 		
	 								Application.getInstance().getDao().insertLettura(l);
	 								LetturaProva letturaProva = new LetturaProva(prova, l);
	 	   							Application.getInstance().getDao().insertLetturaProva(letturaProva);
									
								}
   						 		
   						 		
   						 		
   								// run on the main thread
//   								EventQueue.invokeLater(new Runnable() {
//   									@Override
//   									public void run() {
//   								 	}
//   								});
   							}
   						 		
   						 	
   						 	
   						 	if (failed==false){
								start=start+5;
								}  
   						 	 
					 			
							} catch (IOException e) {						 
								e.printStackTrace();
							}
							
							if (exit==true){
								break;
							}

				 
						i=i+1;
					 
// 						Thread.sleep(2000); 
					 
 
					} 
				 	
				 	
				 	

					
//					CANCELLAZIONE FILE //
					
					startTime = System.currentTimeMillis();
					elapsed=0;
					boolean run = true;
					
					if(dialog!=null){
						dialog.dispose();
						dialog = null;
					}
					
					
					while (elapsed <= DURATA_CANC) {


						current = System.currentTimeMillis();
						elapsed = (current - startTime);

					
					
					Runnable runme = new Runnable() {

						public void run() {
							progressBar.setValue((int)((elapsed)/((double)DURATA_CANC)*100));
							int proc = (int) Math
									.round(progressBar.getPercentComplete() * 100);
							progressBar.setString(proc + " %");
						}
					};
					SwingUtilities.invokeLater(runme);

					
					if(dialog==null){
						
						dialog = new JDialog
								(Application.getInstance().getMainFrame(), sensore.getId_sonda() + " - "+ "Cancellazione in corso....." , true);
						
						dialog.setContentPane(progressBar);
						dialog.setResizable(false);
						dialog.pack();
						dialog.setLocationRelativeTo(null);

						class DialogWorker extends SwingWorker<Void,Void>{

							Dialog d;
							public DialogWorker(Dialog d){
								this.d = d;
							}
							@Override
							protected Void doInBackground()
									throws Exception {
								d.setVisible(true);
								return null;
							}

						}

						new DialogWorker(dialog).execute();

					}
					
					 
					
					if (run){
					Thread.sleep(2000);
					
					output.write((">" + digit + "C??!\r\n").getBytes());
					  
					 
					output.flush();
					Thread.sleep(1000);

					byte[] readBuffer = new byte[120];

					try {
						while (input.available() > 0) {
							int numBytes = input.read(readBuffer);
						} 
					}
					catch (IOException e) {}


					String riga = new String(readBuffer);
					

					
					run=false;
					
					
					}
					
					Thread.sleep(2000); 
					
					
					
				}
				 	
				 	
				 	try { if(output!=null) output.close();} catch (IOException e) {}
					try { if(input!=null) input.close();} catch (IOException e) {}
					try { if(serialPort!=null) serialPort.close();} catch (Exception e) {}
				 	
 		
					
					if(dialog!=null){
						dialog.dispose();
						dialog = null;
						if(callback!=null){
							SwingUtilities.invokeLater(callback);
						}

					}
					
 

				} catch (NoSuchPortException e) {
					System.out.println("Exception in Adding Listener" + e);
					reply=false; 
				} 
				catch (PortInUseException e) {
					System.out.println("Exception in Adding Listener" + e);
					reply=false; 
				} 
				catch (IOException e) {
					System.out.println("Exception in Adding Listener" + e);
					reply=false; 
				}
				catch (InterruptedException e) {
					System.out.println("Exception in Adding Listener" + e);
					reply=false; 
				}


				try { if(output!=null) output.close();} catch (IOException e) {}
				try { if(input!=null) input.close();} catch (IOException e) {}
				try { if(serialPort!=null) serialPort.close();} catch (Exception e) {}
				
				
				
			}
			
			}.start();	
				
			return true;

			}
		 
 
	public static String fillString(char fillChar, int count){
		char[] chars = new char[count];
		while (count>0) chars[--count] = fillChar;
		return new String(chars);
	} 
	
	
	
	public static byte[] read() throws IOException {

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        } 

        byte[] buffer = new byte[528];

        int len = 0;
 
        while (input.available() > 0) {
            len = input.available();
            input.read(buffer,0,528);
        }

        return buffer;
    }
	
	
	public static String writeAndRead(int indirizzo, int sleepMS, int start) throws InterruptedException, IOException {
		try {
			
			int maxSleepTry = 10;

			int currentTry = maxSleepTry;
			char digit = (char) indirizzo;
			
			char campioni = (char) start;
			String hexStart = Integer.toHexString(campioni).toUpperCase();
			hexStart = fillString('0',  (4-(hexStart.length()))) + hexStart;
			
			input.skip(input.available());// scarto porcheria ricevuta prima della richiesta
			
			output.write((">" + digit + "L#" + hexStart + "??!\r\n").getBytes());
			output.flush();

			// Thread.sleep(900);
			
			/*
			 
				  final byte[] readBuffer = new byte[MESSAGE_SIZE];
				  int total = 0;
				  int read = 0;
				  while (total < MESSAGE_SIZE
				            && (read = input.read(readBuffer, total, MESSAGE_SIZE - total)) >= 0) {
				    total += read;
				  }
				
			*/
			
			

			byte[] readBuffer = new byte[512];

			StringBuilder sb = new StringBuilder();
			try {
				while(currentTry>0 && sb.length()<MESSAGE_SIZE){
					//faccio un tentativo
					Thread.sleep(100);
					int available = input.available();
					if(available>0){
						//logger.debug(getTag(), "offset : "+offset+" maxReadable : "+maxReadable);
						int read = input.read(readBuffer);
						sb.append(new String(readBuffer, 0, read).trim());
					}
					currentTry--;
				}

			} catch (IOException e) {
				logger.error(TAG, "Scarico dati Logger su " + port
						+ ": Generic error while using port", e);
			}
			
			String line = sb.toString(); // "-<\"SOTTO1\"T_0B6_000_B3";
			logger.debug(TAG, "Scarico dati Logger letto: \"["+line+"]\" size: \""+ ((line==null) ? "null ": line.length())+"\" pause necessarie : "+ (maxSleepTry -currentTry));
			if (line != null && !line.trim().isEmpty()) {
				return line;
			} else {
				return null;
			}
//		}catch (InterruptedException ee){
//			throw ee;
		}  catch (IOException e) {
			logger.error(TAG, "Receiver Logger su " + port
					+ ": Generic error while using port", e);
			throw e;
		}
	}

}