package com.econorma.io;

import com.econorma.data.Lettura;
import com.econorma.util.Logger;

public class Receiver {

	private static final String TAG = Receiver.class.getSimpleName();

	private static final Logger logger = Logger.getLogger(Receiver.class);

	private String com;
	private int baudRate;
	private boolean started = false;
	private ReceiverLettore workingThread;
	private ReadListener listener;
	
	Thread caller;

	public Receiver(String com, int baudRate) {
		logger.info(TAG, "ReceiverMonodirezionale creato" );
		this.baudRate=baudRate;
		setCom(com);
		caller = Thread.currentThread();
	}

	public void setCom(String com) {
		this.com = com;
		if(com==null){
			logger.info(TAG," nessuna seriale indicata..." );
		}else{
			logger.info(TAG," seriale da utilizzare : "+com );
		}
	}
	
	public void setBaudRate(int baudRate){
		this.baudRate = baudRate;
	}

	/**
	 * 
	 * Inizia la ricezione dei dati dalla porta selezionata. Se non c'è nessuna
	 * porta selezionata la richiesta viene ignorata
	 * 
	 * @return true se l'ascolto inizia. false se non inizia oppure è gia
	 *         iniziato
	 */
	public boolean start() {
		if (!started) {
			if(com != null){
				//workingThread = new ReceiverLettoreRxTx(com,this);
				workingThread = new ReceiverLettorejSSC(com,baudRate,this);
				workingThread.start();
				started = true;
				return true;
			}else{
				logger.info(TAG,"cannot start, port is null!");
			}
		}
		return false;
	}

	public void stop() {
		
		if (workingThread != null) {
			workingThread.die();
			try {
				workingThread.join(1000);
			} catch (InterruptedException e) {
				//ignore
			}
			workingThread = null;
		}
		started = false;
	}

	public void setOnReadListener(ReadListener listener) {
		this.listener = listener;
	}

	public ReadListener getOnReadListener() {
		return this.listener;
	}
	

	public interface ReadListener {

		/**
		 * callback is performed my the background thread manage synchronization
		 * in the onRead body
		 * 
		 * @param lettura
		 */
		public void onRead(Lettura lettura);

	
	}

}
