package com.econorma.io;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.MDC;

import com.econorma.Application;
import com.econorma.data.Sensore;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

import jssc.SerialPort;
import jssc.SerialPortException;


public class ConfiguraLogger {
	
	private static final Logger logger = Logger.getLogger(ConfiguraLogger.class);
	private static final String TAG = ConfiguraLogger.class.getSimpleName();
	private static final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmmss");
	
	private SerialPort serialPort;
	private Sensore sensore;

	public ConfiguraLogger (Sensore sensore) {
		this.sensore = sensore;
	}

	public boolean write(int trasmission) throws InterruptedException {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			/* */
		}
		
		boolean check = false;
		
		try {
			String portName = Application.getInstance().getPreferences().getPort();
			int baudRate = Application.getInstance().getPreferences().getBaudRate();
			boolean connected = connect(portName, baudRate);
		
			MDC.put("event_name", "Verifica Logger");
			if(connected){
				
				final int MAX_RETRY = 2;
				int error_count = 0;
				while(true){
					String hexTrasmission = Integer.toHexString(trasmission).toUpperCase();
					String campioni = fillString('0',  (4-(hexTrasmission.length()))) + hexTrasmission;
					String rfTrasmission = fillString('0',  (4-(hexTrasmission.length()))) + hexTrasmission;
					
					StringBuilder array = Application.getInstance().getSensorsArray();
					String sensorsArray = fillString('0',  (6-(array.toString().length()))) + array.toString();
					
					String cmd = ">" + "dTIME_" + getHexFromDate() + "_" + campioni + "_" + rfTrasmission + "_" + sensorsArray + "_" + "??!\r\n";
			
					logger.info(TAG, "Richiesta: "+ cmd);
					serialPort.writeString(cmd.trim());
		
//					Thread.sleep(2000);
					Thread.sleep(1000);
					String lines = null;
					int available = serialPort.getInputBufferBytesCount();
					if(available>0){
						lines = serialPort.readString(available).trim();
					}
					logger.debug(TAG, "Stringa ricevuta: " + lines);
					
					if(lines!= null && lines.length()>0) {
						logger.info(TAG, "Sensore configurato correttamente");
						logger.log(LoggerCustomLevel.AUDIT, "Sensore configurato correttamente");
						check = true;
						break;
					}else{
						logger.debug(TAG, "ricevuta risposta da un sensore imprevisto...");
					}
					
					if(!check){
						error_count++;
					}
					if(error_count>=MAX_RETRY){
						break;
					}
				}
			}
		} catch (SerialPortException e) {
			logger.error(TAG, e.getPortName()+ ": write error ", e);
		}finally{
			disconnect();
		}
		return check;
	}
	
	
	private void disconnect(){
		if(serialPort!=null) { 
			try {
				serialPort.closePort();
			} catch (SerialPortException e) {
				/* ignore */
				logger.debug(TAG,"Error While Closing Port", e);
			}
			logger.debug(TAG, "Porta seriale "+serialPort.getPortName()+" chiusa");	
			serialPort = null;
		}
	}

	private boolean connect(String portName, int baudRate) {
		
		logger.debug(TAG, "connecting on "+portName);
		try {
			if(serialPort!=null){
				logger.info(TAG, "chiudo la seriale gia aperta..."+ portName);
				try{serialPort.closePort();}catch (Exception e) {
					logger.error(TAG, e);
				}
			}

			SerialPort serialPort = new SerialPort(portName);

			boolean opened = false;
			try{
				opened = serialPort.openPort();
				this.serialPort = serialPort;
				logger.debug(TAG, portName +" "+(opened ?  "opened " : "not opened"));
				if(opened){
					serialPort.setParams(baudRate,
							SerialPort.DATABITS_8,
							SerialPort.STOPBITS_1,
							SerialPort.PARITY_NONE);
					return true;
				}
			}catch(SerialPortException spe){
				logger.error(TAG, "ERRORE SERIALE :"+spe.getMessage(),spe);
			}
			return false;
		} catch (Exception e) {
			logger.fatal(TAG, "Receiver su " + portName+ ": Generic error while using port", e);
			return false;
		}
	}
	
	public static String fillString(char fillChar, int count){
		char[] chars = new char[count];
		while (count>0) chars[--count] = fillChar;
		return new String(chars);
	} 
	
	public static String getHexFromDate() {
		
		String data = null;
		
		try {
			String dataStr = sdf.format(new Date());
			int gg = Integer.parseInt(dataStr.substring(0,2));
			int mm = Integer.parseInt(dataStr.substring(2,4));
			int aa = Integer.parseInt(dataStr.substring(4,6));
			int hh = Integer.parseInt(dataStr.substring(6,8));
			int mi = Integer.parseInt(dataStr.substring(8,10));
			int ss = Integer.parseInt(dataStr.substring(10,12));
			
			
			String hhHex = Integer.toHexString(hh).toUpperCase();
			String miHex = Integer.toHexString(mi).toUpperCase();
			String ssHex = Integer.toHexString(ss).toUpperCase();
			String ggHex = Integer.toHexString(gg).toUpperCase();
			String mmHex = Integer.toHexString(mm).toUpperCase();
			String aaHex = Integer.toHexString(aa).toUpperCase();
			
			
			String ora = fillString('0',  (2-(hhHex.length()))) + hhHex;
			String minuti = fillString('0',  (2-(miHex.length()))) + miHex;
			String secondi = fillString('0',  (2-(ssHex.length()))) + ssHex;
			String giorno = fillString('0',  (2-(ggHex.length()))) + ggHex;
			String mese = fillString('0',  (2-(mmHex.length()))) + mmHex;
			String anno = fillString('0',  (2-(aaHex.length()))) + aaHex;
			
			
			data = ora + minuti + secondi + giorno + mese + anno;
			
		} catch (Exception e) {
			logger.error(TAG,e);
		}
		
		return data;
		
	}

}