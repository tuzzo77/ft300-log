package com.econorma.io;

import java.awt.EventQueue;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import jssc.SerialPort;
import jssc.SerialPortException;

import com.econorma.data.Lettura;
import com.econorma.io.Receiver.ReadListener;
import com.econorma.util.Logger;

public class ReceiverLettorejSSC extends ReceiverLettore {
	
	private static final Logger logger = Logger.getLogger(ReceiverLettorejSSC.class);
	private static final String TAG = ReceiverLettorejSSC.class.getSimpleName();
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	private String portName;
	private int baudRate;
	private SerialPort serialPort;
	private boolean running;
	private final Receiver receiver;

	public ReceiverLettorejSSC(String com, int baudRate, Receiver receiver) {
		super("ReceiverLettorejSSCThread");
		this.receiver = receiver;
		portName = com;
		this.baudRate=baudRate;
		running = true;
		setDaemon(true);
	}

	@Override
	public void run() {
		
		logger.info(TAG, "Receiver su " + portName + ": [CONNECTING...]");
		
		try{
			if (connect()) {
				
				SerialBuffer serialBuffer = new SerialBuffer();
				logger.info(TAG, "Receiver su " + portName + ": [CONNECTED]");
				while (running) {
					try {
						
						 int available = serialPort.getInputBufferBytesCount();
						// leggi
						 
						 if(available>0){
							 String readString = serialPort.readString(available);
							 
							 logger.debug(TAG,  portName + ": Data " + sdf.format(new Date()) + ": lettura parziale [" + readString + "]" );
							 
							 serialBuffer.add(readString);
							 
							 List<String> lines = serialBuffer.getLines();
								
							 logger.debug(TAG,"\t"+ (lines!=null ? lines.size() : 0)+" righe complete dal buffer");
							 
							 if(lines!=null){
								 	
									for(String line : lines){
										logger.debug(TAG, "\t"+ portName + ": parsing [" + line + "]" + " Data " + sdf.format(new Date()));
										final Lettura lettura ;
										
										{
											Lettura tmpLettura = null;
											try{
												tmpLettura = Parsers.createLettura(line);
											}catch (Exception e) {
												//questo e' proprio una programmazione errata nella classe di parsing
												logger.warn(TAG, "Receiver su " + portName + ": cannot parse data [" + line + "]");
											}
											lettura = tmpLettura;
										}
										if (lettura != null) {
											logger.debug(TAG, "Receiver su " + portName + ": lettura ok");
											//TODO
//											EventBusService.publish(new Events.PortEvent(
//															serialPort,
//															Events.PortEvent.READ_EVENT));
											
											final ReadListener listener = this.receiver.getOnReadListener();
											if (listener != null) {
												// run on the main thread
												EventQueue.invokeLater(new Runnable() {
			
													@Override
													public void run() {
														listener.onRead(lettura);
													}
												});
			
											}
										}else{
											logger.debug(TAG, "Receiver su " + portName + ": lettura fail");
										}
										
									}
								}
						 }else{
							 //se non ho niente dormi un po'
							 
							 Thread.sleep(100);
						 }
						 
							
						if (isInterrupted()) {
							logger.debug(TAG, "Receiver su " + portName + ": interrupt received. Quit");
							break;
						}
					// exits
					} catch (InterruptedException e1) {
						// ignore
						logger.debug(TAG, "Receiver su " + portName + ": interrupt received. Quitting? "+!running);
					} catch (Exception e) {
						logger.error(TAG, "Receiver su " + portName + ", errore", e);
						//TODO
//						EventBusService.publish(new Events.PortEvent(serialPort,
//								Events.PortEvent.ERROR_GENERIC, e));
					}
				} // for loop
				logger.info(TAG, "Receiver su " + portName + " exiting... running = ["+running+"]");
			} // connect
		} finally {
			
			try{ if(serialPort!=null)serialPort.closePort();}catch (Exception e) {}
			logger.info(TAG, "Receiver su " + portName + " uscito [stream chiuso]");
		}
		
		logger.info(TAG, "Receiver su " + portName + ": [TERMINATED]");

	}

	public void die() {
		running = false;
		this.interrupt();
		logger.info(TAG, "Receiver su " + portName + " : Quit PLEASE!");
	}

	private boolean connect() {
		try {
			if(serialPort!=null){
				logger.info(TAG, "Receiver su " + portName
						+ ": Closing current port...");
				try{serialPort.closePort();}catch (Exception e) {}
				serialPort = null;
			}
			
			SerialPort serialPort = new SerialPort(portName);
			
			boolean opened = false;
			try{
				opened = serialPort.openPort();
				if(opened){
					 this.serialPort = serialPort;
					 serialPort.setParams(baudRate,
//					 serialPort.setParams(SerialPort.BAUDRATE_9600,
                             SerialPort.DATABITS_8,
                             SerialPort.STOPBITS_1,
                             SerialPort.PARITY_NONE);//Set params. Also you can set params by this string: serialPort.setParams(9600, 8, 1, 0);
					 return true;
				}
			}catch(SerialPortException spe){
				if(SerialPortException.TYPE_PORT_BUSY.equals(spe.getExceptionType())){
					logger.info(TAG, "Receiver su " + portName
							+ ": Port in use");
					
					//TODO change events indipendenti da rxtx
//					EventBusService.publish(new Events.PortEvent(serialPort,
//							Events.PortEvent.ERROR_OCCUPIED));
				}
				
			}
			
			if(opened){
				//TODO
//				EventBusService.publish(new Events.PortEvent(
//						serialPort, Events.PortEvent.CONNECTED));
			}
			return false;
		} catch (Exception e) {
			logger.fatal(TAG, "Receiver su " + portName
					+ ": Generic error while using port", e);
			//TODO
//			EventBusService.publish(new Events.PortEvent(serialPort,
//					Events.PortEvent.ERROR_GENERIC, e));
			return false;
		}
	}

}