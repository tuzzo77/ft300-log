package com.econorma.io;

import java.io.IOException;

import com.econorma.io.Receiver.ReadListener;
import com.econorma.util.Logger;

import jssc.SerialPort;
import jssc.SerialPortException;

public class ReceiverLogger {
	
	private static final String TAG = ReceiverLogger.class.getSimpleName();

	private static final Logger logger = Logger.getLogger(ReceiverLogger.class);

	private String com;
	private int baudRate;
	private boolean started = false;
	private Lettore workingThread;
	private ReadListener listener;
	
	Thread caller;
	
	private Task task;
	public enum Task{
		Discovery,
		Misure,
		Modem
	}
	
	public ReceiverLogger(String com, int baudRate, Task task) {
		logger.info(TAG, "ReceiverLogger creato, task : "+task );
		if(com==null){
			logger.info(TAG," nessuna seriale indicata..." );
		}else{
			logger.info(TAG," seriale da utilizzare : "+com );
		}
		this.com = com;
		this.baudRate=baudRate;
	 	caller = Thread.currentThread();
	 	this.task = task;
	}
	
	public Task getTask(){
		return task;
	}
	
	public String getCom(){
		return com;
	}
	
	public int getBaudRate(){
		return baudRate;
	}
	 
	/**
	 * 
	 * Inizia la ricezione dei dati dalla porta selezionata. Se non c'è nessuna
	 * porta selezionata la richiesta viene ignorata
	 * 
	 * @return true se l'ascolto inizia. false se non inizia oppure è gia
	 *         iniziato
	 */
	public boolean start() {
		logger.debug(TAG, "start called : "+task);
		if (!started) {
			if(com!=null){
				switch (task) {
				case Discovery:
					workingThread =  new LettoreDiscoveryLogger(com,baudRate, listener);
					break;
				case Misure:
					Integer frequenza=60;
					if(frequenza==null || frequenza<=0 )
						frequenza  = 10;
					workingThread = new LettoreMisureLogger(com,baudRate,listener,frequenza);
					break;
//				case Modem:
//					workingThread = new LettoreModem(com, baudRate, listener);
//					break;
				default:
					throw new IllegalArgumentException("task non gestito");
				}
				workingThread.start();
				started = true;
				return true;
			}else{
				logger.info(TAG,"cannot start, port is null!");
			}
		}
		return false;
	}

	public void stop() {
		if (workingThread != null) {
			workingThread.die();
			try {
				workingThread.join();
			} catch (InterruptedException e) {
				logger.error(TAG,"Interruzione Non Gestita", e);
			}
			workingThread = null;
		}
		started = false;
	}

 
	
	public void setOnReadListener(ReadListener listener) {
		this.listener = listener;
	}
 
	
	
	public abstract static class Lettore extends Thread {
		
		private static int last_id;
		
		protected String portName;
		protected SerialPort serialPort;
		private boolean running;
		protected int baudRate;

		private  ReadListener listener;
		private int id;
 
		private final Logger logger;
		public Lettore(String com, int baudRate, ReadListener listener, Logger logger) {
			
			super();
			logger.debug(getTag(), "CREATED!");
			setName(generateName());
			portName = com;
			this.baudRate=baudRate;
			running = true;
			setDaemon(true);
			this.listener = listener;
			this.logger = logger;
		}
		
		private String generateName(){
			synchronized (Lettore.class) {
				last_id++;
				id = last_id;
			}
			return getClass().getSimpleName()+""+id;
		}
		
		public abstract int getMessageSize();
		
		/**
		 * 
		 * @return il numero di millisecondi di attesa prima della prossima chiamata al runLogic
		 * @throws InterruptedException
		 * @throws IOException
		 */
		public abstract int runLogic() throws InterruptedException, IOException;
		
		public void onPortConnected() throws InterruptedException, IOException {};
		
		public String getTag(){ return getName();};
		
		
		@Override
		public void run() {
			try{
				if (connect()) {
					logger.info(getTag(), "Receiver Logger su " + portName + ": [connesso]");
					try {
						if(running){
							onPortConnected();
						}
						while (running) {							
							int sleep = runLogic(); 
							if(sleep>0)
								Thread.sleep(sleep);
						}
					}
					catch (InterruptedException e) {
						logger.info(getTag(), "Receiver Logger : esco per interrupt die ");
					}
					catch (Exception e) {
						logger.error(getTag(), "Receiver Logger su " + portName + ", errore", e);
					}
				}
			}finally{
				disconnect();
				onEnded();
			}

		}
		
		public ReadListener getReadListener(){
			return listener;
		}
		
		public synchronized void  die() {
			running = false;
			if(Thread.currentThread()!=this)
				this.interrupt();// se lo chiamo dal lettore stesso non lancio interrupt
			logger.debug(getTag(), "Receiver Logger su " + portName + " sta uscendo");
		}
		
		
		protected void onEnded(){
			
		}
		
		private void disconnect(){
			try { 
				if(serialPort!=null) { 
					logger.debug(getTag(), "chiudo la prota "+serialPort.getPortName());	
					serialPort.closePort();
					
				} else {
					logger.debug(getTag(), "nessuna porta da chiudere");	
				}				
			} catch (Exception e){ 
				logger.error(getTag(), "Errore chiusura porta" ,e);
			}finally{
				serialPort = null;
			}
		}
		
		private boolean connect() {
			try {
				if(serialPort!=null){
					logger.info(TAG, "Receiver Logger su " + portName
							+ ": Closing current port...");
					try {
						serialPort.closePort();
					} catch (SerialPortException e) {
						/* ignore */
						logger.debug(TAG,"Error While Closing Port", e);
					}
					serialPort = null;
				}
				
				SerialPort serialPort = new SerialPort(portName);
				
				boolean opened = false;
				try{
					opened = serialPort.openPort();
					logger.debug(TAG, "Connesso su "+serialPort.getPortName());
					if(opened){
						 this.serialPort = serialPort;
//						 serialPort.setParams(SerialPort.BAUDRATE_9600,
						 serialPort.setParams(baudRate,
	                             SerialPort.DATABITS_8,
	                             SerialPort.STOPBITS_1,
	                             SerialPort.PARITY_NONE);
						 return true;
					}
				}catch(SerialPortException spe){
					if(SerialPortException.TYPE_PORT_BUSY.equals(spe.getExceptionType())){
						logger.error(TAG, "Receiver su " + portName
								+ ": Port in use");
					}
					
				}
				return false;
			} catch (Exception e) {
				logger.fatal(TAG, "Receiver su " + portName
						+ ": Generic error while using port", e);
				return false;
			}
		}
		
		protected String read(int sleepMS) throws InterruptedException, IOException {
			int maxSleepTry = 10;

			int currentTry = maxSleepTry;
			byte[] readBuffer = new byte[1000];

			StringBuilder sb = new StringBuilder();
			try {
				while(currentTry>0 && sb.length()<getMessageSize()){
					//faccio un tentativo
					Thread.sleep(100);
//					int available = input.available();
					 int available = serialPort.getInputBufferBytesCount();
					if(available>0){
						//logger.debug(getTag(), "offset : "+offset+" maxReadable : "+maxReadable);
//						int read = input.read(readBuffer);
//						sb.append(new String(readBuffer, 0, read).trim());
						
						String readString = serialPort.readString(available);
						sb.append(readString.trim());
						
						
					}
					currentTry--;
				}

			} catch (Exception e) {
				logger.error(getTag(), "Receiver Logger su " + portName
						+ ": Generic error while using port", e);
//				EventBusService.publish(new Events.PortEvent(serialPort,
//						Events.PortEvent.ERROR_GENERIC, e));

			}
			
			String line = sb.toString(); // "-<\"SOTTO1\"T_0B6_000_B3";
			logger.debug(getTag(), "Receiver Logger letto: \"["+line+"]\" size: \""+ ((line==null) ? "null ": line.length())+"\" pause necessarie : "+ (maxSleepTry -currentTry));
			if (line != null && !line.trim().isEmpty()) {
				return line;
			} else {
				return null;
			}
		}
		
		protected void write(String cmd) throws InterruptedException, IOException {
			try {
//				input.skip(input.available());// scarto porcheria ricevuta prima della richiesta
//				output.write(cmd.getBytes());
//				output.flush();
				
				 try {
					serialPort.writeString(cmd.trim());
				} catch (SerialPortException e) {
					logger.error(getTag(), "Receiver Logger su " + portName
							+ ": write error ", e);
				}
				
				
			}  catch (Exception e) {
				logger.error(getTag(), "Receiver Logger su " + portName
						+ ": write error ", e);
//				EventBusService.publish(new Events.PortEvent(serialPort,
//						Events.PortEvent.ERROR_GENERIC, e));	
//				throw e;
			}
		}
		
	}
	
}
