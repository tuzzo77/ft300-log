package com.econorma.io;


import org.apache.log4j.MDC;

import com.econorma.Application;
import com.econorma.data.LoggerResponse;
import com.econorma.data.Sensore;
import com.econorma.io.ParsersLogger.StatusResponse;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

import jssc.SerialPort;
import jssc.SerialPortException;


public class VerificaLogger {
	
	private static final Logger logger = Logger.getLogger(VerificaLogger.class);
	private static final String TAG = VerificaLogger.class.getSimpleName();

	private SerialPort serialPort;
	private Sensore sensore;

	public VerificaLogger (Sensore sensore) {
		this.sensore = sensore;
	}

	public LoggerResponse verifica() throws InterruptedException {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			/* */
		}
		
		boolean check = false;
		LoggerResponse response = null;
		try {
			String portName = Application.getInstance().getPreferences().getPort();
			int baudRate = Application.getInstance().getPreferences().getBaudRate();
			boolean connected = connect(portName, baudRate);
		
			MDC.put("event_name", "Verifica Logger");
			if(connected){
				
				final int MAX_RETRY = 2;
				int error_count = 0;
				while(true){
					String cmd = ">" + "dR??!\r\n";
			
					logger.debug(TAG, "Richiesta: "+ cmd);
					serialPort.writeString(cmd.trim());
		
					Thread.sleep(2000);
//					Thread.sleep(1000);
					String lines = null;
					int available = serialPort.getInputBufferBytesCount();
					if(available>0){
						lines = serialPort.readString(available).trim();
					}
					logger.debug(TAG, "Stringa ricevuta: " + lines);
					
					if (lines==null || lines.trim().length()==0) {
						check = false;
						break;
					}
					
					response = StatusResponse.buildStatusResponse(lines, null);
					if(response.isValid()) {
						logger.info(TAG, "sensore presente "+response.getIdSonda());
						logger.log(LoggerCustomLevel.AUDIT, "sensore presente "+response.getIdSonda());
						check = true;
						break;
					}else{
						logger.debug(TAG, "ricevuta risposta da un sensore imprevisto...");
					}
					
					if(!check){
						error_count++;
					}
					if(error_count>=MAX_RETRY){
						break;
					}
				}
			}
		} catch (SerialPortException e) {
			logger.error(TAG, e.getPortName()+ ": write error ", e);
		}finally{
			disconnect();
		}
		return response;
	}
	
	
	private void disconnect(){
		if(serialPort!=null) { 
			try {
				serialPort.closePort();
			} catch (SerialPortException e) {
				/* ignore */
				logger.debug(TAG,"Error While Closing Port", e);
			}
			logger.debug(TAG, "Porta seriale "+serialPort.getPortName()+" chiusa");	
			serialPort = null;
		}
	}

	private boolean connect(String portName, int baudRate) {
		
		logger.debug(TAG, "connecting on "+portName);
		try {
			if(serialPort!=null){
				logger.info(TAG, "chiudo la seriale gia aperta..."+ portName);
				try{serialPort.closePort();}catch (Exception e) {
					logger.error(TAG, e);
				}
			}

			SerialPort serialPort = new SerialPort(portName);

			boolean opened = false;
			try{
				opened = serialPort.openPort();
				this.serialPort = serialPort;
				logger.debug(TAG, portName +" "+(opened ?  "opened " : "not opened"));
				if(opened){
					serialPort.setParams(baudRate,
							SerialPort.DATABITS_8,
							SerialPort.STOPBITS_1,
							SerialPort.PARITY_NONE);
					return true;
				}
			}catch(SerialPortException spe){
				logger.error(TAG, "ERRORE SERIALE :"+spe.getMessage(),spe);
			}
			return false;
		} catch (Exception e) {
			logger.fatal(TAG, "Receiver su " + portName+ ": Generic error while using port", e);
			return false;
		}
	}

}