package com.econorma.io;

import java.awt.EventQueue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.MDC;

import com.adamtaft.eb.EventBusService;
import com.econorma.data.Lettura;
import com.econorma.data.Sensore;
import com.econorma.io.Receiver.ReadListener;
import com.econorma.logic.SensoreManager;
import com.econorma.ui.Events;
import com.econorma.ui.Events.DiscoveryEndEvent;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

public class LettoreDiscoveryLogger extends LettoreLogger {

	
	private Map<String,Lettura> ultime = new HashMap<String,Lettura>();
	
	private boolean ok = false;
	private int correct = 0;
	private List<String> sensoriTrovati = new ArrayList<String>();
	
	private static final Logger logger = Logger.getLogger(LettoreDiscoveryLogger.class);
	
	public LettoreDiscoveryLogger(String com, int baudRate, ReadListener listener) {
		super(com,baudRate,listener,logger);
	}

	@SuppressWarnings("finally")
	@Override
	public int runLogic() throws InterruptedException, IOException {
		try{
			sensoriTrovati.clear();
			HashSet<String> ids = new HashSet<String>();
			HashMap<Integer,String> mapping = new HashMap<Integer, String>();
			correct=0;
			
			MDC.put("event_name", "Lettore Discovery Logger");
			
			ok = true;
			for(int i = LOW_ADD; i<=HIGH_ADD; i++){
				
				logger.debug(getTag(), "discovery indirizzo : \""+i+"\"");
				
				write(i);
				String line1 = read(2000);
				write(i);
				String line2 = read(2000);
				logger.debug(getTag(), "   line1 : \""+line1+"\"");
				logger.debug(getTag(), "   line2 : \""+line2+"\"");
				if(line1!=null && line2!=null){
					
					
					//ho il sensore
					Lettura l1 = Parsers.createLettura(line1);
					Lettura l2 = Parsers.createLettura(line2);
					if(l2!=null && l2!=null && l1.getIdSonda().equals(l2.getIdSonda())){
						// indirizzo i = sonda idSonda
						if(!ids.contains(l1.getIdSonda())){
							mapping.put(i, l1.getIdSonda());
							ultime.put(l2.getIdSonda(), l2);
							logger.debug(getTag(), "Aggiunta sonda: " + l1.getIdSonda());
							logger.log(LoggerCustomLevel.AUDIT, "Aggiunta Sonda: " + l1.getIdSonda() + " indirizzo: " + i);
							ok = true;
							correct++;
						}else{
							ok = false;
						}
					}else{
						i--;
					}
				}else if((line1!=null && line2 ==null) || (line2!=null && line1 ==null)){
					ok = false;
				}else{
					// line1 == null && line2 == null
					//ok 
				}
			}
			
//			if(ok){
			if(correct>0){
				for(Map.Entry<Integer,String> e: mapping.entrySet()){
					String id_sonda = e.getValue();
					int indirizzo = e.getKey();
					Sensore sensore = Sensore.newInstance(id_sonda);
					SensoreManager.getInstance().addUnknownIfNew(sensore);
					SensoreManager.getInstance().aggiornaIndirizzo(id_sonda, indirizzo);
					sensoriTrovati.add(id_sonda);
					logger.debug(getTag(), "Scrittura indirizzo sensore: " + id_sonda);
//					SensoreManager.getInstance().updateState(sensore);
				}
				
					SensoreManager.getInstance().saveState();
			}
		}finally{
			die();
			return 0;
		}
	}
	
	@Override
	public void onEnded() {
		super.onEnded();
		DiscoveryEndEvent discoveryEndEvent = new Events.DiscoveryEndEvent(ok, sensoriTrovati);
		logger.info(getTag(), discoveryEndEvent.toString());
//		if(ok){
			if(correct>0){
			if (getReadListener() != null) {
				// run on the main thread
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						for (Lettura l : ultime.values()){
							l.setDiscovery(true);
							getReadListener().onRead(l);
						}
					}
				});
			}
			
		}
		EventBusService.publish(discoveryEndEvent);
	}

}
