package com.econorma.io;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.econorma.data.Lettura;
import com.econorma.data.Lettura.TipoMisura;
import com.econorma.data.LoggerResponse;
import com.econorma.data.SampleResponse;
import com.econorma.data.Sensore;
import com.econorma.util.Logger;
import com.econorma.util.StringUtils;

public class CopyParsersLogger {


	private static final Pattern LOGGER_READ_REGEX = Pattern.compile(getReadPattern());
	

	private static final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmmss");

	private static final int SAMPLE_1 = 3;
	private static final int SAMPLE_2 = 4;
	private static final int SAMPLE_3 = 5;
	private static final int SAMPLE_4 = 6;
	private static final int SAMPLE_5 = 7;
	private static final int LOGGER_NOME_SONDA_POS = 2;
	private static final int LOGGER_TIPO_LETTURA = 2;

	private static final int LOGGER_CHECKSUM_POS = 18;

	private static final Logger logger = Logger.getLogger(CopyParsersLogger.class);
	private static final String TAG = CopyParsersLogger.class.getSimpleName();


	public static boolean isSensorReadMessage(String sensorName, String raw){
		if(StringUtils.isSome(raw)){
			logger.info(TAG, "Regex" + LOGGER_READ_REGEX.toString() + "|" + raw.length());
			Matcher matcher = LOGGER_READ_REGEX.matcher(raw);
			if(matcher.find()){
				logger.info(TAG, "Regex: " + true);
				String id_sonda = Sensore.LOGGER + matcher.group(LOGGER_NOME_SONDA_POS);
				logger.info(TAG, "SensorName: " + id_sonda);
				return Objects.equals(id_sonda, sensorName);
			}else
				logger.info(TAG, "Regex: " + false);
			return false;
		}else{
			return false;
		}	

	}

	/**
	 * il messaggio e' valido, dal sensore esaminato, e non contiene dati
	 * @param sensorName
	 * @param raw
	 * @return
	 */
	public static boolean isReadEndToken(String sensorName, String raw){
		if(StringUtils.isSome(raw)){
			Matcher matcher = LOGGER_READ_REGEX.matcher(raw);
			if(matcher.find()){

				String sample1 = matcher.group(SAMPLE_1);
				String sample2 = matcher.group(SAMPLE_2);
				String sample3 = matcher.group(SAMPLE_3);
				String sample4 = matcher.group(SAMPLE_4);
				String sample5 = matcher.group(SAMPLE_5);
				String id_sonda = Sensore.LOGGER + matcher.group(LOGGER_NOME_SONDA_POS);
				boolean containSensorName = Objects.equals(sensorName, id_sonda);
				boolean fullEmptyPattern = sample1.contains("---------------") && sample2.contains("---------------")&& sample3.contains("---------------")&& sample4.contains("---------------")&& sample5.contains("---------------");

				logger.debug(TAG, "Response: " + fullEmptyPattern);
				return containSensorName && fullEmptyPattern;
			}else{
				logger.debug(TAG, "Response: " + false);
				return false;
			}
		}else
			return false;
	}

	public static List<Lettura> createLettura(String raw) {

		List<Lettura> letture = new ArrayList<Lettura>();

		//  -(e>"B28056"T_0CE_000_BC4
		// -<"LOG-01"T_130613165737/0F2/000_130613170237/0F2/000_130613170737/0F2/000_130613171237/0F2/000_130613171737/0F2/0000C

		//			raw = "-<\"LOG-01\"T_000000000000/000/000_000015130142/000/000_000015130142/000/000_000015130140/000/000_000015130142/000/00028";

		boolean checkSum = checkCheckSum(raw);
		logger.debug(TAG, "CheckSum result:" +  checkSum);
		if (!checkSum)
			return null;


		Matcher matcher = LOGGER_READ_REGEX.matcher(raw);
		if(matcher.find()){

			double temperatura = Double.MIN_VALUE;
			double umidita = 0d;
			Lettura.TipoMisura tipoMisura; 

			String id_sonda = Sensore.LOGGER + matcher.group(LOGGER_NOME_SONDA_POS);
			String tipo_lettura = TipoMisura.TEMPERATURA_UMIDITA.name();
			String checksum = matcher.group(LOGGER_CHECKSUM_POS);

			logger.debug(TAG, "Parser: " +  id_sonda + "|" + tipo_lettura + "|" + checksum);
			int error=0;

			for (int x = 3; x < 1027; x++) {

				String stringa = matcher.group(x);

				if (stringa.contains("---------------------")) {
					if (error>5) {
						logger.debug(TAG, "Exit loop");
						break;
					}
					error++;
					continue;
				}

				logger.debug(TAG, "Sample: " + stringa);
				SampleResponse sample = buildSampleResponse(stringa);

				Lettura lettura = new Lettura();
				lettura.setTipoMisura(TipoMisura.TEMPERATURA_UMIDITA);
				lettura.setStatoBatteria("a");
				lettura.setTemperaturaGrezza(sample.getTemperature());
				lettura.setUmiditaGrezza(sample.getHumidity());
				lettura.setIdSonda(id_sonda);
				lettura.setData(sample.getData());
				letture.add(lettura); 
				logger.debug(TAG, "Lettura aggiunta");

			}

			return letture;


		}else {
			// ho ricevuto una stringa corrotta
		}

		return null;
	}

	public static SampleResponse buildSampleResponse(String lines){

		SampleResponse sample = null;
		String type = null;
		double temperatura = Double.MIN_VALUE;
		double umidita = 0d;
		String data = null;

		try {
			type = lines.substring(0, 1); 

			String tempHex = lines.substring(13, 17);
			int temperatura10 = Integer.parseInt(tempHex,16);
			if (temperatura10 > 56000) {
				temperatura10 = (-1 * (65536 - temperatura10));
			}
			temperatura = temperatura10 / 100d;

			String urHex = lines.substring(17, 21);
			int umiditaUr = Integer.parseInt(urHex,16);
			//			umidita = Umidita.parse(umiditaUr, temperatura);
			umidita = Math.round(umiditaUr*10.0)/10.0;

			String dataHex = lines.substring(1, 13);

			int hh = Integer.parseInt(dataHex.substring(0,2),16);
			int mi = Integer.parseInt(dataHex.substring(2,4),16);
			int ss = Integer.parseInt(dataHex.substring(4,6),16);
			int gg = Integer.parseInt(dataHex.substring(6,8),16);
			int mm = Integer.parseInt(dataHex.substring(8,10),16);
			int aa = Integer.parseInt(dataHex.substring(10,12),16);
			data = String.format("%02d", gg) +String.format("%02d", mm)+String.format("%02d", aa)+String.format("%02d", hh)+String.format("%02d", mi)+String.format("%02d", ss);

			sample = new SampleResponse();
			sample.setType(type);
			sample.setData(sdf.parse(data));
			sample.setTemperature(temperatura);
			sample.setHumidity(umidita);
			return sample;

		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		}



	}

	private static boolean checkCheckSum(String s) {
		if (s == null)
			return false; 

		int FirstGreater = s.indexOf("<");

		String Stringa = null;
		if (FirstGreater >= 0 && s.length() >= 0) {
			Stringa = s.substring(0, s.trim().length()-2); 

			int sum = 0;
			int size = Stringa.length();
			for (int i = 0; i < size; i++) {
				sum = sum + (int) Stringa.charAt(i);
			}

			String check_sum1 = Integer.toHexString(sum);
			String check_sum = check_sum1.substring(check_sum1.length()-2, check_sum1.length());

			String stringa_check_sum = s.substring(s.trim().length()-2,
					s.trim().length());

			logger.debug(TAG,"Cheksum calculated: " + check_sum1);
			logger.debug(TAG,"Cheksum device: " + stringa_check_sum);

			boolean result = stringa_check_sum.toUpperCase().equals(
					check_sum.toUpperCase());
			return result;
		} else
			return false;
	}


	public static class StatusResponse{

		//		<dR_ZE3_a_173018060114_0E10_0A30_0038_000065_1D
		private static final Pattern LOGGER_CHECK_REGEX = Pattern.compile("(.*)_(...)_(.)_([a-zA-Z0-9]{12})_(....)_(....)_(....)_(......)_(..)");

		private static final int POS_SENSORE = 2;
		private static final int POS_STATO_BATTERIA = 3;
		private static final int POS_DATA = 4;
		private static final int POS_TRASMISSION = 5;
		private static final int POS_TEMP = 6;
		private static final int POS_UR = 7;
		private static final int POS_CAMPIONI = 8;
		private static final int POS_FULL = 9;

		public static LoggerResponse buildStatusResponse(String lines, Sensore sensore){

			String full = null;
			String idSonda = null;
			String statoBatteria = null;
			int trasmission = 0;
			int campioni = 0;
			double temperatura = Double.MIN_VALUE;
			double umidita = 0d;
			String data = null;
			boolean isValid = false;
			LoggerResponse response =null;

			try {


				if(StringUtils.isSome(lines)){
					Matcher matcher = LOGGER_CHECK_REGEX.matcher(lines);
					logger.info(TAG, "Line to parse: " + lines);
					if(matcher.find()){
						full = matcher.group(POS_FULL); //TODO calcolare il checksum qui
						statoBatteria = matcher.group(POS_STATO_BATTERIA);
						idSonda = matcher.group(POS_SENSORE);
						String trasmission_hex = matcher.group(POS_TRASMISSION);
						trasmission = Integer.parseInt(trasmission_hex,16);

						String campioni_hex = matcher.group(POS_CAMPIONI);
						campioni = Integer.parseInt(campioni_hex,16);

						String tempHex = matcher.group(POS_TEMP);
						int temperatura10 = Integer.parseInt(tempHex,16);
						if (temperatura10 > 56000) {
							temperatura10 = (-1 * (65536 - temperatura10));
						}
						temperatura = temperatura10 / 100d;

						String urHex = matcher.group(POS_UR);	
						int umiditaUr = Integer.parseInt(urHex,16);
						//					umidita = Umidita.parse(umiditaUr, temperatura);
						umidita = Math.round(umiditaUr*10.0)/10.0;

						String dataHex = matcher.group(POS_DATA);

						int hh = Integer.parseInt(dataHex.substring(0,2),16);
						int mi = Integer.parseInt(dataHex.substring(2,4),16);
						int ss = Integer.parseInt(dataHex.substring(4,6),16);
						int gg = Integer.parseInt(dataHex.substring(6,8),16);
						int mm = Integer.parseInt(dataHex.substring(8,10),16);
						int aa = Integer.parseInt(dataHex.substring(10,12),16);
						data = String.format("%02d", gg) +String.format("%02d", mm)+String.format("%02d", aa)+String.format("%02d", hh)+String.format("%02d", mi)+String.format("%02d", ss);
						if(idSonda != null){
							logger.info(TAG, "sensore presente "+idSonda + "|" + "Data " + data + "|" + statoBatteria);
							isValid = true;
						}else{
							logger.debug(TAG, "ricevuta risposta da un sensore imprevisto...");
						}
					}
				}

				response = new LoggerResponse();
				response.setIdSonda(idSonda);
				response.setFull(full);
				response.setStatoBatteria(statoBatteria);
				response.setTrasmission(trasmission);
				response.setValid(isValid);
				response.setData(sdf.parse(data));
				response.setTemperature(temperatura);
				response.setHumidity(umidita);
				response.setSamples(campioni);

			} catch (Exception e) {
				logger.error(TAG, e);
			}


			return response;
		} 
	}

	public static String getReadPattern(){
		String start = "(.*)_(...)_";
		String samples = "";
		String end =  "(..)";
		for (int i = 0; i < 1024; i++) {
			samples = samples.trim() +  "([a-zA-Z0-9-\\-]{21})_";
		}

		String pattern = start.trim() + samples.trim() + end.trim();
		return pattern;
	}


}
