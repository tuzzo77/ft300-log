package com.econorma.io;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.Lettura.TipoMisura;
import com.econorma.data.LoggerResponse;
import com.econorma.data.SampleResponse;
import com.econorma.data.Sensore;
import com.econorma.io.ParsersLogger.StatusResponse.VERSION;
import com.econorma.logic.SensoreManager;
import com.econorma.util.Logger;
import com.econorma.util.StringUtils;

public class ParsersLogger {


	private static final Pattern LOGGER_READ_REGEX_V1 = Pattern.compile("((.*)_(...)_(([a-zA-Z0-9-\\-]{34}\\_){1024})(..))");
	private static final Pattern LOGGER_READ_REGEX_V2 = Pattern.compile("((.*)_(...)_(([a-zA-Z0-9-\\-]{34}\\_){1024})(......)_(..))");
	private static Pattern LOGGER_READ;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmmss");

	private static final int LOGGER_NOME_SONDA_POS = 3;
	private static final int LOGGER_SAMPLES = 4;
	private static final int LOGGER_LAST_SAMPLE = 5;
	private static final int LOGGER_CHECKSUM_POS = 6;
	
	private static final double BASE_VOLT = 10.00;
	private static final double BASE_MA = 20.00;

	private static final Logger logger = Logger.getLogger(ParsersLogger.class);
	private static final String TAG = ParsersLogger.class.getSimpleName();
	
	public static Pattern getVersionPattern(String sensorName) {
		Pattern pattern = null;
		Sensore sensore = SensoreManager.getInstance().getSensoreById(sensorName);
		VERSION versione = sensore.getVersione();
		
		switch (versione) {
		case V1:
			pattern = LOGGER_READ_REGEX_V1;
			break;
		case V2:
			pattern = LOGGER_READ_REGEX_V2;
			break;
		default:
			pattern = LOGGER_READ_REGEX_V1;
			break;
		}
		return pattern;
	}


	public static boolean isSensorReadMessage(String sensorName, String raw){
		if(StringUtils.isSome(raw)){
			
			LOGGER_READ = getVersionPattern(sensorName);
			
			logger.debug(TAG, "Regex" + LOGGER_READ.toString() + "|" + raw.length());
			Matcher matcher = LOGGER_READ.matcher(raw);
			if(matcher.find()){
				logger.debug(TAG, "Regex: " + true);
				String id_sonda = (Sensore.LOGGER + matcher.group(LOGGER_NOME_SONDA_POS)).toUpperCase();
				logger.debug(TAG, "SensorName: " + id_sonda);
				return Objects.equals(id_sonda, sensorName);
			}else
				logger.debug(TAG, "Regex: " + false);
			return false;
		}else{
			return false;
		}	

	}

	/**
	 * il messaggio e' valido, dal sensore esaminato, e non contiene dati
	 * @param sensorName
	 * @param raw
	 * @return
	 */
	public static boolean isReadEndToken(String sensorName, String raw){
		if(StringUtils.isSome(raw)){
			LOGGER_READ = getVersionPattern(sensorName);
			Matcher matcher = LOGGER_READ.matcher(raw);
			if(matcher.find()){
				String id_sonda = (Sensore.LOGGER + matcher.group(LOGGER_NOME_SONDA_POS)).toUpperCase();
				String samples = matcher.group(LOGGER_SAMPLES) + matcher.group(LOGGER_LAST_SAMPLE);
				
				boolean hasDigit = samples.matches(".*\\d+.*");
				
				boolean containSensorName = Objects.equals(sensorName, id_sonda);
		
				logger.debug(TAG, "Response has digit: " + hasDigit);
				return containSensorName && !hasDigit;
			}else{
				logger.debug(TAG, "Response: " + false);
				return false;
			}
		}else
			return false;
	}

	public static List<Lettura> createLettura(String raw) {

		List<Lettura> letture = new ArrayList<Lettura>();

		boolean checkSum = checkCheckSum(raw);
		logger.debug(TAG, "CheckSum result:" +  checkSum);
		if (!checkSum)
			return null;


		Matcher matcher = LOGGER_READ.matcher(raw);
		if(matcher.find()){

			double temperatura = Double.MIN_VALUE;
			double umidita = 0d;
			Lettura.TipoMisura tipoMisura; 

			String id_sonda = (Sensore.LOGGER + matcher.group(LOGGER_NOME_SONDA_POS)).toUpperCase();
			String tipo_lettura = TipoMisura.TEMPERATURA_UMIDITA.name();
			String checksum = matcher.group(LOGGER_CHECKSUM_POS);

			logger.debug(TAG, "Parser: " +  id_sonda + "|" + tipo_lettura + "|" + checksum);
			int error=0;
			
			String lines = matcher.group(LOGGER_SAMPLES) + matcher.group(LOGGER_LAST_SAMPLE);
			
			String[] samples = lines.split("_");
			for (String stringa : samples) {
			 
				stringa = stringa.replaceAll("_", "");
				
				if (stringa.contains("---------------------")) {
					if (error>5) {
						logger.debug(TAG, "Exit loop");
						break;
					}
					error++;
					continue;
				}
		 
//				logger.debug(TAG, "Sample: " + stringa);
				SampleResponse sample = buildSampleResponse(stringa);

				Lettura lettura = new Lettura();
				lettura.setTipoMisura(TipoMisura.TEMPERATURA_UMIDITA);
				lettura.setStatoBatteria("a");
				lettura.setTemperaturaGrezza(sample.getTemperature());
				lettura.setUmiditaGrezza(sample.getHumidity());
				lettura.setVoltGrezzo(sample.getVolt());
				lettura.setMilliampereGrezzo(sample.getMilliampere());
				lettura.setOhmGrezzo(sample.getOhm());
				lettura.setApertoChiuso(sample.getApertoChiuso());
				lettura.setIdSonda(id_sonda);
				lettura.setData(sample.getData());
				lettura.setSensore(SensoreManager.getInstance().getSensoreById(id_sonda));
				letture.add(lettura); 
				logger.debug(TAG, "Lettura aggiunta");

			}

			return letture;


		}else {
			// ho ricevuto una stringa corrotta
		}

		return null;
	}

	public static SampleResponse buildSampleResponse(String lines){

		SampleResponse sample = null;
		String type = null;
		double temperatura = Double.MIN_VALUE;
		double umidita = 0d;
		double volt = 0d;
		double milliampere = 0d;
		double ohm = 0d;
		String apertoChiuso = null;
		String data = null;

		try {
			type = lines.substring(0, 1); 

			String tempHex = lines.substring(13, 17);
			int temperatura10 = Integer.parseInt(tempHex,16);
			if (temperatura10 > 56000) {
				temperatura10 = (-1 * (65536 - temperatura10));
			}
			temperatura = temperatura10 / 100d;

			String urHex = lines.substring(17, 21);
			int umiditaUr = Integer.parseInt(urHex,16);
			umidita = Math.round(umiditaUr*10.0)/10.0;
			
			String voltHex = lines.substring(21, 25);	
			int voltInt = Integer.parseInt(voltHex,16);
			volt = Math.round(voltInt*10.0)/10.0;
			double voltA = (volt*BASE_VOLT)/4096;
			
			
			String maHex =  lines.substring(25, 29);	
			int maInt = Integer.parseInt(maHex,16);
			milliampere = Math.round(maInt*10.0)/10.0;
			double mA = (milliampere*BASE_MA)/4096;
			
			String ohmHex =  lines.substring(29, 33);	
			int ohmInt = Integer.parseInt(ohmHex,16);
			ohm = ohmInt/100.0; 
			ohm = getRealTemp(ohm);

			apertoChiuso = lines.substring(33, 34);	
			

			if (!Application.getInstance().isOhm()) {
				ohm = 0.0;	
			}
			if (!Application.getInstance().isVolt()) {
				volt = 0.0;
			}
			if (!Application.getInstance().isApertoChiuso()) {
				apertoChiuso = "0";
			}
			if (!Application.getInstance().isMilliampere()) {
				milliampere = 0.0;
			}
			
			String dataHex = lines.substring(1, 13);
			
			int hh = Integer.parseInt(dataHex.substring(0,2),16);
			int mi = Integer.parseInt(dataHex.substring(2,4),16);
			int ss = Integer.parseInt(dataHex.substring(4,6),16);
			int gg = Integer.parseInt(dataHex.substring(6,8),16);
			int mm = Integer.parseInt(dataHex.substring(8,10),16);
			int aa = Integer.parseInt(dataHex.substring(10,12),16);
			data = String.format("%02d", gg) +String.format("%02d", mm)+String.format("%02d", aa)+String.format("%02d", hh)+String.format("%02d", mi)+String.format("%02d", ss);
			logger.info(TAG, "Data letta: " + data);
			
			
			sample = new SampleResponse();
			sample.setType(type);
			sample.setData(sdf.parse(data));
			sample.setTemperature(temperatura);
			sample.setHumidity(umidita);
			sample.setVolt(voltA);
			sample.setMilliampere(mA);
			sample.setOhm(ohm);
			sample.setApertoChiuso(apertoChiuso);
			return sample;

		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		}

	}

	private static boolean checkCheckSum(String s) {
		if (s == null)
			return false; 

		int FirstGreater = s.indexOf("<");

		String Stringa = null;
		if (FirstGreater >= 0 && s.length() >= 0) {
			Stringa = s.substring(0, s.trim().length()-2); 

			int sum = 0;
			int size = Stringa.length();
			for (int i = 0; i < size; i++) {
				sum = sum + (int) Stringa.charAt(i);
			}

			String check_sum1 = Integer.toHexString(sum);
			String check_sum = check_sum1.substring(check_sum1.length()-2, check_sum1.length());

			String stringa_check_sum = s.substring(s.trim().length()-2,
					s.trim().length());

			logger.debug(TAG,"Cheksum calculated: " + check_sum1);
			logger.debug(TAG,"Cheksum device: " + stringa_check_sum);

			boolean result = stringa_check_sum.toUpperCase().equals(
					check_sum.toUpperCase());
			return result;
		} else
			return false;
	}


	public static class StatusResponse{

		//		<dR_ZE3_a_173018060114_0E10_0A30_0038_000065_1D
		// NEW !! <dR_c9A_d_022523040214_000A_000A_0894_0029_0000_0000_2E93_0_000001_55
		private static final Pattern LOGGER_CHECK_REGEX_V1 = Pattern.compile("(.*)_(...)_(.)_([a-zA-Z0-9]{12})_(....)_(....)_(....)_(....)_(....)_(....)_(....)_(.)_(......)_(..)");
		private static final Pattern LOGGER_CHECK_REGEX_V2 = Pattern.compile("(.*)_(...)_(.)_([a-zA-Z0-9]{12})_(....)_(....)_(....)_(....)_(....)_(....)_(....)_(.)_(......)_(......)_(..)");

		private static final int POS_SENSORE = 2;
		private static final int POS_STATO_BATTERIA = 3;
		private static final int POS_DATA = 4;
		private static final int POS_TRASMISSION = 5;
		private static final int POS_TRASMISSIONRF = 6;
		private static final int POS_TEMP = 7;
		private static final int POS_UR = 8;
		private static final int POS_VOLT = 9;
		private static final int POS_MA = 10;
		private static final int POS_OHM = 11;
		private static final int POS_AC = 12;
		private static final int POS_CAMPIONI = 13;
		private static final int POS_SENSORS_ARRAY = 14;
		private static final int POS_FULL_V2 = 15;
		private static final int POS_FULL_V1 = 14;
		
		public static enum VERSION {
			V1, V2, V3
		}

		public static LoggerResponse buildStatusResponse(String lines, Sensore sensore){

			String full = null;
			String idSonda = null;
			String statoBatteria = null;
			int trasmission = 0;
			int campioni = 0;
			double temperatura = Double.MIN_VALUE;
			double umidita = 0d;
			double voltA = 0d;
			double mA = 0d;
			double ohm = 0d;
			String apertoChiuso = null;
			String data = null;
			boolean isValid = false;
			LoggerResponse response =null;
			String sensorsArray=null;
			VERSION version = VERSION.V1;
			boolean match = false;

			try {

				if(StringUtils.isSome(lines)){
					Matcher matcher = LOGGER_CHECK_REGEX_V2.matcher(lines);
					logger.debug(TAG, "Line to parse: " + lines);
					if (matcher.find()){
						version = VERSION.V2;
						match = true;
					}
					
					if(!match){
						matcher = LOGGER_CHECK_REGEX_V1.matcher(lines);
						if (matcher.find()){
							version = VERSION.V1;
							match = true;
						}
					}
					if(match){
						if (version==VERSION.V1) {
							full = matcher.group(POS_FULL_V1); //TODO calcolare il checksum qui	
						} else {
							full = matcher.group(POS_FULL_V2); //TODO calcolare il checksum qui
						}
						
						statoBatteria = matcher.group(POS_STATO_BATTERIA);
						idSonda = matcher.group(POS_SENSORE);
						String trasmission_hex = matcher.group(POS_TRASMISSION);
						trasmission = Integer.parseInt(trasmission_hex,16);

						String campioni_hex = matcher.group(POS_CAMPIONI);
						campioni = Integer.parseInt(campioni_hex,16);

						String tempHex = matcher.group(POS_TEMP);
						int temperatura10 = Integer.parseInt(tempHex,16);
						if (temperatura10 > 56000) {
							temperatura10 = (-1 * (65536 - temperatura10));
						}
						temperatura = temperatura10 / 100d;

						String urHex = matcher.group(POS_UR);	
						int umiditaUr = Integer.parseInt(urHex,16);
						umidita = Math.round(umiditaUr*10.0)/10.0;
						
						String voltHex = matcher.group(POS_VOLT);	
						int voltInt = Integer.parseInt(voltHex,16);
						double volt = Math.round(voltInt*10.0)/10.0;
						voltA = (volt*BASE_VOLT)/4096;
						
						
						String maHex = matcher.group(POS_MA);	
						int maInt = Integer.parseInt(maHex,16);
						double milliampere = Math.round(maInt*10.0)/10.0;
						mA = (milliampere*BASE_MA)/4096;
						
						String ohmHex = matcher.group(POS_OHM);	
						int ohmInt = Integer.parseInt(ohmHex,16);
						ohm = getRealTemp(ohmInt/100);

						String dataHex = matcher.group(POS_DATA);
						apertoChiuso = matcher.group(POS_AC);
						
						if (version==VERSION.V2) {
							sensorsArray = matcher.group(POS_SENSORS_ARRAY);	
						} else {
							sensorsArray = null;	
						}
						
					 
						int hh = Integer.parseInt(dataHex.substring(0,2),16);
						int mi = Integer.parseInt(dataHex.substring(2,4),16);
						int ss = Integer.parseInt(dataHex.substring(4,6),16);
						int gg = Integer.parseInt(dataHex.substring(6,8),16);
						int mm = Integer.parseInt(dataHex.substring(8,10),16);
						int aa = Integer.parseInt(dataHex.substring(10,12),16);
						data = String.format("%02d", gg) +String.format("%02d", mm)+String.format("%02d", aa)+String.format("%02d", hh)+String.format("%02d", mi)+String.format("%02d", ss);
						if(idSonda != null){
							logger.debug(TAG, "sensore presente "+idSonda + "|" + "Data " + data + "|" + statoBatteria);
							isValid = true;
						}else{
							logger.debug(TAG, "ricevuta risposta da un sensore imprevisto...");
						}
					}
				}

				response = new LoggerResponse();
				response.setIdSonda(idSonda);
				response.setFull(full);
				response.setStatoBatteria(statoBatteria);
				response.setTrasmission(trasmission);
				response.setValid(isValid);
				response.setData(sdf.parse(data));
				response.setTemperature(temperatura);
				response.setHumidity(umidita);
				response.setApertoChiuso(apertoChiuso);
				response.setMilliampere(mA);
				response.setOhm(ohm);
				response.setVolt(voltA);
				response.setSamples(campioni);
				response.setVersione(version);
				response.setSensorsArray(sensorsArray != null ? fromString(sensorsArray) : null);

			} catch (Exception e) {
				logger.error(TAG, e);
			}


			return response;
		} 
	}

	public static String getReadPattern(){
		String start = "(.*)_(...)_";
		String samples = "";
		String end =  "(..)";
		for (int i = 0; i < 1024; i++) {
			samples = samples.trim() +  "([a-zA-Z0-9-\\-]{21})_";
		}

		String pattern = start.trim() + samples.trim() + end.trim();
		return pattern;
	}
	
	public static double getRealTemp(double ohms) {
		
		if (ohms==0) {
			return 0.0;
		}
		
		double c0 = -245.19;
		double c1 = 2.5293;
		double c2 = -0.066046;
		double c3 = 4.0422E-3;
		double c4 = -2.0697E-6;
		double c5 = -0.025422;
		double c6 = 1.6883E-3;;
		double c7 = -1.3601E-6;
		
		
        double a = (ohms*c7) + c6;
        a = (a*ohms) + c5;
        a = (a*ohms) + 1;
        double b = (ohms*c4) + c3;
        b = (b*ohms) + c2;
        b = (b*ohms) + c1;
        b = b*ohms;
        b = (b/a) + c0;
        if (b < -200 || b > 850.00) {
        	logger.error(TAG, "Resistance Ohm out of range");
       	} else {
       		logger.debug(TAG, "Temperature from Ohm: " + b + "�C");
        }
     
        return b;
     
	}
	
	private static BitSet fromString(String binary) {
	    BitSet bitset = new BitSet(binary.length());
	    int len = binary.length();
	    for (int i = len-1; i >= 0; i--) {
	        if (binary.charAt(i) == '1') {
	            bitset.set(len-i-1);
	        }
	    }
	    return bitset;
	}



}
