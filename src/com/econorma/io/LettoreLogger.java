package com.econorma.io;

import java.io.IOException;

import com.econorma.io.Receiver.ReadListener;
import com.econorma.util.Logger;

public abstract class LettoreLogger extends ReceiverLogger.Lettore{

	protected static final int LOW_ADD = 101;
	protected static final int HIGH_ADD = 111;
	
	private static final int MESSAGE_SIZE = 23; // con carriage return
	
	public LettoreLogger(String com, int baudRate, ReadListener listener, Logger logger) {
		super(com, baudRate, listener, logger);
	}
	
	protected void write(int indirizzo) throws InterruptedException, IOException {
		char digit = (char) indirizzo;
		String cmd = ">" + digit + "D??!\r\n";
		super.write(cmd);
	}
	
	@Override
	public int getMessageSize(){
		return MESSAGE_SIZE;
	}
	
}
