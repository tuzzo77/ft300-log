package com.econorma.io;

import java.awt.EventQueue;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.SwingUtilities;

import com.econorma.data.Lettura;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.io.Receiver.ReadListener;
import com.econorma.logic.SensoreManager;
import com.econorma.util.Logger;

public class LettoreMisureLogger extends LettoreLogger {

	private static boolean polling;
	
	final private int intervallo;
	
	private static final Logger logger = Logger.getLogger(LettoreMisureLogger.class);
	
	public LettoreMisureLogger(String com, int baudRate, ReadListener listener, int intervallo) {
		super(com, baudRate, listener,logger);
		this.intervallo = intervallo*1000;
		
		polling=false;
	}
	
	
	private boolean isPolling(){
		return polling;
	}
	
	@Override
	public int runLogic() throws InterruptedException, IOException{
		if(isPolling()){
			// mando i comandi per leggere e scrivere
			long begin = System.currentTimeMillis();
			
			final Collection<Sensore> allSensori  = new ArrayList<Sensore>();
			
			try {
				SwingUtilities.invokeAndWait(new Runnable() {
					
					@Override
					public void run() {
						allSensori.addAll(SensoreManager.getInstance().getAllSensori(Location.INTERNO));
					}
				});
			} catch (InvocationTargetException e) {
				 
			 
			} catch (InterruptedException e) {
				/* ignore */
				 
			}
			
		
			for(Sensore s: allSensori){
				 
//					write(s.getIndirizzo());
					String line = read(900);
					
			 		if (line != null) {
						
//			 			EventBusService.publish(new Events.PortEvent(serialPort,Events.PortEvent.READ_EVENT));
			 			 

			 			final Lettura lettura = Parsers.createLettura(line);
						
						logger.debug(getTag(), "Receiver Logger su " + portName + ": "+ line.trim() + " check[" + (lettura != null)+ "]");
						
						if(lettura != null){
							if(lettura.getIdSonda().equals(s.getId_sonda())){
								if (getReadListener() != null) {
									// run on the main thread
									EventQueue.invokeLater(new Runnable() {
										@Override
										public void run() {
											getReadListener().onRead(lettura);
										}
									});
								}
							}else{
								logger.debug(getTag(), "lettura da un sensore diverso dal richiesto: \""+s.getId_sonda()+"\" ricevuto:\""+lettura.getIdSonda()+"\"");
							}
						}
					} else {
					 		logger.debug(getTag(), "Letta riga null");
					}
				 
			}
			
			int elapsed = (int) (System.currentTimeMillis()- begin);
			
			if(elapsed<intervallo){
				return  (intervallo - elapsed);
			}else{
				return 0;
			}
	
		}else{
			logger.info(getTag(), "Receiver Logger attivo, LETTURE BLOCCATE! (MONITORAGGIO_AUTOMATICO_LOGGER disabilitato) ");
			// il ricevitore è avviato ma dorme sempre
			return Integer.MAX_VALUE;
		}	
	}	
	
	
}