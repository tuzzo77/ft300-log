package com.econorma.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.econorma.data.AlterTable;
import com.econorma.util.Logger;

public class DAOTableStruct {

	private static final String TAG = DAOTableStruct.class.getSimpleName();

	private static final Logger logger = Logger.getLogger(DAOTableStruct.class);

	static final String TABLE_TEMPERATURE = "TEMPERATURE";
	static final String TABLE_SONDE = "SONDE";
	static final String TABLE_ALLARMI = "ALLARMI";
	static final String TABLE_PROVA = "PROVE";
	static final String TABLE_LETTURE_PROVA = "LETTURE_PROVA";
	static final String TABLE_PARAMETRI_PASTORIZZAZIONE = "PARAMETRI_PASTORIZZAZIONE";
	static final String TABLE_AUDIT = "AUDIT_LOG";
	static final String TABLE_UTENTI = "UTENTI";
	static final String TABLE_DEVICES= "DEVICES";
	static final String TABLE_PULLDOWN= "PULLDOWN";

	static final String F_ID_SONDA = "ID_SONDA TEXT";
	static final String F_TEMPERATURA="TEMPERATURA REAL";
	static final String F_DATA="DATA TIMESTAMP";
	static final String F_UMIDITA = "UMIDITA REAL";
	static final String F_TIPO_LETTURA= "TIPO_LETTURA TEXT";
	static final String F_LATITUDINE = "LATITUDINE REAL";
	static final String F_LONGITUDINE = "LONGITUDINE REAL";
	static final String F_STATO_BATTERIA = "STATO_BATTERIA TEXT";

	static final String F_ID = "ID TEXT";
	static final String F_TESTO = "TESTO TEXT";
	static final String F_MITTENTE = "MITTENTE TEXT";
	static final String F_DESTINATARIO = "DESTINATARIO TEXT";
	static final String F_SMTP = "SMTP TEXT";
	static final String F_PORT = "PORT TEXT";
	static final String F_TLS = "TLS BOOLEAN";
	static final String F_ACCOUNT = "ACCOUNT TEXT";
	static final String F_PASSWORD = "PASSWORD TEXT";
	static final String F_ATTIVODA = "ATTIVODA TIME";
	static final String F_ATTIVOA = "ATTIVOA TIME";
	static final String F_GSM = "GSM TEXT";
	static final String F_AVVISO_APERTURA_CHIUSURA = "AVVISO_APERTURA_CHIUSURA BOOLEAN";
	static final String F_AVVISO_BATTERIA_SCARICA = "AVVISO_BATTERIA_SCARICA BOOLEAN";
	static final String F_AVVISO_MANCATA_TRASMISSIONE = "AVVISO_MANCATA_TRASMISSIONE BOOLEAN";
	static final String F_SMTP_AUTHENTICATION = "SMTP_AUTHENTICATION BOOLEAN";
	static final String F_TIME_MANCATA_TRASMISSIONE = "TIME_MANCATA_TRASMISSIONE INTEGER";
	static final String F_CERT = "CERT BOOLEAN";

	static final String F_ID_PASTO = "ID TEXT";
	static final String F_UP_MINIMA = "UP_MINIMA REAL";
	static final String F_UP_MASSIMA = "UP_MASSIMA REAL";
	static final String F_TEMPERATURA_RIFERIMENTO = "TEMPERATURA_RIFERIMENTO REAL";
	static final String F_TEMPERATURA_MINIMA = "TEMPERATURA_MINIMA REAL";
	static final String F_DURATA_TRATTAMENTO = "DURATA_TRATTAMENTO TIME";
	static final String F_COEFFICENTE_Z = "COEFFICENTE_Z REAL";

	static final String F_ID_SONDA_SONDE = "ID_SONDA TEXT";
	static final String F_OFFSET = "OFFSET REAL";
	static final String F_DESCRIZIONE = "DESCRIZIONE TEXT";
	static final String F_LOCATION = "LOCATION TEXT";
	static final String F_RANGEMIN = "RANGEMIN REAL";
	static final String F_RANGEMAX = "RANGEMAX REAL";
	static final String F_RANGEMINURT = "RANGEMINURT REAL";
	static final String F_RANGEMAXURT = "RANGEMAXURT REAL";
	static final String F_INDIRIZZO = "INDIRIZZO REAL";
	static final String F_RITARDO_ALLARME = "RITARDO_ALLARME TIME";
	static final String F_UM = "UM TEXT";
	static final String F_VALORE_MIN_SONDA = "VALORE_MIN_SONDA REAL";
	static final String F_VALORE_MAX_SONDA = "VALORE_MAX_SONDA REAL";
	static final String F_VALORE_MIN_UM = "VALORE_MIN_UM REAL";
	static final String F_VALORE_MAX_UM = "VALORE_MAX_UM REAL";
	static final String F_TEMPI_TRASMISSIONE = "TEMPI_TRASMISSIONE TIME";
	static final String F_OFFSET_URT = "OFFSET_URT REAL";
	static final String F_INCERTEZZA = "INCERTEZZA REAL";
	static final String F_INCERTEZZA_URT = "INCERTEZZA_URT REAL";
	static final String F_ENABLE_ALARM = "ENABLE_ALARM BOOLEAN";
	static final String F_DAYS_OF_WEEK = "DAYS_OF_WEEK INTEGER";

	static final String F_ID_PROVA = "ID INTEGER";
	static final String F_TARGA = "TARGA VARCHAR";
	static final String F_RESPONSABILE = "RESPONSABILE VARCHAR";
	static final String F_DATA_PROVA = "DATA DATE";
	static final String F_ORA_INIZIO = "ORA_INIZIO DATETIME";
	static final String F_ORA_FINE = "ORA_FINE DATETIME";
	static final String F_DATA_SCARICO = "DATA_SCARICO DATETIME";
	static final String F_ID_SENSORI = "ID_SENSORI VARCHAR";
	static final String F_DURATA_TEORICA = "DURATA_TEORICA INTEGER";
	static final String F_DURATA_EFFETTIVA = "DURATA_EFFETTIVA INTEGER";
	static final String F_LOTTO = "LOTTO VARCHAR";
	static final String F_LABORATORIO = "LABORATORIO VARCHAR";
	static final String F_NUMERO_PROVA = "NUMERO_PROVA VARCHAR";

	static final String F_ID_LETTURE_PROVA = "ID INTEGER";
	static final String F_LETTURE_PROVA_ID = "PROVA_ID INTEGER";
	static final String F_ID_SONDA_LETTURE_PROVA = "ID_SONDA VARCHAR";
	static final String F_VALORE_LETTURE_PROVA = "VALORE REAL";
	static final String F_VALORE_GREZZO_LETTURE_PROVA = "VALORE_GREZZO REAL";
	static final String F_DATA_LETTURE_PROVA = "DATA DATETIME";
	static final String F_VALORE_UMIDITA_LETTURE_PROVA = "VALORE_UMIDITA REAL";
	static final String F_TIPO_LETTURA_LETTURE_PROVA = "TIPO_LETTURA TEXT";
	static final String F_LATITUDINE_LETTURE_PROVA = "LATITUDINE REAL";
	static final String F_LONGITUDINE_LETTURE_PROVA = "LONGITUDINE REAL";
	static final String F_GRUPPO_LETTURE_PROVA = "GRUPPO INTEGER";
	static final String F_STATO_BATTERIA_LETTURE_PROVA = "STATO_BATTERIA TEXT";
	static final String F_EFFETTO_LETALE_LETTURE_PROVA = "EFFETTO_LETALE REAL";
	static final String F_F0_LETTURE_PROVA = "F0 REAL";
	static final String F_VALORE_UMIDITA_GREZZO_LETTURE_PROVA = "VALORE_UMIDITA_GREZZO REAL";
	static final String F_OFFSET_LETTURE_PROVA = "OFFSET REAL";
	static final String F_OFFSET_URT_LETTURE_PROVA = "OFFSET_URT REAL";
	static final String F_INCERTEZZA_LETTURE_PROVA = "INCERTEZZA REAL";
	static final String F_INCERTEZZA_URT_URT_LETTURE_PROVA = "INCERTEZZA_URT REAL";
	
	static final String F_ID_USER = "ID INTEGER";
	static final String F_USER = "USER TEXT";
	static final String F_PASSWORD_USER = "PASSWORD TEXT";
	static final String F_NAME_USER = "NAME TEXT";
	static final String F_TYPE_USER = "TYPE TEXT";
	static final String F_LANGUAGE_USER = "LANGUAGE TEXT";
	
	static final String F_ID_AUDIT = "ID INTEGER";
	static final String F_DATA_AUDIT = "DATA DATETIME";
	static final String F_USER_AUDIT = "USER TEXT";
	static final String F_COMPUTER_AUDIT = "HOSTNAME TEXT";
	static final String F_EVENT_NAME_AUDIT = "EVENT_NAME TEXT";
	static final String F_EVENT_MESSAGGE_AUDIT = "EVENT_MESSAGGE TEXT";
	static final String F_EVENT_CLASS_AUDIT = "EVENT_CLASS TEXT";
	
	static final String F_ID_DEVICES = "ID INTEGER";
	static final String F_NAME_DEVICES = "NAME TEXT";
	static final String F_PLATFORM = "PLATFORM TEXT";
	static final String F_REGISTRATION_ID = "REGISTRATION_ID TEXT";
	
	private static final String F_ID_PULLDOWN = "ID INTEGER";
	private static final String F_CLASSE = "CLASSE TEXT";
	private static final String F_TEMP_30="TEMP_30 INTEGER";
	private static final String F_TEMP_29="TEMP_29 INTEGER";
	private static final String F_TEMP_28="TEMP_28 INTEGER";
	private static final String F_TEMP_27="TEMP_27 INTEGER";
	private static final String F_TEMP_26="TEMP_26 INTEGER";
	private static final String F_TEMP_25="TEMP_25 INTEGER";
	private static final String F_TEMP_24="TEMP_24 INTEGER";
	private static final String F_TEMP_23="TEMP_23 INTEGER";
	private static final String F_TEMP_22="TEMP_22 INTEGER";
	private static final String F_TEMP_21="TEMP_21 INTEGER";
	private static final String F_TEMP_20="TEMP_20 INTEGER";
	private static final String F_TEMP_19="TEMP_19 INTEGER";
	private static final String F_TEMP_18="TEMP_18 INTEGER";
	private static final String F_TEMP_17="TEMP_17 INTEGER";
	private static final String F_TEMP_16="TEMP_16 INTEGER";
	private static final String F_TEMP_15="TEMP_15 INTEGER";
	 

	static final List <AlterTable> missingColumns = new ArrayList <AlterTable>();



	public static boolean comparetableStruct(Connection connection) {

		int i=0;

		String table = TABLE_TEMPERATURE;
 

		Collection<String> columnNamesActual = new ArrayList<String>();
		columnNamesActual.add(F_ID_SONDA);
		columnNamesActual.add(F_TEMPERATURA);
		columnNamesActual.add(F_DATA);
		columnNamesActual.add(F_UMIDITA);
		columnNamesActual.add(F_TIPO_LETTURA);
		columnNamesActual.add(F_LATITUDINE);
		columnNamesActual.add(F_LONGITUDINE);
		columnNamesActual.add(F_STATO_BATTERIA);


		if (!tableCompare(connection, table, columnNamesActual)){
			i++;
		}


		table = TABLE_ALLARMI;
		columnNamesActual.clear();
		columnNamesActual = new ArrayList<String>();
		columnNamesActual.add(F_ID);
		columnNamesActual.add(F_TESTO);
		columnNamesActual.add(F_MITTENTE);
		columnNamesActual.add(F_DESTINATARIO);
		columnNamesActual.add(F_SMTP);
		columnNamesActual.add(F_PORT);
		columnNamesActual.add(F_TLS);
		columnNamesActual.add(F_ACCOUNT);
		columnNamesActual.add(F_PASSWORD);
		columnNamesActual.add(F_ATTIVODA);
		columnNamesActual.add(F_ATTIVOA);
		columnNamesActual.add(F_GSM);
		columnNamesActual.add(F_AVVISO_APERTURA_CHIUSURA);
		columnNamesActual.add(F_AVVISO_BATTERIA_SCARICA);
		columnNamesActual.add(F_AVVISO_MANCATA_TRASMISSIONE);
		columnNamesActual.add(F_SMTP_AUTHENTICATION);
		columnNamesActual.add(F_TIME_MANCATA_TRASMISSIONE);
		columnNamesActual.add(F_CERT);

		if (!tableCompare(connection, table, columnNamesActual)){
			i++;
		}

		table = TABLE_PARAMETRI_PASTORIZZAZIONE;
		columnNamesActual.clear();
		columnNamesActual = new ArrayList<String>();
		columnNamesActual.add(F_ID_PASTO);
		columnNamesActual.add(F_UP_MINIMA);
		columnNamesActual.add(F_UP_MASSIMA);
		columnNamesActual.add(F_TEMPERATURA_RIFERIMENTO);
		columnNamesActual.add(F_TEMPERATURA_MINIMA);
		columnNamesActual.add(F_DURATA_TRATTAMENTO);
		columnNamesActual.add(F_COEFFICENTE_Z);


		if (!tableCompare(connection, table, columnNamesActual)){
			i++;
		}

		table = TABLE_SONDE;
		columnNamesActual.clear();
		columnNamesActual = new ArrayList<String>();
		columnNamesActual.add(F_ID_SONDA_SONDE);
		columnNamesActual.add(F_OFFSET);
		columnNamesActual.add(F_DESCRIZIONE);
		columnNamesActual.add(F_LOCATION);
		columnNamesActual.add(F_RANGEMIN);
		columnNamesActual.add(F_RANGEMAX);
		columnNamesActual.add(F_RANGEMINURT);
		columnNamesActual.add(F_RANGEMAXURT);
		columnNamesActual.add(F_INDIRIZZO);
		columnNamesActual.add(F_RITARDO_ALLARME);
		columnNamesActual.add(F_UM);
		columnNamesActual.add(F_VALORE_MIN_SONDA);
		columnNamesActual.add(F_VALORE_MAX_SONDA);
		columnNamesActual.add(F_VALORE_MIN_UM);
		columnNamesActual.add(F_VALORE_MAX_UM);
		columnNamesActual.add(F_TEMPI_TRASMISSIONE);
		columnNamesActual.add(F_OFFSET_URT);
		columnNamesActual.add(F_INCERTEZZA);
		columnNamesActual.add(F_INCERTEZZA_URT);
		columnNamesActual.add(F_ENABLE_ALARM);
		columnNamesActual.add(F_DAYS_OF_WEEK);


		if (!tableCompare(connection, table, columnNamesActual)){
			i++;
		}


		table = TABLE_PROVA;
		columnNamesActual.clear();
		columnNamesActual = new ArrayList<String>();
		columnNamesActual.add(F_ID_PROVA);
		columnNamesActual.add(F_TARGA);
		columnNamesActual.add(F_RESPONSABILE);
		columnNamesActual.add(F_DATA_PROVA);
		columnNamesActual.add(F_ORA_INIZIO);
		columnNamesActual.add(F_ORA_FINE);
		columnNamesActual.add(F_ID_SENSORI);
		columnNamesActual.add(F_DURATA_TEORICA);
		columnNamesActual.add(F_DURATA_EFFETTIVA);
		columnNamesActual.add(F_LOTTO);
		columnNamesActual.add(F_LABORATORIO);
		columnNamesActual.add(F_NUMERO_PROVA);
		columnNamesActual.add(F_DATA_SCARICO);


		if (!tableCompare(connection, table, columnNamesActual)){
			i++;
		}



		table = TABLE_LETTURE_PROVA;
		columnNamesActual.clear();
		columnNamesActual = new ArrayList<String>();
		columnNamesActual.add(F_ID_LETTURE_PROVA);
		columnNamesActual.add(F_LETTURE_PROVA_ID);
		columnNamesActual.add(F_ID_SONDA_LETTURE_PROVA);
		columnNamesActual.add(F_VALORE_LETTURE_PROVA);
		columnNamesActual.add(F_VALORE_GREZZO_LETTURE_PROVA);
		columnNamesActual.add(F_DATA_LETTURE_PROVA);
		columnNamesActual.add(F_VALORE_UMIDITA_LETTURE_PROVA);
		columnNamesActual.add(F_TIPO_LETTURA_LETTURE_PROVA);
		columnNamesActual.add(F_LATITUDINE_LETTURE_PROVA);
		columnNamesActual.add(F_LONGITUDINE_LETTURE_PROVA);
		columnNamesActual.add(F_GRUPPO_LETTURE_PROVA);
		columnNamesActual.add(F_STATO_BATTERIA_LETTURE_PROVA);
		columnNamesActual.add(F_EFFETTO_LETALE_LETTURE_PROVA);
		columnNamesActual.add(F_F0_LETTURE_PROVA);
		columnNamesActual.add(F_VALORE_UMIDITA_GREZZO_LETTURE_PROVA);
		columnNamesActual.add(F_OFFSET_LETTURE_PROVA);
		columnNamesActual.add(F_OFFSET_URT_LETTURE_PROVA);
		columnNamesActual.add(F_INCERTEZZA_LETTURE_PROVA);
		columnNamesActual.add(F_INCERTEZZA_URT_URT_LETTURE_PROVA);


		if (!tableCompare(connection, table, columnNamesActual)){
			i++;
		}
		
		
		table = TABLE_UTENTI;
		columnNamesActual.clear();
		columnNamesActual = new ArrayList<String>();
		columnNamesActual.add(F_ID_USER);
		columnNamesActual.add(F_USER);
		columnNamesActual.add(F_PASSWORD_USER);
		columnNamesActual.add(F_NAME_USER);
		columnNamesActual.add(F_TYPE_USER);
		columnNamesActual.add(F_LANGUAGE_USER);
	

		if (!tableCompare(connection, table, columnNamesActual)){
			i++;
		}
		
		
		table = TABLE_AUDIT;
		columnNamesActual.clear();
		columnNamesActual = new ArrayList<String>();
		columnNamesActual.add(F_ID_AUDIT);
		columnNamesActual.add(F_DATA_AUDIT);
		columnNamesActual.add(F_USER_AUDIT);
		columnNamesActual.add(F_COMPUTER_AUDIT);
		columnNamesActual.add(F_EVENT_NAME_AUDIT);
		columnNamesActual.add(F_EVENT_MESSAGGE_AUDIT);
		columnNamesActual.add(F_EVENT_CLASS_AUDIT);
 

		if (!tableCompare(connection, table, columnNamesActual)){
			i++;
		}
		
		
		table = TABLE_DEVICES;
		columnNamesActual.clear();
		columnNamesActual = new ArrayList<String>();
		columnNamesActual.add(F_ID_DEVICES);
		columnNamesActual.add(F_NAME_DEVICES);
		columnNamesActual.add(F_PLATFORM);
		columnNamesActual.add(F_REGISTRATION_ID);
		
		if (!tableCompare(connection, table, columnNamesActual)){
			i++;
		}
		
		table = TABLE_PULLDOWN;
		columnNamesActual.clear();
		columnNamesActual = new ArrayList<String>();
		columnNamesActual.add(F_ID_PULLDOWN);
		columnNamesActual.add(F_CLASSE);
		columnNamesActual.add(F_TEMP_30);
		columnNamesActual.add(F_TEMP_29);
		columnNamesActual.add(F_TEMP_28);
		columnNamesActual.add(F_TEMP_27);
		columnNamesActual.add(F_TEMP_26);
		columnNamesActual.add(F_TEMP_25);
		columnNamesActual.add(F_TEMP_24);
		columnNamesActual.add(F_TEMP_23);
		columnNamesActual.add(F_TEMP_22);
		columnNamesActual.add(F_TEMP_21);
		columnNamesActual.add(F_TEMP_20);
		columnNamesActual.add(F_TEMP_19);
		columnNamesActual.add(F_TEMP_18);
		columnNamesActual.add(F_TEMP_17);
		columnNamesActual.add(F_TEMP_16);
		columnNamesActual.add(F_TEMP_15);
		
		if (!tableCompare(connection, table, columnNamesActual)){
			i++;
		}
		
		

		if (i>0){
			return false;
		}

		return true;


	}	


	public static boolean tableCompare(Connection connection, String table, Collection<String> columnActual) {

		String SQL = "PRAGMA table_info(" + table +");";


		ResultSet rs = null;

		boolean findAll = true;

		Collection<String> column = new ArrayList<String>();

		try {
			Statement select = connection.createStatement();
			rs = select.executeQuery(SQL);
			int idx=0;

			while(rs.next()){
				column.add(rs.getString(2) + " " + rs.getString(3));
				idx++;
			}

			//			return compareList(columnActual,column);

		} catch (Exception e) {
			throw new PersistenceException(e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
				}
		}

		columnActual.removeAll(column);

		if (columnActual.isEmpty()){
			return true;
		} else {

			for (String s : columnActual) {
				AlterTable at = new AlterTable();
				
				String[] parts = s.split(" ");
				String columnName = parts[0];  
				String columnTye  = parts[1];
				
				at.setTableName(table);
				at.setColumnName(columnName);
				at.setColumnType(columnTye);
				missingColumns.add(at);
			}

			logger.info(TAG,"Table structure difference: " + table + " - " + columnActual.toString());

			return false;
		}


	}


	public static boolean compareList(List ls1,List ls2){
		return ls1.toString().contentEquals(ls2.toString())?true:false;
	}

	 
	
	public static List<AlterTable> getMissingFields(){
		return missingColumns;
	}


}
