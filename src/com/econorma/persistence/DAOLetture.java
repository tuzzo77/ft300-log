package com.econorma.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.econorma.data.Lettura;

class DAOLetture {

	private static final String TAG = DAOLetture.class.getCanonicalName();
	private static final Logger logger = Logger.getLogger(DAOLetture.class);
	private static final String TABLE = "TEMPERATURE";
	
	private static final DateFormat dateformat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	
	public static void  creaIndice(Connection connection) throws SQLException{
		PreparedStatement st = connection.prepareStatement("CREATE INDEX temperature_idx ON TEMPERATURE ( ID_SONDA, DATA )");
		 boolean execute = st.execute();
	}
	
	public static boolean insert(Connection connection, Lettura lettura){
		 try{
			  PreparedStatement st = connection.prepareStatement("INSERT INTO TEMPERATURE (ID_SONDA, TEMPERATURA, DATA, UMIDITA, TIPO_LETTURA, STATO_BATTERIA, VOLT, MILLIAMPERE, OHM, APERTO_CHIUSO) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			  st.setString(1,lettura.getIdSonda());
			  st.setDouble(2,lettura.getValore());
			  st.setDate(3, new java.sql.Date(lettura.getData().getTime()));
			  st.setDouble(4, lettura.getUmidita());
			  String tipo = "T";
			  switch (lettura.getTipoMisura()) {
				case TEMPERATURA:
					tipo = "T";
					break;
				case TEMPERATURA_UMIDITA:
					tipo = "E";
					break;
				case TENSIONE_CORRENTE:
					tipo = "V";
					break;
				default:
					break;
				}
			  st.setString(5, tipo);
			  st.setString(6, lettura.getStatoBatteria());
			  st.setDouble(7, lettura.getVolt());
			  st.setDouble(8, lettura.getMilliampere());
			  st.setDouble(9, lettura.getOhm());
			  st.setString(10, lettura.getApertoChiuso());
			  
			  boolean execute = st.execute();
			  return execute;
		  }catch(SQLException e){
			  logger.error(e);
			  return false;
		  }
	}
	
	public static boolean insertLetture(Connection connection, List<Lettura> letture){
		 try{
			  PreparedStatement st = connection.prepareStatement("INSERT INTO TEMPERATURE (ID_SONDA, TEMPERATURA, DATA, UMIDITA, TIPO_LETTURA, STATO_BATTERIA, VOLT, MILLIAMPERE, OHM, APERTO_CHIUSO) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			  connection.setAutoCommit(false);
			  
			   for (Lettura lettura: letture) {
				   	  st.setString(1,lettura.getIdSonda());
					  st.setDouble(2,lettura.getValore());
					  st.setDate(3, new java.sql.Date(lettura.getData().getTime()));
					  st.setDouble(4, lettura.getUmidita());
					  String tipo = "T";
					  switch (lettura.getTipoMisura()) {
						case TEMPERATURA:
							tipo = "T";
							break;
						case TEMPERATURA_UMIDITA:
							tipo = "E";
							break;
						case TENSIONE_CORRENTE:
							tipo = "V";
							break;
						default:
							break;
						}
					  st.setString(5, tipo);
					  st.setString(6, lettura.getStatoBatteria());
					  st.setDouble(7, lettura.getVolt());
					  st.setDouble(8, lettura.getMilliampere());
					  st.setDouble(9, lettura.getOhm());
					  st.setString(10, lettura.getApertoChiuso());
					  st.addBatch();
			   }
			  
			
			  int[] count = st.executeBatch();
			  connection.commit();
			  
			  return true;
		  }catch(SQLException e){
			  logger.error(e);
			  return false;
		  }
	}
	
	/**
	 * 
	 * @param rs
	 * @return
	 */
	public static  Lettura asObject(ResultSet rs){
		try {
			Lettura lettura = new Lettura();
			String  id_sonda = rs.getString(1);
			double temp_a = rs.getDouble(2);
			String tempo = rs.getString(3);
			double umidit_a = rs.getDouble(4);
			double volt_a = rs.getDouble(5);
			double m_a = rs.getDouble(6);
			double ohm_a = rs.getDouble(7);
			String aperto_chiuso = rs.getString(8);
			String tipo_lettura = rs.getString(9);
			String batteria = rs.getString(10);
			
			lettura.setIdSonda(id_sonda);
			lettura.setTemperaturaGrezza(temp_a);
			lettura.setUmiditaGrezza(umidit_a);
			lettura.setVoltGrezzo(volt_a);
			lettura.setMilliampereGrezzo(m_a);
			lettura.setOhmGrezzo(ohm_a);
			lettura.setApertoChiuso(aperto_chiuso);
			lettura.setStatoBatteria(batteria);
			
			if(!DAO.USE_SQLITE){
				lettura.setData(dateformat.parse(tempo));
			}else{
				lettura.setData(new java.util.Date(Long.parseLong(tempo)));
			}
			return lettura;
		} catch (Exception e) {
			logger.error(e);
			throw new PersistenceException(e);
		}
	}
	
	public static void deleteBySonda(Connection connection,  String p_id_sonda, Date p_Da, Date p_A){

		try{
			PreparedStatement st =null;

			String dataDa=null;
			String dataA=null;
			String where=null;

			GregorianCalendar c=new GregorianCalendar() ;
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0); 
			Date today=c.getTime();


			GregorianCalendar c1=new GregorianCalendar() ;
			if (p_Da!=null){
				c1.setTime(p_Da);	
				int giornoX=c1.get(Calendar.DAY_OF_MONTH);
				int annoX=c1.get(Calendar.YEAR);
				int meseX=c1.get(Calendar.MONTH);
				GregorianCalendar c2=new GregorianCalendar() ;
				c2.set(annoX, meseX, giornoX);
				c2.set(Calendar.HOUR, 0);
				c2.set(Calendar.MINUTE, 0);
				c2.set(Calendar.SECOND, 0);
				c2.set(Calendar.MILLISECOND, 0); 
				dataDa = sdf.format(p_Da.getTime()) ;
			}
			else{
				GregorianCalendar c3=new GregorianCalendar() ;
				c3.set(Calendar.HOUR, 0);
				c3.set(Calendar.MINUTE, 0);
				c3.set(Calendar.SECOND, 0);
				c3.set(Calendar.MILLISECOND, 0); 
				p_Da=c3.getTime();
			}

			GregorianCalendar c4=new GregorianCalendar() ;
			if (p_A!=null){
				c1.setTime(p_A);	
				int giornoX=c4.get(Calendar.DAY_OF_MONTH);
				int annoX=c4.get(Calendar.YEAR);
				int meseX=c4.get(Calendar.MONTH);
				GregorianCalendar c5=new GregorianCalendar() ;
				c5.set(annoX, meseX, giornoX);
				c5.set(Calendar.HOUR, 0);
				c5.set(Calendar.MINUTE, 0);
				c5.set(Calendar.SECOND, 0);
				c5.set(Calendar.MILLISECOND, 0); 
				dataA = sdf.format(p_A.getTime()) ;
			}
			else{
				GregorianCalendar c6=new GregorianCalendar() ;
				c6.set(Calendar.HOUR, 0);
				c6.set(Calendar.MINUTE, 0);
				c6.set(Calendar.SECOND, 0);
				c6.set(Calendar.MILLISECOND, 0); 
				p_A=c6.getTime();

			}


			if ((p_id_sonda!=null) || (!p_Da.equals(null)) || (!p_A.equals(null))) {

				if (p_id_sonda!=null){

					if (!p_id_sonda.equals("TUTTI")){
						where = "ID_SONDA" + " = '"+ p_id_sonda +"' ";
					}


					if (!p_Da.equals(null)){
						long epoch1=0;	

						try {
							epoch1 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataDa).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}

						if (where!=null){
							where = where.trim() + " AND " + "DATA" + ">=" + epoch1;
						} else
						{
							where = "DATA" + ">=" + epoch1;	
						}
					}



					if (!p_A.equals(null)){
						long epoch2=0;	

						try {
							epoch2 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataA).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}
						where = where.trim() + " AND " + "DATA" + "<=" + epoch2;
					}
				}

			}



			String query = "DELETE " +
					"FROM "+TABLE+"  WHERE "+ where;

			st = connection.prepareStatement(query);

			st.executeUpdate();

		}catch (SQLException e) {
			logger.error(TAG,e);
		}

	}
}
