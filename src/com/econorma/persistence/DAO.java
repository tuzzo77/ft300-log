package com.econorma.persistence;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import com.adamtaft.eb.EventBusService;
import com.econorma.Application;
import com.econorma.data.Lettura;
import com.econorma.data.LetturaProva;
import com.econorma.data.Log;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.data.User;
import com.econorma.logic.DatabaseManager;
import com.econorma.ui.Events;
import com.econorma.util.Logger;

import net.sf.cglib.proxy.Callback;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

 

public class DAO {

	private static final Logger logger = Logger.getLogger(DAO.class);

	private static final String TAG = "DAO";


	public static final boolean USE_SQLITE = true;

	static final String TABLE_TEMPERATURE = "TEMPERATURE";
	static final String TABLE_SONDE = "SONDE";
	static final String TABLE_AUDIT = "AUDIT_LOG";
	static final String TABLE_USER = "UTENTI";
	
	private DatabaseManager databaseManager;

	public DAO(){}

	public void setDatabaseManager(DatabaseManager dm){
		this.databaseManager = dm;
	}
	
	public void init() {
		if (USE_SQLITE) {
			Connection conn = databaseManager.getConnection();
			// creiamo le tabelle
			try {
				try {
					Statement stat;
					stat = conn.createStatement();
					// stat.executeUpdate("drop table if exists people;");
					stat.executeUpdate("CREATE TABLE " + TABLE_TEMPERATURE
							+ " (" + "ID_SONDA TEXT NOT NULL, "
							+ "TEMPERATURA REAL, " + "DATA TIMESTAMP, "
							+ "UMIDITA REAL, "
							+ "VOLT REAL, "
							+ "MILLIAMPERE REAL, "
							+ "OHM REAL, "
							+ "APERTO_CHIUSO TEXT, "
							+ "TIPO_LETTURA TEXT NOT NULL, "
							+ "STATO_BATTERIA TEXT)");

				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}

				try {
					Statement stat;
					stat = conn.createStatement();
					// stat.executeUpdate("drop table if exists people;");
					stat.executeUpdate("CREATE TABLE " + TABLE_SONDE + " ("
							+ "ID_SONDA TEXT NOT NULL, " 
							+ "DESCRIZIONE TEXT, "
							+ "TRASMISSION INTEGER, "
							+ "DATA TIMESTAMP,"
							+ "OFFSET REAL, "
							+ "OFFSET_URT REAL, "
							+ "INCERTEZZA REAL, " 
							+ "INCERTEZZA_URT REAL, "
							+ "LOCATION TEXT, " + "RANGEMIN REAL, " + "RANGEMAX REAL, " 
							+ "RANGEMINURT REAL, " + "RANGEMAXURT REAL, " 
							+ "TIPO_LETTURA TEXT, "
							+ "STATO_BATTERIA TEXT, "
							+ "TEMPERATURA REAL, "
							+ "UMIDITA REAL,  "
							+ "VOLT REAL, "
							+ "MILLIAMPERE REAL, "
							+ "OHM REAL, "
							+ "APERTO_CHIUSO TEXT, "
							+ "OFFSET_VOLT REAL, "
							+ "OFFSET_MILLIAMPERE REAL, "
							+ "OFFSET_OHM REAL, "
							+ "RANGEMINOHM REAL, " + "RANGEMAXOHM REAL, "
							+ "CAMPIONI INTEGER, "
							+ "VERSIONE TEXT "
							+");");

				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}

				
				try {
					Statement stat;
					stat = conn.createStatement();
					// stat.executeUpdate("drop table if exists people;");
					stat.executeUpdate("CREATE TABLE " + TABLE_USER + " ("
							+ "ID               INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL," 
    					 	+ "USER TEXT, "
    						+ "PASSWORD TEXT, "
    						+ "NAME TEXT, "
    						+ "TYPE TEXT, "
    						+ "LANGUAGE TEXT "
							+");");

				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}
				
				
				try {
					Statement stat;
					stat = conn.createStatement();
					// stat.executeUpdate("drop table if exists people;");
					stat.executeUpdate("CREATE TABLE " + TABLE_AUDIT + " ("
							+ "ID               INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL," 
    					 	+ "DATA DATETIME, "
							+ "USER TEXT, "   
							+ "HOSTNAME TEXT, "
    					 	+ "EVENT_NAME TEXT, " 
							+ "EVENT_MESSAGGE TEXT, "
							+ "EVENT_CLASS TEXT "
							+");");

				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}
			 
				
				try {
					DAOProve.createTable(conn);
				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}

				try {
					DAOLettureProva.createTable(conn);
				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}

		 
				try {
					DAOLettureProva.creaIndici(conn);
				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}

				try {
					DAOLettureProva.createView(conn);
				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}
				try {
					DAOLettureProva.createViewExport(conn);
				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}

				try {
					DAOLetture.creaIndice(conn);
				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}
				
				try {
					DAOProve.creaIndice(conn);
				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}
				
				try {
					DAOLog.creaIndice(conn);
				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}
				
				try {
					DAOUser.createDefault(conn);
				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}

				if (Application.getInstance().getUpgradeDB()){
					TableStructure.analysis(conn);
				}

			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}


	}

	public boolean insertLettura(Lettura lettura) {
		Connection connection = null;
		boolean insert = false;
		try {
			connection = databaseManager.getConnection();
			insert = DAOLetture.insert(connection, lettura);
		} finally {
			silentClose(connection);
		}
		return insert;
	}
	
	public boolean insertLetture(List<Lettura> letture) {
		Connection connection = null;
		boolean insert = false;
		try {
			connection = databaseManager.getConnection();
			insert = DAOLetture.insertLetture(connection, letture);
		} finally {
			silentClose(connection);
		}
		return insert;
	}
	
	

	public Lettura getUltimaLettura(Sensore sensore) {
		Connection connection = databaseManager.getConnection();
		String SQL = "SELECT * FROM TEMPERATURE WHERE ID_SONDA = '"
				+ sensore.getId_sonda() + "' ORDER BY DATA DESC LIMIT 1";
		ResultSet rs = null;
		try {
			Statement select = connection.createStatement();
			rs = select.executeQuery(SQL);
			if (rs.next()) {
				Lettura lettura = DAOLetture.asObject(rs);
				// viene fatto dopo
				// lettura.setSensore(sensore);
				return lettura;
			} else
				return null;
		} catch (Exception e) {
			throw new PersistenceException(e);
		} finally {
			silentClose(rs);
			silentClose(connection);
		}
	}

	private static void silentClose(ResultSet rs) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// ignore
		}
	}
	
	public static void silentClose(Connection conn) {
		try {
			if (conn != null)
				conn.close();
		} catch (Exception e) {
			// ignore
		}
	}

	public void deleteSensori() {
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOSensori.deleteAll(connection);
		} finally {
			silentClose(connection);
		}
	}

	public void deleteUser(User user) {
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOUser.delete(connection, user);
		} finally {
			silentClose(connection);
		}
	}
	
	public void updateUser(User user) {
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOUser.updateUser(connection, user);
		} finally {
			silentClose(connection);
		}
	}
	
	public void insertSensori(Collection<Sensore> sensori) {
		if (sensori == null || sensori.isEmpty())
			return;

		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOSensori.insert(connection, sensori);
		} finally {
			silentClose(connection);
		}
	}
	
	public void insertSensore(Sensore sensore) {
		if (sensore == null )
			return;
		
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOSensori.insertSensore(connection, sensore);
		} finally {
			silentClose(connection);
		}
	}

	public void updateSensore(Sensore sensore) {
		if (sensore == null )
			return;

		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOSensori.update(connection, sensore);
		} finally {
			silentClose(connection);
		}
	}
	
	public void deleteSensore(Sensore sensore) {
		if (sensore == null )
			return;

		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOSensori.deleteBySonda(connection, sensore);
		} finally {
			silentClose(connection);
		}
	}

	public List<Sensore> loadSensori() {
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOSensori.loadAll(connection);
		} finally {
			silentClose(connection);
		}
	}
	
	public Sensore loadSensorById(String idSonda) {
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOSensori.loadSensorById(connection, idSonda);
		} finally {
			silentClose(connection);
		}
	}


	public boolean insertOrUpdateProva(Prova prova){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOProve.insertOrUpdate(connection, prova);
		} finally {
			silentClose(connection);
		}
	}
	
	public boolean insertOrUpdateLetturaProva(Prova prova){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOLettureProva.insertOrUpdate(connection, prova);
		} finally {
			silentClose(connection);
		}
	}
	
	public boolean insertUser(User user){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOUser.insert(connection, user);
		} finally {
			silentClose(connection);
		}
	}
	
	
	public boolean insertLetturaProva(LetturaProva lettura){
		Connection connection = null;
		try {

			EventBusService.publish(new Events.NuovaLetturaProva(lettura));
			connection = databaseManager.getConnection();

//			if (!DAOLettureProva.checkLetturaProva(connection, lettura)){
				return DAOLettureProva.insert(connection, lettura);
//			}

//			return false;

		} finally {
			silentClose(connection);
		}
	}
	
	public boolean insertLettureProva(List<LetturaProva> letture){
		Connection connection = null;
		try {

			EventBusService.publish(new Events.NuovaLetturaProva(letture.get(letture.size()-1)));
			connection = databaseManager.getConnection();

//			if (!DAOLettureProva.checkLetturaProva(connection, lettura)){
				return DAOLettureProva.insertLetture(connection, letture);
//			}

//			return false;

		} finally {
			silentClose(connection);
		}
	}

	public boolean checkLetturaProva(LetturaProva lettura){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOLettureProva.checkLetturaProva(connection, lettura);
		} finally {
			silentClose(connection);
		}
	}

	public boolean checkProvaIsEmpty(Prova prova){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOLettureProva.checkProvaIsEmpty(connection, prova);
		} finally {
			silentClose(connection);
		}
	}


	public List<LetturaProva> findLettureProvaByProvaAndSonda(Prova prova, String id_sonda){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOLettureProva.findByProvaAndSonda(connection, prova, id_sonda);
		} finally {
			silentClose(connection);
		}
	}
	
	public List<LetturaProva> findLettureProvaByProvaAndSondaPrefix(Prova prova, String idSondaPrefix){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOLettureProva.findByProvaAndSondaPrefix(connection, prova, idSondaPrefix);
		} finally {
			silentClose(connection);
		}
	}
 

	public List<LetturaProva> findLettureProvaBySonda(String id_sonda, Date Da, Date A, boolean onlyInAlarm){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOLettureProva.findBySonda(connection, id_sonda, Da, A, onlyInAlarm);
		} finally {
			silentClose(connection);
		}
	}
	
	public List<LetturaProva> findLastByData(Date Da, Date A){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOLettureProva.findLastByData(connection, Da, A);
		} finally {
			silentClose(connection);
		}
	}
	
	public List<LetturaProva> findMedieProvaBySonda(String id_sonda, Date Da, Date A, boolean onlyInAlarm, int minutes){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOLettureProva.findMedieBySonda(connection, id_sonda, Da, A, onlyInAlarm, minutes);
		} finally {
			silentClose(connection);
		}
	}


	public List<Prova> findAllProve(){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOProve.findAll(connection);
		} finally {
			silentClose(connection);
		}
	}

	public Prova findUltimaProva(){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOProve.findUltimaProva(connection);
		} finally {
			silentClose(connection);
		}
	}
	
	public void deleteSondeUnknown(){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOSensori.deleteSondeUnknown(connection);
		} finally {
			silentClose(connection);
		}
	}
	
	public void enableAlarms(){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOSensori.enableAlarms(connection);
		} finally {
			silentClose(connection);
		}
	}
 	
	public List<LetturaProva> findLettureProvaByProva(Prova prova, boolean filter){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOLettureProva.findByProva(connection, prova, filter);
		} finally {
			silentClose(connection);
		}
	}

	public List<LetturaProva> findAllLettureProva(){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOLettureProva.findAll(connection);
		} finally {
			silentClose(connection);
		}
	}

	public void deleteLettureProvaByProva(Prova prova){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOLettureProva.deleteByProva(connection, prova);
		} finally {
			silentClose(connection);
		}
	}

	public void deleteProvaByProva(Prova prova){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOProve.deleteByProva(connection, prova);
		} finally {
			silentClose(connection);
		}
	}

	public void deleteLettureProvaBySonda(String id_sonda, Date Da, Date A){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOLettureProva.deleteBySonda(connection, id_sonda, Da, A);
		} finally {
			silentClose(connection);
		}
	}
	
	public void deleteLettureBySonda(String id_sonda, Date Da, Date A){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			DAOLetture.deleteBySonda(connection, id_sonda, Da, A);
		} finally {
			silentClose(connection);
		}
	}
	
	
	public List<Log> findLogByDate(Date Da, Date A){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOLog.findLogByDate(connection,  Da, A);
		} finally {
			silentClose(connection);
		}
	}
	
	public List<User> findAllUser(){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOUser.findAll(connection);
		} finally {
			silentClose(connection);
		}
	}
	
	public User findByUser(String user){
		Connection connection = null;
		try {
			connection = databaseManager.getConnection();
			return DAOUser.findByUser(connection, user);
		} finally {
			silentClose(connection);
		}
	}
	
	
	public void shrinkDatabase() {

		Connection connection = databaseManager.getConnection();
		String SQL = "vacuum";

		ResultSet rs = null;
		try {
			Statement shrink = connection.createStatement();
			shrink.execute(SQL);

		} catch (Exception e) {
			throw new PersistenceException(e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
				}
		}

	}


	public static DAO create(DatabaseManager manager){
		DAO dao = new DAO();
		dao = DAOEnhancer.enhance(dao);
		dao.setDatabaseManager(manager);
		return dao;
	}


	private static class TableStructure{

		private static final Logger logger = Logger.getLogger(TableStructure.class);

		private static final String TAG = "DAO.tablestruct";

		public static void analysis(Connection conn){

			boolean result=true;

			try {
				result  = DAOTableStruct.comparetableStruct(conn);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (result){
				JOptionPane.showMessageDialog(null,"Database aggiornato all'ultima versione", "Informazione", JOptionPane.INFORMATION_MESSAGE);
			} else {

				int i = JOptionPane.showConfirmDialog(
						null,
						"Database non corretto. Eseguire aggiornamento",
						"Errore",
						JOptionPane.YES_NO_OPTION);

				if(i == JOptionPane.YES_OPTION) {
					
					try {
						DAOAlterTable.alterTable(conn);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					
				}
				else {
					System.exit(0); 
				}

			}

		}

	}

	private static class Performance{	

		private static final Logger logger = Logger.getLogger(Performance.class);
		private static final String TAG = "DAO.perf";

		public static DAO analysis(DAO dao){
			Enhancer enhancer = new Enhancer();
			enhancer.setSuperclass(dao.getClass());
			enhancer.setCallback(analysis());
			return (DAO) enhancer.create();
		}
		
		public static Callback analysis(){
			return new MethodInterceptor() {
				@Override
				public Object intercept(Object obj, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {

					long start = System.currentTimeMillis();
					Object invokeSuper = methodProxy.invokeSuper(obj, args);

					long stop = System.currentTimeMillis();
					if(logger.isDebugEnabled())
						logger.debug(TAG, "DAO method: \""+method.getName()+"\" took "+(stop-start)+" (ms)");
					return invokeSuper;
				}
			};
		}
	}
	
	private static class DBConcurrency{	

		private static final Logger logger = Logger.getLogger(DBConcurrency.class);

		private static final String TAG = "DAO.concurrency";

		private DBConcurrency(){ /**/}
		
		
		public static DAO serialize(DAO dao){
			Enhancer enhancer = new Enhancer();
			enhancer.setSuperclass(dao.getClass());
			enhancer.setCallback(serialize());
			return (DAO) enhancer.create();
		}
		
		public static Callback serialize(){
			return new MethodInterceptor() {
				StringBuilder sb = new StringBuilder();
				
				@Override
				public Object intercept(Object obj, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
					synchronized (obj) {
						if(logger.isDebugEnabled()){
							sb.setLength(0);
							sb.append(method.getName()).append("(");
							Class<?>[] parameterTypes = method.getParameterTypes();
							if(parameterTypes!=null){
								int l = parameterTypes.length;
								for(int i=0; i<l; i++){
									sb.append(parameterTypes[i].getSimpleName());
									if(i<l-1){
										sb.append(",");
									}
								}
							}
							sb.append(")");
							
							logger.debug(TAG, "Invoking "+sb);
						}
						Object invokeSuper = methodProxy.invokeSuper(obj, args);
						return invokeSuper;
					}
				}
			};
		}
	}
	
	private static class DAOEnhancer{
		
		private static class DBConcurrency{}
		private static class Performance{}
		private static final Logger LOGGER_C = Logger.getLogger(DBConcurrency.class);
		private static final Logger LOGGER_P = Logger.getLogger(Performance.class);

		private static final String TAG_P = "DAO.perf";
		private static final String TAG_C = "DAO.concurrency";
		
		private final static boolean ENABLE_P = LOGGER_P.isDebugEnabled();
		private final static boolean ENABLE_C = LOGGER_C.isDebugEnabled();
		
		public static DAO enhance(DAO dao){
			Enhancer enhancer = new Enhancer();
			enhancer.setSuperclass(dao.getClass());
			enhancer.setCallback(createInterceptor());
			return (DAO) enhancer.create();
		}
		
		public static Callback createInterceptor(){
			return new MethodInterceptor() {
				@Override
				public Object intercept(Object obj, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
					long start = 0;
					if(ENABLE_P){
						start = System.currentTimeMillis();
					}
					Object invokeSuper;
					
					synchronized (obj) {
						if(ENABLE_C){
							StringBuilder sb = new StringBuilder();
							sb.setLength(0);
							sb.append(method.getName()).append("(");
							Class<?>[] parameterTypes = method.getParameterTypes();
							if(parameterTypes!=null){
								int l = parameterTypes.length;
								for(int i=0; i<l; i++){
									sb.append(parameterTypes[i].getSimpleName());
									if(i<l-1){
										sb.append(",");
									}
								}
							}
							sb.append(")");
							LOGGER_C.debug(TAG_C, "Invoking "+sb);
						}
						invokeSuper = methodProxy.invokeSuper(obj, args);
					}
					if(ENABLE_P){
						long stop = System.currentTimeMillis();
						LOGGER_P.debug(TAG, "DAO method: \""+method.getName()+"\" took "+(stop-start)+" (ms)");
					}
					
					return invokeSuper;
				}
			};
		}
	}
	


}
