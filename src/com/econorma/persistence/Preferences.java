package com.econorma.persistence;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.econorma.Main;
import com.econorma.util.StringUtils;

public class Preferences {

	private static final String PORT = "port";
	private static final String PORTMODEM = "portModem";
	private static final String BAUDRATE="baudrate";

	private static final String TARGA_AUTOMEZZO = "targa";
	private static final String RESPONSABILE = "responsabile";
	private static final String LABORATORIO = "laboratorio";
	private static final String DATA_PROVA = "dataProva";
	private static final String NUMERO_PROVA = "numeroProva";
	
	private static final String FILE_SINOTTICO = "fileSinottico";
	private static final String SENSORI_SINOTTICO = "sensoriSinottico";
	
	private static final String FILE_LOGGER = "fileLogger";
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	
	private static final String X = "X";
	private static final String Y = "Y";
	private static final String W = "W";
	private static final String H = "H";
	
	private static final String WChart = "width chart export";
	private static final String WData = "width data export";
 
	public static class Values{
		public static final String PORT_DISABLED = "portDisabled";
	}
	
	private java.util.prefs.Preferences p; 
	
	public Preferences(){
		if (!Main.getFreeInstances()){
			p = java.util.prefs.Preferences.userRoot().node("com.econorma.ft300LOG");
		} else {
			String dir = System.getProperty("user.dir").replace("//", ".").replace("\\", ".").replace(":", "");
			p = java.util.prefs.Preferences.userRoot().node("com.econorma.ft300LOG." + dir);
		}
	}
	
	public String getPort() {
		String port = p.get(PORT, null);
		return port;
	}

	public void setPort(String port) {
		p.put(PORT, port);
	}
	
	public String getPortModem() {
		String port = p.get(PORTMODEM, null);
		return port;
	}

	public void setPortModem(String port) {
		p.put(PORTMODEM, port);
	}

	public String getTargaAutomezzo() {
		return p.get(TARGA_AUTOMEZZO, null);
	}

	public void setTargaAutomezzo(String targa) {
		p.put(TARGA_AUTOMEZZO, targa);
	}

	public String getResponsabile() {
		return p.get(RESPONSABILE, null);
	}

	public void setResponsabile(String responsabile) {
		p.put(RESPONSABILE, responsabile);
	}
	
	public String getLaboratorio() {
		return p.get(LABORATORIO, null);
	}

	public void setLaboratorio(String laboratorio) {
		p.put(LABORATORIO, laboratorio);
	}
	
	public String getNumeroProva() {
		return p.get(NUMERO_PROVA, null);
	}

	public void setNumeroProva(String numeroProva) {
		p.put(NUMERO_PROVA, numeroProva);
	}
	
	public String getFileLogger() {
		return p.get(FILE_LOGGER, null);
	}

	public void setFileLogger(String fileLogger) {
		p.put(FILE_LOGGER, fileLogger);
	}
	
	public Date getDataProva() {
		String dateAsString = p.get(DATA_PROVA, null);
		if(!StringUtils.nullOrEmpty(dateAsString)){
			try{
				return sdf.parse(dateAsString);
			}catch(Exception e ){
				//ignore
			}
		}
		return null;
	}

	public void setDataProva(Date data) {
		if(data!=null)
			p.put(DATA_PROVA, sdf.format(data));
		else
			p.put(DATA_PROVA, null);
	}
	
	public int getX() {
		return p.getInt(X, 0);
	}

	public void setX(int x) {
		p.putInt(X, x);
	}
	

	public int getY() {
		return p.getInt(Y, 0);
	}

	public void setY(int y) {
		p.putInt(Y, y);
	}
	
	public int getW() {
		return p.getInt(W, 0);
	}

	public void setW(int x) {
		p.putInt(W, x);
	}
	

	public int getH() {
		return p.getInt(H, 0);
	}

	public void setH(int y) {
		p.putInt(H, y);
	}
	
	public int getWchart() {
		return p.getInt(WChart, 0);
	}
	
	public void setWChart(int x) {
		p.putInt(WChart, x);
	}
	
	public int getWData() {
		return p.getInt(WData, 0);
	}
	
	public void setWData(int x) {
		p.putInt(WData, x);
	}
	
	public String getFileSinottico() {
		return p.get(FILE_SINOTTICO, null);
	}

	public void setFileSinottico(String sinottico) {
		p.put(FILE_SINOTTICO, sinottico);
	}
	
	public String getSensoriSinottico() {
		return p.get(SENSORI_SINOTTICO, null);
	}

	public void setSensoriSinottico(String sinottico) {
		p.put(SENSORI_SINOTTICO, sinottico);
	}
	
	public static boolean  isPortDisabled(String port){
		return Values.PORT_DISABLED.equals(port);
	}
	
	public int getBaudRate() {
		return p.getInt(BAUDRATE, 0);
	}

	public void setBaudRate(int baudRate) {
		p.putInt(BAUDRATE, baudRate);
	}
	
}
