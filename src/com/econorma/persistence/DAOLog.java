package com.econorma.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.econorma.data.Lettura.TipoMisura;
import com.econorma.data.LetturaProva;
import com.econorma.data.Log;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.gui.GUI;
import com.econorma.Application;
import com.econorma.util.Logger;
import com.econorma.util.StringUtils;

public class DAOLog {

	private static final Logger logger = Logger.getLogger(DAOLog.class);
	private static final String TAG = DAOLog.class.getCanonicalName();

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	private static final SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
	private static final SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
	 

	private static final String TABLE = "AUDIT_LOG";
	
	private static final String F_ID = "ID";
	private static final String F_DATA ="DATA";
	private static final String F_USER = "USER";
	private static final String F_HOSTNAME= "HOSTNAME";
	private static final String F_EVENT_NAME = "EVENT_NAME";
	private static final String F_EVENT_MESSAGGE = "EVENT_MESSAGGE";
	private static final String F_EVENT_CLASS = "EVENT_CLASS";
	 
	
	public static void  creaIndice(Connection connection) throws SQLException{
		PreparedStatement st = connection.prepareStatement("CREATE INDEX audit_log_idx ON AUDIT_LOG ( DATA, ID );");
		boolean execute = st.execute();
	}


	public static List<Log>findLogByDate(Connection connection, Date p_Da, Date p_A){
		
		try{
			PreparedStatement st =null;

			String dataDa=null;
			String dataA=null;
			String where=null;

			GregorianCalendar c=new GregorianCalendar() ;
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0); 
			Date today=c.getTime();


			GregorianCalendar c1=new GregorianCalendar() ;
			if (p_Da!=null){
				c1.setTime(p_Da);	
				int giornoX=c1.get(Calendar.DAY_OF_MONTH);
				int annoX=c1.get(Calendar.YEAR);
				int meseX=c1.get(Calendar.MONTH);
				GregorianCalendar c2=new GregorianCalendar() ;
				c2.set(annoX, meseX, giornoX);
				c2.set(Calendar.HOUR, 0);
				c2.set(Calendar.MINUTE, 0);
				c2.set(Calendar.SECOND, 0);
				c2.set(Calendar.MILLISECOND, 0); 
				dataDa = sdf.format(p_Da.getTime()) ;
			}
			else{
				GregorianCalendar c3=new GregorianCalendar() ;
				c3.set(Calendar.HOUR, 0);
				c3.set(Calendar.MINUTE, 0);
				c3.set(Calendar.SECOND, 0);
				c3.set(Calendar.MILLISECOND, 0); 
				p_Da=c3.getTime();
			}

			GregorianCalendar c4=new GregorianCalendar() ;
			if (p_A!=null){
				c1.setTime(p_A);	
				int giornoX=c4.get(Calendar.DAY_OF_MONTH);
				int annoX=c4.get(Calendar.YEAR);
				int meseX=c4.get(Calendar.MONTH);
				GregorianCalendar c5=new GregorianCalendar() ;
				c5.set(annoX, meseX, giornoX);
				c5.set(Calendar.HOUR, 0);
				c5.set(Calendar.MINUTE, 0);
				c5.set(Calendar.SECOND, 0);
				c5.set(Calendar.MILLISECOND, 0); 
				dataA = sdf.format(p_A.getTime()) ;
			}
			else{
				GregorianCalendar c6=new GregorianCalendar() ;
				c6.set(Calendar.HOUR, 0);
				c6.set(Calendar.MINUTE, 0);
				c6.set(Calendar.SECOND, 0);
				c6.set(Calendar.MILLISECOND, 0); 
				p_A=c6.getTime();

			}


			if ((!p_Da.equals(null)) || (!p_A.equals(null))) {
 
				if (dataDa!=null) {
				
			 
					if (!p_Da.equals(null)){
						
						/*long epoch1=0;	

						try {
							epoch1 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataDa).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}

					 
							where = F_DATA + ">=" + epoch1;	*/
						
							String minDate = sdf.format(p_Da);
							where = F_DATA + " >= " +  "'" + minDate + "'";	
							}
							
						

			 
					if (!p_A.equals(null)){
					/*	long epoch2=0;	

						try {
							epoch2 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataA).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}
						where = where.trim() + " AND " + F_DATA + "<=" + epoch2;*/
						
						
						String maxDate = sdf.format(p_A);
						 
						where = where.trim() + " AND " +  F_DATA + " <= " + "'" + maxDate + "'";
					}
					
				}
					
				}
				 
 


		 String select = "SELECT " +
					F_ID+", "+
					F_DATA+", "+
					F_USER+", "+
					F_HOSTNAME+", "+
					F_EVENT_NAME+", "+
					F_EVENT_MESSAGGE+", "+
					F_EVENT_CLASS+"  "+
					"FROM "+TABLE+"  WHERE "+ where + " ORDER BY "+F_DATA+" ASC" ; 
			
		/*	String select = "SELECT " +
					F_ID+", "+
					F_DATA+", "+
					F_USER+", "+
					F_HOSTNAME+", "+
					F_EVENT_NAME+", "+
					F_EVENT_MESSAGGE+", "+
					F_EVENT_CLASS+"  "+
					"FROM "+TABLE ;*/


			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();

			List<Log> logDati = new ArrayList<Log>();
			
			while(rs.next()){
				Long id = rs.getLong(1);
				Date date = null;
				 
				try {
					date = sdf.parse(rs.getString(2));
				} catch (ParseException e) {
				}
				String user = rs.getString(3);
				String hostname = rs.getString(4);
				String event_name = rs.getString(5);
				String event_messagge = rs.getString(6);
				String event_class = rs.getString(7);
			 
				Log log = new Log();
				log.setId(id);
				log.setData(date);
				log.setHost(hostname);
				log.setUser(user);
				log.setType(event_name);
				log.setMessagge(event_messagge);
				log.setJavaClass(event_class);
				
				log.setDataString(sdfDate.format(date));
				log.setOraString(sdfTime.format(date));
			 
					
				logDati.add(log);

			}
			rs.close();
			return logDati;
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}
	
	 


}
