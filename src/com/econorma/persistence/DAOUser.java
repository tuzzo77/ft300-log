package com.econorma.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import com.econorma.data.User;
import com.econorma.resources.Testo;
import com.econorma.util.Logger;

public class DAOUser {

	private static final Logger logger = Logger.getLogger(DAOUser.class);
	private static final String TAG = DAOUser.class.getCanonicalName();

	 
	private static final String TABLE = "UTENTI";
	 

	private static final String F_ID = "ID";
	private static final String F_USER="USER";
	private static final String F_PASSWORD = "PASSWORD";
	private static final String F_NAME= "NAME";
	private static final String F_TYPE = "TYPE";
	private static final String F_LANGUAGE = "LANGUAGE";
	 
	
	public static boolean exist (Connection connection){
		try{
			PreparedStatement st =null;
			boolean found = false;
		   String select = "SELECT name FROM sqlite_master WHERE type='table' AND name=" +"'"+ TABLE + "'";
		 
			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
 			
			while(rs.next()){
				found = true;
			}
			rs.close();
			return found;
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		 
		return true;
	}
	

	public static List<User>findAll(Connection connection){
		
		try{
			PreparedStatement st =null;
  
		   String select = "SELECT " +
					F_ID+", "+
					F_USER+", "+
					F_PASSWORD+", "+
					F_NAME+", "+
					F_TYPE + ", " +
					F_LANGUAGE+ " " +
					"FROM "+TABLE+" ORDER BY "+F_USER; 
		 
			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();

			List<User> userDati = new ArrayList<User>();
			
			while(rs.next()){
				Long id = rs.getLong(1);
				String userString = rs.getString(2);
				String password = rs.getString(3);
				String name = rs.getString(4);
				String type = rs.getString(5);
				String language = rs.getString(6);
			 
				User user = new User();
			 	user.setUser(userString);
				user.setPassword(password);
				user.setUser_complete(name);
				user.setType(type);;
				user.setLanguage(language);
			 		
				userDati.add(user);

			}
			rs.close();
			return userDati;
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}
	
	public static void createDefault(Connection connection) throws SQLException{
		User user = new User();
		user.setUser("admin");
		user.setPassword(getSHA1("econorma"));
		user.setUser_complete(Testo.ADMINISTRATOR);
		user.setType(Testo.ADMINISTRATOR);
		user.setLanguage(Testo.ITALIAN);
		insert(connection, user);
	}
	
	public static boolean insert(Connection connection, User user){
		
		 try{
			 
		 long id =0;
		 boolean execute=true;
		
		 String findId = "SELECT id from "+TABLE+" WHERE user="+ "'" + user.getUser() + "'";
		  Statement createStatement = connection.createStatement();
		  ResultSet executeQuery = createStatement.executeQuery(findId);
		  if(executeQuery.next()){
			  id = executeQuery.getLong(1);
		  }
		  
		  if (id==0){
			  
			  PreparedStatement st = connection.prepareStatement("INSERT INTO UTENTI (USER, PASSWORD, NAME, TYPE, LANGUAGE) VALUES (?, ?, ?, ?, ?)");
			  st.setString(1,user.getUser());
			  st.setString(2,user.getPassword());
			  st.setString(3, user.getUser_complete());
			  st.setString(4, user.getType());
			  st.setString(5, user.getLanguage());
			  execute = st.execute();
			  
			  
		  }
		  
		  return execute;
		 
			 
		  }
		 	
		 	catch(SQLException e){
			  logger.error(TAG, e);
			  return false;
		  }
	}
	
	public static boolean delete(Connection connection, User user){
		
		 try{
			 
		 long id =0;
		 boolean execute=true;
		
		 String findId = "SELECT id from "+TABLE+" WHERE user="+ "'" + user.getUser() + "'";
		  Statement createStatement = connection.createStatement();
		  ResultSet executeQuery = createStatement.executeQuery(findId);
		  if(executeQuery.next()){
			  id = executeQuery.getLong(1);
		  }
		  
		  if (id!=0){
			  PreparedStatement st = connection.prepareStatement("DELETE FROM UTENTI WHERE user ="+ "'" + user.getUser() + "'");
			  execute = st.execute();
		  }
		  
		  return execute;
		 
			 
		  }
		 	
		 	catch(SQLException e){
			  logger.error(TAG, e);
			  return false;
		  }
	}
	
	public static User findByUser(Connection connection, String user){
		
		try{
			PreparedStatement st =null;
  
		   String select = "SELECT " +
					F_ID+", "+
					F_USER+", "+
					F_PASSWORD+", "+
					F_NAME+", "+
					F_TYPE + " " +
					"FROM "+TABLE+ " WHERE user="+ "'" + user +  "'"; 
		 
			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
			User u =null;
			 
			while(rs.next()){
				Long id = rs.getLong(1);
				String userString = rs.getString(2);
				String password = rs.getString(3);
				String name = rs.getString(4);
				String type = rs.getString(5);
			 
				u = new User();
			 	u.setUser(userString);
				u.setPassword(password);
				u.setUser_complete(name);
				u.setType(type);;
		 
			}
			rs.close();
			return u;
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}
	
	public static boolean updateUser(Connection connection, User user){
		
		 try{
			 
		 long id =0;
		 boolean execute=false;
		
		 String findId = "SELECT id from "+TABLE+" WHERE user="+ "'" + user.getUser() + "'";
		  Statement createStatement = connection.createStatement();
		  ResultSet executeQuery = createStatement.executeQuery(findId);
		  if(executeQuery.next()){
			  id = executeQuery.getLong(1);
		  }
		  
		  if (id!=0){
			  PreparedStatement st = connection.prepareStatement("UPDATE UTENTI SET LANGUAGE = ? WHERE user =" + "'" + user.getUser() + "'");
			  st.setString(1, user.getLanguage());
			  st.executeUpdate();
			  execute = true;
		  }
		  
		  return execute;
		 
			 
		  }
		 	
		 	catch(SQLException e){
			  logger.error(TAG, e);
			  return false;
		  }
	}
	
	private static String getSHA1(String text){
		return  DigestUtils.shaHex(text);
	}


}
