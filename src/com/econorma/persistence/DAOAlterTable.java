package com.econorma.persistence;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import com.econorma.data.AlterTable;
import com.econorma.data.Lettura;
import com.econorma.data.Sensore;
import com.econorma.logic.SensoreManager;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

public class DAOAlterTable {


	private static final String TAG = DAOAlterTable.class.getSimpleName();

	private static final Logger logger = Logger.getLogger(DAOAlterTable.class);

	public static void alterTable(Connection conn) throws SQLException{


		List<AlterTable> alterTable = DAOTableStruct.getMissingFields();

		for(AlterTable at: alterTable){

			try {
				Statement stat = conn.createStatement();
				String query = "ALTER TABLE "+ at.getTableName() +" ADD COLUMN "+ at.getColumnName() + " " + at.getColumnType() +";";
				stat.executeUpdate(query);
				logger.info(TAG,"Table modified:" + at.getTableName() + " - " + at.getColumnName() + " - " + at.getColumnType());
				logger.log(LoggerCustomLevel.AUDIT, "Aggiornamento Table Database: " + at.getTableName() + " - " + at.getColumnName() + " - " + at.getColumnType());
			}  catch (SQLException e) {
				logger.error(TAG,"Alter table error:" + e);
			} 

		}

	}
 

}
