package com.econorma.persistence;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.econorma.data.Lettura;
import com.econorma.data.Lettura.TipoMisura;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.logic.DatabaseManager;
import com.econorma.logic.logger.SalvaLoggerInDB;
import com.econorma.util.Logger;

public class DAOInsertFromSelect {
	
	private static final Logger LOGGER = Logger.getLogger(DAOInsertFromSelect.class);
	private static final String TAG = SalvaLoggerInDB.class.getSimpleName();
	private DatabaseManager databaseManager; 
	private DAO dao;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
	
	public static void main(String[] args) {
		 DAOInsertFromSelect myDao = new DAOInsertFromSelect();
		 myDao.init();
		 myDao.executeSingle();
	}
	
	public void init(){
		databaseManager = DatabaseManager.getInstance();
		databaseManager.init();
	}
	 
 
	public boolean executeBatch(){
		
		dao = DAO.create(databaseManager);
		
		 try {
			 Date startDate = new Date();
			 LOGGER.info(TAG, "Inizio scrittura letture :"+  sdf.format(startDate));
			 
			 	Prova prova = new Prova();
				prova.setResponsabile("Test Sqlite");
				prova.setId_sensore("MY SENSOR");
				prova.setData_inizio(new Date());
				prova.setData_fine(new Date());
				prova.setOra_inizio(new Date());
				prova.setOra_fine(new Date());
				prova.setData_scarico(new Date());
				dao.insertOrUpdateProva(prova);
				
				List<Lettura> letture = new ArrayList<Lettura>();
				List<LetturaProva> lettureProva = new ArrayList<LetturaProva>();
			 
				for (int i=0;  i<=1000; i++) {
					Lettura l = new Lettura();
					l.setIdSonda("TEST");
					l.setData(new Date());
					l.setTemperaturaGrezza(20+i);
					l.setUmiditaGrezza(50+i);
					l.setStatoBatteria("b");
					l.setTipoMisura(TipoMisura.TEMPERATURA_UMIDITA);
					letture.add(l);
					LetturaProva letturaProva = new LetturaProva(prova, l);
					lettureProva.add(letturaProva);
				}
				
				dao.insertLetture(letture);
				dao.insertLettureProva(lettureProva);
				
				Date endDate = new Date();
				LOGGER.info(TAG, "Fine scrittura letture :"+  sdf.format(endDate));
				LOGGER.info(TAG, "Durata scrittura letture :"+  calcDuration(startDate, endDate));
			 
				
		} catch (Exception e) {
			LOGGER.error(TAG, e); 
		}
		
		return true;
	}
	
	public boolean executeSingle(){
		
		dao = DAO.create(databaseManager);
	 	
		 try {
			 Date startDate = new Date();
			 LOGGER.info(TAG, "Inizio scrittura letture :"+  sdf.format(startDate));
			 
			 	Prova prova = new Prova();
				prova.setResponsabile("Test Sqlite");
				prova.setId_sensore("MY SENSOR");
				prova.setData_inizio(new Date());
				prova.setData_fine(new Date());
				prova.setOra_inizio(new Date());
				prova.setOra_fine(new Date());
				prova.setData_scarico(new Date());
				dao.insertOrUpdateProva(prova);
			 
				for (int i=0;  i<=1000; i++) {
					Lettura l = new Lettura();
					l.setIdSonda("TEST");
					l.setData(new Date());
					l.setTemperaturaGrezza(20+i);
					l.setUmiditaGrezza(50+i);
					l.setStatoBatteria("b");
					l.setTipoMisura(TipoMisura.TEMPERATURA_UMIDITA);
					LetturaProva letturaProva = new LetturaProva(prova, l);
					dao.insertLettura(l);
					dao.insertLetturaProva(letturaProva);
				}
				
				Date endDate = new Date();
				LOGGER.info(TAG, "Fine scrittura letture :"+  sdf.format(endDate));
				LOGGER.info(TAG, "Durata scrittura letture :"+  calcDuration(startDate, endDate));
			 
				
		} catch (Exception e) {
			LOGGER.error(TAG, e); 
		}
		
		return true;
	}
	
	public String calcDuration(Date startDate, Date endDate){

	    long different = endDate.getTime() - startDate.getTime();

	    long secondsInMilli = 1000;
	    long minutesInMilli = secondsInMilli * 60;
	    long hoursInMilli = minutesInMilli * 60;

	    long elapsedHours = different / hoursInMilli;
	    different = different % hoursInMilli;

	    long elapsedMinutes = different / minutesInMilli;
	    different = different % minutesInMilli;

	    long elapsedSeconds = different / secondsInMilli;
	    
	    return  String.format ("%d:%d:%d", elapsedHours, elapsedMinutes, elapsedSeconds);

	}
 
}
