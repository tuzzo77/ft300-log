package com.econorma.persistence;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.econorma.data.Prova;
import com.econorma.util.Logger;

public class DAOProve {

	
	private static final Logger logger = Logger.getLogger(DAOProve.class);
	
	private static final String TAG = DAOProve.class.getCanonicalName();
	
	private static final String TABLE = "PROVE";
	
	private static final String F_ID = "ID";
	private static final String F_RESPONSABILE = "RESPONSABILE";
	private static final String F_DATA_INIZIO = "DATA_INIZIO";
	private static final String F_DATA_FINE = "DATA_FINE";
	private static final String F_ORA_INIZIO = "ORA_INIZIO";
	private static final String F_ORA_FINE = "ORA_FINE";
	private static final String F_ID_SENSORE = "ID_SENSORE";
	private static final String F_DATA_SCARICO = "DATA_SCARICO";
	
	
	public static void  creaIndice(Connection connection) throws SQLException{
		PreparedStatement st = connection.prepareStatement("CREATE INDEX prova_idx ON PROVE (ID);");
		boolean execute = st.execute();
	}
	

	public static void createTable(Connection conn) throws SQLException{
		Statement stat = conn.createStatement();
		String query = "CREATE TABLE PROVE ("+
			    "ID               INTEGER  PRIMARY KEY AUTOINCREMENT"+
			    "                          NOT NULL,"+
			    "ID_SENSORE       VARCHAR,"+
			    "RESPONSABILE     VARCHAR,"+
			    "DATA_INIZIO       DATE     ,"+
			    "ORA_INIZIO       DATETIME,"+
			    "DATA_FINE       DATE     ,"+
			    "ORA_FINE        DATETIME  ,"+
			    "DATA_SCARICO       DATE     "+
		  	");";
		stat.executeQuery(query);
	}
	
	public static boolean insertOrUpdate(Connection connection, Prova prova){
		
		 try{
			 boolean insert = prova.getId()==null;
			 PreparedStatement st;
			  if(insert){
				 st = connection.prepareStatement("INSERT INTO "+TABLE+" (" +
			  		F_RESPONSABILE +", "+
			  		F_DATA_INIZIO +", "+
			  		F_ORA_INIZIO +", "+
			  		F_DATA_FINE +", "+
			  		F_ORA_FINE +", "+
			  		F_ID_SENSORE +",  "+
			  		F_DATA_SCARICO +" "+
			  		
			  				") VALUES (?, ?, ?, ?, ?, ?, ?)");
			  }else{
				  st = connection.prepareStatement("UPDATE "+TABLE+" SET " +
					  		F_RESPONSABILE +"=?, "+
					  		F_DATA_INIZIO +"=?, "+
					  		F_ORA_INIZIO +"=?, "+
					  		F_DATA_FINE +"=?, "+
					  		F_ORA_FINE +"=?, "+
					  		F_ID_SENSORE +"=?, "+
					  		F_DATA_SCARICO +"=? "+
		  				"WHERE "+F_ID+" = ?");
			  }
			  
			  int idx = 1;
			  st.setString(idx++, prova.getResponsabile());
			  st.setDate(idx++, new java.sql.Date(prova.getData_inizio().getTime()));
			  st.setDate(idx++, new java.sql.Date(prova.getOra_inizio().getTime()));
			  st.setDate(idx++, new java.sql.Date(prova.getData_fine().getTime()));
			  st.setDate(idx++, new java.sql.Date(prova.getOra_fine().getTime()));
			  
			  st.setString(idx++, prova.getId_sensore());
			  st.setDate(idx++, new java.sql.Date(prova.getData_scarico().getTime()));
			  
			  if(!insert)
				  st.setLong(idx++, prova.getId());
			 
			  boolean execute = st.execute();
			  ResultSet rs = st.getGeneratedKeys();
			  while(rs.next()){rs.getMetaData();
				  int row_id = rs.getInt("last_insert_rowid()");
				  String findIdQuery = "SELECT id from "+TABLE+" WHERE rowid="+row_id;
				  Statement createStatement = connection.createStatement();
				  ResultSet executeQuery = createStatement.executeQuery(findIdQuery);
				  if(executeQuery.next()){
					  long id = executeQuery.getLong(1);
					  prova.setId(id);
				  }
				  
			  }
			  rs.close();
			  return true;
		  }catch(SQLException e){
			  logger.error(TAG, e);
			  return false;
		  }
	}
	
	public static List<Prova> findAll(Connection connection){
		return find( connection,  0);
	}
	
	public static Prova findUltimaProva(Connection connection){
		List<Prova> find = find( connection,  1);
		if(find!=null && !find.isEmpty()){
			return find.get(0);
		}else
			return null; 
	}
	
	public static List<Prova> find(Connection connection, int limit){
		List<Prova> result = new ArrayList<Prova>();
		String query ="SELECT " +
				F_ID+", " +
				F_RESPONSABILE +", "+
		  		F_DATA_INIZIO +", "+
		  		F_ORA_INIZIO +", "+
		  		F_DATA_FINE +", "+
		  		F_ORA_FINE +", "+
		  		F_ID_SENSORE +", "+
		  		F_DATA_SCARICO +" "+
		  		"FROM "+TABLE+" "+
		  		"ORDER BY "+F_ID+" DESC";
		if(limit>0)
			query = query+" LIMIT "+limit;

		ResultSet rs = null;
		try{
			PreparedStatement st = connection.prepareStatement(query);
			rs = st.executeQuery();
			while(rs.next()){
				Long id = rs.getLong(1);
				String responsabile = rs.getString(2);
				Date data_inizio = rs.getDate(3);
				Date ora_inizio = rs.getDate(4);
				Date data_fine = rs.getDate(5);
				Date ora_fine = rs.getDate(6);
				String id_sensore = rs.getString(7);
				Date data_scarico = rs.getDate(8);
				
				Prova prova = new Prova();
				prova.setId(id);
				prova.setResponsabile(responsabile);
				prova.setData_inizio(data_inizio);
				prova.setOra_inizio(ora_inizio);
				prova.setData_fine(data_fine);
				prova.setOra_fine(ora_fine);
				prova.setId_sensore(id_sensore);
				prova.setData_scarico(data_scarico);
				
				result.add(prova);
			}
		}catch(SQLException e){
			logger.error(TAG, e);
			return null;
		}finally{
			if(rs!=null)
				try {
					rs.close();
				} catch (SQLException e) {
				}
		}
		
		return result;
		
	}
	
	
	public static void deleteByProva(Connection connection, Prova p_prova){
 	try{
 		 PreparedStatement st =null;
		 String query ="DELETE " +
				"FROM "+TABLE+" WHERE "+F_ID+"="+p_prova.getId();
		 st = connection.prepareStatement(query);
		st.executeUpdate();
			 
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		 
	}
	
	
	 
}
