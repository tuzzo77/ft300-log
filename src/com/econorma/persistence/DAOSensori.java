package com.econorma.persistence;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.econorma.data.Lettura.TipoMisura;
import com.econorma.data.Sensore;
import com.econorma.data.Sensore.Location;
import com.econorma.io.ParsersLogger.StatusResponse.VERSION;
import com.econorma.util.Logger;
import com.econorma.util.StringUtils;

class DAOSensori {

	private static final Logger logger = Logger.getLogger(DAOSensori.class);
	private static final String TAG = "DAOSensori";

	public static void deleteAll(Connection connection) {
		try {
			PreparedStatement st = connection.prepareStatement("DELETE FROM "
					+ DAO.TABLE_SONDE);
			st.execute();
		} catch (SQLException e) {
			logger.error(TAG, "Sql non valido!",e);
			throw new RuntimeException(e);
		}
	}
	
	public static boolean insertSensore(Connection connection, Sensore s) {
		
		
		 try{
			 
			 String id =null;
			 boolean execute=true;
			
			 String findId = "SELECT ID_SONDA FROM "+DAO.TABLE_SONDE+" WHERE ID_SONDA="+ "'" + s.getId_sonda() + "'";
			  Statement createStatement = connection.createStatement();
			  ResultSet executeQuery = createStatement.executeQuery(findId);
			  if(executeQuery.next()){
				  id = executeQuery.getString(1);
			  }
			  
			  if (id==null){
				  
				  PreparedStatement st = connection
							.prepareStatement("INSERT INTO "
									+ DAO.TABLE_SONDE
									+ " (ID_SONDA, OFFSET, DESCRIZIONE, LOCATION, RANGEMIN, RANGEMAX, RANGEMINURT, RANGEMAXURT, "
									+ " OFFSET_URT, INCERTEZZA, INCERTEZZA_URT, TRASMISSION, DATA, TIPO_LETTURA, STATO_BATTERIA, "
									+ " TEMPERATURA, UMIDITA, VOLT, MILLIAMPERE, OHM, APERTO_CHIUSO, RANGEMINOHM, RANGEMAXOHM, CAMPIONI, VERSIONE) "
									+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					
					st.setString(1, s.getId_sonda());
					st.setDouble(2, s.getOffset());
					st.setString(3, s.getDescrizione());
					st.setString(4, s.getLocation().name());
					st.setDouble(5, s.getRangeMin());
					st.setDouble(6, s.getRangeMax());
					st.setDouble(7, s.getRangeMinURT());
					st.setDouble(8, s.getRangeMaxURT());
					st.setDouble(9, s.getOffsetURT());
					st.setDouble(10, s.getIncertezza());
					st.setDouble(11, s.getIncertezzaURT());
					st.setInt(12,  s.getTrasmission());
					st.setDate(13, new java.sql.Date(s.getData().getTime()));
					st.setString(14, s.getTipoMisura().name());
					st.setString(15,  s.getStatoBatteria());
					st.setDouble(16, s.getTemperature());
					st.setDouble(17, s.getHumidity());
					st.setDouble(18, s.getVolt());
					st.setDouble(19, s.getMilliampere());
					st.setDouble(20, s.getOhm());
					st.setString(21, s.getApertoChiuso());
					st.setDouble(22, s.getRangeMinOhm());
					st.setDouble(23, s.getRangeMinOhm());
					st.setInt(24, s.getCampioni());
					st.setString(25, s.getVersione().toString());
				  
				  execute = st.execute();
				  
				  
			  } else {
				 
				  PreparedStatement st = connection.prepareStatement("UPDATE "+ DAO.TABLE_SONDE	+ " SET"
				  + " DESCRIZIONE =? " + "," 
				  + " OFFSET =? "  + ","
				  + " RANGEMIN =? " + ","
				  + " RANGEMAX =? " + ","
				  + " RANGEMINURT =? " + ","
				  + " RANGEMAXURT =? " + ","
				  + " OFFSET_URT =? " 
				  + " WHERE ID_SONDA = ?");
				  st.setString(1, s.getDescrizione());
				  st.setDouble(2, s.getOffset());
				  st.setDouble(3, s.getRangeMin());
				  st.setDouble(4, s.getRangeMax());
				  st.setDouble(5, s.getRangeMinURT());
				  st.setDouble(6, s.getRangeMaxURT());
				  st.setDouble(7, s.getOffsetURT());
				  st.setString(8, s.getId_sonda());
				  execute = st.execute();
			  }
			 
			  
			  return execute;
			 
				 
			  }
			 	
			 	catch(SQLException e){
				  logger.error(TAG, e);
				  return false;
			  }
		 
			
	}

	public static int[] insert(Connection connection,
			Collection<Sensore> sensori) {



		if (sensori == null)
			return new int[0];
		try {
			PreparedStatement st = connection
					.prepareStatement("INSERT INTO "
							+ DAO.TABLE_SONDE
							+ " (ID_SONDA, OFFSET, DESCRIZIONE, LOCATION, RANGEMIN, RANGEMAX, RANGEMINURT, RANGEMAXURT, "
							+ " OFFSET_URT, INCERTEZZA, INCERTEZZA_URT, TRASMISSION, DATA, TIPO_LETTURA, STATO_BATTERIA, "
							+ "TEMPERATURA, UMIDITA, CAMPIONI, VOLT, MILLIAMPERE, OHM, APERTO_CHIUSO, RANGEMINOHM, RANGEMAXOHM, VERSIONE) "
							+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			for (Sensore s : sensori) {
				st.setString(1, s.getId_sonda());
				st.setDouble(2, s.getOffset());
				st.setString(3, s.getDescrizione());
				st.setString(4, s.getLocation().name());
				st.setDouble(5, s.getRangeMin());
				st.setDouble(6, s.getRangeMax());
				st.setDouble(7, s.getRangeMinURT());
				st.setDouble(8, s.getRangeMaxURT());
				st.setDouble(9, s.getOffsetURT());
				st.setDouble(10, s.getIncertezza());
				st.setDouble(11, s.getIncertezzaURT());
				st.setInt(12,  s.getTrasmission());
				st.setDate(13, new java.sql.Date(s.getData().getTime()));
				st.setString(14, s.getTipoMisura().name());
				st.setString(15,  s.getStatoBatteria());
				st.setDouble(16, s.getTemperature());
				st.setDouble(17, s.getHumidity());
				st.setInt(18, s.getCampioni());
				st.setDouble(19, s.getVolt());
				st.setDouble(20, s.getMilliampere());
				st.setDouble(21, s.getOhm());
				st.setString(22, s.getApertoChiuso());
				st.setDouble(23, s.getRangeMinOhm());
				st.setDouble(24, s.getRangeMaxOhm());
				st.setString(25, s.getVersione().name());
				st.addBatch();

			}
			int[] ids = st.executeBatch();
			return ids;
		} catch (SQLException e) {
			logger.error(TAG, "Sql non valido!",e);
			throw new RuntimeException(e);
		}
	}

	public static boolean update(Connection connection, Sensore sensore) {
		try {
			PreparedStatement st = connection
					.prepareStatement("UPDATE "
							+ DAO.TABLE_SONDE
							+ " SET "
							+ "INDIRIZZO =? "
							+ "WHERE ID_SONDA = ?");

			st.setString(2, sensore.getId_sonda());
			return st.execute();
		} catch (SQLException e) {
			logger.error(TAG, "Sql non valido!",e);
			throw new RuntimeException(e);

		}
	}

	public static boolean deleteBySonda(Connection connection, Sensore sensore) {
		try {
			PreparedStatement st = connection
					.prepareStatement("DELETE FROM "
							+ DAO.TABLE_SONDE
							+ " WHERE ID_SONDA = ?");

			st.setString(1, sensore.getId_sonda());

			return st.execute();
		} catch (SQLException e) {
			logger.error(TAG, "Sql non valido!",e);
			throw new RuntimeException(e);

		}
	}

	public static void deleteSondeUnknown(Connection connection) {
		try {
			PreparedStatement st = connection
					.prepareStatement("DELETE FROM "
							+ DAO.TABLE_SONDE
							+ " WHERE LOCATION = 'UNKNOWN'");

			st.execute();
		} catch (SQLException e) {
			logger.error(TAG, "Sql non valido!",e);
			throw new RuntimeException(e);

		}
	}

	public static void enableAlarms(Connection connection) {

		int result = 0;
		boolean[] daysOfWeek = new boolean[7];
		Arrays.fill(daysOfWeek, true);
		for(boolean b:daysOfWeek)result=result*2+(b?1:0);

		try {
			PreparedStatement st = connection
					.prepareStatement("UPDATE "
							+ DAO.TABLE_SONDE
							+ " SET "
							+ "ENABLE_ALARM =?"
							+ ","
							+ "DAYS_OF_WEEK =?");

			st.setBoolean(1, true);
			st.setInt(2, result);

			st.execute();
		} catch (SQLException e) {
			logger.error(TAG, "Sql non valido!",e);
			throw new RuntimeException(e);

		}
	}

	public static List<Sensore> loadAll(Connection connection) {
		List<Sensore> result = new ArrayList<Sensore>();
		ResultSet rs = null;
		try {
			Statement select = connection.createStatement();
			rs = select.executeQuery("SELECT ID_SONDA, OFFSET, DESCRIZIONE,LOCATION, "
					+ "RANGEMIN, RANGEMAX, RANGEMINURT, RANGEMAXURT, "
					+ "OFFSET_URT, INCERTEZZA, INCERTEZZA_URT, TRASMISSION, DATA, TIPO_LETTURA, STATO_BATTERIA, "
					+ "TEMPERATURA, UMIDITA, CAMPIONI, VOLT, MILLIAMPERE, OHM, APERTO_CHIUSO, OFFSET_VOLT, OFFSET_MILLIAMPERE, OFFSET_OHM, RANGEMINOHM, RANGEMAXOHM, VERSIONE "
					+ "FROM " + DAO.TABLE_SONDE
					+ " ORDER BY ID_SONDA");
			while (rs.next()) {
				String id_sonda = rs.getString("ID_SONDA");
				if (StringUtils.nullOrEmpty(id_sonda))
					// TODO Log
					// ignore
					continue;
				double offset = rs.getDouble("OFFSET");
				String descrizione = rs.getString("DESCRIZIONE");
				Location location = Location.fromString(rs.getString("LOCATION"));
				double rangeMin = rs.getDouble("RANGEMIN");
				double rangeMax = rs.getDouble("RANGEMAX");
				double rangeMinURT = rs.getDouble("RANGEMINURT");
				double rangeMaxURT = rs.getDouble("RANGEMAXURT");
			
				double offsetURT = rs.getDouble("OFFSET_URT");
				double incertezza = rs.getDouble("INCERTEZZA");
				double incertezzaURT = rs.getDouble("INCERTEZZA_URT");
				
				int trasmission = rs.getInt("TRASMISSION");
				Date data = rs.getDate("DATA");
				String tipo_lettura = rs.getString("TIPO_LETTURA");
				String stato_batteria = rs.getString("STATO_BATTERIA");

				double temperatura = rs.getDouble("TEMPERATURA");
				double umidita = rs.getDouble("UMIDITA");
				int campioni = rs.getInt("CAMPIONI");
				
				double volt = rs.getDouble("VOLT");
				double milliampere = rs.getDouble("MILLIAMPERE");
				double ohm = rs.getDouble("OHM");
				String apertoChiuso = rs.getString("APERTO_CHIUSO");
				
				double offsetVolt = rs.getDouble("OFFSET_VOLT");
				double offsetMilliampere = rs.getDouble("OFFSET_MILLIAMPERE");
				double offsetOhm = rs.getDouble("OFFSET_OHM");
				
				double rangeMinOhm = rs.getDouble("RANGEMINOHM");
				double rangeMaxOhm = rs.getDouble("RANGEMAXOHM");
				
				VERSION versione = VERSION.valueOf(rs.getString("VERSIONE"));
				
				Sensore s = Sensore.newInstance(id_sonda, offset, descrizione,
						location, rangeMin, rangeMax, rangeMinURT, rangeMaxURT, 
						offsetURT, incertezza, incertezzaURT, trasmission, data, TipoMisura.valueOf(tipo_lettura), 
						stato_batteria, temperatura, umidita, volt, milliampere, ohm, apertoChiuso, 
						offsetVolt, offsetMilliampere, offsetOhm, rangeMinOhm, rangeMaxOhm, campioni, versione);
				result.add(s);
			}
			return result;
		} catch (Exception e) {
			throw new PersistenceException(e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
				}
		}
	}
	
	public static Sensore loadSensorById(Connection connection, String idSonda) {
		Sensore sensor = null;
		ResultSet rs = null;
		try {
			Statement select = connection.createStatement();
			rs = select.executeQuery("SELECT ID_SONDA, OFFSET, DESCRIZIONE,LOCATION, "
					+ "RANGEMIN, RANGEMAX, RANGEMINURT, RANGEMAXURT, "
					+ "OFFSET_URT, INCERTEZZA, INCERTEZZA_URT, TRASMISSION, DATA, TIPO_LETTURA, STATO_BATTERIA, "
					+ "TEMPERATURA, UMIDITA, CAMPIONI, VOLT, MILLIAMPERE, OHM, APERTO_CHIUSO, OFFSET_VOLT, OFFSET_MILLIAMPERE, OFFSET_OHM, RANGEMINOHM, RANGEMAXOHM, VERSIONE "
					+ "FROM " + DAO.TABLE_SONDE + " "
					+ "WHERE ID_SONDA =" + "'" + idSonda + "'"
					+ " ORDER BY ID_SONDA");
			while (rs.next()) {
				String id_sonda = rs.getString("ID_SONDA");
				if (StringUtils.nullOrEmpty(id_sonda))
					// TODO Log
					// ignore
					continue;
				double offset = rs.getDouble("OFFSET");
				String descrizione = rs.getString("DESCRIZIONE");
				Location location = Location.fromString(rs.getString("LOCATION"));
				double rangeMin = rs.getDouble("RANGEMIN");
				double rangeMax = rs.getDouble("RANGEMAX");
				double rangeMinURT = rs.getDouble("RANGEMINURT");
				double rangeMaxURT = rs.getDouble("RANGEMAXURT");
			
				double offsetURT = rs.getDouble("OFFSET_URT");
				double incertezza = rs.getDouble("INCERTEZZA");
				double incertezzaURT = rs.getDouble("INCERTEZZA_URT");
				
				int trasmission = rs.getInt("TRASMISSION");
				Date data = rs.getDate("DATA");
				String tipo_lettura = rs.getString("TIPO_LETTURA");
				String stato_batteria = rs.getString("STATO_BATTERIA");

				double temperatura = rs.getDouble("TEMPERATURA");
				double umidita = rs.getDouble("UMIDITA");
				int campioni = rs.getInt("CAMPIONI");
				
				double volt = rs.getDouble("VOLT");
				double milliampere = rs.getDouble("MILLIAMPERE");
				double ohm = rs.getDouble("OHM");
				String apertoChiuso = rs.getString("APERTO_CHIUSO");
				
				double offsetVolt = rs.getDouble("OFFSET_VOLT");
				double offsetMilliampere = rs.getDouble("OFFSET_MILLIAMPERE");
				double offsetOhm = rs.getDouble("OFFSET_OHM");
				
				double rangeMinOhm = rs.getDouble("RANGEMINOHM");
				double rangeMaxOhm = rs.getDouble("RANGEMAXOHM");
				
				VERSION versione = VERSION.valueOf(rs.getString("VERSIONE"));
				
				sensor = Sensore.newInstance(id_sonda, offset, descrizione,
						location, rangeMin, rangeMax, rangeMinURT, rangeMaxURT, 
						offsetURT, incertezza, incertezzaURT, trasmission, data, TipoMisura.valueOf(tipo_lettura), 
						stato_batteria, temperatura, umidita, volt, milliampere, ohm, apertoChiuso, 
						offsetVolt, offsetMilliampere, offsetOhm, rangeMinOhm, rangeMaxOhm, campioni, versione);
				 
			}
			return sensor;
		} catch (Exception e) {
			throw new PersistenceException(e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
				}
		}
	}
}
