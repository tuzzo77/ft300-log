package com.econorma.persistence;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.jdesktop.application.Application;

import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;

public class AppConfChanger {

	private static final String FILE_NAME = "Application.properties";
	private static Logger logger = Logger.getLogger(AppConfChanger.class);
	private static final String TAG = "AppConfChanger";
	
	private Application app;
	private Properties properties;
	
	
	private File f;
	
	public static class AppCfg{
		
		public final static String TIPO_SENSORI = "Application.tipoSensori";
		public final static String THEME = "Application.theme";
		public static final String UPGRADE_DB = "Application.upgradeDB";
		public final static String NAME = "Application.name";
		public final static String TITLE = "Application.title";
		public final static String DELETE_AFTER_DOWNLOAD = "Application.deleteAfterDownload";
		public static final String EXIT_ALERT = "Application.exitAlert";
		public static final String SYSTEM_TRAY = "Application.systemTray";
		public static final String PATH_FILE = "Application.pathFile";
		public static final String INTERFACE = "Application.interface";
	}
	
	
	public AppConfChanger (Application app){
	
		f = new File("application.properties");
		FileInputStream fis=null;
		
		boolean load=false;
		
		try {
			if (f.exists()){
				logger.debug(TAG, "File gi� esistente:" + f);
				properties=new Properties();
				fis = new FileInputStream(f);
				properties.load(fis);
				load=true;
			}
			
		} catch (Exception e) {
			logger.error(TAG, e);
		}finally{
			IOUtils.closeQuietly(fis);
		}
		
		
		if (!load){
			InputStream is=null;
			OutputStream out =null;
			
			try {
				properties = new Properties();
				
				is = getStream();
				properties.load(is);
				
				out= new FileOutputStream(f);
				logger.debug(TAG, "Creazione File properties:" + f);
			    properties.store(out, "Options file for settings");
				
				
			} catch (Exception e) {
				logger.error(TAG, e);
			} finally{
				IOUtils.closeQuietly(is);
				IOUtils.closeQuietly(out);
			}
		}
		
		        
	}
	
	
	public String getValue(String key) {
		return properties.getProperty(key);
		
	}
	
	public void setValue(String key, String value) {
		properties.setProperty(key, value);
		
	}
	
 
		
	 
	
	/**
	 *  non funziona nel jar
	 *  
	 * @return
	 */
	private URL getURL(){
		String resourcesDir = app.getContext().getResourceManager().getResourceMap().getResourcesDir();
		logger.debug(TAG, "Resource Dir : "+ resourcesDir);
		URL resource = Thread.currentThread().getContextClassLoader().getResource("com/econorma/resources/Application.properties");
		logger.debug(TAG, "Resource url : "+ resource);
		return resource;
	}
	
	private InputStream getStream(){
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("com/econorma/resources/Application.properties");
		logger.debug(TAG, "Resource stream : "+ (in!=null));
		return in;
	}
	
	public void setProperty(String prop, String val){
		properties.setProperty(prop, val);
	}
	
	
	public boolean save(){
		
		logger.info(TAG, "Salvo APP CONF");
		logger.log(LoggerCustomLevel.AUDIT, "Programma riavviato per salvataggio Opzioni");

		OutputStream out=null;
		try {
		 	out= new FileOutputStream( f );
		    properties.store(out, "Options file for settings");
			return true;
			
		} catch (Exception e) {
			return false;
		} finally{
			IOUtils.closeQuietly(out);
		}
		
		
	}
	
 
 
}
