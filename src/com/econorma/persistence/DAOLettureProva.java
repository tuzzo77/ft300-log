package com.econorma.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.econorma.Application;
import com.econorma.data.LastSensor;
import com.econorma.data.Lettura.TipoMisura;
import com.econorma.data.LetturaProva;
import com.econorma.data.Prova;
import com.econorma.data.Sensore;
import com.econorma.gui.GUI;
import com.econorma.logic.SensoreManager;
import com.econorma.logic.StandardDeviation;
import com.econorma.util.Logger;
import com.econorma.util.StringUtils;

public class DAOLettureProva {

	private static final Logger logger = Logger.getLogger(DAOLettureProva.class);
	private static final String TAG = DAOLettureProva.class.getCanonicalName();

	private static final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	private static final SimpleDateFormat sdfTime = new SimpleDateFormat("HH");

	private static final String TABLE = "LETTURE_PROVA";
	private static final String TABLE_VIEW = "letture_prova_view";

	private static final String F_ID = "ID";
	private static final String F_PROVA_ID ="PROVA_ID";
	private static final String F_VALORE = "VALORE";
	private static final String F_VALORE_GREZZO= "VALORE_GREZZO";
	private static final String F_ID_SONDA = "ID_SONDA";
	private static final String F_DATA = "DATA";
	private static final String F_VALORE_UMIDITA = "VALORE_UMIDITA";
	private static final String F_TIPO_LETTURA = "TIPO_LETTURA";
	private static final String F_DESCRIZIONE = "DESCRIZIONE";
	private static final String F_STATO_BATTERIA = "STATO_BATTERIA";
	private static final String F_VALORE_UMIDITA_GREZZO= "VALORE_UMIDITA_GREZZO";
	private static final String F_OFFSET= "OFFSET";
	private static final String F_OFFSET_URT= "OFFSET_URT";
	private static final String F_OFFSET_VOLT= "OFFSET_VOLT";
	private static final String F_OFFSET_MILLIAMPERE= "OFFSET_MILLIAMPERE";
	private static final String F_OFFSET_OHM= "OFFSET_OHM";
	private static final String F_INCERTEZZA= "INCERTEZZA";
	private static final String F_INCERTEZZA_URT= "INCERTEZZA_URT";
	private static final String F_VOLT= "VOLT";
	private static final String F_MILLIAMPERE= "MILLIAMPERE";
	private static final String F_OHM= "OHM";
	private static final String F_APERTO_CHIUSO = "APERTO_CHIUSO";
	private static final String F_VOLT_GREZZO= "VOLT_GREZZO";
	private static final String F_MILLIAMPERE_GREZZO= "MILLIAMPERE_GREZZO";
	private static final String F_OHM_GREZZO= "OHM_GREZZO";
	
	public static void  dropIndici(Connection connection) throws SQLException{
		PreparedStatement st = connection.prepareStatement("DROP INDEX letture_prova_idx;");
		boolean execute = st.execute();
		st = connection.prepareStatement("DROP INDEX letture_prova_idx_v2;");
		execute = st.execute();
	}

	public static void  creaIndici(Connection connection) throws SQLException{
		
		boolean execute = false;
		PreparedStatement st=null;
		
		st = connection.prepareStatement("CREATE INDEX letture_prova_idx ON LETTURE_PROVA ( ID_SONDA, DATA);");
		execute = st.execute();
		
		st = connection.prepareStatement("CREATE INDEX letture_prova_idx_v2 ON LETTURE_PROVA ( PROVA_ID, ID_SONDA, DATA );");
		execute = st.execute();
		
		st = connection.prepareStatement("CREATE INDEX letture_prova_idx_v3 ON LETTURE_PROVA ( DATA, ID_SONDA );");
		execute = st.execute();
		 
	}

	public static void createTable(Connection conn) throws SQLException{
		Statement stat = conn.createStatement();
		String query = "CREATE TABLE LETTURE_PROVA ("+
				"ID            INTEGER  PRIMARY KEY AUTOINCREMENT"+
				"                       NOT NULL,"+
				"PROVA_ID      INTEGER  NOT NULL"+
				"                       REFERENCES PROVE ( ID ),"+
				"ID_SONDA      VARCHAR  NOT NULL,"+
				"VALORE        REAL     NOT NULL,"+
				"VALORE_GREZZO REAL     NOT NULL,"+
				"DATA          DATETIME NOT NULL, "+
				"VALORE_UMIDITA REAL    NOT NULL, "+
				"VALORE_UMIDITA_GREZZO REAL     NOT NULL, "+
				"TIPO_LETTURA TEXT      NOT NULL, " +
				"STATO_BATTERIA TEXT    NOT NULL, " +
				"OFFSET             REAL    NOT NULL, " +
				"OFFSET_URT         REAL    NOT NULL, " +
				"INCERTEZZA         REAL    NOT NULL, " +
				"INCERTEZZA_URT     REAL    NOT NULL, " +
				"VOLT               REAL    NOT NULL, "    +
				"VOLT_GREZZO        REAL    NOT NULL, " +
				"MILLIAMPERE        REAL    NOT NULL, "  +
				"MILLIAMPERE_GREZZO  REAL    NOT NULL, " +
				"OHM                REAL    NOT NULL, "  +
				"OHM_GREZZO         REAL    NOT NULL, "  +
				"APERTO_CHIUSO      TEXT    NOT NULL," +
				"OFFSET_VOLT        REAL    NOT NULL," +
				"OFFSET_MILLIAMPERE REAL    NOT NULL," +
				"OFFSET_OHM         REAL    NOT NULL " +
				");";
	
		stat.executeQuery(query);
	}
 

	public static boolean insert(Connection connection, LetturaProva lettura){
		try{
			PreparedStatement st;

			logger.debug(TAG, "LettureProve inserito: Time " + lettura.getId_sonda() + " - " + sdf.format(lettura.getData()));

			st = connection.prepareStatement("INSERT INTO "+TABLE+" (" +
					F_PROVA_ID +", "+
					F_VALORE +", "+
					F_VALORE_GREZZO +", "+
					F_ID_SONDA +", "+
					F_DATA + ", "  +
					F_VALORE_UMIDITA + "," + 
					F_TIPO_LETTURA + "," + 
					F_STATO_BATTERIA + "," +
					F_VALORE_UMIDITA_GREZZO + ","+
					F_OFFSET + ","+
					F_OFFSET_URT + ","+
					F_INCERTEZZA + ","+
					F_INCERTEZZA_URT + ","+ 
					F_VOLT + ","+
					F_VOLT_GREZZO + ","+
					F_MILLIAMPERE + ","+
					F_MILLIAMPERE_GREZZO + ","+
					F_OHM + ","+
					F_OHM_GREZZO + ","+
					F_APERTO_CHIUSO + ","+
					F_OFFSET_VOLT + ","+
					F_OFFSET_MILLIAMPERE + ","+
					F_OFFSET_OHM +  
					") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");


			int idx = 1;
			st.setLong(idx++, lettura.getProva_id());
			st.setDouble(idx++, lettura.getValore());
			st.setDouble(idx++, lettura.getValore_grezzo());
			st.setString(idx++, lettura.getId_sonda());
			st.setDate(idx++, new java.sql.Date(lettura.getData().getTime()));
			st.setDouble(idx++, lettura.getUmidita());


			String tipo = "T";
			switch (lettura.getTipo_misura()) {
			case TEMPERATURA:
				tipo = "T";
				break;
			case TEMPERATURA_UMIDITA:
				tipo = "E";
				break;
			case TENSIONE_CORRENTE:
				tipo = "V";
				break;
			default:
				break;
			}
			st.setString(idx++, tipo);
			
			st.setString(idx++, lettura.getStatoBatteria());
			st.setDouble(idx++, lettura.getUmidita_grezzo());
			
			st.setDouble(idx++, lettura.getOffset());
			st.setDouble(idx++, lettura.getOffsetUrt());
			
			st.setDouble(idx++, lettura.getIncertezza());
			st.setDouble(idx++, lettura.getIncertezzaUrt());
			
			st.setDouble(idx++, lettura.getVolt());
			st.setDouble(idx++, lettura.getVolt_grezzo());
			st.setDouble(idx++, lettura.getMilliampere());
			st.setDouble(idx++, lettura.getMilliampere_grezzo());
			st.setDouble(idx++, lettura.getOhm());
			st.setDouble(idx++, lettura.getOhm_grezzo());
			st.setString(idx++, lettura.getApertoChiuso());
			
			st.setDouble(idx++, lettura.getOffsetVolt());
			st.setDouble(idx++, lettura.getOffsetMilliampere());
			st.setDouble(idx++, lettura.getOffsetOhm());

			boolean execute = st.execute();
			return execute;
		}catch(Exception e){
			logger.error(TAG,e);
			return false;
		}
	}
	
	public static boolean insertLetture(Connection connection, List<LetturaProva> letture){
		try{
			PreparedStatement st;


			st = connection.prepareStatement("INSERT INTO "+TABLE+" (" +
					F_PROVA_ID +", "+
					F_VALORE +", "+
					F_VALORE_GREZZO +", "+
					F_ID_SONDA +", "+
					F_DATA + ", "  +
					F_VALORE_UMIDITA + "," + 
					F_TIPO_LETTURA + "," + 
					F_STATO_BATTERIA + "," +
					F_VALORE_UMIDITA_GREZZO + ","+
					F_OFFSET + ","+
					F_OFFSET_URT + ","+
					F_INCERTEZZA + ","+
					F_INCERTEZZA_URT + ","+ 
					F_VOLT + ","+
					F_VOLT_GREZZO + ","+
					F_MILLIAMPERE + ","+
					F_MILLIAMPERE_GREZZO + ","+
					F_OHM + ","+
					F_OHM_GREZZO + ","+
					F_APERTO_CHIUSO + ","+
					F_OFFSET_VOLT + ","+
					F_OFFSET_MILLIAMPERE + ","+
					F_OFFSET_OHM +  
					") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

				connection.setAutoCommit(false);
			
			
			  for (LetturaProva lettura: letture) {
				  int idx = 1;
				  	st.setLong(idx++, lettura.getProva_id());
					st.setDouble(idx++, lettura.getValore());
					st.setDouble(idx++, lettura.getValore_grezzo());
					st.setString(idx++, lettura.getId_sonda());
					st.setDate(idx++, new java.sql.Date(lettura.getData().getTime()));
					st.setDouble(idx++, lettura.getUmidita());


					String tipo = "T";
					switch (lettura.getTipo_misura()) {
					case TEMPERATURA:
						tipo = "T";
						break;
					case TEMPERATURA_UMIDITA:
						tipo = "E";
						break;
					case TENSIONE_CORRENTE:
						tipo = "V";
						break;
					default:
						break;
					}
					st.setString(idx++, tipo);
					
					st.setString(idx++, lettura.getStatoBatteria());
					st.setDouble(idx++, lettura.getUmidita_grezzo());
					
					st.setDouble(idx++, lettura.getOffset());
					st.setDouble(idx++, lettura.getOffsetUrt());
					
					st.setDouble(idx++, lettura.getIncertezza());
					st.setDouble(idx++, lettura.getIncertezzaUrt());
					
					st.setDouble(idx++, lettura.getVolt());
					st.setDouble(idx++, lettura.getVolt_grezzo());
					st.setDouble(idx++, lettura.getMilliampere());
					st.setDouble(idx++, lettura.getMilliampere_grezzo());
					st.setDouble(idx++, lettura.getOhm());
					st.setDouble(idx++, lettura.getOhm_grezzo());
					st.setString(idx++, lettura.getApertoChiuso());
					
					st.setDouble(idx++, lettura.getOffsetVolt());
					st.setDouble(idx++, lettura.getOffsetMilliampere());
					st.setDouble(idx++, lettura.getOffsetOhm());
					
					st.addBatch();
			  }
			
			  int[] count = st.executeBatch();
			  connection.commit();
			  
			return true;
		}catch(Exception e){
			logger.error(TAG,e);
			return false;
		}
	}

	public static List<LetturaProva> findByProvaAndSonda(Connection connection, Prova p_prova, String p_id_sonda){
		try{
			PreparedStatement st =null;

			StringBuilder sb = new StringBuilder();
			sb.append("SELECT " +
					F_ID+", "+
					F_PROVA_ID+", "+
					F_VALORE+", "+
					F_VALORE_GREZZO+", "+
					F_ID_SONDA+", "+
					F_DATA+", "+
					F_VALORE_UMIDITA+", "+
					F_TIPO_LETTURA +", "+
					F_VALORE_UMIDITA_GREZZO +", " +
					F_OFFSET +", " +
					F_OFFSET_URT +", " +
					F_INCERTEZZA +", " +
					F_INCERTEZZA_URT + ","+ 
					F_VOLT + ","+
					F_VOLT_GREZZO + ","+
					F_MILLIAMPERE + ","+
					F_MILLIAMPERE_GREZZO + ","+
					F_OHM + ","+
					F_OHM_GREZZO + ","+
					F_APERTO_CHIUSO + ","+
					F_OFFSET_VOLT + ","+
					F_OFFSET_MILLIAMPERE + ","+
					F_OFFSET_OHM +  " " +
					"FROM "+TABLE+" WHERE "+F_PROVA_ID+"="+p_prova.getId());

			if(StringUtils.isSome(p_id_sonda)){
				sb.append(" AND "+F_ID_SONDA+"='"+p_id_sonda+"' "); //TODO sql injection hole!!!
			}

			sb.append(" ORDER BY "+F_DATA+" ASC");
			st = connection.prepareStatement(sb.toString());


			ResultSet rs = st.executeQuery();

			List<LetturaProva> letture = new ArrayList<LetturaProva>();
			while(rs.next()){
				Long id = rs.getLong(1);
				Long prova_id = rs.getLong(2);
				double valore = rs.getDouble(3);
				double valore_grezzo = rs.getDouble(4);
				String id_sonda = rs.getString(5);
				Date date = new Date(rs.getDate(6).getTime());
				double valore_umidita = rs.getDouble(7);
				String tipo_l = rs.getString(8);
				double umidita_grezzo = rs.getDouble(9);
				double offset = rs.getDouble(10);
				double offset_urt = rs.getDouble(11);
				double incertezza = rs.getDouble(12);
				double incertezza_urt = rs.getDouble(13);
				double volt = rs.getDouble(14);
				double volt_grezzo = rs.getDouble(15);
				double milliampere = rs.getDouble(16);
				double milliampere_grezzo = rs.getDouble(17);
				double ohm = rs.getDouble(18);
				double ohm_grezzo = rs.getDouble(19);
				String apertoChiuso = rs.getString(20);
				
				double offset_volt = rs.getDouble(21);
				double offset_milliampere = rs.getDouble(22);
				double offset_ohm = rs.getDouble(23);
				
				LetturaProva letturaProva = new LetturaProva();
				letturaProva.setId(id);
				letturaProva.setProva_id(prova_id);
				letturaProva.setValore(valore);
				letturaProva.setValore_grezzo(valore_grezzo);
				letturaProva.setId_sonda(id_sonda);
				letturaProva.setData(date);
				letturaProva.setUmidita(valore_umidita);
				
				if("T".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TEMPERATURA);
				}else if("E".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TEMPERATURA_UMIDITA);
				}else  if("V".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TENSIONE_CORRENTE);
				}
				
				letturaProva.setUmidita_grezzo(umidita_grezzo);
				letturaProva.setOffset(offset);
				letturaProva.setOffsetUrt(offset_urt);
				letturaProva.setIncertezza(incertezza);
				letturaProva.setIncertezzaUrt(incertezza_urt);
				
				letturaProva.setVolt(volt);
				letturaProva.setMilliampere(milliampere);
				letturaProva.setOhm(ohm);
				letturaProva.setApertoChiuso(apertoChiuso);
				
				letturaProva.setOffsetVolt(offset_volt);
				letturaProva.setOffsetMilliampere(offset_milliampere);
				letturaProva.setOffsetOhm(offset_ohm);
				
				letturaProva.setVolt_grezzo(volt_grezzo);
				letturaProva.setMilliampere_grezzo(milliampere_grezzo);
				letturaProva.setOhm_grezzo(ohm_grezzo);

				letture.add(letturaProva);

			}
			rs.close();
			return letture;
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}

	public static List<LetturaProva> findByProvaAndSondaPrefix(Connection connection, Prova p_prova, String p_id_sonda){
		try{
			PreparedStatement st =null;

			StringBuilder sb = new StringBuilder();
			sb.append("SELECT " +
					F_ID+", "+
					F_PROVA_ID+", "+
					F_VALORE+", "+
					F_VALORE_GREZZO+", "+
					F_ID_SONDA+", "+
					F_DATA+", "+
					F_VALORE_UMIDITA+", "+
					F_TIPO_LETTURA +", "+
					F_VALORE_UMIDITA_GREZZO +", " +
					F_OFFSET +", " +
					F_OFFSET_URT +", " +
					F_INCERTEZZA +", " +
					F_INCERTEZZA_URT + ","+ 
					F_VOLT + ","+
					F_VOLT_GREZZO + ","+
					F_MILLIAMPERE + ","+
					F_MILLIAMPERE_GREZZO + ","+
					F_OHM + ","+
					F_OHM_GREZZO + ","+
					F_APERTO_CHIUSO + ","+
					F_OFFSET_VOLT + ","+
					F_OFFSET_MILLIAMPERE + ","+
					F_OFFSET_OHM +  " " +
					"FROM "+TABLE+" WHERE "+F_PROVA_ID+"="+p_prova.getId());

			if(StringUtils.isSome(p_id_sonda)){
				sb.append(" AND "+F_ID_SONDA+" LIKE '"+p_id_sonda+"%' "); //TODO sql injection hole!!!
			}

			sb.append(" ORDER BY "+F_DATA+" ASC");
			st = connection.prepareStatement(sb.toString());


			ResultSet rs = st.executeQuery();

			List<LetturaProva> letture = new ArrayList<LetturaProva>();
			while(rs.next()){
				Long id = rs.getLong(1);
				Long prova_id = rs.getLong(2);
				double valore = rs.getDouble(3);
				double valore_grezzo = rs.getDouble(4);
				String id_sonda = rs.getString(5);
				Date date = new Date(rs.getDate(6).getTime());
				double valore_umidita = rs.getDouble(7);
				String tipo_l = rs.getString(8);
				double umidita_grezzo = rs.getDouble(9);
				double offset = rs.getDouble(10);
				double offset_urt = rs.getDouble(11);
				double incertezza = rs.getDouble(12);
				double incertezza_urt = rs.getDouble(13);
				
				double volt = rs.getDouble(14);
				double volt_grezzo = rs.getDouble(15);
				double milliampere = rs.getDouble(16);
				double milliampere_grezzo = rs.getDouble(17);
				double ohm = rs.getDouble(18);
				double ohm_grezzo = rs.getDouble(19);
				String apertoChiuso = rs.getString(20);
				
				double offset_volt = rs.getDouble(21);
				double offset_milliampere = rs.getDouble(22);
				double offset_ohm = rs.getDouble(23);
		 

				LetturaProva letturaProva = new LetturaProva();
				letturaProva.setId(id);
				letturaProva.setProva_id(prova_id);
				letturaProva.setValore(valore);
				letturaProva.setValore_grezzo(valore_grezzo);
				letturaProva.setId_sonda(id_sonda);
				letturaProva.setData(date);
				letturaProva.setUmidita(valore_umidita);
				
				if("T".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TEMPERATURA);
				}else if("E".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TEMPERATURA_UMIDITA);
				}else  if("V".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TENSIONE_CORRENTE);
				}
				
				letturaProva.setUmidita_grezzo(umidita_grezzo);
				letturaProva.setOffset(offset);
				letturaProva.setOffsetUrt(offset_urt);
				letturaProva.setIncertezza(incertezza);
				letturaProva.setIncertezzaUrt(incertezza_urt);

				letturaProva.setVolt(volt);
				letturaProva.setMilliampere(milliampere);
				letturaProva.setOhm(ohm);
				letturaProva.setApertoChiuso(apertoChiuso);
				
				letturaProva.setOffsetVolt(offset_volt);
				letturaProva.setOffsetMilliampere(offset_milliampere);
				letturaProva.setOffsetOhm(offset_ohm);
				
				letturaProva.setVolt_grezzo(volt_grezzo);
				letturaProva.setMilliampere_grezzo(milliampere_grezzo);
				letturaProva.setOhm_grezzo(ohm_grezzo);

				letture.add(letturaProva);

			}
			rs.close();
			return letture;
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}
	//TODO questo è contorto oltre soglia
	public static List<LetturaProva>findByProva(Connection connection, Prova p_prova, boolean filter){

		String query =null;

		if (filter) {

			GUI gui = Application.getInstance().getGui();
			String idSonda = gui.getIdSondaProvider().getValue();
			String descrizioneSonda = gui.getDescrizioneSondaProvider().getValue();
			Date dataRegistrazione = gui.getDataRegistrazioneProvider().getValue();


			String queryAna =null;
			String where=null;
			String whereAna=null;
			String dataQuery=null;

			GregorianCalendar c=new GregorianCalendar() ;
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0); 
			Date today=c.getTime();


			GregorianCalendar c1=new GregorianCalendar() ;
			if (dataRegistrazione!=null){
				c1.setTime(dataRegistrazione);	
				int giornoX=c1.get(Calendar.DAY_OF_MONTH);
				int annoX=c1.get(Calendar.YEAR);
				int meseX=c1.get(Calendar.MONTH);
				GregorianCalendar c2=new GregorianCalendar() ;
				c2.set(annoX, meseX, giornoX);
				c2.set(Calendar.HOUR, 0);
				c2.set(Calendar.MINUTE, 0);
				c2.set(Calendar.SECOND, 0);
				c2.set(Calendar.MILLISECOND, 0); 
				dataRegistrazione=c2.getTime(); 
				dataQuery = "#" + (meseX+1) + "/" + giornoX + "/" + annoX + "#";
			}
			else{
				GregorianCalendar c3=new GregorianCalendar() ;
				c3.set(Calendar.HOUR, 0);
				c3.set(Calendar.MINUTE, 0);
				c3.set(Calendar.SECOND, 0);
				c3.set(Calendar.MILLISECOND, 0); 
				dataRegistrazione=c3.getTime();
			}

			if (!idSonda.equals("") || !descrizioneSonda.equals("") || (dataRegistrazione.getTime() != today.getTime())) {

				where = TABLE + "." + F_PROVA_ID + "=" +  p_prova.getId(); 


				if (!idSonda.equals("")){


					if (where==null) {
						where = TABLE + "." + F_ID_SONDA + " LIKE '%"+ idSonda +"%' ";
					}
					else
					{
						where = where.trim() + " AND " + TABLE + "." + F_ID_SONDA + " LIKE '%"+ idSonda +"%' ";
					}

				}



				if (!descrizioneSonda.equals("")){

					where = TABLE_VIEW + "." + F_PROVA_ID + "=" +  p_prova.getId(); 

					if (where==null) {
						where = TABLE_VIEW + "." + F_DESCRIZIONE + " LIKE '%"+ descrizioneSonda +"%' ";
					}
					else
					{
						where = where.trim() + " AND " + TABLE_VIEW + "." + F_DESCRIZIONE + " LIKE '%"+ descrizioneSonda +"%' ";
					}

				}



				if (!dataRegistrazione.equals(null) && (dataRegistrazione.getTime() != today.getTime())){

					if (where==null) {
						where = TABLE + "." + F_DATA + "=" + dataQuery;
					}

					else
					{
						where = where.trim() + " AND " + TABLE + "." + F_DATA + "=" + dataQuery;
					}

				}


				if (!descrizioneSonda.equals("")){
					query ="SELECT " +
							F_ID+", "+
							F_PROVA_ID+", "+
							F_VALORE+", "+
							F_VALORE_GREZZO+", "+
							F_ID_SONDA+", "+
							F_DATA+", "+
							F_VALORE_UMIDITA+", "+
							F_TIPO_LETTURA+", "+
							F_STATO_BATTERIA + ", " +
							F_VALORE_UMIDITA_GREZZO + ", " +
							F_OFFSET + ","+
							F_OFFSET_URT + ","+
							F_INCERTEZZA + ","+
							F_INCERTEZZA_URT + ","+ 
							F_VOLT + ","+
							F_VOLT_GREZZO + ","+
							F_MILLIAMPERE + ","+
							F_MILLIAMPERE_GREZZO + ","+
							F_OHM + ","+
							F_OHM_GREZZO + ","+
							F_APERTO_CHIUSO + ","+
							F_OFFSET_VOLT + ","+
							F_OFFSET_MILLIAMPERE + ","+
							F_OFFSET_OHM +  " " +
							"FROM "+ TABLE_VIEW+ " " +
							"WHERE " + where + " " + 
							"ORDER BY "+ F_DATA+" ASC";

				}else {
					query ="SELECT " +
							F_ID+", "+
							F_PROVA_ID+", "+
							F_VALORE+", "+
							F_VALORE_GREZZO+", "+
							F_ID_SONDA+", "+
							F_DATA+", "+
							F_VALORE_UMIDITA+", "+
							F_TIPO_LETTURA+", "+
							F_STATO_BATTERIA + ", " +
							F_VALORE_UMIDITA_GREZZO + ", " +
							F_OFFSET + ","+
							F_OFFSET_URT + ","+
							F_INCERTEZZA + ","+
							F_INCERTEZZA_URT + ","+ 
							F_VOLT + ","+
							F_VOLT_GREZZO + ","+
							F_MILLIAMPERE + ","+
							F_MILLIAMPERE_GREZZO + ","+
							F_OHM + ","+
							F_OHM_GREZZO + ","+
							F_APERTO_CHIUSO + ","+
							F_OFFSET_VOLT + ","+
							F_OFFSET_MILLIAMPERE + ","+
							F_OFFSET_OHM +  " " +
							"FROM "+ TABLE+ " " +
							"WHERE " + where + " " + 
							"ORDER BY "+ F_DATA+" ASC";
				}


			}
			else {


				query ="SELECT " +
						F_ID+", "+
						F_PROVA_ID+", "+
						F_VALORE+", "+
						F_VALORE_GREZZO+", "+
						F_ID_SONDA+", "+
						F_DATA+", "+
						F_VALORE_UMIDITA+", "+
						F_TIPO_LETTURA+", "+
						F_STATO_BATTERIA + ", " +
						F_VALORE_UMIDITA_GREZZO + ", " +
						F_OFFSET + ","+
						F_OFFSET_URT + ","+
						F_INCERTEZZA + ","+
						F_INCERTEZZA_URT + ","+ 
						F_VOLT + ","+
						F_VOLT_GREZZO + ","+
						F_MILLIAMPERE + ","+
						F_MILLIAMPERE_GREZZO + ","+
						F_OHM + ","+
						F_OHM_GREZZO + ","+
						F_APERTO_CHIUSO + ","+
						F_OFFSET_VOLT + ","+
						F_OFFSET_MILLIAMPERE + ","+
						F_OFFSET_OHM +  " " +
						"FROM "+TABLE+" WHERE "+F_PROVA_ID+"="+p_prova.getId()+" ORDER BY "+ F_DATA+" ASC";



			} 

		} else {

			query ="SELECT " +
					F_ID+", "+
					F_PROVA_ID+", "+
					F_VALORE+", "+
					F_VALORE_GREZZO+", "+
					F_ID_SONDA+", "+
					F_DATA+", "+
					F_VALORE_UMIDITA+", "+
					F_TIPO_LETTURA+", "+
					F_STATO_BATTERIA + ", " +
					F_VALORE_UMIDITA_GREZZO + ", " +
					F_OFFSET + ","+
					F_OFFSET_URT + ","+
					F_INCERTEZZA + ","+
					F_INCERTEZZA_URT + ","+ 
					F_VOLT + ","+
					F_VOLT_GREZZO + ","+
					F_MILLIAMPERE + ","+
					F_MILLIAMPERE_GREZZO + ","+
					F_OHM + ","+
					F_OHM_GREZZO + ","+
					F_APERTO_CHIUSO + ","+
					F_OFFSET_VOLT + ","+
					F_OFFSET_MILLIAMPERE + ","+
					F_OFFSET_OHM +  " " +
					"FROM "+TABLE+" WHERE "+F_PROVA_ID+"="+p_prova.getId()+" ORDER BY "+ F_DATA+" ASC";
		}


		try{

			PreparedStatement st =null;

			st = connection.prepareStatement(query);

		 
			ResultSet rs = st.executeQuery();

			List<LetturaProva> letture = new ArrayList<LetturaProva>();
			while(rs.next()){
				Long id = rs.getLong(1);
				Long prova_id = rs.getLong(2);
				double valore = rs.getDouble(3);
				double valore_grezzo = rs.getDouble(4);
				String id_sonda = rs.getString(5);
				Date date = new Date(rs.getDate(6).getTime());
				double valore_umidita = rs.getDouble(7);
				String tipo_l = rs.getString(8);
				String stato_batteria = rs.getString(9);
				double umidita_grezzo = rs.getDouble(10);
				double offset = rs.getDouble(11);
				double offset_urt = rs.getDouble(12);
				double incertezza = rs.getDouble(13);
				double incertezza_urt = rs.getDouble(14);
				
				double volt = rs.getDouble(15);
				double volt_grezzo = rs.getDouble(16);
				double milliampere = rs.getDouble(17);
				double milliampere_grezzo = rs.getDouble(18);
				double ohm = rs.getDouble(19);
				double ohm_grezzo = rs.getDouble(20);
				String apertoChiuso = rs.getString(21);
				
				double offset_volt = rs.getDouble(22);
				double offset_milliampere = rs.getDouble(23);
				double offset_ohm = rs.getDouble(24);
				
				LetturaProva letturaProva = new LetturaProva();
				letturaProva.setId(id);
				letturaProva.setProva_id(prova_id);
				letturaProva.setValore(valore);
				letturaProva.setValore_grezzo(valore_grezzo);
				letturaProva.setId_sonda(id_sonda);
				letturaProva.setData(date);
				letturaProva.setUmidita(valore_umidita);
				
				if("T".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TEMPERATURA);
				}else if("E".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TEMPERATURA_UMIDITA);
				}else  if("V".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TENSIONE_CORRENTE);
				}
				
				letturaProva.setStatoBatteria(stato_batteria);
				letturaProva.setUmidita_grezzo(umidita_grezzo);
				letturaProva.setOffset(offset);
				letturaProva.setOffsetUrt(offset_urt);
				letturaProva.setIncertezza(incertezza);
				letturaProva.setIncertezzaUrt(incertezza_urt);
				
				letturaProva.setVolt(volt);
				letturaProva.setMilliampere(milliampere);
				letturaProva.setOhm(ohm);
				letturaProva.setApertoChiuso(apertoChiuso);

				letturaProva.setOffsetVolt(offset_volt);
				letturaProva.setOffsetMilliampere(offset_milliampere);
				letturaProva.setOffsetOhm(offset_ohm);
				
				letturaProva.setVolt_grezzo(volt_grezzo);
				letturaProva.setMilliampere_grezzo(milliampere_grezzo);
				letturaProva.setOhm_grezzo(ohm_grezzo);
				
				letture.add(letturaProva);

			}
			rs.close();
			return letture;
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}


	public static List<LetturaProva>findBySonda(Connection connection, String p_id_sonda, Date p_Da, Date p_A, boolean onlyInAlarm){
		try{
			PreparedStatement st =null;

			String dataDa=null;
			String dataA=null;
			String where=null;

			GregorianCalendar c=new GregorianCalendar() ;
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0); 
			Date today=c.getTime();


			GregorianCalendar c1=new GregorianCalendar() ;
			if (p_Da!=null){
				c1.setTime(p_Da);	
				int giornoX=c1.get(Calendar.DAY_OF_MONTH);
				int annoX=c1.get(Calendar.YEAR);
				int meseX=c1.get(Calendar.MONTH);
				GregorianCalendar c2=new GregorianCalendar() ;
				c2.set(annoX, meseX, giornoX);
				c2.set(Calendar.HOUR, 0);
				c2.set(Calendar.MINUTE, 0);
				c2.set(Calendar.SECOND, 0);
				c2.set(Calendar.MILLISECOND, 0); 
				// 				p_Da=c2.getTime(); 
				//				dataDa = (meseX+1) + "/" + giornoX + "/" + annoX;
				dataDa = sdf.format(p_Da.getTime()) ;
			}
			else{
				GregorianCalendar c3=new GregorianCalendar() ;
				c3.set(Calendar.HOUR, 0);
				c3.set(Calendar.MINUTE, 0);
				c3.set(Calendar.SECOND, 0);
				c3.set(Calendar.MILLISECOND, 0); 
				p_Da=c3.getTime();
			}

			GregorianCalendar c4=new GregorianCalendar() ;
			if (p_A!=null){
				c1.setTime(p_A);	
				int giornoX=c4.get(Calendar.DAY_OF_MONTH);
				int annoX=c4.get(Calendar.YEAR);
				int meseX=c4.get(Calendar.MONTH);
				GregorianCalendar c5=new GregorianCalendar() ;
				c5.set(annoX, meseX, giornoX);
				c5.set(Calendar.HOUR, 0);
				c5.set(Calendar.MINUTE, 0);
				c5.set(Calendar.SECOND, 0);
				c5.set(Calendar.MILLISECOND, 0); 
				// 				p_A=c5.getTime(); 
				//				dataA = (meseX+1) + "/" + giornoX + "/" + annoX;
				dataA = sdf.format(p_A.getTime()) ;
			}
			else{
				GregorianCalendar c6=new GregorianCalendar() ;
				c6.set(Calendar.HOUR, 0);
				c6.set(Calendar.MINUTE, 0);
				c6.set(Calendar.SECOND, 0);
				c6.set(Calendar.MILLISECOND, 0); 
				p_A=c6.getTime();

			}


			if ((p_id_sonda!=null) || (!p_Da.equals(null)) || (!p_A.equals(null))) {

				if (p_id_sonda!=null) {

					if (!p_id_sonda.equals("TUTTI")){
						where = F_ID_SONDA + " = '"+ p_id_sonda +"' ";
					}


					if (!p_Da.equals(null)){
						long epoch1=0;	

						try {
							epoch1 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataDa).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}

						if (where!=null){
							where = where.trim() + " AND " + F_DATA + ">=" + epoch1;
						} else
						{
							where = F_DATA + ">=" + epoch1;	
						}

					}



					if (!p_A.equals(null)){
						long epoch2=0;	

						try {
							epoch2 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataA).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}
						where = where.trim() + " AND " + F_DATA + "<=" + epoch2;
					}

				}

			}



			String select = "SELECT " +
					F_ID+", "+
					F_PROVA_ID+", "+
					F_VALORE+", "+
					F_VALORE_GREZZO+", "+
					F_ID_SONDA+", "+
					F_DATA+", "+
					F_VALORE_UMIDITA+", "+
					F_TIPO_LETTURA + ", " +
					F_VALORE_UMIDITA_GREZZO + ", " +
					F_OFFSET + ","+
					F_OFFSET_URT + ","+
					F_INCERTEZZA + ","+
					F_INCERTEZZA_URT + ","+ 
					F_VOLT + ","+
					F_VOLT_GREZZO + ","+
					F_MILLIAMPERE + ","+
					F_MILLIAMPERE_GREZZO + ","+
					F_OHM + ","+
					F_OHM_GREZZO + ","+
					F_APERTO_CHIUSO + ","+
					F_OFFSET_VOLT + ","+
					F_OFFSET_MILLIAMPERE + ","+
					F_OFFSET_OHM +  " " +
					"FROM "+TABLE+"  WHERE "+ where + " ORDER BY "+F_DATA+" ASC" ;


			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();



			List<LetturaProva> letture = new ArrayList<LetturaProva>();
			while(rs.next()){
				Long id = rs.getLong(1);
				Long prova_id = rs.getLong(2);
				double valore = rs.getDouble(3);
				double valore_grezzo = rs.getDouble(4);
				String id_sonda = rs.getString(5);
				Date date = new Date(rs.getDate(6).getTime());
				double valore_umidita = rs.getDouble(7);
				String tipo_l = rs.getString(8);
				double umidita_grezzo = rs.getDouble(9);
				double offset = rs.getDouble(10);
				double offset_urt = rs.getDouble(11);
				double incertezza = rs.getDouble(12);
				double incertezza_urt = rs.getDouble(13);
				
				double volt = rs.getDouble(14);
				double volt_grezzo = rs.getDouble(15);
				double milliampere = rs.getDouble(16);
				double milliampere_grezzo = rs.getDouble(17);
				double ohm = rs.getDouble(18);
				double ohm_grezzo = rs.getDouble(19);
				String apertoChiuso = rs.getString(20);
				
				double offset_volt = rs.getDouble(21);
				double offset_milliampere = rs.getDouble(22);
				double offset_ohm = rs.getDouble(23);
				
				LetturaProva letturaProva = new LetturaProva();
				letturaProva.setId(id);
				letturaProva.setProva_id(prova_id);
				letturaProva.setValore(valore);
				letturaProva.setValore_grezzo(valore_grezzo);
				letturaProva.setId_sonda(id_sonda);
				letturaProva.setData(date);
				letturaProva.setUmidita(valore_umidita);
			
				if("T".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TEMPERATURA);
				}else if("E".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TEMPERATURA_UMIDITA);
				}else  if("V".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TENSIONE_CORRENTE);
				}
				
				letturaProva.setUmidita_grezzo(umidita_grezzo);
				letturaProva.setOffset(offset);
				letturaProva.setOffsetUrt(offset_urt);
				letturaProva.setIncertezza(incertezza);
				letturaProva.setIncertezzaUrt(incertezza_urt);
				
				letturaProva.setVolt(volt);
				letturaProva.setMilliampere(milliampere);
				letturaProva.setOhm(ohm);
				letturaProva.setApertoChiuso(apertoChiuso);

				letturaProva.setOffsetVolt(offset_volt);
				letturaProva.setOffsetMilliampere(offset_milliampere);
				letturaProva.setOffsetOhm(offset_ohm);
				
				letturaProva.setVolt_grezzo(volt_grezzo);
				letturaProva.setMilliampere_grezzo(milliampere_grezzo);
				letturaProva.setOhm_grezzo(ohm_grezzo);
				
				boolean toFilter = true;
				
				if (onlyInAlarm){
					Sensore sensore = SensoreManager.getInstance().getSensoreById(id_sonda);
					if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0 || sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0) {
						if (outOfLimit(letturaProva)){
							toFilter = false;
						}
					
				 
			      }
					
					if (!toFilter)	{
						letture.add(letturaProva);	
					}
					
				} else {
					letture.add(letturaProva);
				}
				
				
		
			}
			rs.close();
			return letture;
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}
	
	public static List<LetturaProva>findMedieBySonda(Connection connection, String p_id_sonda, Date p_Da, Date p_A, boolean onlyInAlarm, int minutes){
		try{
			PreparedStatement st =null;

			String dataDa=null;
			String dataA=null;
			String where=null;

			GregorianCalendar c=new GregorianCalendar() ;
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0); 
			Date today=c.getTime();


			GregorianCalendar c1=new GregorianCalendar() ;
			if (p_Da!=null){
				c1.setTime(p_Da);	
				int giornoX=c1.get(Calendar.DAY_OF_MONTH);
				int annoX=c1.get(Calendar.YEAR);
				int meseX=c1.get(Calendar.MONTH);
				GregorianCalendar c2=new GregorianCalendar() ;
				c2.set(annoX, meseX, giornoX);
				c2.set(Calendar.HOUR, 0);
				c2.set(Calendar.MINUTE, 0);
				c2.set(Calendar.SECOND, 0);
				c2.set(Calendar.MILLISECOND, 0); 
				dataDa = sdf.format(p_Da.getTime()) ;
			}
			else{
				GregorianCalendar c3=new GregorianCalendar() ;
				c3.set(Calendar.HOUR, 0);
				c3.set(Calendar.MINUTE, 0);
				c3.set(Calendar.SECOND, 0);
				c3.set(Calendar.MILLISECOND, 0); 
				p_Da=c3.getTime();
			}

			GregorianCalendar c4=new GregorianCalendar() ;
			if (p_A!=null){
				c1.setTime(p_A);	
				int giornoX=c4.get(Calendar.DAY_OF_MONTH);
				int annoX=c4.get(Calendar.YEAR);
				int meseX=c4.get(Calendar.MONTH);
				GregorianCalendar c5=new GregorianCalendar() ;
				c5.set(annoX, meseX, giornoX);
				c5.set(Calendar.HOUR, 0);
				c5.set(Calendar.MINUTE, 0);
				c5.set(Calendar.SECOND, 0);
				c5.set(Calendar.MILLISECOND, 0); 
				dataA = sdf.format(p_A.getTime()) ;
			}
			else{
				GregorianCalendar c6=new GregorianCalendar() ;
				c6.set(Calendar.HOUR, 0);
				c6.set(Calendar.MINUTE, 0);
				c6.set(Calendar.SECOND, 0);
				c6.set(Calendar.MILLISECOND, 0); 
				p_A=c6.getTime();

			}


			if ((p_id_sonda!=null) || (!p_Da.equals(null)) || (!p_A.equals(null))) {

				if (p_id_sonda!=null) {

					if (!p_id_sonda.equals("TUTTI")){
						where = F_ID_SONDA + " = '"+ p_id_sonda +"' ";
					}


					if (!p_Da.equals(null)){
						long epoch1=0;	

						try {
							epoch1 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataDa).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}

						if (where!=null){
							where = where.trim() + " AND " + F_DATA + ">=" + epoch1;
						} else
						{
							where = F_DATA + ">=" + epoch1;	
						}

					}



					if (!p_A.equals(null)){
						long epoch2=0;	

						try {
							epoch2 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataA).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}
						where = where.trim() + " AND " + F_DATA + "<=" + epoch2;
					}

				}

			}



			String select = "SELECT " +
					F_ID+", "+
					F_PROVA_ID+", "+
					F_VALORE+", "+
					F_VALORE_GREZZO+", "+
					F_ID_SONDA+", "+
					F_DATA+", "+
					F_VALORE_UMIDITA+", "+
					F_TIPO_LETTURA + ", " +
					F_VALORE_UMIDITA_GREZZO + ", " +
					F_OFFSET + ","+
					F_OFFSET_URT + ","+
					F_INCERTEZZA + ","+
					F_INCERTEZZA_URT + ","+ 
					F_VOLT + ","+
					F_VOLT_GREZZO + ","+
					F_MILLIAMPERE + ","+
					F_MILLIAMPERE_GREZZO + ","+
					F_OHM + ","+
					F_OHM_GREZZO + ","+
					F_APERTO_CHIUSO + ","+
					F_OFFSET_VOLT + ","+
					F_OFFSET_MILLIAMPERE + ","+
					F_OFFSET_OHM +  " " +
					"FROM "+TABLE+"  WHERE "+ where + " ORDER BY "+F_DATA+" ASC" ;


			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();

			Date saveDate = null;
		 	boolean first = true;
			List<LetturaProva> lettureMedie = new ArrayList<LetturaProva>();
			
			List<LetturaProva> letture = new ArrayList<LetturaProva>();
			while(rs.next()){
				
				Long id = rs.getLong(1);
				Long prova_id = rs.getLong(2);
				double valore = rs.getDouble(3);
				double valore_grezzo = rs.getDouble(4);
				String id_sonda = rs.getString(5);
				Date date = new Date(rs.getDate(6).getTime());
				double valore_umidita = rs.getDouble(7);
				String tipo_l = rs.getString(11);
				double umidita_grezzo = rs.getDouble(14);
				double offset = rs.getDouble(15);
				double offset_urt = rs.getDouble(16);
				double incertezza = rs.getDouble(17);
				double incertezza_urt = rs.getDouble(18);
				
				double volt = rs.getDouble(19);
				double volt_grezzo = rs.getDouble(20);
				double milliampere = rs.getDouble(21);
				double milliampere_grezzo = rs.getDouble(22);
				double ohm = rs.getDouble(23);
				double ohm_grezzo = rs.getDouble(24);
				String apertoChiuso = rs.getString(25);
				
				double offset_volt = rs.getDouble(26);
				double offset_milliampere = rs.getDouble(27);
				double offset_ohm = rs.getDouble(28);
				
				Date mediaDate = date;
			 
				LetturaProva letturaProva = new LetturaProva();
				letturaProva.setId(id);
				letturaProva.setProva_id(prova_id);
				letturaProva.setValore(valore);
				letturaProva.setValore_grezzo(valore_grezzo);
				letturaProva.setId_sonda(id_sonda);
				letturaProva.setData(date);
				letturaProva.setUmidita(valore_umidita);
				
				if("T".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TEMPERATURA);
				}else if("E".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TEMPERATURA_UMIDITA);
				}else  if("V".equals(tipo_l)){
					letturaProva.setTipo_misura(TipoMisura.TENSIONE_CORRENTE);
				}
				
				letturaProva.setUmidita_grezzo(umidita_grezzo);
				letturaProva.setOffset(offset);
				letturaProva.setOffsetUrt(offset_urt);
				letturaProva.setIncertezza(incertezza);
				letturaProva.setIncertezzaUrt(incertezza_urt);
				
				letturaProva.setVolt(volt);
				letturaProva.setMilliampere(milliampere);
				letturaProva.setOhm(ohm);
				letturaProva.setApertoChiuso(apertoChiuso);

				letturaProva.setOffsetVolt(offset_volt);
				letturaProva.setOffsetMilliampere(offset_milliampere);
				letturaProva.setOffsetOhm(offset_ohm);

				letturaProva.setVolt_grezzo(volt_grezzo);
				letturaProva.setMilliampere_grezzo(milliampere_grezzo);
				letturaProva.setOhm_grezzo(ohm_grezzo);
				
				boolean toFilter = true;
				
				if (onlyInAlarm){
					Sensore sensore = SensoreManager.getInstance().getSensoreById(id_sonda);
					if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0 || sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0) {
						if (outOfLimit(letturaProva)){
							toFilter = false;
						}
			 
			      }
				 
					
				} else {
					toFilter = false;
				}
				
				if (toFilter){
					continue;
				}
				
				
				if (!first){
					int minutesDifference = minutesDifference(date, saveDate);
					
					if (minutesDifference >= minutes) {
						StandardDeviation stdDeviation = new StandardDeviation(lettureMedie);
						stdDeviation.calculate();
						double mediaTemp = stdDeviation.getMediaTemp();
						letturaProva.setValore(mediaTemp);
						letturaProva.setValore_grezzo(mediaTemp);
						double mediaUrt = stdDeviation.getMediaUrt();
						letturaProva.setUmidita(mediaUrt);
						letturaProva.setUmidita_grezzo(mediaUrt);
						mediaDate = truncateDate(saveDate, minutes);
						letturaProva.setData(mediaDate);
						letture.add(letturaProva);
						first = true;
						lettureMedie.clear();
					}
				}
				if (first){
					saveDate = mediaDate;
					first = false;
				}
				
				lettureMedie.add(letturaProva);
			}
			
			if (!first) {
				StandardDeviation stdDeviation = new StandardDeviation(lettureMedie);
				stdDeviation.calculate();
				double mediaTemp = stdDeviation.getMediaTemp();
				LetturaProva letturaProva = lettureMedie.get(lettureMedie.size() - 1);
				letturaProva.setValore(mediaTemp);
				letturaProva.setValore_grezzo(mediaTemp);
				double mediaUrt = stdDeviation.getMediaUrt();
				letturaProva.setUmidita(mediaUrt);
				letturaProva.setUmidita_grezzo(mediaUrt);
				letturaProva.setData(truncateDate(saveDate, minutes));
				letture.add(letturaProva);
			}
			
			rs.close();
		  	return letture;
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}
	
	
	public static List<LetturaProva> findAll(Connection connection){
		
		try{
			PreparedStatement st =null;

	 
		String select = "SELECT " +
				F_ID+", "+
				F_PROVA_ID+", "+
				F_VALORE+", "+
				F_VALORE_GREZZO+", "+
				F_ID_SONDA+", "+
				F_DATA+", "+
				F_VALORE_UMIDITA+", "+
				F_TIPO_LETTURA + ", " +
				F_STATO_BATTERIA + ", " +
				F_VALORE_UMIDITA_GREZZO +", "+
				F_OFFSET + ","+
				F_OFFSET_URT + ","+
				F_INCERTEZZA + ","+
				F_INCERTEZZA_URT + ","+ 
				F_VOLT + ","+
				F_VOLT_GREZZO + ","+
				F_MILLIAMPERE + ","+
				F_MILLIAMPERE_GREZZO + ","+
				F_OHM + ","+
				F_OHM_GREZZO + ","+
				F_APERTO_CHIUSO + ","+
				F_OFFSET_VOLT + ","+
				F_OFFSET_MILLIAMPERE + ","+
				F_OFFSET_OHM +  " " +
				"FROM "+TABLE+ " ORDER BY "+F_DATA+" ASC" ;
		
		st = connection.prepareStatement(select);

		ResultSet rs = st.executeQuery();
 

		List<LetturaProva> letture = new ArrayList<LetturaProva>();
		while(rs.next()){
			Long id = rs.getLong(1);
			Long prova_id = rs.getLong(2);
			double valore = rs.getDouble(3);
			double valore_grezzo = rs.getDouble(4);
			String id_sonda = rs.getString(5);
			Date date = new Date(rs.getDate(6).getTime());
			double valore_umidita = rs.getDouble(7);
			String tipo_l = rs.getString(11);
			String stato_batteria = rs.getString(12);
			double umidita_grezzo = rs.getDouble(13);
			double offset = rs.getDouble(14);
			double offset_urt = rs.getDouble(15);
			double incertezza = rs.getDouble(16);
			double incertezza_urt = rs.getDouble(17);
			
			double volt = rs.getDouble(18);
			double volt_grezzo = rs.getDouble(19);
			double milliampere = rs.getDouble(20);
			double milliampere_grezzo = rs.getDouble(21);
			double ohm = rs.getDouble(22);
			double ohm_grezzo = rs.getDouble(23);
			String apertoChiuso = rs.getString(24);
			
			double offset_volt = rs.getDouble(25);
			double offset_milliampere = rs.getDouble(26);
			double offset_ohm = rs.getDouble(27);
			
			LetturaProva letturaProva = new LetturaProva();
			letturaProva.setId(id);
			letturaProva.setProva_id(prova_id);
			letturaProva.setValore(valore);
			letturaProva.setValore_grezzo(valore_grezzo);
			letturaProva.setId_sonda(id_sonda);
			letturaProva.setData(date);
			letturaProva.setUmidita(valore_umidita);
			letturaProva.setStatoBatteria(stato_batteria);

			if("T".equals(tipo_l)){
				letturaProva.setTipo_misura(TipoMisura.TEMPERATURA);
			}else if("E".equals(tipo_l)){
				letturaProva.setTipo_misura(TipoMisura.TEMPERATURA_UMIDITA);
			}else  if("V".equals(tipo_l)){
				letturaProva.setTipo_misura(TipoMisura.TENSIONE_CORRENTE);
			}
			
			letturaProva.setUmidita_grezzo(umidita_grezzo);
			letturaProva.setOffset(offset);
			letturaProva.setOffsetUrt(offset_urt);
			letturaProva.setIncertezza(incertezza);
			letturaProva.setIncertezzaUrt(incertezza_urt);
			
			letturaProva.setVolt(volt);
			letturaProva.setMilliampere(milliampere);
			letturaProva.setOhm(ohm);
			letturaProva.setApertoChiuso(apertoChiuso);

			letturaProva.setOffsetVolt(offset_volt);
			letturaProva.setOffsetMilliampere(offset_milliampere);
			letturaProva.setOffsetOhm(offset_ohm);
			
			letturaProva.setVolt_grezzo(volt_grezzo);
			letturaProva.setMilliampere_grezzo(milliampere_grezzo);
			letturaProva.setOhm_grezzo(ohm_grezzo);
				
			letture.add(letturaProva);

		}
		rs.close();
		return letture;
		
		 
		 
	}catch (SQLException e) {
		logger.error(TAG,e);
	}
	return null;
		
	}


	public static void deleteByProva(Connection connection, Prova p_prova){
		try{
			PreparedStatement st =null;
			String query ="DELETE " +
					"FROM "+TABLE+" WHERE "+F_PROVA_ID+"="+p_prova.getId();
			st = connection.prepareStatement(query);
			st.executeUpdate();

		}catch (SQLException e) {
			logger.error(TAG,e);
		}

	}

	public static void deleteBySonda(Connection connection,  String p_id_sonda, Date p_Da, Date p_A){

		try{
			PreparedStatement st =null;

			String dataDa=null;
			String dataA=null;
			String where=null;

			GregorianCalendar c=new GregorianCalendar() ;
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0); 
			Date today=c.getTime();


			GregorianCalendar c1=new GregorianCalendar() ;
			if (p_Da!=null){
				c1.setTime(p_Da);	
				int giornoX=c1.get(Calendar.DAY_OF_MONTH);
				int annoX=c1.get(Calendar.YEAR);
				int meseX=c1.get(Calendar.MONTH);
				GregorianCalendar c2=new GregorianCalendar() ;
				c2.set(annoX, meseX, giornoX);
				c2.set(Calendar.HOUR, 0);
				c2.set(Calendar.MINUTE, 0);
				c2.set(Calendar.SECOND, 0);
				c2.set(Calendar.MILLISECOND, 0); 
				dataDa = sdf.format(p_Da.getTime()) ;
			}
			else{
				GregorianCalendar c3=new GregorianCalendar() ;
				c3.set(Calendar.HOUR, 0);
				c3.set(Calendar.MINUTE, 0);
				c3.set(Calendar.SECOND, 0);
				c3.set(Calendar.MILLISECOND, 0); 
				p_Da=c3.getTime();
			}

			GregorianCalendar c4=new GregorianCalendar() ;
			if (p_A!=null){
				c1.setTime(p_A);	
				int giornoX=c4.get(Calendar.DAY_OF_MONTH);
				int annoX=c4.get(Calendar.YEAR);
				int meseX=c4.get(Calendar.MONTH);
				GregorianCalendar c5=new GregorianCalendar() ;
				c5.set(annoX, meseX, giornoX);
				c5.set(Calendar.HOUR, 0);
				c5.set(Calendar.MINUTE, 0);
				c5.set(Calendar.SECOND, 0);
				c5.set(Calendar.MILLISECOND, 0); 
				dataA = sdf.format(p_A.getTime()) ;
			}
			else{
				GregorianCalendar c6=new GregorianCalendar() ;
				c6.set(Calendar.HOUR, 0);
				c6.set(Calendar.MINUTE, 0);
				c6.set(Calendar.SECOND, 0);
				c6.set(Calendar.MILLISECOND, 0); 
				p_A=c6.getTime();

			}


			if ((p_id_sonda!=null) || (!p_Da.equals(null)) || (!p_A.equals(null))) {

				if (p_id_sonda!=null){

					if (!p_id_sonda.equals("TUTTI")){
						where = F_ID_SONDA + " = '"+ p_id_sonda +"' ";
					}


					if (!p_Da.equals(null)){
						long epoch1=0;	

						try {
							epoch1 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataDa).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}

						if (where!=null){
							where = where.trim() + " AND " + F_DATA + ">=" + epoch1;
						} else
						{
							where = F_DATA + ">=" + epoch1;	
						}
					}



					if (!p_A.equals(null)){
						long epoch2=0;	

						try {
							epoch2 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataA).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}
						where = where.trim() + " AND " + F_DATA + "<=" + epoch2;
					}
				}

			}



			String query = "DELETE " +
					"FROM "+TABLE+"  WHERE "+ where;

			st = connection.prepareStatement(query);

			st.executeUpdate();

		}catch (SQLException e) {
			logger.error(TAG,e);
		}

	}

	
	


	public static void createView(Connection conn) throws SQLException{
		Statement stat = conn.createStatement();
		String query = "CREATE VIEW LETTURE_PROVA_VIEW AS " +  
				"SELECT LETTURE_PROVA.ID, LETTURE_PROVA.PROVA_ID, LETTURE_PROVA.ID_SONDA, "+
				"LETTURE_PROVA.VALORE, LETTURE_PROVA.VALORE_GREZZO, LETTURE_PROVA.DATA, "+
				"LETTURE_PROVA.VALORE_UMIDITA, LETTURE_PROVA.TIPO_LETTURA, LETTURE_PROVA.EFFETTO_LETALE, LETTURE_PROVA.F0, LETTURE_PROVA.VALORE_UMIDITA_GREZZO," + 
				"SONDE.DESCRIZIONE, SONDE.LOCATION " +
				"FROM LETTURE_PROVA, SONDE " +
				"WHERE LETTURE_PROVA.ID_SONDA=SONDE.ID_SONDA "
				;
		stat.executeQuery(query);

	}
	
	public static void createViewExport(Connection conn) throws SQLException {
		Statement stat = conn.createStatement();
		String query = "CREATE VIEW LETTURE_PROVA_VIEW_EXPORT AS " +
			     "SELECT PROVE.ID, PROVE.RESPONSABILE, PROVE.TARGA," +
			     "TIME( PROVE.ORA_INIZIO / 1000, 'unixepoch', 'localtime' )," +
			     "TIME( PROVE.ORA_FINE / 1000, 'unixepoch', 'localtime' )," +
			     "TIME( PROVE.DURATA_TEORICA / 1000, 'unixepoch', 'localtime' )," +
			     "TIME( PROVE.DURATA_EFFETTIVA / 1000, 'unixepoch', 'localtime' )," +
			     "LETTURE_PROVA.ID_SONDA,"+
		         "DATETIME( LETTURE_PROVA.DATA / 1000, 'unixepoch', 'localtime' )," +
		         "LETTURE_PROVA.VALORE, LETTURE_PROVA.VALORE_GREZZO, LETTURE_PROVA.GRUPPO " +
		         "FROM PROVE, LETTURE_PROVA WHERE PROVE.ID = LETTURE_PROVA.PROVA_ID";
		stat.executeQuery(query);
	}

	public static boolean checkLetturaProva(Connection connection, LetturaProva lettura){

		try{

			int count = 0;


			String p_idSonda =  lettura.getId_sonda();
			String where = null;
			String data = sdf.format(lettura.getData().getTime()) ;


			where = F_ID_SONDA + " = '"+ p_idSonda +"' ";


			long epoch=0;	

			try {
				epoch = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(data).getTime();
				epoch = lettura.getData().getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}



			where = where.trim() + " AND " + F_DATA + "=" + epoch +  " AND " + F_PROVA_ID + "=" + lettura.getProva_id() + " AND " + F_VALORE + "=" + lettura.getValore();


			PreparedStatement st =null;

			st = connection.prepareStatement("SELECT COUNT(*)" +
					"FROM "+TABLE+" WHERE "+ where);


			ResultSet rs = st.executeQuery();

			if (rs.next())
				count = rs.getInt(1);

			rs.close();


			if (count == 0)
				return false;
			else{
				return true;
			}


		}catch (SQLException e) {
			logger.error(TAG,e);
			return false;
		}



	}

	public static boolean checkProvaIsEmpty(Connection connection, Prova prova){

		try{

			int count = 0;


			String	where = F_PROVA_ID + "=" + prova.getId();


			PreparedStatement st =null;

			st = connection.prepareStatement("SELECT COUNT(*)" +
					"FROM "+TABLE+" WHERE "+ where);


			ResultSet rs = st.executeQuery();

			if (rs.next())
				count = rs.getInt(1);

			rs.close();


			if (count == 0)
				return true;
			else{
				return false;
			}


		}catch (SQLException e) {
			logger.error(TAG,e);
			return false;
		}


	}
	
 
	
	public static List<LetturaProva> findLastByData(Connection connection, Date p_Da, Date p_A){
		
		List<LetturaProva> letture = new ArrayList<LetturaProva>();

		LinkedHashMap<LastSensor, Double> sensori = new LinkedHashMap<LastSensor, Double>();
		
		try{
			PreparedStatement st =null;

			String dataDa=null;
			String dataA=null;
			String where=null;

			GregorianCalendar c=new GregorianCalendar() ;
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0); 
			Date today=c.getTime();


			GregorianCalendar c1=new GregorianCalendar() ;
			if (p_Da!=null){
				c1.setTime(p_Da);	
				int giornoX=c1.get(Calendar.DAY_OF_MONTH);
				int annoX=c1.get(Calendar.YEAR);
				int meseX=c1.get(Calendar.MONTH);
				GregorianCalendar c2=new GregorianCalendar() ;
				c2.set(annoX, meseX, giornoX);
				c2.set(Calendar.HOUR, 0);
				c2.set(Calendar.MINUTE, 0);
				c2.set(Calendar.SECOND, 0);
				c2.set(Calendar.MILLISECOND, 0); 
				// 				p_Da=c2.getTime(); 
				//				dataDa = (meseX+1) + "/" + giornoX + "/" + annoX;
				dataDa = sdf.format(p_Da.getTime()) ;
			}
			else{
				GregorianCalendar c3=new GregorianCalendar() ;
				c3.set(Calendar.HOUR, 0);
				c3.set(Calendar.MINUTE, 0);
				c3.set(Calendar.SECOND, 0);
				c3.set(Calendar.MILLISECOND, 0); 
				p_Da=c3.getTime();
			}

			GregorianCalendar c4=new GregorianCalendar() ;
			if (p_A!=null){
				c1.setTime(p_A);	
				int giornoX=c4.get(Calendar.DAY_OF_MONTH);
				int annoX=c4.get(Calendar.YEAR);
				int meseX=c4.get(Calendar.MONTH);
				GregorianCalendar c5=new GregorianCalendar() ;
				c5.set(annoX, meseX, giornoX);
				c5.set(Calendar.HOUR, 0);
				c5.set(Calendar.MINUTE, 0);
				c5.set(Calendar.SECOND, 0);
				c5.set(Calendar.MILLISECOND, 0); 
				// 				p_A=c5.getTime(); 
				//				dataA = (meseX+1) + "/" + giornoX + "/" + annoX;
				dataA = sdf.format(p_A.getTime()) ;
			}
			else{
				GregorianCalendar c6=new GregorianCalendar() ;
				c6.set(Calendar.HOUR, 0);
				c6.set(Calendar.MINUTE, 0);
				c6.set(Calendar.SECOND, 0);
				c6.set(Calendar.MILLISECOND, 0); 
				p_A=c6.getTime();

			}
			
//			where = " WHERE ID_SONDA = 'BD58A4' ";

			if ((!p_Da.equals(null)) || (!p_A.equals(null))) {
				
				  

				if (!p_Da.equals(null)){
						long epoch1=0;	

						try {
							epoch1 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataDa).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}

						if (where!=null){
							where = where.trim() + " AND " + F_DATA + ">=" + epoch1;
						} else	{
							where = " WHERE " + F_DATA + ">=" + epoch1;	
						}

					}

				if (!p_A.equals(null)){
						long epoch2=0;	

						try {
							epoch2 = new java.text.SimpleDateFormat ("MM/dd/yyyy HH:mm:ss").parse(dataA).getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}
						where = where.trim() + " AND " + F_DATA + "<=" + epoch2;
					}
 

			}

			String select = "SELECT " +
					F_ID+", "+
					F_PROVA_ID+", "+
					F_VALORE+", "+
					F_VALORE_GREZZO+", "+
					F_ID_SONDA+", "+
					F_DATA+", "+
					F_VALORE_UMIDITA+", "+
					F_TIPO_LETTURA + ", " +
					F_VALORE_UMIDITA_GREZZO + ", " +
					F_OFFSET + ","+
					F_OFFSET_URT + ","+
					F_INCERTEZZA + ","+
					F_INCERTEZZA_URT + ","+ 
					F_VOLT + ","+
					F_VOLT_GREZZO + ","+
					F_MILLIAMPERE + ","+
					F_MILLIAMPERE_GREZZO + ","+
					F_OHM + ","+
					F_OHM_GREZZO + ","+
					F_APERTO_CHIUSO + ","+
					F_OFFSET_VOLT + ","+
					F_OFFSET_MILLIAMPERE + ","+
					F_OFFSET_OHM +  " " +
					"FROM "+TABLE + " " +   where + " ORDER BY "+ F_ID_SONDA + ", "  +  F_DATA +" ASC";
//					"FROM "+TABLE+ " WHERE " +  where + " ORDER BY "+ F_DATA+" ASC" + ", "  + F_ID_SONDA;



			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
 
			while(rs.next()){
				Long id = rs.getLong(1);
				Long prova_id = rs.getLong(2);
				double valore = rs.getDouble(3);
				double valore_grezzo = rs.getDouble(4);
				String id_sonda = rs.getString(5);
				Date date = new Date(rs.getDate(6).getTime());
				double valore_umidita = rs.getDouble(7);
				String tipo_l = rs.getString(8);
				double umidita_grezzo = rs.getDouble(9);
				double offset = rs.getDouble(10);
				double offset_urt = rs.getDouble(11);
				double incertezza = rs.getDouble(12);
				double incertezza_urt = rs.getDouble(13);
			    
				double volt = rs.getDouble(14);
				double volt_grezzo = rs.getDouble(15);
				double milliampere = rs.getDouble(16);
				double milliampere_grezzo = rs.getDouble(17);
				double ohm = rs.getDouble(18);
				double ohm_grezzo = rs.getDouble(19);
				String apertoChiuso = rs.getString(20);
				
				double offset_volt = rs.getDouble(21);
				double offset_milliampere = rs.getDouble(22);
				double offset_ohm = rs.getDouble(23);
				 
				
			    String time = sdfTime.format(date);
			    LastSensor last = new LastSensor(time, id_sonda);
			    sensori.put(last, valore);	 
			    
			}
			rs.close();
			
			 
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		 
		for (Map.Entry<LastSensor, Double> entry : sensori.entrySet()) {
		    LastSensor key = entry.getKey();
		    Double valore = entry.getValue();
		    Calendar calendar = Calendar.getInstance();
		    calendar.setTime(p_Da);
		    calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(key.getTime()));
		    calendar.set(Calendar.MILLISECOND, 0);
		    calendar.set(Calendar.SECOND, 0);
		    calendar.set(Calendar.MINUTE, 0);
		    LetturaProva lp = new LetturaProva();
		    lp.setId_sonda(key.getIdSonda());
		    lp.setData(calendar.getTime());
		    lp.setValore(valore);
		    letture.add(lp);
		    logger.debug(TAG, "Record letto da Sensori: " + key.getTime() + "|" + key.getIdSonda() + "|" + valore);
		}
		
		return letture;
		
	}
	
	public static boolean insertOrUpdate(Connection connection, Prova prova){
		
		 try{
			 
			 PreparedStatement st;
			 
			  st = connection.prepareStatement("UPDATE "+TABLE+" SET " +
					  		F_DATA +"=?" +
					  		"WHERE " + F_PROVA_ID+"="+prova.getId());
			  
			  int idx = 1;
			  st.setDate(idx++, new java.sql.Date(prova.getData_inizio().getTime()));
			  boolean execute = st.execute();
			  return execute;
		  }catch(SQLException e){
			  logger.error(TAG, e);
			  return false;
		  }
	}
	
	
	private static boolean outOfLimit(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMin()!=0 || sensore.getRangeMax()!= 0){
			return letturaProva.getValore() < sensore.getRangeMin() || letturaProva.getValore() > sensore.getRangeMax();
		}
		return false;

	}

	private static boolean outOfLimitURT(LetturaProva letturaProva){
		Sensore sensore = SensoreManager.getInstance().getSensoreById(letturaProva.getId_sonda());
		if (sensore.getRangeMinURT()!=0 || sensore.getRangeMaxURT()!= 0){
			return letturaProva.getUmidita() < sensore.getRangeMinURT() || letturaProva.getUmidita() > sensore.getRangeMaxURT();
		}
		return false;

	}

	private static int minutesDifference(Date date1, Date date2) {
//	    final int MILLI_TO_HOUR = 1000 * 60 * 60;
	    final int MILLI_TO_MINUTES = 1000 * 60;
	    return (int) (date1.getTime() - date2.getTime()) / MILLI_TO_MINUTES;
	}
	
	private static Date truncateDate(Date saveDate, int minutes)  {
		Calendar cal = Calendar.getInstance();
		cal.setTime(saveDate);
		cal.add(Calendar.MINUTE, minutes);
	 	cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
  

}
