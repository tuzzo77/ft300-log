package com.econorma.persistence;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jdesktop.application.Application;

import com.econorma.util.Logger;

public class Templates {
 
	private static Logger logger = Logger.getLogger(Templates.class);
	private static final String TAG = "Templates";
	
	private Application app;
	
	private File templateV1;
	private File templateV2;
 
	
	
	public Templates (Application app){
	
		templateV1 = new File("template_V1.txt");
		templateV2 = new File("template_V2.txt");
		FileInputStream fis=null;
		
		boolean load=false;
		
		try {
			if (templateV1.exists()){
				logger.debug(TAG, "File gi� esistente:" + templateV1);
				load=true;
			}
			
		} catch (Exception e) {
			logger.error(TAG, e);
		}finally{
			IOUtils.closeQuietly(fis);
		}
		
		
		if (!load){
			InputStream is1=null;
			InputStream is2=null;
			
			try {
				
				is1 = getStreamV1();
				is2 = getStreamV2();
				
				FileUtils.copyInputStreamToFile(is1, templateV1);
				FileUtils.copyInputStreamToFile(is2, templateV2);
				logger.debug(TAG, "Creazione File templates");
			    
				
				
			} catch (Exception e) {
				logger.error(TAG, e);
			} finally{
				IOUtils.closeQuietly(is1);
				IOUtils.closeQuietly(is2);
			}
		}
		
		        
	}
 
	private InputStream getStreamV1(){
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("com/econorma/resources/template_V1.txt");
		logger.debug(TAG, "Resource stream : "+ (in!=null));
		return in;
	}
	
	private InputStream getStreamV2(){
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("com/econorma/resources/template_V2.txt");
		logger.debug(TAG, "Resource stream : "+ (in!=null));
		return in;
	}
 
}
