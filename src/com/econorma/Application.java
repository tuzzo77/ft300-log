package com.econorma;

import java.awt.Component;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventObject;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.MDC;
import org.jdesktop.application.SingleFrameApplication;

import com.adamtaft.eb.EventBusService;
import com.adamtaft.eb.EventHandler;
import com.econorma.gui.EconormaLoginDialog;
import com.econorma.gui.GUI;
import com.econorma.io.ReceiverLogger;
import com.econorma.language.Language;
import com.econorma.logic.DatabaseManager;
import com.econorma.logic.SensoreManager;
import com.econorma.logic.SerialUtil;
import com.econorma.logic.logger.ScaricoLoggerManager;
import com.econorma.persistence.AppConfChanger;
import com.econorma.persistence.AppConfChanger.AppCfg;
import com.econorma.persistence.DAO;
import com.econorma.persistence.Preferences;
import com.econorma.persistence.Templates;
import com.econorma.resources.Testo;
import com.econorma.ui.Events;
import com.econorma.ui.MainMenuBar;
import com.econorma.ui.MainWindow;
import com.econorma.util.Logger;
import com.econorma.util.LoggerCustomLevel;
import com.econorma.util.SwingUtils;

public class Application extends SingleFrameApplication {

	private static final String TAG = Application.class.getSimpleName();

	private static final Logger logger = Logger.getLogger(Application.class);

	private TrayIcon trayIcon;
	private SystemTray tray;
	private SensoreManager sensoreManager;
	private DatabaseManager databaseManager;
	private ReceiverLogger receiverLogger;
	private DAO dao;
	private Preferences preferences;
	private GUI gui;
	private AppConfChanger appConfChanger;
	private Templates templates;
	private MainWindow mainWindow; 
	private ScaricoLoggerManager scaricoLoggerManager;


	private ExecutorService executorService = Executors
			.newSingleThreadExecutor();

	private boolean configMode;
	private TIPO_MISURE tipoMisure;
 	private THEME theme;
 	private INTERFACE interfaccia;
 	private boolean upgradeDB;
	private Language language;
	private ResourceBundle bundle;
	private String name;
	private String title;
	private boolean restart;
	private boolean deleteAfterDownload;
	private boolean systemTray;
	private boolean exitAlert;
	private String pathFile;
	private List<String> tipoSensori;
	private StringBuilder sensorsArray;
	private boolean temperature;
	private boolean temperatureHumidity;
	private boolean volt;
	private boolean milliampere;
	private boolean ohm;
	private boolean apertoChiuso;
 
	public enum TIPO_MISURE {
		TEMPERATURA, TEMPERATURA_UMIDITA, VOLT, MILLIAMPERE, OHM, APERTO_CHIUSO, MISTA
	}
 	
	public enum THEME {
		LIGHT, DARK
	}
	
	public enum INTERFACE {
		NEW, OLD
	}
	

	public void init() {

		{
			System.setProperty("org.apache.jasper.compiler.disablejsr199", "true");
			appConfChanger = new AppConfChanger(this);
			templates = new Templates(this);
			 
			tipoSensori = Arrays.asList(getAppConfChanger().getValue(AppCfg.TIPO_SENSORI).split("\\|", -1));
			setTipoSensore();
			
			if (temperature || ohm || volt || apertoChiuso) {
				tipoMisure = TIPO_MISURE.TEMPERATURA;
			}
			if (temperatureHumidity) {
				tipoMisure = TIPO_MISURE.TEMPERATURA_UMIDITA;
			}

			
			String tema = getAppConfChanger().getValue(AppCfg.THEME);
			theme = THEME.valueOf(tema);
			
			String myInterface = getAppConfChanger().getValue(AppCfg.INTERFACE);
			interfaccia = INTERFACE.valueOf(myInterface);
			
			upgradeDB = Boolean.parseBoolean(getAppConfChanger().getValue(
					AppCfg.UPGRADE_DB));
			
			name = getAppConfChanger().getValue(AppCfg.NAME);
			title = getAppConfChanger().getValue(AppCfg.TITLE);
			
			systemTray = Boolean.parseBoolean(getAppConfChanger().getValue(
					AppCfg.SYSTEM_TRAY));
			
			exitAlert = Boolean.parseBoolean(getAppConfChanger().getValue(
					AppCfg.EXIT_ALERT));
			
			deleteAfterDownload = Boolean.parseBoolean(getAppConfChanger().getValue(
					AppCfg.DELETE_AFTER_DOWNLOAD));
		
			pathFile =  getAppConfChanger().getValue(AppCfg.PATH_FILE);
			
			restart=false;
	
		}

		sensoreManager = SensoreManager.getInstance();
		databaseManager = DatabaseManager.getInstance();
		preferences = new Preferences();
		dao = DAO.create(databaseManager);
		gui = new GUI();
		scaricoLoggerManager = new ScaricoLoggerManager();
		scaricoLoggerManager.init();

		language = new Language();
		bundle = language.getLocaleFromUser(EconormaLoginDialog.getUser());

		databaseManager.init();
		dao.init(); // after db
		sensoreManager.init();
		
//		scaricoBidirezionaleManager.init();

		String port = null;
		 
		String oldPort = getPreferences().getPort();
		int baudRate = getPreferences().getBaudRate();
		if (baudRate==0){
			baudRate=Testo.BAUDRATE_115200;
			getPreferences().setBaudRate(baudRate);
		}
		
		if (!Preferences.isPortDisabled(oldPort)) {

			String[] serialPorts = SerialUtil.getSerialPorts();

			if (serialPorts != null && serialPorts.length>0) {

				if (serialPorts.length == 1) {
					// se c'è un'unica porta imposta quella
					port = serialPorts[0];
				} else if (oldPort != null) {

					for(String s : serialPorts) {
						if (oldPort.equals(s)) {
							port = oldPort;
							break;
						}
					}

				}
			}
			if (port!=null){
				getPreferences().setPort(port);
			}
		}

		
		EventBusService.subscribe(this);
	}

	public void start() {
		sensoreManager.restoreLastMeasure();
	}
 

	public DAO getDao() {
		return dao;
	}

	public static synchronized Application getInstance() {
		return (Application) org.jdesktop.application.Application.getInstance();
	}

	public boolean isConfigMode() {
		return configMode;
	}

	public void setConfigMode(boolean configMode) {
		this.configMode = configMode;
		EventBusService.publish(Events.APPLICATION_MODE_CHANGED);

	}
 

	public Preferences getPreferences() {
		return preferences;
	}
	
	public ScaricoLoggerManager getScaricoLoggerManager() {
		return scaricoLoggerManager;
	}

	 

	@Override
	protected void shutdown() {
 		
		MDC.put("event_name", "Shut down");
		logger.info(TAG, "Shutdown");
		logger.log(LoggerCustomLevel.AUDIT, "Chiusura programma");
	  
		try {
			super.shutdown();
		} catch (Exception e1) {
		}


	}

	 
	@EventHandler
	public void handlerExit(String event) {
		if (event == Events.EXIT) {
		 	exit();
		}
	}
 
	/**
	 * chiamato sincrono da codice
	 * @param dischargeEvent
	 */
	public void handleEvent(final Events.DischargeEvent dischargeEvent) {
		switch (dischargeEvent.action) {
		case RUNNING:

		
//			receiver.stop();
		

			break;

		case COMPLETE:
//				receiver.start();

			break;

		}

	}

	@EventHandler
	public void handlePortChanged(final Events.PortChanged event) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				//				CommPortIdentifier port = event.getPort();
				String port = event.getPort();
				int baudRate=event.getBaudRate();
				String portName = null;
				if (port != null)
					portName = port;
//				switch (Application.getInstance().getSystem()) {
//				case MONODIREZIONALE:
//					receiver.stop();
//					receiver.setCom(portName);
//					receiver.setBaudRate(baudRate);
//					receiver.start();
//					break;
//				case BIDIREZIONALE:
//					receiverBidirezionale.stop();
//					receiverBidirezionale.setOnReadListener(null);
//					receiverBidirezionale = new ReceiverBidirezionale(portName,baudRate,
//							Task.Misure);
//					receiverBidirezionale.setOnReadListener(sensoreManager);
//					receiverBidirezionale.start();
//					break;
//				case GSM:
//					receiver.stop();
//					receiver.setCom(portName);
//					receiver.setBaudRate(baudRate);
//					receiver.start();
//					break;
//				default:
//					break;
//				}
			}
		});

	}

	@EventHandler
	public void handlePortChanged(final Events.PortModemChanged event) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				//				CommPortIdentifier port = event.getModemPort();
				String port = event.getModemPort();
				String portName = null;
				if (port != null)
					portName = port;

//				switch (Application.getInstance().getSystem()) {
//				case MONODIREZIONALE:
//					break;
//				case BIDIREZIONALE:
//					break;
//				case GSM:
//					receiverModem.stop();
//					receiverModem.setOnReadListener(null);
//					receiverModem = new ReceiverBidirezionale(portName, Testo.BAUDRATE_9600,
//							receiverModem.getTask());
//					receiverModem.setOnReadListener(sensoreManager);
//					receiverModem.start();
//
//				}
			}
		});
	}

	@Override
	protected void initialize(String[] args) {
		super.initialize(args);
	}
	
	
	@Override
	protected void startup() {
		logger.info(TAG, "Startup");
		
		init();
		
		SwingUtils.integrateWithSystem();

		mainWindow = new MainWindow(this);
		final MainMenuBar menu = new MainMenuBar(mainWindow);
		getMainFrame().setTitle(title);
		getMainFrame().setJMenuBar(menu);
		getMainFrame().setContentPane(mainWindow);
		getMainFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		
		addExitListener(new ExitListener() {

			@Override
			public boolean canExit(EventObject arg0) {
				
				boolean conferm=false;
				
				if (!restart) {
					
					int i = JOptionPane.showConfirmDialog(
							null,
							Application.getInstance().getBundle().getString("application.confermaUscita"),
							Application.getInstance().getBundle().getString("application.attenzione"),
							JOptionPane.YES_NO_OPTION);

					if(i == JOptionPane.YES_OPTION) {
						conferm=true;
					 } else {
						 conferm=false;
					}
					
					} else {
						conferm=true;
						
					} 
				 
				
				
				return conferm;
				
			} 

			@Override
			public void willExit(EventObject arg0) {
				
			 
				
			} 
			 
			}); 
			 
		
		show(getMainFrame());
		
		
		getMainFrame().addWindowFocusListener(new WindowFocusListener() {

			@Override
			public void windowLostFocus(WindowEvent arg0) {
				menu.loseFocusHack();

			}

			@Override
			public void windowGainedFocus(WindowEvent arg0) {

			}
		});

		// remove default ugly blue image

		BufferedImage logo;
		try {
			logo = ImageIO.read((InputStream) Thread.currentThread()
					.getContextClassLoader()
					.getResourceAsStream("com/econorma/ui/resources/logo.png")); //vecchia icona logo.png
			getMainFrame().setIconImage(logo);
		} catch (IOException e1) {
			getMainFrame().setIconImage(null);
		}

	}

	 

	@Override
	protected void ready() {

		super.ready();
		logger.info(TAG, "Ready hook");
 		start();
	}

	@Override
	protected void end() {
		super.end();
		logger.info(TAG, "end");
	}

	public GUI getGui() {
		return gui;
	}

	public ExecutorService getExecutorservice() {
		return executorService;
	}

	 
	public THEME getTheme(){
		return theme;
	}

	public MainWindow getMainWindow() {
		return mainWindow;
	}

	public Component getMainComponent() {
		return mainWindow.getTabbedPane().getComponent(0);
	}

	public Component getSinotticoComponent() {
		return mainWindow.getTabbedPane().getComponent(1);
	}

	public TIPO_MISURE getTipoMisure() {
		return tipoMisure;
	}

	 

	/**
	 * sostituisce il receiver attuale, deve esistere già un receiver
	 * bidirezionale
	 * 
	 * @param task
	 */
//	public void swapBidirezionaleTaskAndStart(ReceiverBidirezionale.Task task) {
//		receiverBidirezionale.stop();
//		receiverBidirezionale.setOnReadListener(null);
//		receiverBidirezionale = new ReceiverBidirezionale(
//				receiverBidirezionale.getCom(), receiverBidirezionale.getBaudRate(), task);
//		receiverBidirezionale.setOnReadListener(sensoreManager);
//		receiverBidirezionale.start();
//	}

//	public void stopReceiverModem() {
//		receiverModem.stop();
//	}
//
//	public void startReceiverModem() {
//		receiverModem.start();
//	}

	public AppConfChanger getAppConfChanger() {
		return appConfChanger;
	}

	 
 
 
	public ResourceBundle getBundle() {
		return bundle;
	}

	public void setBundle(ResourceBundle bundle) {
		this.bundle = bundle;
	}
	
	public boolean getUpgradeDB() {
		return upgradeDB;
	}
	
	public boolean isDeleteAfterDownload() {
		return deleteAfterDownload;
	}
 
	public boolean isSystemTray() {
		return systemTray;
	}

	public boolean isExitAlert() {
		return exitAlert;
	}

	public String getPathFile() {
		return pathFile;
	}
	
 

	public void restart() {

		final String javaBin = "java";

		Map<String, String> getenv = System.getenv();
		String classPath = getenv.get("CLASSPATH");
		File currentJar;
		try {
			currentJar = new File(Main.class.getProtectionDomain()
					.getCodeSource().getLocation().getPath());

			File f = new File(System.getProperty("java.class.path"));
			File dir = f.getAbsoluteFile().getParentFile();

			logger.info(TAG, "nome jar:" + f.getAbsoluteFile());
		
			/* is it a jar file? */

			/* Build command: java -jar application.jar */
			final ArrayList<String> command = new ArrayList<String>();
			command.add(javaBin);
			command.add("-jar");
			command.add(f.getAbsolutePath());

			final ProcessBuilder builder = new ProcessBuilder(command);

			builder.start();
			restart=true;
			exit();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<String> getTipoSensori() {
		return tipoSensori;
	}
	
	public void setTipoSensore() {
		temperature = Boolean.parseBoolean(tipoSensori.get(0));
		temperatureHumidity = Boolean.parseBoolean(tipoSensori.get(1));
		volt = Boolean.parseBoolean(tipoSensori.get(2));
		milliampere = Boolean.parseBoolean(tipoSensori.get(3));
		ohm = Boolean.parseBoolean(tipoSensori.get(4));
		apertoChiuso = Boolean.parseBoolean(tipoSensori.get(5));
		setSensorsArray();
	}
	
	public void setSensorsArray() {
		sensorsArray = new StringBuilder();
		sensorsArray.append(toNumeralString(temperature));
		sensorsArray.append(toNumeralString(temperatureHumidity));
		sensorsArray.append(toNumeralString(volt));
		sensorsArray.append(toNumeralString(milliampere));
		sensorsArray.append(toNumeralString(ohm));
		sensorsArray.append(toNumeralString(apertoChiuso));
	}

	public boolean isTemperature() {
		return temperature;
	}
 
	public boolean isTemperatureHumidity() {
		return temperatureHumidity;
	}
 
	public boolean isMilliampere() {
		return milliampere;
	}
 
	public boolean isOhm() {
		return ohm;
	}
 
	public boolean isApertoChiuso() {
		return apertoChiuso;
	}

	public boolean isVolt() {
		return volt;
	}

	public StringBuilder getSensorsArray() {
		return sensorsArray;
	}
	
	public String toNumeralString(final Boolean input) {
		  if (input == null) {
		    return "null";
		  } else {
		    return input.booleanValue() ? "1" : "0";
		  }
		}

	public INTERFACE getInterfaccia() {
		return interfaccia;
	}

	public void setInterfaccia(INTERFACE interfaccia) {
		this.interfaccia = interfaccia;
	}
	
 
}
