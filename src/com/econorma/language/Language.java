package com.econorma.language;

import java.util.Locale;
import java.util.ResourceBundle;

import com.econorma.data.User;
import com.econorma.resources.Testo;

public class Language {

	public ResourceBundle getDefault(){
		
		Locale defaultLocale = Locale.getDefault();
		if (defaultLocale.getCountry().equals(Testo.IT)){
			defaultLocale = Locale.ITALIAN;
		} else {
			defaultLocale =  Locale.ENGLISH;
		}

		ResourceBundle bundle = ResourceBundle.getBundle("language", defaultLocale);
		return bundle;
	}

	public ResourceBundle getLocaleFromUser(User user){
		
		Locale defaultLocale = null;
		if (user==null || user.getLanguage().equals(Testo.ITALIAN)){
			defaultLocale= Locale.ITALIAN;
		} else {
			defaultLocale= Locale.ENGLISH;
		}
		ResourceBundle bundle = ResourceBundle.getBundle("language", defaultLocale);
		return bundle;
	}

}
